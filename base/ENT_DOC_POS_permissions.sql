-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ENT_DOC_POS
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id_permission` int(11) NOT NULL AUTO_INCREMENT,
  `create_users` tinyint(1) DEFAULT NULL,
  `edit_users` tinyint(1) DEFAULT NULL,
  `create_aspirant` tinyint(1) DEFAULT NULL,
  `edit_aspirant` tinyint(1) DEFAULT NULL,
  `post` tinyint(1) DEFAULT NULL,
  `view_digital_document` tinyint(1) DEFAULT NULL,
  `delete_digital_document` tinyint(1) DEFAULT NULL,
  `validate_digital_document` tinyint(1) DEFAULT NULL,
  `receive_physical_document` tinyint(1) DEFAULT NULL,
  `validate_physical_document` tinyint(1) DEFAULT NULL,
  `validate` tinyint(1) DEFAULT NULL,
  `reports` tinyint(1) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_permission`),
  KEY `fk_permissions_users1_idx` (`users_id`),
  CONSTRAINT `fk_permissions_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,'2017-04-07 10:00:00',NULL),(2,1,1,1,1,1,1,1,1,1,1,1,1,2,'2017-04-07 10:00:00',NULL),(3,0,0,0,0,1,0,0,0,0,0,0,0,3,'2017-04-07 10:00:00',NULL),(4,1,1,0,1,1,1,0,1,1,1,1,1,4,'2017-04-07 10:00:00',NULL),(5,0,0,0,0,1,1,0,1,1,1,1,1,5,'2017-04-17 10:00:00',NULL),(6,0,0,0,0,1,1,0,1,1,1,1,1,6,'2017-04-17 10:00:00',NULL),(7,0,0,0,0,1,1,0,1,1,1,1,1,7,'2017-04-17 10:00:00',NULL),(8,0,0,0,0,1,1,0,1,1,1,1,1,8,'2017-04-17 10:00:00',NULL),(9,0,0,0,0,1,1,0,1,1,1,1,1,9,'2017-04-17 10:00:00',NULL),(10,0,0,0,0,1,1,0,1,1,1,1,1,10,'2017-04-17 10:00:00',NULL),(11,0,0,0,0,1,1,0,1,1,1,1,1,11,'2017-04-17 10:00:00',NULL),(12,0,0,0,0,1,1,0,1,1,1,1,1,12,'2017-04-17 10:00:00',NULL),(13,1,1,1,1,1,1,1,1,1,1,1,1,13,'2017-04-07 10:00:00',NULL),(14,1,1,1,1,1,1,1,1,1,1,1,1,14,'2017-04-07 15:00:00',NULL),(15,1,1,1,1,1,1,1,1,1,1,1,1,15,'2017-04-07 15:00:00',NULL),(16,1,1,1,1,1,1,1,1,1,1,1,1,16,'2017-04-07 15:00:00',NULL),(17,0,0,0,0,1,1,0,1,1,1,1,1,17,'2017-04-17 15:00:00',NULL),(18,0,0,0,0,1,1,0,1,1,1,1,1,18,'2017-04-17 15:00:00',NULL),(19,0,0,0,0,1,1,0,1,1,1,1,1,19,'2017-04-17 15:00:00',NULL),(20,0,0,0,0,1,1,0,1,1,1,1,1,20,'2017-04-17 15:00:00',NULL),(21,0,0,0,0,1,1,1,1,1,1,NULL,1,21,'2017-05-02 13:53:13',NULL),(22,0,0,0,0,1,1,1,1,1,1,NULL,1,22,'2017-05-02 13:54:49',NULL),(23,0,0,0,0,1,1,1,1,1,1,NULL,1,23,'2017-05-08 07:15:54',NULL),(24,0,0,0,0,1,1,0,1,1,1,0,1,24,'2017-06-16 16:37:25',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-30 20:10:12
