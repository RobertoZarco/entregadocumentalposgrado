-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ENT_DOC_POS
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plans`
--

DROP TABLE IF EXISTS `plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans` (
  `id_plan` int(11) NOT NULL AUTO_INCREMENT,
  `plan` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `minimum_compulsory_credits` int(11) DEFAULT NULL,
  `minimum_optional_credits` int(11) DEFAULT NULL,
  `maximum_duration_tc` int(11) DEFAULT NULL,
  `maximum_duration_tp` int(11) DEFAULT NULL,
  `planscol` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `level` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `objetive` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `key_sep` int(11) DEFAULT NULL,
  `link` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `type_plan` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `programs_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_plan`),
  KEY `fk_plans_programs1_idx` (`programs_id`),
  KEY `fk_plans_areas1_idx` (`areas_id`),
  CONSTRAINT `fk_plans_areas1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id_area`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_plans_programs1` FOREIGN KEY (`programs_id`) REFERENCES `programs` (`id_program`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5173 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plans`
--

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` VALUES (4172,'Maestría en Ciencias de la Sostenibilidad',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,210,13,'2017-04-29 01:00:00','2017-04-29 01:00:00'),(4175,'Maestría en Docencia para la Educación Media Superior (Geografía)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,6,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4176,'Maestría en Docencia para la Educación Media Superior (Inglés)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,12,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4177,'Maestría en Docencia para la Educación Media Superior (Psicología)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,11,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4178,'Maestría en Docencia para la Educación Media Superior (Biología)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,1,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4179,'Maestría en Docencia para la Educación Media Superior (Español)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,3,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4180,'Maestría en Docencia para la Educación Media Superior (Matemáticas)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,8,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4182,'Maestría en Docencia para la Educación Media Superior (Física)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,5,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4183,'Maestría en Docencia para la Educación Media Superior (Ciencias Sociales)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,2,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4184,'Maestría en Docencia para la Educación Media Superior (Química)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,9,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4185,'Maestría en Docencia para la Educación Media Superior (Letras Clásicas)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,10,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4186,'Maestría en Docencia para la Educación Media Superior (Historia)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,7,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(4187,'Maestría en Docencia para la Educación Media Superior (Filosofía)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,414,4,'2017-04-08 01:00:00','2017-04-08 01:00:00'),(5172,'Doctorado en Ciencias de la Sostenibilidad',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,210,13,'2017-04-29 01:00:00','2017-04-29 01:00:00');
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-30 20:10:10
