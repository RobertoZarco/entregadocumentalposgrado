<?php

function cambia_tildes($dato) {
	//nombre original de la function latin9_to_utf8
	//replaces utf8_encode()
	$trans = array("�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�");
	$wrong_utf8str = utf8_encode($dato);
	$utf8str = strtr($wrong_utf8str, $trans);
	return $utf8str;
}

if(isset($usuario)){

	for($i = 0; $i < count($usuario); $i++){
		$folio            = $usuario[$i]['aspi_folio'];
		$name             = explode("*", $usuario[$i]['aspi_nombre']);
		$paterno          = cambia_tildes(trim($name[0]));
		$materno          = cambia_tildes(trim($name[1]));
		$nombre           = cambia_tildes(trim($name[2]));
		$correo           = $usuario[$i]['aspi_email'];
		$telefono         = $usuario[$i]['aspi_telefono'];
		$lugar_residencia = $usuario[$i]['aspi_entidad'];
		$periodo          = $usuario[$i]['aspi_periodo'];
		$tipo_ingreso     = $usuario[$i]['aspi_tipo_ingreso'];
		$sistema          = $usuario[$i]['aspi_opc_sis1'];
		$concurso         = $usuario[$i]['aspi_cve_concurso'];

		echo "
				<a href='' id='folios' data-content='$folio|$paterno|$materno|$nombre|$correo|$telefono|$lugar_residencia|$periodo|$tipo_ingreso|$sistema|$concurso'>
					$paterno $materno $nombre
				</a><br>
				";
	}
}


?>
