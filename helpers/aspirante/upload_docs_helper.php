<?php

//funcion para mostrar el formulario de subida de archivos
function setFormUploadDoc( $nameForm, $titulo, $nombreDoc, $tipoDoc){
  ?>

  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>
      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white; font-size:12px">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento">
          </a>
        </div>
      </td>

      <td>
        <input type="hidden" name="cat_tipo_doc"   id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"     id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference" id="reference"                     value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success" disabled id="submit_<?php echo $nombreDoc ?>" style="font-size:12px; z-index:9999;">
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" style="display:none;" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" style="display:none;" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
      </td>

      <td align='center' style="display:none;" id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

      <td align='center' style="display:none;" id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

    </tr>
  </form>


  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}


function setFormUploadDocMaestria($nameForm, $titulo, $nombreDoc, $tipoDoc){
  ?>
  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>
      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white; font-size:12px">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento">
          </a>
        </div>
      </td>

      <td>
        <input type="hidden" name="cat_tipo_doc"   id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"     id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference" id="reference"                     value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success" disabled id="submit_<?php echo $nombreDoc ?>" style="font-size:12px">
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" style="display:none;" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" style="display:none;" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
      </td>

      <td align='center' style="display:none;" id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

      <td align='center' style="display:none;" id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

    </tr>
  </form>

  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}


function setFormUploadDocDoctorado($nameForm, $titulo, $nombreDoc, $tipoDoc){
  ?>
  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>
      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white; font-size:12px">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento">
          </a>
        </div>
      </td>

      <td class="text-right">
        <input type="hidden" name="cat_tipo_doc"   id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"     id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference" id="reference"                     value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success text-right" disabled id="submit_<?php echo $nombreDoc ?>" style="font-size:12px">
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" style="display:none;" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" style="display:none;" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" style="display:none;" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
      </td>

      <td align='center' style="display:none;" id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

      <td align='center' style="display:none;" id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-question-sign' style='color:grey' aria-hidden='true'></span>
      </td>

    </tr>
  </form>

  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}


function setFormShowDoc($nameForm, $titulo, $nombreDoc, $tipoDoc, $idDocumento, $nombreDocumento, $status_validacion, $status_fisic){
  ?>

  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>

      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white;" disabled="true" id="a-btn-file-<?php echo $nombreDoc ?>">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento" disabled="true">
          </a>
        </div>
      </td>

      <td class="text-right">
        <input type="hidden" name="cat_tipo_doc"      id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"        id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference"         id="reference"                          value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success" id="submit_<?php echo $nombreDoc ?>" >
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" value="<?php echo $nombreDocumento ?>" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
        <input type="hidden" name="referencia"   id="reference" value="<?php echo $_SESSION['reference']; ?>" >
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center"  id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" ></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar" <?php if($status_validacion == '1' ){echo "disabled";}; ?>>
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>

      <td align='center' id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_validacion) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>


      <td align='center' id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_fisic) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>

    </tr>
  </form>

  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="<?php echo $idDocumento ?>">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}


function setFormShowDocMaestria($nameForm, $titulo, $nombreDoc, $tipoDoc, $idDocumento, $nombreDocumento, $status_validacion, $status_fisic){

  //echo $nombreDocumento."<br>";

  ?>
  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>

      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white;" disabled="true" id="a-btn-file-<?php echo $nombreDoc ?>">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento" disabled="true">
          </a>
        </div>
      </td>

      <td class="text-right">
        <input type="hidden" name="cat_tipo_doc"      id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"        id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference"    id="reference"                     value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success" id="submit_<?php echo $nombreDoc ?>" >
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" value="<?php echo $nombreDocumento ?>" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
        <input type="hidden" name="referencia"   id="reference" value="<?php echo $_SESSION['reference']; ?>" >
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center"  id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" ></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar" <?php if($status_validacion == '1' ){echo "disabled";}; ?>>
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>

      <td align='center' id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_validacion) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>


      <td align='center' id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_fisic) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>

    </tr>


  </form>

  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="<?php echo $idDocumento ?>">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}


function setFormShowDocDoctorado($nameForm, $titulo, $nombreDoc, $tipoDoc, $idDocumento, $nombreDocumento, $status_validacion, $status_fisic){

  //echo $nombreDocumento."<br>";

  ?>
  <form action="" method="POST" enctype="multipart/form-data" id="form<?php echo $nameForm; ?>UploadPDF" class="form-horizontal" role="form">
    <tr style="font-size:12px">
      <td>
        <?php echo $titulo ?>
        <span class='label label-info' id='upload-file-info-<?php echo $nombreDoc ?>'></span>
      </td>

      <td>
        <div style="position:relative;">
          <a class='btn btn-primary' href='javascript:;' style="color:white;" disabled="true" id="a-btn-file-<?php echo $nombreDoc ?>">
            Selecionar
            <input type="file" id="<?php echo $nombreDoc ?>" class="file" name="documento" disabled="true">
          </a>
        </div>
      </td>

      <td class="text-right">
        <input type="hidden" name="cat_tipo_doc"      id="cat_tipo_doc"                       value="<?php echo $tipoDoc ?>">
        <input type="hidden" name="nombre_doc"        id="nombreDoc_<?php echo $nombreDoc ?>" value="<?php echo $nombreDoc ?>" >
        <input type="hidden" name="reference"    id="reference"                     value="<?php echo $_SESSION['reference']; ?>" >
        <button type="submit" class="btn btn-success" id="submit_<?php echo $nombreDoc ?>" >
          Guardar
        </button>
      </td>

      <td id="<?php echo $nombreDoc ?>Load" style="display:none;">
        <img src="../../../assets/img/loader.gif" class="img-responsive" alt="Load" >
      </td>

      <td class="text-center" id="td-file-<?php echo $nombreDoc ?>">
        <button type="button" class="btn btn-default" aria-label="Left Align" value="<?php echo $nombreDocumento ?>" title="ver Documento" id="file-<?php echo $nombreDoc ?>">
        <input type="hidden" name="referencia"   id="reference" value="<?php echo $_SESSION['reference']; ?>" >
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
        </button>
      </td>

      <td class="text-center"  id="td-ok-upload-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok" title="Guardado" id="ok-upload-<?php echo $nombreDoc ?>"aria-hidden="true" ></span>
      </td>

      <td class="text-center" style="display:none;" id="td-ok-validate-<?php echo $nombreDoc ?>">
        <span class="glyphicon glyphicon-ok-sign" title="Validado" id="ok-validate-<?php echo $nombreDoc ?>"aria-hidden="true" style="display:none;"></span>
      </td>

      <td class="text-center" id="td-trash-<?php echo $nombreDoc ?>">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_<?php echo $nombreDoc ?>" title="Eliminar" <?php if($status_validacion == '1' ){echo "disabled";}; ?>>
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>

      <td align='center' id="td-show_status-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_validacion) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>


      <td align='center' id="td-show_status-fisic-<?php echo $nombreDoc ?>">
        <span class='glyphicon glyphicon-<?php switch ($status_fisic) { case null: echo "question-sign' style='color:grey'"; break; case '0': echo "remove-sign' style='color:red'"; break; case '1': echo "ok-sign ' style='color:green'"; break; };  ?> aria-hidden='true'></span>
      </td>

    </tr>


  </form>

  <!-- Modal -->
  <div class="modal fade" id="modal_<?php echo $nombreDoc ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Eliminar Documento <?php echo $titulo ?></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            ¿Estas seguro de eliminar <strong><?php echo $titulo ?> </strong>?
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-<?php echo $nombreDoc ?>" value="<?php echo $idDocumento ?>">Eliminar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="mensaje_<?php echo $nombreDoc; ?>" style="display:none;"></div>

  <?php
}



?>


