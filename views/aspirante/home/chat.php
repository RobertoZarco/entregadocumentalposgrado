<div class="container-fluid">
	<section>
		<div class="row">
			<h3 class="text-center">Servicio de Mensajeria</h3>
			<hr>
		</div>

		<div class="row">
			<form action="" id="formChat" role="form" >

				<?php
					//print_r($_SESSION);
					//emisor
					$emisor = $_SESSION['referencia'];
				?>
					<input type="hidden" id="emisor" name="emisor" value="<?php echo $_SESSION['referencia'] ?>">
					<!-- //receptor -->
					<input type="hidden" id="receptor" name="receptor" value="3">
					<input type="hidden" id="statusChat" name="statusChat" value="pendiente">


				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<legend>Conversación</legend>
							<div id="conversation" style="height:200px; border: 1px solid #ccc; padding: 12px; border-radius: 5px; overflow-x:hidden"></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="message">Dejar mensaje</label>
					<textarea id="message" name="message" cols="30" rows="3" placeholder="Escribir mensaje" class="form-control"></textarea>
				</div>

				<button id="send" class="btn btn-primary">
					Enviar <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
				</button>

			</form>
		</div>

	</section>
</div>