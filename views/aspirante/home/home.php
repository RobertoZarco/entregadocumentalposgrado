<?php
//require_once "../../../controllers/aspirante/documents_controller.php";

ini_set('memory_limit', '2000M');
require_once "../../../controllers/aspirante/home_controller.php";

require_once 'head.php';
require_once "navbar.php";

if(isset($_SESSION['login']) or isset($_SESSION['id_aspirant'])){
  ?>

  <!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>-->

  <?php
  /*
  echo "<script language='Javascript'>
          if (geoplugin_countryName() == 'Mexico'){
           //alert ('Ciudad: '+geoplugin_city()+', País: '+geoplugin_countryName());
            ".session_destroy()."
           window.location='../../../index.php?e=2';
          }
        </script>";
  */
  ?>


  <style type="text/css">
    .main-footer {
      position: relative;
      height:350px;
      bottom:0px;
      top:400px;
      left:0px;
      right:0px;
      margin-bottom:0px;
    }

  </style>

  <main id="maincontent" role="main" class="clearfix" height="1000px">

    <input type="hidden" name="checkManifest" id="checkManifest" value="<?php echo $getCheckManifest[0]['manifest']?>">

    <?php

    if(isset($_GET) and @$_GET['e']==1){
      ?>

      <div class="jumbotron text-center">
        <div class="container">
          <h1>DGAE - MADEMS</h1>
          <p>Te recordamos que el periodo para poder subir tus documentos se ha vencido</p>
        </div>
      </div>

      <?php
      //require_once 'footer.php';
      die();
    }else{
      ?>

      <div class="row">
        <div class="col-sm-12">
          <h2 id="Bienvenido">Bienvenid@ <?php echo $_SESSION['name'] ." ".$_SESSION['first_surname']." ".$_SESSION['second_surname']; ?></h2>
        </div>
      </div>



      <div class="row">
        <div class=" col-xs-offset-1 col-sm-12">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1" id="checkManifiesto" style="margin-right: 20px">
                Hago constar que todos los documentos digitales son verdaderos.
              </label>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <?php require_once 'form_upload.php' ?>
        </div>

        <div class="col-sm-6">
          <div id="pdf">
            <!--Aqui se crea el PDF-->
          </div>
        </div>

        <!--<div class="col-sm-2">-->
          <?php //require_once '../../chat.php' ?>
          <?php //require_once 'chat.php' ?>
        <!--</div>-->
      </div>



      <!-- Sección: Scripts -->
      <!-- jQuery -->
      <!-- <script type="text/javascript" src="../../../assets/js/jquery.js"></script> -->
      <!-- Bootstrap Core JavaScript -->
      <!-- <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script> -->
      <!-- Material Design Bootstrap -->
      <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
      <!-- Analytics -->
      <!-- barra de navegación-->
      <!-- <script type="text/javascript" src="../../../assets/js/navbar.js"></script> -->

      <!-- /Sección: Scripts -->

      <?php
      require_once 'footer.php';
    }


}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>