<html lang="es">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1C3D6C">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Sección: Title del Sitio -->
    <title>DGAE - UNAM</title>
    <!-- /Sección: Title del Sitio -->
    <!-- Sección: Links -->
    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
    <link href="../../../assets/css/login.css" rel="stylesheet">
    <!-- <link href="../../../assets/css/reset.css" rel="stylesheet"> -->
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="../../../assets/css/mdb.css" rel="stylesheet"> -->
    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
    <!-- /Sección: Links -->

    <script src='../../../assets/js/jquery.js' ></script>
    <!-- <script src='../../../assets/js/app/login.js' ></script> -->
  </head>

  <body id="inicio">

    <?php
    /*
    <!-- Navegacion -->
    <nav role="navigation">
      <!-- Fixed navbar -->
      <div class="navbar navbar-fixed-top" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
              <i class="fa fa-bars" style="color: white;"></i>
            </button>
            <div class="small-logo-container">
              <a class="small-logo" href="https://www.dgae.unam.mx/" tabindex="-1">
                UNAM - DGAE
              </a>
            </div>
          </div>

          <!-- Sección: Navegación -->
          <!-- /Sección: Navegación -->
        </div>
      </div>
      <div class=" big-logo-row">
        <div class="container">
          <div>
            <div class="col-lg-12 col-md-12 big-logo-container">
              <div class="big-logo">
                <div class="pull-left logo_grande logo_der">
                  <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                    <img src="../../../assets/images/escudo_unam_completow.svg">
                  </a>
                </div>
                <div class="pull-left logo_chico logo_der">
                  <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                    <img src="../../../assets/images/escudo_unam_solow.svg">
                  </a>
                </div>
                <div class="pull-right logo_grande logo_izq">
                  <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                    <img src="../../../assets/images/escudo_dgae_completow.svg">
                  </a>
                </div>
                <div class="pull-right logo_chico logo_izq">
                  <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                    <img src="../../../assets/images/escudo_dgae_solow.svg">
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    */
    ?>

    <div class="container">
      <div class="row">
        <div>
          <h2 class="text-center">
            Sistema de recepción documental a distancia
          </h2>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-offset-4 col-sm-4">

          <?php
          if(@isset($_GET) and @$_GET['e']){
            $error = $_GET['e'];
            switch ($error) {
              case '1':
                ?>
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>Usuario o contraseña incorrectos favor de verificar</strong>
                </div>
                <?php
                break;
              case '2':
                ?>
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>La dirección IP proviene de México por el momento no podemos atenderte</strong>
                </div>
                <?php
                break;

              case '3':
                ?>
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <strong>Favor de realizar primero tu registro</strong>
                </div>
                <?php
                break;

              default:
                # code...
                break;
            }
          }
          ?>
        </div>
      </div>
    </div>


    <?php
    /*
    <!--Principio del Footer -->
    <footer class="main-footer">
      <div class="list-info">
        <div class="container">
          <div class="col-sm-3">
            <h5 tabindex="0">Contacto</h5>
            <p class="pmenor" tabindex="0">
              <i class="fa fa-phone"></i>
              &nbsp;Atención por Teléfono
              <br> 5622 - 1524
              <br> 5622 - 1525
              <br> De 9:00 a 19:30 hrs.
            </p>
          </div>
          <div class="col-sm-6">
            <p class="pmenor" tabindex="0">
              Se brinda información de:
              <ul tabindex="0">
                <li>Convocatoria para los concursos de selección</li>
                <li>Examen COMIPEMS</li>
                <li>Ingreso a Iniciación Universitaria</li>
                <li>Ingreso a Licenciatura por Pase Reg+lamentado</li>
                <li>Resultados de los concursos de selección</li>
                <li>Trámites y Servicios Escolares en general</li>
                <li>Ubicación de dependencias de la UNAM</li>
                <li>Venta de Guías y Planes de Estudio</li>
              </ul>
            </p>
          </div>

          <div class="col-sm-3">
            <h5><i class="fa fa-sitemap"></i> &nbsp;<a href="https://www.dgae.unam.mx/mapasitio.html" class="link_footer" tabindex="0">Mapa de sitio</a></h5>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
      <div class="row" id="fondo">
        <div class="col-sm-12">
          <p class="pmenor" tabindex="0">
            Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2009 - 2014.
            <br>Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, requiere permiso previo por escrito de la institución
            <br>
            <br>Sitio web administrado por: Dirección General de Administración Escolar
          </p>
        </div>
      </div>
    </footer>
    */
    ?>

    <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- Analytics -->
    <script type="text/javascript" src="../../../assets/js/analytics.js"></script>
    <!-- barra de navegación-->
    <script type="text/javascript" src="../../../assets/js/navbar.js"></script>

    <!-- /Sección: Scripts -->
  </body>

  </html>