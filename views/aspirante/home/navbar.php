  <!-- Navegacion -->
  <nav role="navigation">
    <!-- Fixed navbar -->
    <div class="navbar navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
            <i class="fa fa-bars" style="color: white;"></i>
          </button>
          <div class="small-logo-container">
            <a class="small-logo" href="https://www.dgae.unam.mx/" tabindex="-1">
              UNAM - DGAE
            </a>
          </div>
        </div>


        <!-- Sección: Navegación -->
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <!--
            <li>
              <a class="link-barra" href="home.php" tabindex="0">Inicio</a>
            </li>
            -->

            <li>
              <!--<a class="link-barra" href="close_sesion.php" tabindex="0" >Cerrar Sesion</a>-->
            </li>

            <li>
              <a class="link-barra" href="close_sesion.php" tabindex="0">Cerrar Session</a>
            </li>

          </ul>
        </div>
        <!-- /Sección: Navegación -->


      </div>
    </div>
    <div class=" big-logo-row">
      <div class="container">
        <div>
          <div class="col-lg-12 col-md-12 big-logo-container">
            <div class="big-logo">
              <div class="pull-left logo_grande logo_der">
                <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                  <img src="../../../assets/images/escudo_unam_completow.svg">
                </a>
              </div>
              <div class="pull-left logo_chico logo_der">
                <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                  <img src="../../../assets/images/escudo_unam_solow.svg">
                </a>
              </div>
              <div class="pull-right logo_grande logo_izq">
                <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                  <img src="../../../assets/images/escudo_dgae_completow.svg">
                </a>
              </div>
              <div class="pull-right logo_chico logo_izq">
                <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                  <img src="../../../assets/images/escudo_dgae_solow.svg">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
