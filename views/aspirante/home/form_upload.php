<?php
require_once '../../../helpers/aspirante/upload_docs_helper.php';

?>
<!--Formulario para obligatorios-->
<div class="row">
  <div class="col-xs-12">
    <legend>
      Documentos obligatorios.
    </legend>
    <div class="table-responsive">
      <table class="table table-bordered table-condensed table-hover table-striped">
        <?php

        $arrayCatTipoDocs = array();
        $arrayDocs        = array();

        if(count($getDocumentoByReference) > 0){
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="250px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento"  >
                <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  class="text-center" id="status1" title="Validación Digital" >
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th class="text-center" id="status3" title="Validación Físico">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

            <?php
            //obtenemos los tipos de documentos del catalogo
            for($i = 0; $i < count($getCatTipoDocumento); $i++){
              array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
            }


            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);
            }

            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            //obtenemos los documentos que tiene el aspirante
            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);


              $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
              $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
              $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
              $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

              //echo "tipoDoc -> $tipoDoc<br>";
              switch ($tipoDoc) {
                case '1':
                  setFormShowDoc("Acta", "ACTA DE NACIMIENTO*", "actaPDF", "1", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                  break;

                case '2':
                  setFormShowDoc("CURP", "CURP AMPLIADA AL 200%*", "curpPDF", "2", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                  break;

                case '5':
                  setFormShowDoc("Iden_Ofic", "IDENTIFICACIÓN OFICIAL *", "identOficPDF", "5", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                  break;

              }
            }

            //documentos faltantes
            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            foreach ($result as $indice => $valor) {
              //echo "$indice -> $valor<br>";
              switch ($valor) {

                case 1:
                  setFormUploadDoc("Acta", "ACTA DE NACIMIENTO *", "actaPDF", "1");
                  break;

                case 2:
                  setFormUploadDoc("CURP",  "CURP AMPLIADA AL 200%*", "curpPDF", "2");
                  break;

                case 5:
                  setFormUploadDoc("Iden_Ofic", "IDENTIFICACIÓN OFICIAL *", "identOficPDF",    "5");
                  break;
              }
            }
        }else{
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" style="display:none;" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  id="status" title="Validación Digital" style="display:none;">
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th  id="status_fis" title="Validación Físico" style="display:none;">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

              <?php
                setFormUploadDoc("Acta",         "ACTA DE NACIMIENTO *",       "actaPDF",         "1");
                setFormUploadDoc("CURP",         "CURP AMPLIADA AL 200%*",                     "curpPDF",         "2");
                setFormUploadDoc("Iden_Ofic",    "IDENTIFICACIÓN OFICIAL *",     "identOficPDF",    "5");
              ?>



            </tbody>
          <?php
        }

        ?>
      </table>
    </div>
  </div>
</div>
<!--Formulario para obligatorios (titulo)-->
<div class="row">
  <div class="col-xs-12">
    <legend>
      Selecciona: Título de licenciatura o acta de examen profesional o carta de registro de la
      opción de titulación mediante estudios de posgrado.
    </legend>
    <div class="table-responsive">
      <table class="table table-bordered table-condensed table-hover table-striped">
        <?php

        $arrayCatTipoDocs = array();
        $arrayDocs        = array();

        if(count($getDocumentoByReference) > 0){
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento"  >
                <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  class="text-center" id="status1" title="Validación Digital" >
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th class="text-center" id="status3" title="Validación Físico">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

            <?php
            //obtenemos los tipos de documentos del catalogo
            for($i = 0; $i < count($getCatTipoDocumento); $i++){
              array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
            }


            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);
            }

            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            //obtenemos los documentos que tiene el aspirante
            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);


              $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
              $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
              $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
              $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

              //echo "tipoDoc -> $tipoDoc<br>";
              switch ($tipoDoc) {

                case '3':
                    setFormShowDoc("Titulo",  "TÍTULO LICENCIATURA", "tituloPDF", "3", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                  case '7':
                    setFormShowDoc("ActaExamProf",  "ACTA DE EXAMEN PROFESIONAL (en caso de no tener título)", "actaExamProfPDF", "7", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '8':
                    setFormShowDoc("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;
              }
            }

            //documentos faltantes
            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            foreach ($result as $indice => $valor) {
              //echo "$indice -> $valor<br>";
              switch ($valor) {

                case 3:
                  setFormUploadDoc("Titulo",       "TÍTULO LICENCIATURA",        "tituloPDF",       "3");
                  break;

                case 7:
                  setFormUploadDoc("ActaExamProf", "ACTA DE EXAMEN PROFESIONAL  (en caso de no tener título)", "actaExamProfPDF", "7");
                  break;

                case 8:
                  setFormUploadDoc("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8");
                  break;
              }
            }
        }else{
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" style="display:none;" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  id="status" title="Validación Digital" style="display:none;">
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th  id="status_fis" title="Validación Físico" style="display:none;">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

              <?php
                setFormUploadDoc("Titulo",       "TÍTULO LICENCIATURA",        "tituloPDF",       "3");
                setFormUploadDoc("ActaExamProf", "ACTA DE EXAMEN PROFESIONAL  (en caso de no tener título)", "actaExamProfPDF", "7");
                setFormUploadDoc("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8");
              ?>



            </tbody>
          <?php
        }

        ?>
      </table>
    </div>
  </div>
</div>

<!--Formulario para obligatorios (certificado)-->
<div class="row">
  <div class="col-xs-12">
    <legend>
      Selecciona: Certificado de estudios de licenciatura y, en su caso, constancia de promedio.
    </legend>
    <div class="table-responsive">
      <table class="table table-bordered table-condensed table-hover table-striped">
        <?php

        $arrayCatTipoDocs = array();
        $arrayDocs        = array();

        if(count($getDocumentoByReference) > 0){
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento"  >
                <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  class="text-center" id="status1" title="Validación Digital" >
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th class="text-center" id="status3" title="Validación Físico">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

            <?php
            //obtenemos los tipos de documentos del catalogo
            for($i = 0; $i < count($getCatTipoDocumento); $i++){
              array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
            }


            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);
            }

            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            //obtenemos los documentos que tiene el aspirante
            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);


              $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
              $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
              $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
              $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

              //echo "tipoDoc -> $tipoDoc<br>";
              switch ($tipoDoc) {

                case '4':
                    setFormShowDoc("CertLic", "CERTIFICADO LICENCIATURA", "certifPDF", "4", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                case '6':
                    setFormShowDoc("ConstProm", "CONSTANCIA DE PROMEDIO DE LICENCIATURA", "constPromPDF", "6", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;
              }
            }

            //documentos faltantes
            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            foreach ($result as $indice => $valor) {
              //echo "$indice -> $valor<br>";
              switch ($valor) {

                case 4:
                  setFormUploadDoc("CertLic",      "CERTIFICADO LICENCIATURA",   "certifPDF",       "4");
                  break;

                case 6:
                  setFormUploadDoc("ConstProm",    "CONSTANCIA DE PROMEDIO DE LICENCIATURA",     "constPromPDF",    "6");
                  break;
              }
            }
        }else{
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" style="display:none;" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  id="status" title="Validación Digital" style="display:none;">
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th  id="status_fis" title="Validación Físico" style="display:none;">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

              <?php
              setFormUploadDoc("CertLic",      "CERTIFICADO LICENCIATURA",   "certifPDF",       "4");
              setFormUploadDoc("ConstProm",    "CONSTANCIA DE PROMEDIO DE LICENCIATURA",     "constPromPDF",    "6");
              ?>



            </tbody>
          <?php
        }

        ?>
      </table>
    </div>
  </div>
</div>

<!--Formulario para opcionales-->
<div class="row">
  <div class="col-xs-12">
    <legend>
      Si cuentas con los siguientes documentos al momento del registro, también se deberán de adjuntar.
    </legend>
    <div class="table-responsive">
      <table class="table table-bordered table-condensed table-hover table-striped">
        <?php
        $arrayCatTipoDocs = array();
        $arrayDocs        = array();

        if(count($getDocumentoByReference) > 0){
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento"  >
                <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  class="text-center" id="status1" title="Validación Digital" >
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th class="text-center" id="status3" title="Validación Físico">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

            <?php
            //obtenemos los tipos de documentos del catalogo
            for($i = 0; $i < count($getCatTipoDocumento); $i++){
              array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
            }


            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);
            }

            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            //obtenemos los documentos que tiene el aspirante
            for($i = 0; $i < count($getDocumentoByReference); $i++){
              $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
              array_push($arrayDocs, $tipoDoc);


              $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
              $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
              $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
              $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

              //echo "tipoDoc -> $tipoDoc<br>";
              switch ($tipoDoc) {

                case '14':
                    setFormShowDoc("equivaProm",      "EQUIVALENCIA DE PROMEDIO (PARA ESTUDIOS REALIZADOS EN EL EXTRANJERO)",   "equivaPromPDF",       "14", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                case '15':
                    setFormShowDoc("cedProf",      "CÉDULA PROFESIONAL",   "cedProfPDF",       "15", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                case '16':
                    setFormShowDoc("consCompreLectu",      "CONSTANCIA DE COMPRENSIÓN DE LECTURA DE TEXTOS DIFERENTES A ESPAÑOL",   "consCompreLectuPDF",       "16", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                case '17':
                    setFormShowDoc("consEspanol",      "CONSTANCIA DE DOMINIO DE ESPAÑOL (PARA NO HISPANOHABLANTES)",   "consEspanolPDF",       "17", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

              }
            }

            //documentos faltantes
            $result = array_diff($arrayCatTipoDocs, $arrayDocs);

            foreach ($result as $indice => $valor) {
              //echo "$indice -> $valor<br>";
              switch ($valor) {

                case 14:
                  setFormUploadDoc("equivaProm",      "EQUIVALENCIA DE PROMEDIO (PARA ESTUDIOS REALIZADOS EN EL EXTRANJERO)",   "equivaPromPDF",       "14", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);

                  break;

                case 15:
                  setFormUploadDoc("cedProf",      "CÉDULA PROFESIONAL",   "cedProfPDF",       "15", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);

                  break;

                case 16:
                  setFormUploadDoc("consCompreLectu",      "CONSTANCIA DE COMPRENSIÓN DE LECTURA DE TEXTOS DIFERENTES A ESPAÑOL",   "consCompreLectuPDF",       "16", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);

                  break;

                case 17:
                  setFormUploadDoc("consEspanol",      "CONSTANCIA DE DOMINIO DE ESPAÑOL (PARA NO HISPANOHABLANTES)",   "consEspanolPDF",       "17", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);

                  break;
              }
            }
        }else{
          ?>
          <thead>
            <tr style="font-size:12px">
              <th class="text-center">Archivo</th>
              <th class="text-center" width="150px">Seleccionar</th>
              <th class="text-center">Guardar</th>

              <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
              </th>

              <th class="text-center" id="save" title="Guardado" style="display:none;" >
                <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
              </th>

              <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
              </th>

              <th  id="status" title="Validación Digital" style="display:none;">
                <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
              </th>

              <th  id="status_fis" title="Validación Físico" style="display:none;">
                <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
              </th>
            </tr>
          </thead>
          <tbody>

              <?php
                setFormUploadDoc("equivaProm",      "EQUIVALENCIA DE PROMEDIO (PARA ESTUDIOS REALIZADOS EN EL EXTRANJERO)",   "equivaPromPDF",       "14");
                setFormUploadDoc("cedProf",      "CÉDULA PROFESIONAL",   "cedProfPDF",       "15");
                setFormUploadDoc("consCompreLectu",      "CONSTANCIA DE COMPRENSIÓN DE LECTURA DE TEXTOS DIFERENTES A ESPAÑOL",   "consCompreLectuPDF",       "16");
                setFormUploadDoc("consEspanol",      "CONSTANCIA DE DOMINIO DE ESPAÑOL (PARA NO HISPANOHABLANTES)",   "consEspanolPDF",       "17");
              ?>
          </tbody>
          <?php
        }

        ?>
      </table>
    </div>
  </div>
</div>


<?php
/*
if($_SESSION['plan_id_plan'] == '4172'){
 ?>
  <!--Maestria-->
  <div class="row" style="margin-top:25px;">
    <div class="col-xs-12">
      <legend>Documentos de Maestría</legend>
      <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover table-striped">
          <?php

          $arrayCatTipoDocs = array();
          $arrayDocs        = array();

          if(count($getDocumentoByReference) > 0){
            ?>
            <thead>
              <tr style="font-size:12px">
                <th class="text-center">Archivo</th>
                <th class="text-center" width="150px">Seleccionar</th>
                <th class="text-center">Guardar</th>

                <th class="text-center" id="ver" title="ver Documento"  >
                  <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
                </th>

                <th class="text-center" id="save" title="Guardado" >
                  <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
                </th>

                <th class="text-center" id="trash" title="Eliminar">
                  <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
                </th>

                <th  class="text-center" id="status1" title="Validación Digital" >
                  <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
                </th>

                <th class="text-center" id="status3" title="Validación Físico">
                  <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
                </th>
              </tr>
            </thead>
              <?php
              //obtenemos los tipos de documentos del catalogo
              for($i = 0; $i < count($getCatTipoDocumento); $i++){
                array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
              }


              for($i = 0; $i < count($getDocumentoByReference); $i++){
                $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
                array_push($arrayDocs, $tipoDoc);
              }

              $result = array_diff($arrayCatTipoDocs, $arrayDocs);

              //obtenemos los documentos que tiene el aspirante
              for($i = 0; $i < count($getDocumentoByReference); $i++){
                $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
                array_push($arrayDocs, $tipoDoc);


                $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
                $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
                $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
                $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

                //echo "tipoDoc -> $tipoDoc<br>";
                switch ($tipoDoc) {
                  //maestria
                  case '4':
                    setFormShowDocMaestria("CertLic", "CERTIFICADO LICENCIATURA", "certifPDF", "4", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                  case '6':
                    setFormShowDocMaestria("ConstProm", "CONSTANCIA DE PROMEDIO DE LICENCIATURA", "constPromPDF", "6", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '3':
                    setFormShowDocMaestria("Titulo",  "TÍTULO LICENCIATURA", "tituloPDF", "3", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                  case '7':
                    setFormShowDocMaestria("ActaExamProf",  "ACTA DE EXAMEN PROFESIONAL (en caso de no tener título)", "actaExamProfPDF", "7", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '8':
                    setFormShowDocMaestria("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                }
              }

              //documentos faltantes
              $result = array_diff($arrayCatTipoDocs, $arrayDocs);

              foreach ($result as $indice => $valor) {
                //echo "$indice -> $valor<br>";
                switch ($valor) {
                  //maestria
                  case 4:
                    setFormUploadDocMaestria("CertLic", "CERTIFICADO LICENCIATURA", "certifPDF", "4");
                    break;

                  case 6:
                    setFormUploadDocMaestria("ConstProm", "CONSTANCIA DE PROMEDIO DE LICENCIATURA", "constPromPDF", "6");
                    break;

                  case 3:
                    setFormUploadDocMaestria("Titulo", "TÍTULO LICENCIATURA", "tituloPDF", "3");
                    break;

                  case 7:
                    setFormUploadDocMaestria("ActaExamProf", "ACTA DE EXAMEN PROFESIONAL (en caso de no tener título)", "actaExamProfPDF", "7");
                    break;

                  case 8:
                    setFormUploadDocMaestria("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8");
                    break;

                }
              }
          }else{
            ?>
            <thead>
              <tr style="font-size:12px">
                <th class="text-center">Archivo</th>
                <th class="text-center" width="150px">Seleccionar</th>
                <th class="text-center">Guardar</th>

                <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                  <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
                </th>

                <th class="text-center" id="save" title="Guardado" style="display:none;" >
                  <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
                </th>

                <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                  <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
                </th>

                <th  id="status" title="Validación Digital" style="display:none;">
                  <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
                </th>

                <th  id="status_fis" title="Validación Físico" style="display:none;">
                  <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              setFormUploadDocMaestria("CertLic",      "CERTIFICADO LICENCIATURA",   "certifPDF",       "4");
              setFormUploadDocMaestria("ConstProm",    "CONSTANCIA DE PROMEDIO DE LICENCIATURA",     "constPromPDF",    "6");
              setFormUploadDocMaestria("Titulo",       "TÍTULO LICENCIATURA",        "tituloPDF",       "3");
              setFormUploadDocMaestria("ActaExamProf", "ACTA DE EXAMEN PROFESIONAL  (en caso de no tener título)", "actaExamProfPDF", "7");
              setFormUploadDocMaestria("CartaRegTit",  "CARTA DE REGISTRO DE OPCIÓN A TITULACIÓN MEDIANTE ESTUDIOS ESTUDIOS DE POSGRADO", "cartaRegTitPDF",  "8");
              ?>


            </tbody>
          <?php
          }
          ?>
        </table>
      </div>
    </div>
  </div>
  <?php
}
*/
?>

<?php
/*
if($_SESSION['plan_id_plan'] == '5172'){
 ?>
  <!--Doctorado-->
  <div class="row" style="margin-top:25px;">
    <div class="col-xs-12">
      <legend>
        Documentos de Doctorado
      </legend>

      <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover table-striped">
          <?php

          $arrayCatTipoDocs = array();
          $arrayDocs        = array();

          if(count($getDocumentoByReference) > 0){
            ?>
            <thead>
              <tr style="font-size:12px">
                <th>No</th>
                <th class="text-center">Archivo</th>
                <th class="text-center" width="150px">Seleccionar</th>
                <th class="text-center">Guardar</th>

                <th class="text-center" id="ver" title="ver Documento"  >
                  <span data-toggle="tooltip" title="Ver Documentos">Ver Documento</span>
                </th>

                <th class="text-center" id="save" title="Guardado" >
                  <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
                </th>

                <th class="text-center" id="trash" title="Eliminar">
                  <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
                </th>

                <th  class="text-center" id="status1" title="Validación Digital" >
                  <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
                </th>

                <th class="text-center" id="status3" title="Validación Físico">
                  <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
                </th>
              </tr>
            </thead>

              <?php
              //obtenemos los tipos de documentos del catalogo
              for($i = 0; $i < count($getCatTipoDocumento); $i++){
                array_push($arrayCatTipoDocs, $getCatTipoDocumento[$i]['id_type_document']);
              }


              for($i = 0; $i < count($getDocumentoByReference); $i++){
                $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
                array_push($arrayDocs, $tipoDoc);
              }

              $result = array_diff($arrayCatTipoDocs, $arrayDocs);

              //obtenemos los documentos que tiene el aspirante
              for($i = 0; $i < count($getDocumentoByReference); $i++){
                $tipoDoc   = $getDocumentoByReference[$i]['ptd_id_plan_type_document'];
                //$order_doctorate = $getDocumentoByReference[$i]['order_doctorate'];
                array_push($arrayDocs, $tipoDoc);


                $status_validacion = $getDocumentoByReference[$i]['validated_digital_document'];
                $status_fisic      = $getDocumentoByReference[$i]['validated_physical_document'];
                $idDocumento       = $getDocumentoByReference[$i]['id_document_aspirant'];
                $nombreDocumento   = $getDocumentoByReference[$i]['reference']."_".$getDocumentoByReference[$i]['ptd_id_plan_type_document'].".pdf";

                //echo $tipoDoc."<br>";

                switch ($tipoDoc) {
                  //doctorado
                  case '3':
                    setFormShowDocDoctorado( "Titulo",  "TÍTULO LICENCIATURA", "tituloPDF", "3", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                  case '4':
                    setFormShowDocDoctorado("CertLic", "CERTIFICADO LICENCIATURA", "certifPDF", "4", $idDocumento, $nombreDocumento, $status_validacion, $status_fisic);
                    break;

                  case '9':
                    setFormShowDocDoctorado("gradoMaestria", "GRADO DE MAESTRÍA", "gradoMaestriaPDF", "9", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '12':
                    setFormShowDocDoctorado("ActaExamProf",  "ACTA DE EXAMEN DE GRADO (en caso de no tener el grado)", "actaExamProfPDF", "12", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '10':
                    setFormShowDocDoctorado("carRegModGrad", "CARTA DE REGISTRO DE LA MODALIDAD DE GRADUACIÓN POR ESTUDIOS DE DOCTORADO (EGRESADOS UNAM)", "carRegModGradPDF", "10", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '11':
                    setFormShowDocDoctorado("certMaestri", "CERTIFICADO DE MAESTRÍA", "certMaestriPDF", "11", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '6':
                    setFormShowDocDoctorado("ConstProm", "CONSTANCIA DE PROMEDIO DE LICENCIATURA", "constPromPDF", "6", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;

                  case '13':
                    setFormShowDocDoctorado("constPromMaestria", "CONSTANCIA DE PROMEDIO DE MAESTRÍA", "constPromMaestriaPDF", "13", $idDocumento, $nombreDocumento, $status_validacion,  $status_fisic);
                    break;
                }
              }

              //documentos faltantes
              $result = array_diff($arrayCatTipoDocs, $arrayDocs);

              //print_r($result);

              foreach ($result as $indice => $valor) {
                //echo "$indice -> $valor<br>";
                switch ($valor) {
                  //doctorado
                  case 3:
                    setFormUploadDocDoctorado("Titulo", "TÍTULO LICENCIATURA", "tituloPDF", "3");
                    break;

                  case 4:
                    setFormUploadDocDoctorado("CertLic", "CERTIFICADO LICENCIATURA", "certifPDF", "4");
                    break;

                  case 9:
                    setFormUploadDocDoctorado("gradoMaestria", "GRADO DE MAESTRÍA", "gradoMaestriaPDF", "9");
                    break;

                  case 12:
                    setFormUploadDocDoctorado("ActaExamProf", "ACTA DE EXAMEN DE GRADO (en caso de no tener el grado)", "actaExamProfPDF", "12");
                    break;

                  case 10:
                    setFormUploadDocDoctorado("carRegModGrad", "CARTA DE REGISTRO DE LA MODALIDAD DE GRADUACIÓN POR ESTUDIOS DE DOCTORADO (EGRESADOS UNAM)", "carRegModGradPDF", "10");
                    break;

                  case 11:
                    setFormUploadDocDoctorado("certMaestri", "CERTIFICADO DE MAESTRÍA", "certMaestriPDF", "11");
                    break;

                  case 6:
                    setFormUploadDocDoctorado("ConstProm", "CONSTANCIA DE PROMEDIO DE LICENCIATURA", "constPromPDF", "6");
                    break;

                  case 13:
                    setFormUploadDocDoctorado("constPromMaestria", "CONSTANCIA DE PROMEDIO DE MAESTRÍA", "constPromMaestriaPDF", "13");
                    break;

                }
              }
          }else{
            ?>
            <thead>
              <tr style="font-size:12px">
                <th>No</th>
                <th class="text-center">Archivo</th>
                <th class="text-center" width="150px">Seleccionar</th>
                <th class="text-center">Guardar</th>

                <th class="text-center" id="ver" title="ver Documento" style="display:none;" >
                  <span data-toggle="tooltip" title="Ver Documento">Ver Documento</span>
                </th>

                <th class="text-center" id="save" title="Guardado" style="display:none;" >
                  <span data-toggle="tooltip" title="Documento Guardado">Documento Guardado</span>
                </th>

                <th class="text-center" id="trash" title="Eliminar" style="display:none;">
                  <span data-toggle="tooltip" title="Eliminar Documento">Eliminar Documento</span>
                </th>

                <th  id="status" title="Validación Digital" style="display:none;">
                  <span data-toggle="tooltip" title="Documento Digital Validado">Documento Digital Validado</span>
                </th>

                <th  id="status_fis" title="Validación Físico" style="display:none;">
                  <span data-toggle="tooltip" title="Documento Físico Validado">Documento Físico Validado</span>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              setFormUploadDocDoctorado("Titulo",       "TÍTULO LICENCIATURA",        "tituloPDF",       "3");
              setFormUploadDocDoctorado("CertLic",      "CERTIFICADO LICENCIATURA",   "certifPDF",       "4");
              setFormUploadDocDoctorado("gradoMaestria",    "GRADO DE MAESTRÍA",     "gradoMaestriaPDF",    "9");
              setFormUploadDocDoctorado("ActaExamProf", "ACTA DE EXAMEN DE GRADO(en caso de no tener el grado)", "actaExamProfPDF", "12");
              setFormUploadDocDoctorado("carRegModGrad",    "CARTA DE REGISTRO DE LA MODALIDAD DE GRADUACIÓN POR ESTUDIOS DE DOCTORADO (EGRESADOS UNAM)",     "carRegModGradPDF",    "10");
              setFormUploadDocDoctorado("certMaestri",    "CERTIFICADO DE MAESTRÍA",     "certMaestriPDF",    "11");
              setFormUploadDocDoctorado("ConstProm",    "CONSTANCIA DE PROMEDIO DE LICENCIATURA",     "constPromPDF",    "6");
              setFormUploadDocDoctorado("constPromMaestria",    "CONSTANCIA DE PROMEDIO DE MAESTRÍA",     "constPromMaestriaPDF",    "13");


              ?>
            </tbody>
            <?php
          }
          ?>
        </table>
      </div>
    </div>
  </div>
  <?php
}
*/
?>
