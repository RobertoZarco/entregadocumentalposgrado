<div class="container">
  <div class="col-sm-13">
    <div class="table-responsive">
      <table class="table table-bordered table-condensed table-hover table-striped">
        <thead>
          <tr>
            <th>Tipo</th>
            <th>Seleccionar </th>
            <th>Adjuntar</th>
            <th>Archivo Subido</th>
          </tr>
        </thead>
        <tbody>
          <tr id="acta">
            <form id="upload_acta" name="upload_acta" enctype="multipart/form-data" method="post">
              <td>
                ACTA DE NACIMIENTO
              </td>
              <td>
                <input type="file" id="documento" name="documento" required/>
              </td>
              <td>
                <input type="submit" id="boton_acta" value="Registrar Documento"/>
              </td>
              <td class="text-center">
                <span id="ok_acta" class="glyphicon glyphicon-ok" aria-hidden="true" style="display:none;"></span>
              </td>
              <input type="hidden" name="cat_tipo_doc" id="cat_tipo_doc" value="1">
            </form>
          </tr>
          <tr id="curp">
            <form id="upload_curp" name="upload_curp" enctype="multipart/form-data" method="post">
              <td>
                CLAVE UNICA DE REGISTRO DE POBLACION (CURP SOLO MEXICANOS)
              </td>
              <td>
                <input type="file" id="documento" name="documento" required/>
              </td>
              <input type="hidden" name="cat_tipo_doc" id="cat_tipo_doc" value="2">
              <td>
                <input type="submit" id="boton_CURP" value="Registrar Documento"/>
              </td>
              <td class="text-center">
                <span id="ok_curp" class="glyphicon glyphicon-ok" aria-hidden="true" style="display:none;"></span>
              </td>
            </form>
          </tr>
          <tr id="certi_sec">
            <form id="upload_certi_sec" name="upload_certi_sec" enctype="multipart/form-data" method="post">
              <td>
                CERTIFICADO DE ESTUDIOS DE SECUNDARIA
              </td>
              <td>
                <input type="file" id="documento" name="documento" required/>
              </td>
              <input type="hidden" name="cat_tipo_doc" id="cat_tipo_doc" value="3">
              <td>
                <input type="submit" id="boton_certi_sec" value="Registrar Documento"/>
              </td>
              <td class="text-center">
                <span id="ok_certi_sec" class="glyphicon glyphicon-ok" aria-hidden="true" style="display:none;"></span>
              </td>
            </form>
          </tr>
          <tr id="certi_bach">
            <form id="upload_certi_bach" name="upload_certi_bach" enctype="multipart/form-data" method="post">
              <td>
                Certificado de Bachillerato
              </td>
              <td>
                <input type="file" id="documento" name="documento" required/>
              </td>
              <input type="hidden" name="cat_tipo_doc" id="cat_tipo_doc" value="4">
              <td>
                <input type="submit" id="boton_certi_bach" value="Registrar Documento"/>
              </td>
              <td class="text-center">
                <span id="ok_certi_bach" class="glyphicon glyphicon-ok" aria-hidden="true" style="display:none;"></span>
              </td>
            </form>
          </tr>
          <tr id="cita">
            <form id="upload_cita" name="upload_cita" enctype="multipart/form-data" method="post">
              <td>
                Cita
              </td>
              <td>
                <input type="file" id="documento" name="documento" required/>
              </td>
              <input type="hidden" name="cat_tipo_doc" id="cat_tipo_doc" value="5">
              <td>
                <input type="submit" id="boton_cita" value="Registrar Documento"/>
              </td>
              <td class="text-center">
                <span id="ok_cita" class="glyphicon glyphicon-ok" aria-hidden="true" style="display:none;"></span>
              </td>
            </form>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>


<?php
//require_once "../../../controllers/documentos_controller.php";
if($acta_pdf_ok == true){
  echo "<script>$(acta ).remove();</script>";
}
if($curp_pdf_ok == true){
  echo "<script>$(curp ).remove();</script>";
}
if($certi_sec_pdf_ok == true){
  echo "<script>$(certi_sec ).remove();</script>";
}
if($certi_bach_pdf_ok == true){
  echo "<script>$(certi_bach ).remove();</script>";
}
if($cita_pdf_ok == true){
  echo "<script>$(cita ).remove();</script>";
}
?>
