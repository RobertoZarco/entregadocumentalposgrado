<?php

require_once '../../../controllers/admin/fiaf_controller.php';


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1C3D6C">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Sección: Título del Sitio -->
    <title>Administración de usuarios</title>
    <!-- Sección: Links -->
    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
    <!-- <link href="../../../assets/css/login.css" rel="stylesheet"> -->
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
    <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
    <script src='../../../assets/js/jquery.js'></script>
    <script src='../../../assets/js/app/admin/create_user.js'></script>
    <script src='../../../assets/js/app/admin/get_user.js'></script>
  </head>

	<?php
  require_once "navbar.php";
  ?>

  <body>

  	<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-bordered table-condensed table-hover table-striped">
							<thead>
								<tr>
									<th>Referencia</th>
									<th>Ruta</th>
									<th>Copiado</th>
								</tr>
							</thead>
							<tbody>
									<?php
									for($i = 0; $i < count($getFiaf); $i++){
										$origen = "/var/www/html/Repos/GEA/entregaDocumental/";
										$destino = "/var/www/html/Repos/GEA/entregaDocumental/uploads/FONABEC/";

										$ruta = explode("/", $getFiaf[$i]['rutaDocumento']);
										?>
										<tr>
											<td><?php echo $getFiaf[$i]['referencia']; ?></td>
											<td><?php echo $getFiaf[$i]['rutaDocumento']; ?></td>
											<td><?php echo $origen.$getFiaf[$i]['rutaDocumento'] ?></td>
											<td><?php echo $destino.$ruta[2] ?></td>
											<td><?php copy($origen.$getFiaf[$i]['rutaDocumento'], $destino.$ruta[2]) ?></td>
										</tr>
										<?php
									}
									?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
  	</div>




    <!--Principio del Footer -->
    <footer class="footer">
      <div class="list-info">
        <div class="container">
          <div class="col-sm-3">
            <h5 tabindex="0">Contacto</h5>
            <p class="pmenor" tabindex="0">
              <i class="fa fa-phone"></i>
              &nbsp;Atención por Teléfono
              <br> 5622 - 1524
              <br> 5622 - 1525
              <br> De 9:00 a 19:30 hrs.
            </p>
          </div>
          <div class="col-sm-6">
            <p class="pmenor" tabindex="0">
              Se brinda información de:
              <ul tabindex="0">
                <li>Convocatoria para los concursos de selección</li>
                <li>Examen COMIPEMS</li>
                <li>Ingreso a Iniciación Universitaria</li>
                <li>Ingreso a Licenciatura por Pase Reg+lamentado</li>
                <li>Resultados de los concursos de selección</li>
                <li>Trámites y Servicios Escolares en general</li>
                <li>Ubicación de dependencias de la UNAM</li>
                <li>Venta de Guías y Planes de Estudio</li>
              </ul>
            </p>
          </div>

          <div class="col-sm-3">
            <h5><i class="fa fa-sitemap"></i> &nbsp;<a href="https://www.dgae.unam.mx/mapasitio.html" class="link_footer" tabindex="0">Mapa de sitio</a></h5>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
      <div class="row" id="fondo">
        <div class="col-sm-12">
          <p class="pmenor" tabindex="0">
            Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2009 - 2014.
            <br>Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, requiere permiso previo por escrito de la institución
            <br>
            <br>Sitio web administrado por: Dirección General de Administración Escolar
          </p>
        </div>
      </div>
    </footer>
    <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- barra de navegación-->
    <script type="text/javascript" src="../../../assets/js/navbar.js"></script>
    <!-- /Sección: Scripts -->
  </body>

  </html>

