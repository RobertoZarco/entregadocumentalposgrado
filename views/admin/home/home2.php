<?php
require_once("../../../controllers/admin/home_controller.php");
/**/

 //print_r($_POST);

if(isset($_SESSION['login'])){
  ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1C3D6C">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Sección: Title del Sitio -->
    <title>DGAE - UNAM</title>
    <!-- /Sección: Title del Sitio -->
    <!-- Sección: Links -->
    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
    <!--<link href="../../../assets/css/login.css" rel="stylesheet">-->
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">


    <script src='../../../assets/js/jquery.js'></script>
    <!-- <script src='../../../assets/js/app/admin/create_user.js'></script> -->
    <script src='../../../assets/js/app/admin/get_user.js'></script>

	<link href="../../../assets/datatables/css/miestilo_dgae.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../../../assets/datatables/css/jquery.dataTables.min.css">

	<!--<script type="text/javascript" language="javascript" src="../../../assets/datatables/js/jquery-1.11.2.js"></script>-->
	<script type="text/javascript" language="javascript" src="../../../assets/datatables/js/jquery.dataTables.min2.js"></script>


<script>
	$(document).ready(function() {
		$('#example').DataTable({
			"order": [[ 1, "asc" ]],
			"language": {
				"lengthMenu": "Mostar _MENU_ registros por pagina",
				"info": "Mostrando pagina _PAGE_ de _PAGES_",
				"infoEmpty": "No se encontraron resultados",
				"infoFiltered": "(filtrada de _MAX_ registros)",
				"search": "Buscar:",
				"paginate": {
					"next":       "Siguiente",
					"previous":   "Anterior"
				},
			}
		});
	} );
</script>
</head>
<body id="inicio">
    <?php
    require_once "navbar.php";
    ?>
  <main id="maincontent" role="main" class="clearfix" height="1000px">

      <div class="row">
        <div class="col-sm-12">
          <h2 id="Bienvenido">Bienvenid@ <?php echo $_SESSION['name'] ." ".$_SESSION['first_surname']." ".$_SESSION['second_surname']; ?></h2>
        </div>
      </div>

      <div id="Foto">
      </div>

      <div class="row">
        <div class="col-sm-7">
          <div id="modal_validacion"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-7">
          <div id="Proped"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div id="errores_home_foto"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div id="errores_home"></div>
        </div>
      </div>


	<div class="row">
	<div class="col-lg-1 col-md-1"></div>
<div class="col-lg-10 col-md-10">
  <!-- contenido principal -->
  <section class="content"  id="contenido_principal">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="min-height:957px !important;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 tabindex="0">
            <center>
              <h2 class="post-titulo" tabindex="0">
                Entrega Documental Posgrado
                <small>Listado de Aspirantes</small>
              </h2>
            </center>
          </h1>
        </section>

      <!-- cargador actividades -->
      <div class="col-lg-4 col-md-4"></div>
    <div class="col-lg-4 col-md-4">
        <div style="display: none;" id="cargador_empresa" align="center">
            <br>
         <label style="color:#FFF; background-color:#ABB6BA; text-align:center">&nbsp;&nbsp;&nbsp;Espere... &nbsp;&nbsp;&nbsp;</label>
         <img src="imagenes/cargando.gif" align="middle" alt="cargador"> &nbsp;<label style="color:#ABB6BA">Realizando tarea solicitada ...</label>
          <br>
         <hr style="color:#003" width="50%">
         <br>
       </div>
      </div>


	<div class="col-md-12">
		<div class="box box-primary col-lg-1 col-md-1"></div>
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Aspirantes</h3>
					<div class="container minav col-lg-12 col-md-12">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
							<ul class="nav navbar-nav">
								<li class="active"><a href="listado_aspirante">Sin Documentos Subidos</a></li>
								<li><a href="listado_aspirantes_docs">Documentos por Subir</a></li>
								<li><a href="listado_aspirantes_validate">Documentos Validados</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<?php
					$aspirantes=10;
				 if(count($getUsuarios) >= 1){
						?>
						<table class="table table-bordered table-condensed table-hover" style="color:black;" id="example" >
	                <thead>
                    <tr>
                      <th>No</th>
                      <th>Numero Cuenta</th>
                      <th>Nombre</th>
                      <th>Pais</th>
                      <?php
                        if($_SESSION['view_digital_document'] == 1){
                          ?>
                          <th>Visualizar</th>
                          <?php
                        }
                      ?>

                      <?php
                        if($_SESSION['edit_aspirant'] == 1){
                          ?>
                          <!--<th>Editar</th>-->
                          <?php
                        }
                      ?>

                      <?php
                        //if($_SESSION['chat'] == 1){
                          ?>
                          <!--<th>Chat</th>-->
                          <?php
                        //}
                      ?>

                      <?php
                        if($_SESSION['post'] == 1){
                          ?>
                          <th>Validado</th>
                          <?php
                        }
                      ?>

                      <?php
                        if($_SESSION['login'] == 'rherrera' or $_SESSION['login'] == 'esanta'){
                          ?>
                          <!--<th>Eliminar</th>-->
                          <?php
                        }
                      ?>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    for($i = 0; $i < count($getUsuarios); $i++){
                      ?>
                      <tr role="row" class="odd">
                        <td><?php echo $i +1; ?></td>
                        <td><?php echo $getUsuarios[$i]['account_number'] ?></td>
                        <td><?php echo $nombre = $getUsuarios[$i]['first_surname'] ." " .$getUsuarios[$i]['second_surname'] ." ". $getUsuarios[$i]['name'] ?></td>
                        <td><?php echo $getUsuarios[$i]['country'] ?></td>
                        <?php
                          if($_SESSION['view_digital_document'] == 1){
                            ?>
                            <td class="text-center">
                              <?php
                                $countDocs = $Admin -> getCountDocsAspi($getUsuarios[$i]['account_number']);
                              ?>

                              <!--a href="visualizar_pdf" class="glyphicon glyphicon-file" title="ver documentos"></a-->
                              <button type="button" id="btnVisuaDocs"
                                value            ="<?php echo $getUsuarios[$i]['account_number']?>"
                                data-id_aspirant ="<?php echo $getUsuarios[$i]['id_aspirant'] ?>"
                                data-nombre      ="<?php echo $getUsuarios[$i]['name'] ?>"
                                data-paterno     ="<?php echo $getUsuarios[$i]['first_surname']?>"
                                data-materno     ="<?php echo $getUsuarios[$i]['second_surname']?>"
                                data-correo      ="<?php echo $getUsuarios[$i]['email']?>"
                                data-validate    ="<?php echo $getUsuarios[$i]['validate']?>"
                                class="glyphicon glyphicon-file" style="color:#337ab7">

                                  <!--Parametros permisos -->
                                  <?php
                                  if($countDocs > 0){
                                    ?>
                                    <span><?php echo $countDocs ?></span>
                                    <?php
                                  }

                                  ?>

                                  <input type="hidden" id="visualizarDocDigital" value="<?php echo $_SESSION['view_digital_document']; ?>" >
                                  <input type="hidden" id="eliminarDocDigital"   value="<?php echo $_SESSION['delete_digital_document']; ?>" >
                                  <input type="hidden" id="validarDocDigital"    value="<?php echo $_SESSION['validate_digital_document']; ?>" >
                                  <input type="hidden" id="recepcionDoc"         value="<?php echo $_SESSION['receive_physical_document']; ?>" >
                                  <input type="hidden" id="validarDocFisico"     value="<?php echo $_SESSION['validate_physical_document']; ?>" >
                              </button>
                            </td>
                            <?php
                          }
                        ?>

                        <?php
                          if($_SESSION['edit_aspirant'] == 1){
                            ?><!--
                            <td class="text-center">
                              <button  type="button" id="btnEditUser" value="<?php echo $getUsuarios[$i]['id_aspirant']?>" class="glyphicon glyphicon-pencil" style="color:#337ab7"></button>
                            </td>-->
                            <?php
                          }
                        ?>


                        <?php
                          if($getUsuarios[$i]['validate'] == 1){
                            ?>
                            <td class="text-center">
                              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </td>
                            <?php
                          }else{
                            ?>
                            <td class="text-center">

                            </td>
                            <?php
                          }
                        ?>

                        <?php
                          if($_SESSION['login'] == 'rherrera' or $_SESSION['login'] == 'esanta'){
                            ?>
                            <!--td class="text-center"><a href="" id="btnTrahsUser" class="glyphicon glyphicon-trash"></a></td>-->
                            <!--
                            <td class="text-center">
                              <button  type="button" id="btnTrahsUser" value="<?php echo $getUsuarios[$i]['id_aspirant']?>" class="glyphicon glyphicon-trash" style="color:#337ab7"></button>
                            </td>
                            -->
                            <?php
                          }
                        ?>

                        <?php
                          /*
                          if($_SESSION['chat'] == 1){
                            <td class="text-center">
                              <form action="" name="formChatSelect" method="POST" id="formChatSelect">
                                <input type="hidden" name="referencia" value="<?php echo $getUsuarios[$i]['referencia']?>" >
                                <input type="hidden" name="nombre" value="<?php echo $nombre?>" >
                                <button
                                  type="submit"
                                  id="referencyChat"
                                  name="referencia"
                                  class="glyphicon glyphicon-comment"
                                  style="color:#337ab7"
                                  value="<?php echo $getUsuarios[$i]['referencia']?>" >

                                  <span id="badge_<?php echo $getUsuarios[$i]['referencia']; ?>" class="badge" ></span>
                                </button>
                              </form>
                            </td>
                          }
                          */
                        ?>
                      </tr>

                      <?php
                    }
                    ?>
                  </tbody>
						</table>
						<?php
					}
					else
					{
						?>
						<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun tramite...</label>  </div>
						<?php
					}
					?>
				</div><!-- Fin Body Box -->
			</div><!-- Fin Box -->

          <div class="row">
            <div class="col-xs-6">
              <div id="correo">
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-5">
          <div id="pdf">
            <!--Aqui se crea el PDF-->
          </div>
        </div>

		</div>
	</div>




      </div><!-- /.content-wrapper -->
    </section>
</div><!-- col 9-->
</div>


</main>
    <!--Principio del Footer -->
    <footer class="footer">
      <div class="list-info">
        <div class="container">
          <div class="col-sm-3">
            <h5 tabindex="0">Contacto</h5>
            <p class="pmenor" tabindex="0">
              <i class="fa fa-phone"></i>
              &nbsp;Atención por Teléfono
              <br> 5622 - 1524
              <br> 5622 - 1525
              <br> De 9:00 a 19:30 hrs.
            </p>
          </div>
          <div class="col-sm-6">
            <p class="pmenor" tabindex="0">
              Se brinda información de:
              <ul tabindex="0">
                <li>Convocatoria para los concursos de selección</li>
                <li>Examen COMIPEMS</li>
                <li>Ingreso a Iniciación Universitaria</li>
                <li>Ingreso a Licenciatura por Pase Reg+lamentado</li>
                <li>Resultados de los concursos de selección</li>
                <li>Trámites y Servicios Escolares en general</li>
                <li>Ubicación de dependencias de la UNAM</li>
                <li>Venta de Guías y Planes de Estudio</li>
              </ul>
            </p>
          </div>

          <div class="col-sm-3">
            <h5><i class="fa fa-sitemap"></i> &nbsp;<a href="https://www.dgae.unam.mx/mapasitio.html" class="link_footer" tabindex="0">Mapa de sitio</a></h5>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
      <div class="row" id="fondo">
        <div class="col-sm-12">
          <p class="pmenor" tabindex="0">
            Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2009 - 2014.
            <br>Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, requiere permiso previo por escrito de la institución
            <br>
            <br>Sitio web administrado por: Dirección General de Administración Escolar
          </p>
        </div>
      </div>
    </footer>

</body>
</html>
<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>