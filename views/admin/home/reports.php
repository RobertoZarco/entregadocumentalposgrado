<?php
require_once("../../../controllers/admin/report_controller.php");

if(isset($_SESSION['login'])){
  ?>

  <!DOCTYPE html>
  <html lang="es">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1C3D6C">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Sección: Title del Sitio -->
    <title>DGAE - UNAM</title>
    <!-- /Sección: Title del Sitio -->
    <!-- Sección: Links -->
    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
    <!--<link href="../../../assets/css/login.css" rel="stylesheet">-->
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
   	<script src='../../../assets/js/jquery.js'></script>


    <script src='../../../assets/js/app/admin/reports.js'></script>
    <!-- /Sección: Links -->

    <!-----------------------------------Estadisticas -----------------------------!-->
		<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>!-->
		<script type="text/javascript" src="../../../assets/js/jquery-1.8.2.min.js" ></script>

  </head>

	<body>

    <?php
    require_once "navbar.php";
    ?>


		<script type="text/javascript">

			$(function () {
			  // Create the chart
			  $('#report').highcharts({
			      chart: {
			          type: 'column'
			      },
			      title: {
			          text: 'Entrega Documental en Linea '
			      },
			      subtitle: {
			          text: ''
			      },
			      xAxis: {
			          type: 'category'
			      },
			      yAxis: {
			          title: {
			              text: 'Total'
			          }

			      },
			      legend: {
			          enabled: false
			      },
			      plotOptions: {
			          series: {
			              borderWidth: 0,
			              dataLabels: {
			                  enabled: true,
			                  format: '{y: 0.f}'
			              }
			          }
			      },

			      tooltip: {
			          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> total<br/>'
			      },

			      series: [{
							          name: "MADEMS",
							          colorByPoint: true,
							          data: [{
							              name: "Total Aspirantes MADEMS",
							              y: <?php echo $getTotalAspiransMADEMS[0]['aspirantesMADEMS']; ?>,
							              drilldown: "MADEMS"
							          }
									/*
							          , {
							              name: "Total Aspirantes Maestría Sostenibilidad",
							              y: <?php echo $getTotalAspiransMSoste[0]['aspirantesMSoste']; ?>,
							              drilldown: "MSoste"
							          }, {
							              name: "Total Aspirantes Doctorado Sostenibilidad",
							              y: <?php echo $getTotalAspiransDSoste[0]['aspirantesDSoste']; ?>,
							              drilldown: "DSoste"
							          } */
			          				]
			      }],

			      drilldown: {
			          series: [{
			              name: "MADEMS",
			              id: "MADEMS",
			              data: [
			                  [
			                      "Total de Aspirantes",
			                      <?php echo $getTotalAspiransMADEMS[0]['aspirantesMADEMS']; ?>
			                  ],
			                  [
			                       "Aspirantes con documentos",
			                      <?php echo $getTotalDocumentsAspirantsMADEMS[0]['documentosMADEMS']; ?>
			                  ],
			                  [
			                      "Accesos al sistema",
			                      <?php echo $getTotalAccesoAspirantsMADEMS[0]['accesosMADEMS']; ?>
			                  ],
			                  [
			                      "Aspirantes validados",
			                      <?php echo $getTotalAspiransValidateMADEMS[0]['validadosMADEMS']; ?>
			                  ],
			                  [
			                      "Aspirantes no validados",
			                      <?php echo $getTotalAspiransMADEMS[0]['aspirantesMADEMS']-$getTotalAspiransValidateMADEMS[0]['validadosMADEMS']; ?>
			                  ]
			              ]
			          }
					/*
			          , {
			              name: "MSoste",
			              id: "MSoste",
			              data: [
			                  [
			                      "Total de Aspirantes",
			                      <?php echo $getTotalAspiransMSoste[0]['aspirantesMSoste']; ?>
			                  ],
			                  [
			                       "Aspirantes con documentos",
			                      <?php echo $getTotalDocumentsAspirantsMSoste[0]['documentosMSoste']; ?>
			                  ],
			                  [
			                      "Accesos al sistema",
			                      <?php echo $getTotalAccesoAspirantsMSoste[0]['accesosMSoste']; ?>
			                  ],
			                  [
			                      "Aspirantes validados",
			                      <?php echo $getTotalAspiransValidateMSoste[0]['validadosMSoste']; ?>
			                  ],
			                  [
			                      "Aspirantes no validados",
			                      <?php echo $getTotalAspiransMSoste[0]['aspirantesMSoste']-$getTotalAspiransValidateMSoste[0]['validadosMSoste']; ?>
			                  ]
			              ]
			          }, {
			              name: "DSoste",
			              id: "DSoste",
			              data: [
			                  [
			                      "Total de Aspirantes",
			                      <?php echo $getTotalAspiransDSoste[0]['aspirantesDSoste']; ?>
			                  ],
			                  [
			                       "Aspirantes con documentos",
			                      <?php echo $getTotalDocumentsAspirantsDSoste[0]['documentosDSoste']; ?>
			                  ],
			                  [
			                      "Accesos al sistema",
			                      <?php echo $getTotalAccesoAspirantsDSoste[0]['accesosDSoste']; ?>
			                  ],
			                  [
			                      "Aspirantes validados",
			                      <?php echo $getTotalAspiransValidateDSoste[0]['validadosDSoste']; ?>
			                  ],
			                  [
			                      "Aspirantes no validados",
			                      <?php echo $getTotalAspiransDSoste[0]['aspirantesDSoste']-$getTotalAspiransValidateDSoste[0]['validadosDSoste']; ?>
			                  ]
			              ]
			          } */
			          ]
			      }

			  });
			});
		</script>

		<div class="row">
			<div class="col-sm-12">
				<div id="report"></div>
			</div>
		</div>

		<!--
		<div class="row" style="margin-top:100px;">
			<div class="col-sm-12">
				<div role="tabpanel">

					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a id="pestanias" href="#home" aria-controls="home" id='reportValidados' data-ruta="./reports/validados.php" role="tab" data-toggle="tab">
								Total de Aspirantes
							</a>
						</li>

						<li role="presentation">
							<a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">
								Aspirantes con documentos
							</a>
						</li>

						<li role="presentation">
							<a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">
								Accesaron al sistema
							</a>
						</li>

						<li role="presentation">
							<a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">
								Aspirantes validados
							</a>
						</li>

						<li role="presentation">
							<a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">
								Aspirantes no validados
							</a>
						</li>
					</ul>


					<div class="tab-content">
						<div id="load"></div>

						<div id="ventanas" class="panel-body">
							<div role="tabpanel" class="tab-pane active" id="home">
								???
							</div>
							<?php echo "<iframe id='objeto' onload='carga()'type='text/html' src='reports/validados.php' width='100%' height='1000px'> </iframe>"; ?>
						</div>



						<div role="tabpanel" class="tab-pane" id="tab">
							******
						</div>


					</div>
				</div>
			</div>
		</div>
		-->

		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">

		</div>
	</body>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../../assets/js/bootstrap.min.js"></script>

	<script src="../../../assets/js/highcharts.js"></script>
	<script src="../../../assets/js/modules/data.js"></script>
	<script src="../../../assets/js/modules/drilldown.js"></script>

  <?php require_once 'footer.php' ?>

	</html>

	<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>

