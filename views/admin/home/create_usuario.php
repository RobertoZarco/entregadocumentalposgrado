<?php
//session_start();
require_once '../../../helpers/admin/usuarios_helper.php';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1C3D6C">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Sección: Título del Sitio -->
    <title>Administración de usuarios</title>
    <!-- Sección: Links -->
    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
    <!-- <link href="../../../assets/css/login.css" rel="stylesheet"> -->
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
    <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
    <script src='../../../assets/js/jquery.js'></script>
    <script src='../../../assets/js/app/admin/create_user_admin.js'></script>
    <script src='../../../assets/js/app/admin/get_user.js'></script>
  </head>

  <body id="inicio">

    <?php require_once "navbar.php" ?>
    <main id="maincontent" role="main" class="clearfix" height="1000px">
    <div class="container">

      <div class="row">
        <div class="col-sm-6">
          <div class="col-sm-12">
            <form action="" id="formCreateUser" method="POST" class="form-horizontal" role="form">
              <div class="form-group">
                <legend>Datos del Usuario</legend>
                <br/>
              </div>

              <div class="form-group">

                <div class="col-sm-6">
                  <label class="" for="paterno">Paterno:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" id="paterno" placeholder="Paterno">
                </div>

                <div class="col-sm-6">
                  <label class="" for="materno">Materno:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" id="materno" placeholder="Materno">
                </div>

                <div class="col-sm-6">
                  <label class="" for="nombre">Nombre:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" id="nombre" placeholder="Nombre(s)">
                </div>

                <div class="col-sm-6">
                  <label class="" for="nombre">Fecha Nacimiento:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" name="fechaNacimiento" id="fechaNacimiento" placeholder="YYYY/MM/DD">
                </div>

                <div class="col-sm-6">
                  <label class="" for="nombre">CURP:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" name="curp" id="CURP" placeholder="CURP">
                </div>


                <div class="col-sm-6">
                  <label class="" for="nombre">Correo:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" name="email" id="email" placeholder="email">
                </div>

                <div class="col-sm-6">
                  <label for="selectGenero" class="">Genero</label>
                </div>

                <div class="col-sm-6">
                  <select name="" id="selectGenero" class="form-control" required="required">
                    <option value="x" disabled="" selected="">SELECCIONA</option>
                    <option value="H">HOMBRE</option>
                    <option value="M">MUJER</option>
                  </select>
                </div>

                  <?php
                      if($_SESSION['login'] == 'esanta' or $_SESSION['login'] == 'rherrera'){
                        ?>
                        <div class="col-sm-6">
                            <label for="selectDependencia" class="">Dependencia</label>
                          </div>
                          <div class="col-sm-6">
                            <select name="" id="selectDependencia" class="form-control" required="required">
                              <option value="x" disabled="" selected="">SELECCIONA</option>
                              <option value="1">POSGRADO</option>
                              <option value="2">LOCAL</option>
                            </select>
                          </div>
                        <?php
                      }else{
                        ?>
                        <input type="hidden" id="selectDependencia" value="<?php echo $_SESSION['cveDependencia'] ?>" class="form-control" required="required">
                        <?php
                      }
                    ?>

                <!-- Sección: Permisos -->
                <div class="col-sm-6">
                  <label for="PermisoCrearUsuario" class="">Crear Usuarios</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoCrearUsuario" placeholder="PermisoCrearUsuario">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoCrearAspi" class="">Crear Aspirantes</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoCrearAspi" placeholder="PermisoCrearAspi">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoEditarUsuarios" class="">Editar  Usuarios</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoEditarUsuarios" placeholder="PermisoEditarUsuarios">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoChat" class="">Chat con Aspirantes</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoChat" placeholder="PermisoChat">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoVerDocDig" class="">Ver Documentos Digitales</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoVerDocDig" placeholder="PermisoVerDocDig">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoElimDocDig" class="">Eliminar Documentos Digitales</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoElimDocDig" placeholder="PermisoElimDocDig">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoValiDocDig" class="">Validar Documentos Digitales</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoValiDocDig" placeholder="PermisoValiDocDig">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoValiRecepDocs" class="">Validar Recepción Documentos</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoValiRecepDocs" placeholder="PermisoValiRecepDocs">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoValiDocFis" class="">Validar Documentos Físicos</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoValiDocFis" placeholder="PermisoValiDocFis">
                </div>

                <div class="col-sm-6">
                  <label for="PermisoReport" class="">Generar Reportes</label>
                </div>
                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="PermisoReport" placeholder="PermisoReport">
                </div>

                <!-- Sección: Permisos -->
                <div class="col-sm-6">
                  <label class="" for="login">Nombre de usuario:</label>
                </div>

                <div class="col-sm-6">
                  <input type="text" class="form-control" id="login" placeholder="Nombre de usuario">
                </div>

                <div class="col-sm-6">
                  <label class="" for="pass">Password:</label>
                </div>

                <div class="col-sm-6">
                  <input type="password" class="form-control" id="pass" placeholder="pass">
                </div>

                <div class="col-sm-6">
                  <label class="" for="pass2">Repetir Password:</label>
                </div>

                <div class="col-sm-6">
                  <input type="password" class="form-control" id="pass2" placeholder="pass">
                </div>

                <div class="col-sm-6">
                  <label class="" for="activo">Activo:</label>
                </div>

                <div class="col-sm-6">
                  <input type="checkbox" class="form-control" id="activo" placeholder="pass">
                </div>

                <div class="col-sm-6">
                  <label class="sr-only" fecha_creacionfor="">Fecha de Creación:</label>
                </div>

                <div class="col-sm-6">
                  <input type="hidden" class="form-control" id="fechaAlta" placeholder="fechaAlta">
                </div>

              </div>

              <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                  <button type="button" id="btnCreateUser" class="btn btn-primary">Crear Usuario</button>
                </div>
              </div>
            </form>

            <div id="create_result"></div>
          </div>
        </div>
      </div>
    </div>


    <!--Principio del Footer -->
  <footer class="main-footer">
    <div class="list-info">
      <div class="container">
        <div class="col-sm-3">
          <h5 tabindex="0">Contacto</h5>
          <p class="pmenor" tabindex="0">
            <i class="fa fa-phone"></i>
            &nbsp;Atención por Teléfono
            <br> 5622 - 1524
            <br> 5622 - 1525
            <br> De 9:00 a 19:30 hrs.
          </p>
        </div>
        <div class="col-sm-6">
          <p class="pmenor" tabindex="0">
            Se brinda información de:
            <ul tabindex="0">
              <li>Convocatoria para los concursos de selección</li>
              <li>Examen COMIPEMS</li>
              <li>Ingreso a Iniciación Universitaria</li>
              <li>Ingreso a Licenciatura por Pase Reg+lamentado</li>
              <li>Resultados de los concursos de selección</li>
              <li>Trámites y Servicios Escolares en general</li>
              <li>Ubicación de dependencias de la UNAM</li>
              <li>Venta de Guías y Planes de Estudio</li>
            </ul>
          </p>
        </div>

        <div class="col-sm-3">
          <h5><i class="fa fa-sitemap"></i> &nbsp;<a href="https://www.dgae.unam.mx/mapasitio.html" class="link_footer" tabindex="0">Mapa de sitio</a></h5>
          <br>
          <br>
          <br>
        </div>
      </div>
    </div>
    <div class="row" id="fondo">
      <div class="col-sm-12">
        <p class="pmenor" tabindex="0">
          Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2009 - 2014.
          <br>Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, requiere permiso previo por escrito de la institución
          <br>
          <br>Sitio web administrado por: Dirección General de Administración Escolar
        </p>
      </div>
    </div>
  </footer>
  <!-- Sección: Scripts -->
  <!-- jQuery -->
  <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
  <!-- Material Design Bootstrap -->
  <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
  <!-- Analytics -->
  <script type="text/javascript" src="../../../assets/js/analytics.js"></script>
  <!-- barra de navegación-->
  <script type="text/javascript" src="../../../assets/js/navbar.js"></script>

    <script src="../../../assets/js/bootstrap.min.js"></script>

  </body>
</html>