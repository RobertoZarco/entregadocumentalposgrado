<?php
require_once("../../../controllers/admin/home_controller.php");
?>
  <!-- Navegacion -->
  <nav role="navigation">
    <!-- Fixed navbar -->
    <div class="navbar navbar-fixed-top" role="navigation" style="background-color: rgba(28, 61, 108, 0); box-shadow: none; top: 100px;">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
            <i class="fa fa-bars" style="color: white;"></i>
          </button>
          <div class="small-logo-container">
            <a class="small-logo" href="https://www.dgae.unam.mx/" tabindex="-1">
              UNAM - DGAE
            </a>
          </div>
        </div>


        <!-- Sección: Navegación -->
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a class="link-barra" href="home.php" tabindex="0">Inicio</a>
            </li>

            <li>
              <a class="link-barra" href="madems.php" tabindex="0">MADEMS</a>
            </li>


            <li class="dropdown">
              <a href="#" class="dropdown-toggle link-barra" data-toggle="dropdown">Sostenibilidad<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="mSostenibilidad.php" tabindex="0">Maestría Sostenibilidad</a></li>
                <li><a href="dSostenibilidad.php" tabindex="0">Doctorado Sostenibilidad</a></li>
              </ul>
            </li>


            <li>
            </li>

            <li>
            </li>

            <?php
              if( $_SESSION['create_user'] == 1){
                ?>

                  <li>
                    <a class="link-barra" href="create_usuario.php" tabindex="0" >Crear Usuario</a>
                  </li>

                <?php
              }
            ?>


            <?php
              if( $_SESSION['create_user'] == 1){
                ?>
                  <li>
                    <a class="link-barra" href="list_user_admin.php" tabindex="0" >Lista Usuarios</a>
                  </li>
                <?php
              }
            ?>


            <?php
              if( $_SESSION['login'] == "rherrera"){
                ?>
                  <!--
                  <li>
                    <a class="link-barra" href="fiaf.php" tabindex="0" >FIAF</a>
                  </li>
                  -->
                <?php
              }
            ?>

            <?php
              if( $_SESSION['reports'] == "1"){
                ?>
                  <li>
                    <a class="link-barra" href="reports.php" tabindex="0" >Grafica</a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle link-barra" data-toggle="dropdown">Listados<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="aspirants.php">Aspirantes</a></li>
                      <li><a href="access.php">Aspirantes accesaron al sistema</a></li>
                      <li><a href="documentsAspirants.php">Aspirantes con documentos</a></li>
                      <li><a href="validatedAspirants.php">Aspirantes validados</a></li>
                      <li><a href="notValidateAspirants.php">Aspirantes sin validar</a></li>
                    </ul>
                  </li>
                <?php
              }
            ?>

            <li>
              <a class="link-barra" href="close_sesion.php" tabindex="0" >Cerrar Sesion</a>
            </li>
          </ul>
        </div>
        <!-- /Sección: Navegación -->
      </div>
    </div>
    <div class=" big-logo-row">
      <div class="container">
        <div>
          <div class="col-lg-12 col-md-12 big-logo-container">
            <div class="big-logo">
              <div class="pull-left logo_grande logo_der">
                <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                  <img src="../../../assets/images/escudo_unam_completow.svg">
                </a>
              </div>
              <div class="pull-left logo_chico logo_der">
                <a href="https://www.unam.mx/" title="UNAM" tabindex="-1">
                  <img src="../../../assets/images/escudo_unam_solow.svg">
                </a>
              </div>
              <div class="pull-right logo_grande logo_izq">
                <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                  <img src="../../../assets/images/escudo_dgae_completow.svg">
                </a>
              </div>
              <div class="pull-right logo_chico logo_izq">
                <a href="https://www.dgae.unam.mx" title="DGAE" tabindex="-1">
                  <img src="../../../assets/images/escudo_dgae_solow.svg">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
