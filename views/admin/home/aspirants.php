<?php
require_once '../../../controllers/admin/reports_controllers.php';

if(isset($_POST) and @$_POST['report'] == 'si'){
	$nombre = "Aspirantes_".date("d-m-Y").".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$nombre");
}else{

	?>
	<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
	    <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="theme-color" content="#1C3D6C">
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	    <!-- Sección: Title del Sitio -->
	    <title>Aspirantes</title>
	    <!-- /Sección: Title del Sitio -->
	    <!-- Sección: Links -->
	    <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
	    <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
	    <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
	    <!--<link href="../../../assets/css/login.css" rel="stylesheet">-->
	    <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
	    <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
	    <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
	    <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
	    <script src='../../../assets/js/jquery.js'></script>
  	</head>
	  <body id="inicio">

	  	<main id="maincontent" role="main" class="clearfix" height="1000px">
				<div class="row">
					<div class="col-xs-12">
						<?php
						require 'navbar.php';
						?>
					</div>
				</div>

			  <div class="container">

					<div class="row">
						<form action="" method="POST" class="form-horizontal" role="form">
							<div class="form-group">
								<legend>Descargar Listado a Excel</legend>
							</div>

							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<input type="hidden" value="si" name="report">
									<button type="submit" class="btn btn-primary">
										Descargar
										<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
									</button>
								</div>
							</div>
						</form>
					</div>

	  <?php

	  //print_r($getFullDocumentsAspirants);
}
?>
				  <div class="row">
				  	<div class="col-sm-12">
				  		<table id="tablaUsuarios" class="table table-condensed table-bordered table-hover table-striped">
				  			<thead>
				  				<tr>
				  					<th>No</th>
				  					<th><?php echo "Número cuenta" ?></th>
				  					<th>1er Apellido</th>
				  					<th>2do Apellido</th>
				  					<th>Nombre</th>
				  					<th>Modalidad</th>
				  					<th>Documentos</th>
				  					<th>Validado</th>
				  					<th>Acceso</th>
				  				</tr>
				  			</thead>
				  			<tbody>
				  				<?php
				  				for ($i = 0; $i < count($getAspirantsReport) ; $i++) {
			  						if($getAspirantsReport[$i]['validate'] == 1){
			  							$validado = "validado";
			  						}else{
			  							$validado = "";
			  						}
				  					?>
					  				<tr>
					  					<td><?php echo $i+1; ?></td>
					  					<td><?php echo $getAspirantsReport[$i]['account_number']; ?></td>
					  					<td><?php echo $getAspirantsReport[$i]['first_surname']; ?></td>
					  					<td><?php echo $getAspirantsReport[$i]['second_surname']; ?></td>
					  					<td><?php echo $getAspirantsReport[$i]['name']; ?></td>
					  					<td><?php echo $getAspirantsReport[$i]['modalidad']; ?></td>
					  					<td><?php echo $getFullDocumentsAspirants[$i]['documents']; ?></td>
					  					<td><?php echo $validado; ?></td>
					  					<th><?php echo $getAspirantsReport[$i]['welcome']?></th>
					  				</tr>
				  					<?php
				  				}

				  				?>
				  			</tbody>
				  		</table>
				  	</div>
				  </div>
				</div>

		    <!--sortable-->
		    <script src="../../../assets/js/jquery-latest.js"></script>
		    <script src="../../../assets/js/jquery.tablesorter.js"></script>
		    <script>
		      $("#tablaUsuarios").tablesorter();
		    </script>

		    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		    <script src="../../../assets/js/bootstrap.min.js"></script> -->
  </body>
</html>
