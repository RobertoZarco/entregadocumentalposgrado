<?php
require_once '../../../../controllers/admin/reports_controllers.php';

if(isset($_POST) and @$_POST['report'] == 'si'){
	$nombre = "Accesaron_".date("d-m-Y").".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$nombre");

}else{

	echo "<link rel='stylesheet' href='../../../../assets/css/bootstrap.css'>";
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Accesaron</title>
  </head>
  <body>


  <div class="container">


	  <div class="row">
	  	<div class="col-sm-12">
	  		<table class="table table-hover">
	  			<thead>
	  				<tr>
	  					<th>No</th>
	  					<th><?php echo "Número cuenta" ?></th>
	  					<th>1er Apellido</th>
	  					<th>2do Apellido</th>
	  					<th>Nombre</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  				<?php
	  				for ($i = 0; $i < count($getAccessAspirants) ; $i++) {
	  					?>
		  				<tr>
		  					<td><?php echo $i+1; ?></td>
		  					<td><?php echo $getAccessAspirants[$i]['account_number']; ?></td>
		  					<td><?php echo $getAccessAspirants[$i]['first_surname']; ?></td>
		  					<td><?php echo $getAccessAspirants[$i]['second_surname']; ?></td>
		  					<td><?php echo $getAccessAspirants[$i]['name']; ?></td>
		  				</tr>
	  					<?php
	  				}

	  				?>
	  			</tbody>
	  		</table>
	  	</div>
	  </div>

	</div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../../assets/js/bootstrap.min.js"></script>
  </body>
</html>
