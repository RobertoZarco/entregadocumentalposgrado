<?php
require_once("../../../controllers/admin/madems_controller.php");

if(isset($_SESSION['login'])){

$snUltimaCol=0;

if(isset($_POST) and @$_POST['report'] == 'si'){
  $nombre = "Aspirantes_MADEMS_".date("d-m-Y").".xls";
  header("Content-type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=$nombre");
  $snUltimaCol=1;
}else{
  require_once "header.php";
  ?>


  <body id="inicio">

    <main id="maincontent" role="main" class="clearfix" height="1000px">

        <div class="row">
          <div class="col-xs-12">
            <?php
            require 'navbar.php';
            ?>
          </div>
        </div>

        <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <h2 id="Bienvenido">Bienvenid@ <?php echo $_SESSION['name'] ." ".$_SESSION['first_surname']." ".$_SESSION['second_surname']; ?></h2>
        </div>
      </div>

      <div class="row">
        <form action="" method="POST" class="form-horizontal" role="form">
          <div class="form-group">
            <legend>Descargar Listado a Excel</legend>
          </div>

          <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
              <input type="hidden" value="si" name="report">
              <button type="submit" class="btn btn-primary">
                Descargar
                <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
              </button>
            </div>
          </div>
        </form>
      </div>
<?php }  ?>
      <div class="row">
        <div class="col-sm-12">
          <?php
            //Mostrar todos los aspirantes para admin local de registro
            if(count($getMadems) >= 1){
              ?>
              <div class="table-responsive">
                <table id="tableAspirants" class="table table-bordered table-condensed table-hover table-striped" style="color:black;" id="example" >
                  <thead>
                    <tr>
                      <th>
                        No <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Numero Cuenta <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Nombre <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Plan <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                       <th>
                        Modalidad <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Documentos <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Acceso <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Validado <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                    <?php if($snUltimaCol!=1){ ?>
                      <th>
                        Visualizar <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                    <?php }  ?>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    for($i = 0; $i < count($getMadems); $i++){
                      ?>
                      <tr>
                        <td><?php echo $i +1; ?></td>
                        <td><?php echo $getMadems[$i]['account_number'] ?></td>
                        <td><?php echo $nombre = $getMadems[$i]['first_surname'] ." " .$getMadems[$i]['second_surname'] ." ". $getMadems[$i]['name'] ?></td>
                        <td>
                          <?php
                            echo $getMadems[$i]['plan'];
                          ?>
                        </td>
                        <td>
                          <?php
                            echo $getUsuarios[$i]['modalidad'];
                          ?>
                        </td>
                        <td class="text-center">
                          <?php
                            $countDocs = $Admin -> getCountDocsAspi($getMadems[$i]['account_number']);

                            if($countDocs > 0){
                              ?>
                              <span><?php echo $countDocs ?></span>
                              <?php
                              }
                          ?>
                        </td>


                        <?php
                          if($getMadems[$i]['welcome'] == 1){
                            ?>
                            <td class="text-center">
                              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                              <input type="hidden" value="1" />
                             <?php  if($snUltimaCol==1){
                                echo "Ok";
                              } ?>
                            </td>
                            <?php
                          }else{
                            ?>
                            <td class="text-center">

                            </td>
                            <?php
                          }
                        ?>


                        <?php
                          if($getMadems[$i]['validate'] == 1){
                            ?>
                            <td class="text-center">
                              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                              <input type="hidden" value="1" />
                             <?php  if($snUltimaCol==1){
                                echo "Ok";
                              } ?>
                            </td>
                            <?php
                          }else{
                            ?>
                            <td class="text-center">

                            </td>
                            <?php
                          }
                        ?>
                        <?php
                          if($_SESSION['view_digital_document'] == 1 && $snUltimaCol!=1){
                            ?>
                            <td class="text-center">
                              <form action="viewAspirant.php" method="POST" class="form-inline" role="form">
                                <input type="hidden" name="account_number" value="<?php echo $getMadems[$i]['account_number']?>">
                                <input type="hidden" name="id_aspirant"    value="<?php echo $getMadems[$i]['id_aspirant'] ?>">
                                <input type="hidden" name="name"           value="<?php echo $getMadems[$i]['name'] ?>">
                                <input type="hidden" name="first_surname"  value="<?php echo $getMadems[$i]['first_surname']?>">
                                <input type="hidden" name="second_surname" value="<?php echo $getMadems[$i]['second_surname']?>">
                                <input type="hidden" name="email"          value="<?php echo $getMadems[$i]['email']?>">
                                <input type="hidden" name="validate"       value="<?php echo $getMadems[$i]['validate']?>">
                                <input type="hidden" name="plan"           value="<?php echo $getMadems[$i]['plan']?>">

                                <input type="hidden" name="searchAspirant" value="si">

                                <button type="submit" class="btn btn-primary">
                                  <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                </button>
                              </form>
                            </td>
                            <?php
                          }
                        ?>

                        <?php
                          if($_SESSION['edit_aspirant'] == 1){
                            ?><!--
                            <td class="text-center">
                              <button  type="button" id="btnEditUser" value="<?php echo $getMadems[$i]['id_aspirant']?>" class="glyphicon glyphicon-pencil" style="color:#337ab7"></button>
                            </td>-->
                            <?php
                          }
                        ?>




                      </tr>

                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <?php
            }else{
              ?>
              <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>No existen aspirantes</strong>
              </div>
              <?php
            }
          ?>
      </div>
    </div>
    </main>


    <?php require_once 'footer.php' ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../assets/js/bootstrap.min.js"></script>

     <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->

    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- barra de navegación-->
    <script type="text/javascipt" src="../../../assets/js/navbar.js"></script>
    <!-- /Sección: Scripts -->

    <script src="../../../assets/js/jquery-latest.js"></script>
    <script src="../../../assets/js/jquery.tablesorter.js"></script>
    <script>
      $("#tableAspirants").tablesorter();
    </script>
  </body>

  </html>

<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>
