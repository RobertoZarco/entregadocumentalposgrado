<?php

require_once '../../../controllers/admin/getUsers_controller.php';

if(isset($_SESSION['login'])){
	require_once "header.php";
  ?>
  <!-- <script src='../../../assets/js/app/admin/create_user.js'></script> -->
  <script src='../../../assets/js/app/admin/viewAspirant.js'></script>
    <script>
		function Recarga() {
		    location.reload();
		}
	</script>
  <!--<script src="../../../assets/js/app/admin/chat.js"></script>-->


	  <body id="inicio">

	    <?php
	    require_once "navbar.php";
	    ?>

	    <main id="maincontent" role="main" class="clearfix" height="1000px">

		    <?php

		    	//print_r($_POST);

		    	$id_aspirant    = $_POST['id_aspirant'];
		    	$id_aspirant    = $_POST['reference'];
					$name           = $_POST['name'];
					$first_surname  = $_POST['first_surname'];
					$second_surname = $_POST['second_surname'];
					$email          = $_POST['email'];
					$validate       = $_POST['validate'];
					$plan           = $_POST['plan'];

					if($validate == 1){
						$validate = 1;
						$checkedValidate = "checked";
						$disabledValidate = "disabled";
					}else{
						$validate = 0;
						$checkedValidate = "";
						$disabledValidate = "";
					}

					$nombre = $name." ".$first_surname." ".$second_surname;
		    ?>


				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-7">
							<?php
							//print_r($_POST);
							if(count($getDocumentoByReference) >= 1){
								?>
								<div class="row">
									<h2>Documentos del aspirante: <?php echo $nombre ?></h2>
								</div>

								<?php
								if($_SESSION['login'] != 'murrutia'){
									?>
									<!--Validar aspirante-->
									<div class="row">
										<div class="table-responsive">
											<table class="table table-bordered table-condensed table-hover table-striped">
												<thead>
													<tr>
														<th>Validado</th>
														<th>Estatus</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>

														<?php
															if($getDocumentoByReference[0]['validate'] == 1){
																$validate = 1;
																$iconValidate      = "ok";
																$iconColorValidate = "#449d44";
																$checkedValidate   = "checked";
															}else{
																$validate = 0;
																$iconValidate      = "remove";
																$iconColorValidate = "#FF0000";
																$checkedValidate   = "";
															}
														?>

														<td>
															<div class="checkbox">
																<label>
																	<input id="validateAspirant" type="checkbox"
																		value="<?php echo $validate ?>"
																		data-idAspirant="<?php echo $getDocumentoByReference[0]['id_aspirant'] ?>"
																		<?php echo $checkedValidate;?>
																	>
																	Validar
																</label>
															</div>
														</td>

														<td>
															<span id="statusValidate" class="glyphicon glyphicon-<?php echo $iconValidate ?>" aria-hidden="true" style="color:<?php echo $iconColorValidate?>" ></span>
														</td>

														<td>
															<a class="btn btn-primary" data-toggle="modal" href='#modalValidacion'>
																Enviar Validación
															</a>

															<div class="modal fade" id="modalValidacion">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																			<h4 class="modal-title">Validación de documentos</h4>
																		</div>
																		<div class="modal-body">
																			Enviar correo de valiadación de documentos al aspirante?
																		</div>
																		<div class="modal-footer">
																			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
																			<button id="btnValidacion" type="button" class="btn btn-primary"
																				data-name="<?php echo $nombre?>"
																				data-email="<?php echo $email?>"
																				data-plan="<?php echo $plan?>"
																				data-idAspirant="<?php echo $id_aspirant?>"
																				data-Admin="<?php echo $_SESSION['id_user']?>"
																				>
																				Aceptar
																			</button>
																		</div>
																	</div>
																</div>
															</div>

															<div class="modal fade" id="modaUpdatedDoc">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																			<h4 class="modal-title">Documento Actualizado</h4>
																		</div>
																		<div class="modal-body">
																			Documento Actualizado Correctamente
																		</div>
																		<div class="modal-footer">
																			<button id="btnValidacion" type="button" class="btn btn-primary" data-dismiss="modal" onclick="Recarga()">
																				Aceptar
																			</button>
																		</div>
																	</div>
																</div>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<?php
								}
								?>

								<!--Mostrar Documentos 1-->
								<?php
									if(count($getDocumentoByReferenceObligato1) >= 1){
								?>
									<div class="row">
										<h3>Documentos Obligatorios</h3>
										<div class="table-responsive">
											<table class="table table-bordered table-condensed table-hover table-striped">
												<thead>
													<tr>
														<th>No</th>
														<th>Documento</th>
														<th>Ver</th>
														<th>Guardado</th>
														<th>Estatus Digital</th>
														<th>Estatus Fisico</th>
														<th>Validar Digital</th>
														<th>Validar Fisico</th>
														<th>Cambiar Tipo Documento</th>
													</tr>
												</thead>
												<tbody>

													<?php
														for($i=0; $i< count($getDocumentoByReferenceObligato1); $i++ ){

															if($getDocumentoByReferenceObligato1[$i]['validated_digital_document'] == 1){
																$iconDigital      = "ok";
																$iconColorDigital = "#449d44";
																$checkedDigital   = "checked";
															}else{
																$iconDigital      = "remove";
																$iconColorDigital = "#FF0000";
																$checkedDigital   = "";
															}

															if($getDocumentoByReferenceObligato1[$i]['validated_physical_document'] == 1){
																$iconPhysical      = "ok";
																$iconColorPhysical = "#449d44";
																$checkedPhysical   = "checked";
															}else{
																$iconPhysical      = "remove";
																$iconColorPhysical = "#FF0000";
																$checkedPhysical   = "";
															}


															?>
															<tr>
																<!--NO-->
																<td><?php echo $i + 1 ?></td>
																<!--nombre documento-->
																<td><?php echo $getDocumentoByReferenceObligato1[$i]['document']; ?></td>
																<!--btn ver-->
																<td>
																	<button class="btn btn-primary" id="viewDocument" data-path="<?php echo $getDocumentoByReferenceObligato1[$i]['document_path'] ?>">
																		<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
																	</button>
																</td>
																<!--ok guardado-->
																<td class="text-center">
																	<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#449d44"></span>
																</td>
																<!--status digital-->
																<td class="text-center">
																	<span id="statusDigital" class="glyphicon glyphicon-<?php echo $iconDigital?>" aria-hidden="true" style="color:<?php echo $iconColorDigital?>"></span>
																</td>
																<!--status fisico-->
																<td class="text-center">
																	<span id="statusPhysical" class="glyphicon glyphicon-<?php echo $iconPhysical?>" aria-hidden="true" style="color:<?php echo $iconColorPhysical?>"></span>
																</td>

																<!--validar digital-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input id="checkedDigital" type="checkbox"
																				value="<?php echo $getDocumentoByReferenceObligato1[$i]['validated_digital_document']?>"
																				data-idDocument="<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>"
																				data-typeDocument="<?php echo $getDocumentoByReferenceObligato1[$i]['id_type_document'] ?>"
																				<?php echo $checkedDigital;?>
																			>
																		</label>
																	</div>
																</td>

																<!--validar fisico-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input id="checkedPhysical" type="checkbox"
																				value="<?php echo $getDocumentoByReferenceObligato1[$i]['validated_physical_document']?>"
																				data-idDocument="<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>"
																				data-typeDocument="<?php echo $getDocumentoByReferenceObligato1[$i]['id_type_document'] ?>"
																				<?php echo $checkedPhysical;?>
																			>
																		</label>
																	</div>
																</td>

																<!--cambiar tipo documento-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" id="changeDocumentCheck-<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>"
																				value="<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>"
																				class="changeDocumentCheck"
																				>
																		</label>
																	</div>


																	<div class="form-group">
																		<select name="changeList"
																			id="changeList-<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>"
																			class="form-control update_tipo_documento"
																			required="required"
																			style="display:none;">

																			<option value="x" selected disabled>
																				Selecione tipo de documento
																			</option>
																			<?php
																			for($j = 0; $j <count($getTypeDocuments); $j++){
																				?>
																				<option value="<?php echo $getTypeDocuments[$j]['id_type_document'] ?>">
																					<?php echo $getTypeDocuments[$j]['document'];?>
																				</option>
																				<?php
																			}
																			?>
																		</select>
																	</div>

																	<div class="form-group">
																		<div class="col-sm-4 col-sm-offset-2">
																			<button type="submit" id="changeDocumentBoton-<?php echo $getDocumentoByReferenceObligato1[$i]['id_document_aspirant'] ?>" class="btn btn-primary" style="display:none;" >Actualizar</button>
																		</div>
																	</div>

																</td>



															</tr>
															<?php
														}

													?>
												</tbody>
											</table>
										</div>
									</div>
								<?php
									}
								?>

								<!--Mostrar Documentos 2-->
								<?php
									if(count($getDocumentoByReferenceObligato2) >= 1){
								?>
									<div class="row">
										<h3>Documentos de Postulación</h3>
										<div class="table-responsive">
											<table class="table table-bordered table-condensed table-hover table-striped">
												<thead>
													<tr>
														<th>No</th>
														<th>Documento</th>
														<th>Ver</th>
														<th>Guardado</th>
														<th>Estatus Digital</th>
														<th>Estatus Fisico</th>
														<th>Validar Digital</th>
														<th>Validar Fisico</th>
														<th>Cambiar Tipo Documento</th>
													</tr>
												</thead>
												<tbody>

													<?php
														for($i=0; $i< count($getDocumentoByReferenceObligato2); $i++ ){

															if($getDocumentoByReferenceObligato2[$i]['validated_digital_document'] == 1){
																$iconDigital      = "ok";
																$iconColorDigital = "#449d44";
																$checkedDigital   = "checked";
															}else{
																$iconDigital      = "remove";
																$iconColorDigital = "#FF0000";
																$checkedDigital   = "";
															}

															if($getDocumentoByReferenceObligato2[$i]['validated_physical_document'] == 1){
																$iconPhysical      = "ok";
																$iconColorPhysical = "#449d44";
																$checkedPhysical   = "checked";
															}else{
																$iconPhysical      = "remove";
																$iconColorPhysical = "#FF0000";
																$checkedPhysical   = "";
															}


															?>
															<tr>
																<!--NO-->
																<td><?php echo $i + 1 ?></td>
																<!--nombre documento-->
																<td><?php echo $getDocumentoByReferenceObligato2[$i]['document']; ?></td>
																<!--btn ver-->
																<td>
																	<button class="btn btn-primary" id="viewDocument" data-path="<?php echo $getDocumentoByReferenceObligato2[$i]['document_path'] ?>">
																		<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
																	</button>
																</td>
																<!--ok guardado-->
																<td class="text-center">
																	<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#449d44"></span>
																</td>
																<!--status digital-->
																<td class="text-center">
																	<span id="statusDigital" class="glyphicon glyphicon-<?php echo $iconDigital?>" aria-hidden="true" style="color:<?php echo $iconColorDigital?>"></span>
																</td>
																<!--status fisico-->
																<td class="text-center">
																	<span id="statusPhysical" class="glyphicon glyphicon-<?php echo $iconPhysical?>" aria-hidden="true" style="color:<?php echo $iconColorPhysical?>"></span>
																</td>

																<!--validar digital-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input id="checkedDigital" type="checkbox"
																				value="<?php echo $getDocumentoByReferenceObligato2[$i]['validated_digital_document']?>"
																				data-idDocument="<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>"
																				data-typeDocument="<?php echo $getDocumentoByReferenceObligato2[$i]['id_type_document'] ?>"
																				<?php echo $checkedDigital;?>
																			>
																		</label>
																	</div>
																</td>

																<!--validar fisico-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input id="checkedPhysical" type="checkbox"
																				value="<?php echo $getDocumentoByReferenceObligato2[$i]['validated_physical_document']?>"
																				data-idDocument="<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>"
																				data-typeDocument="<?php echo $getDocumentoByReferenceObligato2[$i]['id_type_document'] ?>"
																				<?php echo $checkedPhysical;?>
																			>
																		</label>
																	</div>
																</td>

																<!--cambiar tipo documento-->
																<td class="text-center">
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" id="changeDocumentCheck-<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>"
																				value="<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>"
																				class="changeDocumentCheck"
																				>
																		</label>
																	</div>


																	<div class="form-group">
																		<select name="changeList"
																			id="changeList-<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>"
																			class="form-control update_tipo_documento"
																			required="required"
																			style="display:none;">

																			<option value="x" selected disabled>
																				Selecione tipo de documento
																			</option>
																			<?php
																			for($j = 0; $j <count($getTypeDocuments); $j++){
																				?>
																				<option value="<?php echo $getTypeDocuments[$j]['id_type_document'] ?>">
																					<?php echo $getTypeDocuments[$j]['document'];?>
																				</option>
																				<?php
																			}
																			?>
																		</select>
																	</div>

																	<div class="form-group">
																		<div class="col-sm-4 col-sm-offset-2">
																			<button type="submit" id="changeDocumentBoton-<?php echo $getDocumentoByReferenceObligato2[$i]['id_document_aspirant'] ?>" class="btn btn-primary" style="display:none;" >Actualizar</button>
																		</div>
																	</div>

																</td>



															</tr>
															<?php
														}

													?>
												</tbody>
											</table>
										</div>
									</div>
								<?php
									}
								?>


								<!--Mostrar Documentos 3-->
								<?php
									if(count($getDocumentoByReferenceObligato3) >= 1){
								?>
								<div class="row">

									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-hover table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Documento</th>
													<th>Ver</th>
													<th>Guardado</th>
													<th>Estatus Digital</th>
													<th>Estatus Fisico</th>
													<th>Validar Digital</th>
													<th>Validar Fisico</th>
													<th>Cambiar Tipo Documento</th>
												</tr>
											</thead>
											<tbody>

												<?php
													for($i=0; $i< count($getDocumentoByReferenceObligato3); $i++ ){

														if($getDocumentoByReferenceObligato3[$i]['validated_digital_document'] == 1){
															$iconDigital      = "ok";
															$iconColorDigital = "#449d44";
															$checkedDigital   = "checked";
														}else{
															$iconDigital      = "remove";
															$iconColorDigital = "#FF0000";
															$checkedDigital   = "";
														}

														if($getDocumentoByReferenceObligato3[$i]['validated_physical_document'] == 1){
															$iconPhysical      = "ok";
															$iconColorPhysical = "#449d44";
															$checkedPhysical   = "checked";
														}else{
															$iconPhysical      = "remove";
															$iconColorPhysical = "#FF0000";
															$checkedPhysical   = "";
														}


														?>
														<tr>
															<!--NO-->
															<td><?php echo $i + 1 ?></td>
															<!--nombre documento-->
															<td><?php echo $getDocumentoByReferenceObligato3[$i]['document']; ?></td>
															<!--btn ver-->
															<td>
																<button class="btn btn-primary" id="viewDocument" data-path="<?php echo $getDocumentoByReferenceObligato3[$i]['document_path'] ?>">
																	<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
																</button>
															</td>
															<!--ok guardado-->
															<td class="text-center">
																<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#449d44"></span>
															</td>
															<!--status digital-->
															<td class="text-center">
																<span id="statusDigital" class="glyphicon glyphicon-<?php echo $iconDigital?>" aria-hidden="true" style="color:<?php echo $iconColorDigital?>"></span>
															</td>
															<!--status fisico-->
															<td class="text-center">
																<span id="statusPhysical" class="glyphicon glyphicon-<?php echo $iconPhysical?>" aria-hidden="true" style="color:<?php echo $iconColorPhysical?>"></span>
															</td>

															<!--validar digital-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input id="checkedDigital" type="checkbox"
																			value="<?php echo $getDocumentoByReferenceObligato3[$i]['validated_digital_document']?>"
																			data-idDocument="<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>"
																			data-typeDocument="<?php echo $getDocumentoByReferenceObligato3[$i]['id_type_document'] ?>"
																			<?php echo $checkedDigital;?>
																		>
																	</label>
																</div>


																<?php
																		if ($getDocumentoByReferenceObligato3[$i]['id_type_document'] == 4) {

																			$prom = ($getDocumentoByReferenceObligato3[$i]['average'] == null) ? "" : $getDocumentoByReferenceObligato3[$i]['average'];
																			$fol = ($getDocumentoByReferenceObligato3[$i]['certificate_sheet'] == null) ? "" : $getDocumentoByReferenceObligato3[$i]['certificate_sheet'];
																			
																			?>
																			<div class="col-sm-8" id="savePromCertLic" " >
																				<form action="" method="POST" class="form-horizontal" role="form">

																					<div class="form-group">
																						<label class="sr-only" for="">label</label>
																						<input type="text" class="form-control" id="promCertLic" name="promCert" placeholder="promedio Licenciatura" value="<?php echo $prom ?>">
																					</div>

																					<div class="form-group">
																						<label class="sr-only" for="">label</label>
																						<input type="text" class="form-control" id="folioCertLic" name="folioCert" placeholder="folio" value="<?php echo $fol ?>">
																						<input type="hidden" class="form-control" id="referenceLic" name="reference" value="<?php echo $_POST['reference'] ?>" placeholder="reference">
																						<input type="hidden" class="form-control" id="idDocumentLic" name="idDocument" value="<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>" placeholder="reference">

																					</div>

																					<div class="form-group">
																						<div class="col-sm-10 col-sm-offset-2">
																							<button class="btn btn-primary" id='btnPromedio'>Guardar</button>
																						</div>
																					</div>
																				</form>
																			</div>
																		<?php
																		}

																		//capturar comprobate


																		if ($getDocumentoByReferenceObligato3[$i]['id_type_document'] == 6) {

																			$prom = ($getDocumentoByReferenceObligato3[$i]['average'] == null) ? "" : $getDocumentoByReferenceObligato3[$i]['average'];
																			$fol = ($getDocumentoByReferenceObligato3[$i]['certificate_sheet'] == null) ? "" : $getDocumentoByReferenceObligato3[$i]['certificate_sheet'];
																			
																			?>
																			<div class="col-sm-8" id="saveConsProm" " >
																				<form action="" method="POST" class="form-horizontal" role="form">

																					<div class="form-group">
																						<label class="sr-only" for="">label</label>
																						<input type="text" class="form-control" id="ConsProm" name="promCert" placeholder="Constancia de promedio" value="<?php echo $prom ?>">
																					</div>

																					<div class="form-group">
																						<label class="sr-only" for="">label</label>
																						<input type="text" class="form-control" id="folioConsProm" name="folioConsProm" placeholder="folio" value="<?php echo $fol ?>">
																						<input type="hidden" class="form-control" id="referenceConsProm" name="referenceConsProm" value="<?php echo $_POST['reference'] ?>" placeholder="reference">
																						<input type="hidden" class="form-control" id="idDocumentConsProm" name="idDocument" value="<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>" placeholder="reference">

																					</div>

																					<div class="form-group">
																						<div class="col-sm-10 col-sm-offset-2">
																							<button class="btn btn-primary" id='btnConsProm'>Guardar</button>
																						</div>
																					</div>
																				</form>
																			</div>
																		<?php
																		}

																?>
															</td>

															<!--validar fisico-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input id="checkedPhysical" type="checkbox"
																			value="<?php echo $getDocumentoByReferenceObligato3[$i]['validated_physical_document']?>"
																			data-idDocument="<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>"
																			data-typeDocument="<?php echo $getDocumentoByReferenceObligato3[$i]['id_type_document'] ?>"
																			<?php echo $checkedPhysical;?>
																		>
																	</label>
																</div>
															</td>

															<!--cambiar tipo documento-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input type="checkbox" id="changeDocumentCheck-<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>"
																			value="<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>"
																			class="changeDocumentCheck"
																			>
																	</label>
																</div>


																<div class="form-group">
																	<select name="changeList"
																		id="changeList-<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>"
																		class="form-control update_tipo_documento"
																		required="required"
																		style="display:none;">

																		<option value="x" selected disabled>
																			Selecione tipo de documento
																		</option>
																		<?php
																		for($j = 0; $j <count($getTypeDocuments); $j++){
																			?>
																			<option value="<?php echo $getTypeDocuments[$j]['id_type_document'] ?>">
																				<?php echo $getTypeDocuments[$j]['document'];?>
																			</option>
																			<?php
																		}
																		?>
																	</select>
																</div>

																<div class="form-group">
																	<div class="col-sm-4 col-sm-offset-2">
																		<button type="submit" id="changeDocumentBoton-<?php echo $getDocumentoByReferenceObligato3[$i]['id_document_aspirant'] ?>" class="btn btn-primary" style="display:none;" >Actualizar</button>
																	</div>
																</div>

															</td>



														</tr>
														<?php
													}

												?>
											</tbody>
										</table>
									</div>
								</div>
								<?php
									}
								?>

								<!--Mostrar Documentos opcionales-->
								<?php
									if(count($getDocumentoByReferenceOpcional) >= 1){
								?>
								<div class="row">
									<h3>Documentos Académicos (Opcionales)</h3>
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-hover table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Documento</th>
													<th>Ver</th>
													<th>Guardado</th>
													<th>Estatus Digital</th>
													<th>Estatus Fisico</th>
													<th>Validar Digital</th>
													<th>Validar Fisico</th>
													<th>Cambiar Tipo Documento</th>
												</tr>
											</thead>
											<tbody>

												<?php
													for($i=0; $i< count($getDocumentoByReferenceOpcional); $i++ ){

														if($getDocumentoByReferenceOpcional[$i]['validated_digital_document'] == 1){
															$iconDigital      = "ok";
															$iconColorDigital = "#449d44";
															$checkedDigital   = "checked";
														}else{
															$iconDigital      = "remove";
															$iconColorDigital = "#FF0000";
															$checkedDigital   = "";
														}

														if($getDocumentoByReferenceOpcional[$i]['validated_physical_document'] == 1){
															$iconPhysical      = "ok";
															$iconColorPhysical = "#449d44";
															$checkedPhysical   = "checked";
														}else{
															$iconPhysical      = "remove";
															$iconColorPhysical = "#FF0000";
															$checkedPhysical   = "";
														}


														?>
														<tr>
															<!--NO-->
															<td><?php echo $i + 1 ?></td>
															<!--nombre documento-->
															<td><?php echo $getDocumentoByReferenceOpcional[$i]['document']; ?></td>
															<!--btn ver-->
															<td>
																<button class="btn btn-primary" id="viewDocument" data-path="<?php echo $getDocumentoByReferenceOpcional[$i]['document_path'] ?>">
																	<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
																</button>
															</td>
															<!--ok guardado-->
															<td class="text-center">
																<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#449d44"></span>
															</td>
															<!--status digital-->
															<td class="text-center">
																<span id="statusDigital" class="glyphicon glyphicon-<?php echo $iconDigital?>" aria-hidden="true" style="color:<?php echo $iconColorDigital?>"></span>
															</td>
															<!--status fisico-->
															<td class="text-center">
																<span id="statusPhysical" class="glyphicon glyphicon-<?php echo $iconPhysical?>" aria-hidden="true" style="color:<?php echo $iconColorPhysical?>"></span>
															</td>

															<!--validar digital-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input id="checkedDigital" type="checkbox"
																			value="<?php echo $getDocumentoByReferenceOpcional[$i]['validated_digital_document']?>"
																			data-idDocument="<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>"
																			data-typeDocument="<?php echo $getDocumentoByReferenceOpcional[$i]['id_type_document'] ?>"
																			<?php echo $checkedDigital;?>
																		>
																	</label>
																</div>
															</td>

															<!--validar fisico-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input id="checkedPhysical" type="checkbox"
																			value="<?php echo $getDocumentoByReferenceOpcional[$i]['validated_physical_document']?>"
																			data-idDocument="<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>"
																			data-typeDocument="<?php echo $getDocumentoByReferenceOpcional[$i]['id_type_document'] ?>"
																			<?php echo $checkedPhysical;?>
																		>
																	</label>
																</div>
															</td>

															<!--cambiar tipo documento-->
															<td class="text-center">
																<div class="checkbox">
																	<label>
																		<input type="checkbox" id="changeDocumentCheck-<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>"
																			value="<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>"
																			class="changeDocumentCheck"
																			>
																	</label>
																</div>


																<div class="form-group">
																	<select name="changeList"
																		id="changeList-<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>"
																		class="form-control update_tipo_documento"
																		required="required"
																		style="display:none;">

																		<option value="x" selected disabled>
																			Selecione tipo de documento
																		</option>
																		<?php
																		for($j = 0; $j <count($getTypeDocuments); $j++){
																			?>
																			<option value="<?php echo $getTypeDocuments[$j]['id_type_document'] ?>">
																				<?php echo $getTypeDocuments[$j]['document'];?>
																			</option>
																			<?php
																		}
																		?>
																	</select>
																</div>

																<div class="form-group">
																	<div class="col-sm-4 col-sm-offset-2">
																		<button type="submit" id="changeDocumentBoton-<?php echo $getDocumentoByReferenceOpcional[$i]['id_document_aspirant'] ?>" class="btn btn-primary" style="display:none;" >Actualizar</button>
																	</div>
																</div>

															</td>



														</tr>
														<?php
													}

												?>
											</tbody>
										</table>
									</div>
								</div>
								<?php
									}
								?>



								<!--mostrar  para mandar mensajes-->
								<div class="row">
									<div class="col-sm-offset-1">
										<legend>Enviar Correo a: <?php echo $email; ?></legend>
										<textarea id="message" cols="90" rows="10"></textarea>

										<button type="button" class="btn btn-primary" id="btnSendMail"
											data-name="<?php echo $nombre?>"
											data-email="<?php echo $email?>"
											data-plan="<?php echo $plan?>"
											data-idAspirant="<?php echo $id_aspirant?>"
											data-Admin="<?php echo $_SESSION['id_user']?>"
											>
											Enviar
											<span class="glyphicon glyphicon-send" aria-hidden="true"></span>
										</button>
									</div>
								</div>

								<div class="row">
									<div id="erroMail"></div>
								</div>


								<?php
							}else{
								?>
								<h2><?php echo $nombre = $name." ".$first_surname." ".$second_surname; ?> sin Documentos</h2>


								<!--mostrar  para mandar mensajes-->
								<div class="row">
									<div class="col-sm-offset-1">
										<legend>Enviar Correo a: <?php echo $email; ?></legend>
										<textarea id="message" cols="90" rows="10"></textarea>

										<button type="button" class="btn btn-primary" id="btnSendMail"
											data-name="<?php echo $nombre?>"
											data-email="<?php echo $email?>"
											data-plan="<?php echo $plan?>"
											data-idAspirant="<?php echo $id_aspirant?>"
											data-Admin="<?php echo $_SESSION['id_user']?>"
											>
											Enviar
											<span class="glyphicon glyphicon-send" aria-hidden="true"></span>
										</button>
									</div>
								</div>

								<div class="row">
									<div id="erroMail"></div>
								</div>


								<?php
							}
							?>
						</div>
						<div class="col-xs-5" id="pdf"></div>
					</div>
				</div>

	    </main>


      <script src="../../../assets/js/bootstrap.min.js"></script>
	  </body>



    <?php require_once 'footer.php' ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../assets/js/bootstrap.min.js"></script>

     <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
		<script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- barra de navegación-->
    <script type="text/javascipt" src="../../../assets/js/navbar.js"></script>
    <!-- /Sección: Scripts -->

    <script src="../../../assets/js/jquery-latest.js"></script>
    <script src="../../../assets/js/jquery.tablesorter.js"></script>
    <script>
      $("#tableAspirants").tablesorter();
    </script>
  </body>
	<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>
