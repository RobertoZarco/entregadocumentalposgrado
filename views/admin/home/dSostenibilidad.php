<?php
require_once("../../../controllers/admin/dSostenibilidad_controller.php");
/*d*/

if(isset($_SESSION['login'])){

$snUltimaCol=0;

if(isset($_POST) and @$_POST['report'] == 'si'){
  $nombre = "Aspirantes_Sostenibilidad_Doctorado_".date("d-m-Y").".xls";
  header("Content-type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=$nombre");
  $snUltimaCol=1;
}else{
  require_once "header.php";
  ?>



  <body id="inicio">

    <main id="maincontent" role="main" class="clearfix" height="1000px">

        <div class="row">
          <div class="col-xs-12">
            <?php
            require 'navbar.php';
            ?>
          </div>
        </div>

  <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 id="Bienvenido">Bienvenid@ <?php echo $_SESSION['name'] ." ".$_SESSION['first_surname']." ".$_SESSION['second_surname']; ?></h2>
        </div>
      </div>
       <div class="row">
            <form action="" method="POST" class="form-horizontal" role="form">
              <div class="form-group">
                <legend>Descargar Listado a Excel</legend>
              </div>

              <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                  <input type="hidden" value="si" name="report">
                  <button type="submit" class="btn btn-primary">
                    Descargar
                    <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                  </button>
                </div>
              </div>
            </form>
          </div>
<?php }  ?>

      <div class="row">
        <div class="col-sm-12">
          <?php
            //Mostrar todos los aspirantes para admin local de registro
            if(count($getDSostenibilidad) >= 1){
              ?>
              <div class="table-responsive">
                <table id="tableAspirants" class="table table-bordered table-condensed table-hover table-striped" style="color:black;" id="example" >
                  <thead>
                    <tr>
                      <th>
                        No <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Numero Cuenta <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Nombre <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Plan <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Documentos <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th> 
                      <th>
                        Acceso <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                      <th>
                        Validado <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                    <?php if($snUltimaCol!=1){ ?>
                      <th>
                        Visualizar <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                      </th>
                    <?php }  ?>

                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    for($i = 0; $i < count($getDSostenibilidad); $i++){
                      ?>
                      <tr>
                        <td><?php echo $i +1; ?></td>
                        <td><?php echo $getDSostenibilidad[$i]['account_number'] ?></td>
                        <td>

                          <?php
                            if(isset($_POST) and @$_POST['report'] == 'si'){
                              echo $nombre = utf8_decode($getDSostenibilidad[$i]['first_surname'] ." " .$getDSostenibilidad[$i]['second_surname'] ." ". $getDSostenibilidad[$i]['name']);
                            }else{
                              echo $nombre = $getDSostenibilidad[$i]['first_surname'] ." " .$getDSostenibilidad[$i]['second_surname'] ." ". $getDSostenibilidad[$i]['name'];
                            }
                          ?>
                          
                          </td>
                        <td>
                          <?php
                            if(isset($_POST) and @$_POST['report'] == 'si'){
                              echo utf8_decode($getDSostenibilidad[$i]['plan']);
                            }else{
                              echo $getDSostenibilidad[$i]['plan'];
                            }
                          ?>
                        </td>

                        <td class="text-center">
                          <?php
                            $countDocs = $Admin -> getCountDocsAspi($getDSostenibilidad[$i]['account_number']);

                            if($countDocs > 0){
                              ?>
                              <span><?php echo $countDocs ?></span>
                              <?php
                              }
                          ?>
                        </td>


                        <?php
                          if($getDSostenibilidad[$i]['welcome'] == 1){
                            ?>
                            <td class="text-center">
                              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                              <input type="hidden" value="1" />
                             <?php  if($snUltimaCol==1){
                                echo "Ok";
                              } ?>
                            </td>
                            <?php
                          }else{
                            ?>
                            <td class="text-center">

                            </td>
                            <?php
                          }
                        ?>


                        <?php
                          if($getDSostenibilidad[$i]['validate'] == 1){
                            ?>
                            <td class="text-center">
                              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                              <input type="hidden" value="1" />
                             <?php  if($snUltimaCol==1){
                                echo "Ok";
                              } ?>
                            </td>
                            <?php
                          }else{
                            ?>
                            <td class="text-center">

                            </td>
                            <?php
                          }
                        ?>
                        <?php
                          if($_SESSION['view_digital_document'] == 1  && $snUltimaCol!=1){
                            ?>
                            <td class="text-center">
                              <form action="viewAspirant.php" method="POST" class="form-inline" role="form">
                                <input type="hidden" name="account_number" value="<?php echo $getDSostenibilidad[$i]['account_number']?>">
                                <input type="hidden" name="id_aspirant"    value="<?php echo $getDSostenibilidad[$i]['id_aspirant'] ?>">
                                <input type="hidden" name="name"           value="<?php echo $getDSostenibilidad[$i]['name'] ?>">
                                <input type="hidden" name="first_surname"  value="<?php echo $getDSostenibilidad[$i]['first_surname']?>">
                                <input type="hidden" name="second_surname" value="<?php echo $getDSostenibilidad[$i]['second_surname']?>">
                                <input type="hidden" name="email"          value="<?php echo $getDSostenibilidad[$i]['email']?>">
                                <input type="hidden" name="validate"       value="<?php echo $getDSostenibilidad[$i]['validate']?>">
                                <input type="hidden" name="plan"           value="<?php echo $getDSostenibilidad[$i]['plan']?>">

                                <input type="hidden" name="searchAspirant" value="si">

                                <button type="submit" class="btn btn-primary">
                                  <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                </button>
                              </form>
                            </td>
                            <?php
                          }
                        ?>

                        <?php
                          if($_SESSION['edit_aspirant'] == 1){
                            ?><!--
                            <td class="text-center">
                              <button  type="button" id="btnEditUser" value="<?php echo $getDSostenibilidad[$i]['id_aspirant']?>" class="glyphicon glyphicon-pencil" style="color:#337ab7"></button>
                            </td>-->
                            <?php
                          }
                        ?>




                      </tr>

                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <?php
            }else{
              ?>
              <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>No existen aspirantes</strong>
              </div>
              <?php
            }
          ?>

      </div>
    </div>
    </main>

    <?php
      if(!isset($_POST) and @$_POST['report'] == 'si'){
         require_once 'footer.php';
      }
    ?>



    <?php require_once 'footer.php' ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../assets/js/bootstrap.min.js"></script>

     <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->

    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- barra de navegación-->
    <script type="text/javascipt" src="../../../assets/js/navbar.js"></script>
    <!-- /Sección: Scripts -->

    <script src="../../../assets/js/jquery-latest.js"></script>
    <script src="../../../assets/js/jquery.tablesorter.js"></script>
    <script>
      $("#tableAspirants").tablesorter();
    </script>
  </body>

  </html>

<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>