<?php
require_once '../../../../controllers/admin/reports_controllers.php';

$nombre = "Aspirantes_con_documentos_".date("d-m-Y").".xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$nombre");


	function cambia_tildes($dato) {
		//nombre original de la function latin9_to_utf8
		//replaces utf8_encode()
		$trans = array("�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�");
		$wrong_utf8str = utf8_encode($dato);
		$utf8str = strtr($wrong_utf8str, $trans);
		return $utf8str;
	}

	function invierte_tildes($dato){
		//nomnbre original utf8_to_latin9
    	$trans = array("�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�", "�"=>"�");
    	$wrong_utf8str = strtr($dato, $trans);
    	$latin9str = utf8_decode($wrong_utf8str);
    	return $latin9str;
	}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aspirantes con documentos</title>

  </head>
  <body>


  <div class="container">
	  <div class="row">
	  	<div class="col-sm-12">
	  		<table class="table table-hover">
	  			<thead>
	  				<tr>
	  					<th>No</th>
	  					<th><?php echo "Número cuenta" ?></th>
	  					<th>1er Apellido</th>
	  					<th>2do Apellido</th>
	  					<th>Nombre</th>
	  					<th>Documentos</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  				<?php
	  				for ($i = 0; $i < count($getFullDocumentsAspirants) ; $i++) {
	  					?>
		  				<tr>
		  					<td><?php echo $i+1; ?></td>
		  					<td><?php echo $getFullDocumentsAspirants[$i]['account_number']; ?></td>
		  					<td><?php echo $getFullDocumentsAspirants[$i]['first_surname']; ?></td>
		  					<td><?php echo $getFullDocumentsAspirants[$i]['second_surname']; ?></td>
		  					<td><?php echo $getFullDocumentsAspirants[$i]['name']; ?></td>
		  					<td><?php echo $getFullDocumentsAspirants[$i]['documents']; ?></td>
		  				</tr>
	  					<?php
	  				}

	  				?>
	  			</tbody>
	  		</table>
	  	</div>
	  </div>

	</div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../js/bootstrap.min.js"></script>
  </body>
</html>
