

<?php
require_once("../../../controllers/admin/home_controller.php");

if(isset($_SESSION['login'])){
  require_once "header.php";
  ?>

  <script src='../../../assets/js/app/admin/get_user_list.js'></script>
  <body id="inicio">

    <?php
    require_once "navbar.php";
    ?>

    <main id="maincontent" role="main" class="clearfix" height="1000px">

      <div class="row">
        <div class="col-sm-12">
          <h2 id="Bienvenido">Bienvenido <?php echo $_SESSION['login']; ?></h2>
          <h3>Lista de Usuarios Activos</h3>
        </div>
      </div>


      <div class="row">
        <div class="col-sm-6">
          <div id="errores_home"></div>
        </div>
      </div>


      <div class="row">
        <div class="col-sm-6">
          <?php
          if(count($getUsuariosAdmin) >= 1){
            ?>
            <div class="table-responsive">
              <table class="table table-condensed table-bordered table-hover table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nombre</th>
                    <th>Login</th>
                    <?php
                      if($_SESSION['edit_aspirant'] == 1){
                        ?>
                        <th>Editar</th>
                        <?php
                      }
                    ?>
                    <th>Eliminar</th>

                  </tr>
                </thead>
                <tbody>

                  <?php
                    if($_SESSION['login'] == 'esanta' or $_SESSION['login'] == 'rherrera'){
                      for($i = 0; $i < count($getUsuariosAdmin); $i++){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $getUsuariosAdmin[$i]['first_surname'] ." " .$getUsuariosAdmin[$i]['second_surname'] ." ". $getUsuariosAdmin[$i]['name'] ?></td>
                        <td><?php echo $getUsuariosAdmin[$i]['login'] ?></td>
                        <!--  Boton Editar User Admin -->

                        <?php
                          if($_SESSION['edit_aspirant'] == 1){
                            ?>
                            <td class="text-center">
                              <button  type="button" id="btnEditUserAdmin" value="<?php echo $getUsuariosAdmin[$i]['id_user']?>" class="glyphicon glyphicon-pencil" style="color:#337ab7"></button>
                            </td>
                            <?php
                          }
                        ?>
                        <!--td class="text-center"><a href="" id="btnTrahsUser" class="glyphicon glyphicon-trash"></a></td-->
                        <td class="text-center">
                          <button  type="button" id="btnTrahsUserAdmin" value="<?php echo $getUsuariosAdmin[$i]['id_user']?>" class="glyphicon glyphicon-trash" style="color:#337ab7"></button>
                        </td>
                      </tr>
                      <?php

                  }

                    }else{



                    $i2 = null;
                    for($i = 0; $i < count($getUsuariosAdmin); $i++){
                      if($getUsuariosAdmin[$i]['dependency'] == $_SESSION['cveDependencia']){
                        $i2 = $i2 +1;
                      ?>
                      <tr>
                        <td><?php echo $i2; ?></td>
                        <td><?php echo $getUsuariosAdmin[$i]['first_surname'] ." " .$getUsuariosAdmin[$i]['first_surname'] ." ". $getUsuariosAdmin[$i]['name'] ?></td>
                        <td><?php echo $getUsuariosAdmin[$i]['login'] ?></td>
                        <!--  Boton Editar User Admin -->

                        <?php
                          if($_SESSION['editarAspirante'] == 1){
                            ?>
                            <td class="text-center">
                              <button  type="button" id="btnEditUserAdmin" value="<?php echo $getUsuariosAdmin[$i]['id_user']?>" class="glyphicon glyphicon-pencil" style="color:#337ab7"></button>
                            </td>
                            <?php
                          }
                        ?>
                        <!--td class="text-center"><a href="" id="btnTrahsUser" class="glyphicon glyphicon-trash"></a></td-->
                        <td class="text-center">
                          <button  type="button" id="btnTrahsUserAdmin" value="<?php echo $getUsuariosAdmin[$i]['id_user']?>" class="glyphicon glyphicon-trash" style="color:#337ab7"></button>
                        </td>
                      </tr>
                      <?php
                    } //if
                  }
                }//else

                ?>
                </tbody>
              </table>
            </div>
            <?php
          }
          ?>
        </div>

        <div class="col-sm-4">
          <div id="pdf">
            <!--Aqui se crea el PDF-->
          </div>
        </div>


        <div class="col-sm-2">
           <?php //require_once '../../chat.php' ?>
        </div>

      </div>


    </main>


    <?php require_once 'footer.php' ?>
    <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- Analytics -->
    <script type="text/javascript" src="../../../assets/js/analytics.js"></script>
    <!-- barra de navegación-->
    <script type="text/javascript" src="../../../assets/js/navbar.js"></script>

    <!-- /Sección: Scripts -->
  </body>

  </html>

<?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>
