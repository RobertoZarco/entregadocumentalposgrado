<?php
require '../../../controllers/caseta/viewAspirant_controller.php';
require '../../../helpers/caseta/upload_docs_helper.php';
header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Origin: https://entregalocal.dgae.unam.mx');

header('Access-Control-Allow-Origin: https://entregalocal.dgae.unam.mx');
header('Access-Control-Allow-Methods: GET, POST, PUT');
header('Access-Control-Allow-Headers: Content-Type');


if (isset($_SESSION['login'])) {
	//print_r($_SESSION);
	require_once "header.php";
	?>
	<!-- <script src='../../../assets/js/app/admin/create_user.js'></script> -->
	<script src='../../../assets/js/app/caseta/viewAspirant.js'></script>

	<body id="inicio">

		<?php
		require_once "navbar.php";
		?>

		<main id="maincontent" role="main" class="clearfix" height="1000px">

			<?php

			$id_aspirant = $getAspirant[0]['id_aspirant'];
			$name = $getAspirant[0]['name'];
			$first_surname = $getAspirant[0]['first_surname'];
			$second_surname = $getAspirant[0]['second_surname'];
			$email = $getAspirant[0]['email'];
			$validate = $getAspirant[0]['validate'];
			$plan = $getAspirant[0]['plan_id_plan'];

			if ($validate == 1) {
				$validate = 1;
				$checkedValidate = "checked";
				$disabledValidate = "disabled";
			} else {
				$validate = 0;
				$checkedValidate = "";
				$disabledValidate = "";
			}

			$nombre = $name . " " . $first_surname . " " . $second_surname;
			?>

			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-7">

						<?php
						if ($NotFoundAspirant == false) {
							if (count($getDocumentoByReference) >= 1) {
								?>
								<div class="row">
									<h2>Documentos del aspirante: <?php echo $nombre ?></h2>
								</div>

								<!--Validar aspirante-->
								<div class="row">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-hover table-striped">
											<thead>
												<tr>
													<th>Validado</th>
													<th>Estatus</th>
												</tr>
											</thead>
											<tbody>
												<tr>

													<?php
													if ($getDocumentoByReference[0]['validate'] == 1) {
														$validate = 1;
														$iconValidate = "ok";
														$iconColorValidate = "#449d44";
														$checkedValidate = "checked";
													} else {
														$validate = 0;
														$iconValidate = "remove";
														$iconColorValidate = "#FF0000";
														$checkedValidate = "";
													}
													?>

													<td>
														<div class="checkbox">
															<label>
																<input id="validateAspirant" type="checkbox" disabled
																value="<?php echo $validate ?>"
																data-idAspirant="<?php echo $getDocumentoByReference[0]['id_aspirant'] ?>"
																<?php echo $checkedValidate; ?>
																>
																Validar
															</label>
														</div>
													</td>

													<td>
														<span id="statusValidate" class="glyphicon glyphicon-<?php echo $iconValidate ?>" aria-hidden="true" style="color:<?php echo $iconColorValidate ?>" ></span>
													</td>

												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<!-- Docs -->
								<div class="row">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed table-hover table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Documento</th>
													<th>Ver</th>
													<th>Guardado</th>
													<th>Estatus Digital</th>
													<th>Estatus Fisico</th>
													<th>Validar Fisico</th>
												</tr>
											</thead>
											<tbody>

												<?php
												for ($i = 0; $i < count($getDocumentoByReference); $i++) {

													if ($getDocumentoByReference[$i]['validated_digital_document'] == 1) {
														$iconDigital = "ok";
														$iconColorDigital = "#449d44";
														$checkedDigital = "checked";
													} else {
														$iconDigital = "remove";
														$iconColorDigital = "#FF0000";
														$checkedDigital = "";
													}

													if ($getDocumentoByReference[$i]['validated_physical_document'] == 1) {
														$iconPhysical = "ok";
														$iconColorPhysical = "#449d44";
														$checkedPhysical = "checked";
													} else {
														$iconPhysical = "remove";
														$iconColorPhysical = "#FF0000";
														$checkedPhysical = "";
													}

													?>
													<tr>
														<!--NO-->
														<td><?php echo $i + 1 ?></td>
														<!--nombre documento-->
														<td><?php echo $getDocumentoByReference[$i]['document']; ?></td>
														<!--btn ver-->
														<td>
															<button class="btn btn-primary" id="viewDocument" data-path="<?php echo $getDocumentoByReference[$i]['document_path'] ?>">
																<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
															</button>
														</td>
														<!--ok guardado-->
														<td class="text-center">
															<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#449d44"></span>
														</td>
														<!--status digital-->
														<td class="text-center">
															<span id="statusDigital" class="glyphicon glyphicon-<?php echo $iconDigital ?>" aria-hidden="true" style="color:<?php echo $iconColorDigital ?>"></span>
														</td>
														<!--status fisico-->
														<td class="text-center">
															<span id="statusPhysical-<?php echo $getDocumentoByReference[$i]['id_document_aspirant'] ?>" class="glyphicon glyphicon-<?php echo $iconPhysical ?>" aria-hidden="true" style="color:<?php echo $iconColorPhysical ?>"></span>
														</td>
														<!--validar fisico-->
														<td class="text-center">
															<div class="checkbox">
																<label>
																	<input id="checkedPhysical" type="checkbox"
																	value="<?php echo $getDocumentoByReference[$i]['validated_physical_document'] ?>"
																	data-idDocument="<?php echo $getDocumentoByReference[$i]['id_document_aspirant'] ?>"
																	data-typeDocument="<?php echo $getDocumentoByReference[$i]['id_type_document'] ?>"
																	<?php echo $checkedPhysical; ?>
																	>
																</label>
															</div>



															<?php
															if ($getDocumentoByReference[$i]['id_type_document'] == 4) {
																$prom = ($getDocumentoByReference[$i]['average'] == null) ? "" : $getDocumentoByReference[$i]['average'];
																$fol = ($getDocumentoByReference[$i]['certificate_sheet'] == null) ? "" : $getDocumentoByReference[$i]['certificate_sheet'];
																?>
																<div class="col-sm-8" id="savePromCertLic" style="display:none;" >
																	<form action="" method="POST" class="form-horizontal" role="form">

																		<div class="form-group">
																			<label class="sr-only" for="">label</label>
																			<input type="text" class="form-control" id="promCertLic" name="promCert" placeholder="promedio Licenciatura" value="<?php echo $prom ?>">
																		</div>

																		<div class="form-group">
																			<label class="sr-only" for="">label</label>
																			<input type="text" class="form-control" id="folioCertLic" name="folioCert" placeholder="folio" value="<?php echo $fol ?>">
																			<input type="hidden" class="form-control" id="referenceLic" name="reference" value="<?php echo $_POST['findReference'] ?>" placeholder="reference">
																			<input type="hidden" class="form-control" id="idDocumentLic" name="idDocument" value="<?php echo $getDocumentoByReference[$i]['id_document_aspirant'] ?>" placeholder="reference">

																		</div>

																		<div class="form-group">
																			<div class="col-sm-10 col-sm-offset-2">
																				<button class="btn btn-primary" id='btnSavePromCertLic'>Guardar</button>
																			</div>
																		</div>
																	</form>
																</div>
																<?php
															}

															if ($getDocumentoByReference[$i]['id_type_document'] == 11) {
																$prom = ($getDocumentoByReference[$i]['average'] == null) ? "" : $getDocumentoByReference[$i]['average'];
																$fol = ($getDocumentoByReference[$i]['certificate_sheet'] == null) ? "" : $getDocumentoByReference[$i]['certificate_sheet'];
																?>
																<div class="col-sm-8" id="savePromCertMaes" style="display:none;" >
																	<form action="" method="POST" class="form-horizontal" role="form">

																		<div class="form-group">
																			<label class="sr-only" for="">label</label>
																			<input type="text" class="form-control" id="promCertMaes" name="promCert" placeholder="promedio Mestria" value="<?php echo $prom ?>">
																		</div>

																		<div class="form-group">
																			<label class="sr-only" for="">label</label>
																			<input type="text" class="form-control" id="folioCertMaes" name="folioCert" placeholder="folio" value="<?php echo $fol ?>">
																			<input type="hidden" class="form-control" id="referenceMaes" name="reference" value="<?php echo $_POST['findReference'] ?>" placeholder="reference">
																			<input type="hidden" class="form-control" id="idDocumentMaes" name="idDocument" value="<?php echo $getDocumentoByReference[$i]['id_document_aspirant'] ?>" placeholder="reference">

																		</div>

																		<div class="form-group">
																			<div class="col-sm-10 col-sm-offset-2">
																				<button class="btn btn-primary" id='btnSavePromCertMaes'>Guardar</button>
																			</div>
																		</div>
																	</form>
																</div>
																<?php
															}

															?>


														</td>
													</tr>
													<?php
												}

												?>
												<div class="modal fade" id="modaUpdatedDoc">
													<div class="modal-dialog">
														<div class="modal-content">

															<div class="modal-body">
																Documento Actualizado Correctamente
															</div>
															<div class="modal-footer">
																<button id="btnValidacion" type="button" class="btn btn-primary" data-dismiss="modal">
																	Aceptar
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="modal fade" id="modaDelete">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-body">
																Documento Eliminado Correctamente
															</div>
															<div class="modal-footer">
																<button id="btnValidacion" type="button" class="btn btn-primary" data-dismiss="modal">
																	Aceptar
																</button>
															</div>
														</div>
													</div>
												</div>
											</tbody>
										</table>
									</div>
								</div>
								<!-- Docs Fin-->

								<!-- Docs Faltantes-->
								<?php
//print_r($result);
								if (count($result) > 0) {
									?>
									<div class="row">
										<h2>Documentos faltantes</h2>
									</div>

									<div class="row">

										<div class="table-responsive">
											<table class="table table-bordered table-condensed table-hover table-striped">
												<thead>
													<tr>
														<th>Documento faltante</th>
														<th class="text-center">Agregar</th>
													</tr>
												</thead>
												<tbody>
													<?php
													foreach ($result as $indice => $valor) {
														switch ($valor) {

															case 1:
															setFaltante("ACTA DE NACIMIENTO", $valor, $id_aspirant);
															break;

															case 2:
															setFaltante("CURP", $valor, $id_aspirant);
															break;

															case 3:
															setFaltante("TÍTULO LICENCIATURA", $valor, $id_aspirant);
															break;

															case 4:
															setFaltante("CERTIFICADO LICENCIATURA", $valor, $id_aspirant);
															break;

															case 5:
															setFaltante("IDENTIFICACIÓN OFICIAL", $valor, $id_aspirant);
															break;

															case 6:
															setFaltante("CONSTANCIA DE PROMEDIO", $valor, $id_aspirant);
															break;

															case 7:
															setFaltante("ACTA DE EXAMEN PROFESIONAL", $valor, $id_aspirant);
															break;

															case 8:
															setFaltante("CARTA DE REGITRO DE LA OPCIÓN DE TITULACIÓN", $valor, $id_aspirant);
															break;

															case 9:
															setFaltante("GRADO DE MAESTRÍA", $valor, $id_aspirant);
															break;

															case 10:
															setFaltante("CARTA DE REGISTRO DE LA MODALIDAD DE GRADUACIÓN POR ESTUDIOS DE DOCTORADO (EGRESADOS UNAM)", $valor, $id_aspirant);
															break;

															case 11:
															setFaltante("CERTIFICADO DE MAESTRÍA", $valor, $id_aspirant);
															break;

															case 12:
															setFaltante("ACTA DE EXAMEN DE GRADO", $valor, $id_aspirant);
															break;

															case 13:
															setFaltante("CONSTANCIA DE PROMEDIO MAESTRIA", $valor, $id_aspirant);
															break;

															case 14:
															setFaltante("EQUIVALENCIA DE PROMEDIO (DGIRE)", $valor, $id_aspirant);
															break;
														}
													}
													?>

												</tbody>
											</table>
										</div>
									</div>
									<?php
								}
								?>
								<!-- div label-->
								<div class="panel panel-success" tabindex="0">
									<div class="panel-heading">Panel con clase panel-success</div>
									<div class="panel-body">contenido</div>
								</div>
								<!-- div label-->

								<!-- Fin Docs Faltantes-->

								<?php $ncta = $_POST['findReference'];?>

								<!-- botonera para generar documentacion de ingreso -->
								<div class="table-responsive">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Documentación completa</th>
												<th>Carta Compromiso</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<form action="../../../controllers/EntregaDoc/entrega_controller.php" method="POST" class="form-inline" role="form" target="_blank">
														<div class="form-group">
															<label class="sr-only" for="">label</label>
															<button type="submit" class="btn btn-success">Documentación completa</button>
															<input type="hidden" class="form-control" id="ncta" name="ncta" value="<?php echo $ncta ?>">
															<input type="hidden" class="form-control" id="doc" name="doc" value="1">
														</div>
													</form>
												</td>

												<td>
													<form action="../../../controllers/EntregaDoc/entrega_controller.php" method="POST" class="form-inline" role="form" target="_blank">
														<div class="form-group">
															<label class="sr-only" for="">label</label>
															<button type="submit" class="btn btn-warning">Carta Compromiso</button>
															<input type="hidden" class="form-control" id="ncta" name="ncta" value="<?php echo $ncta ?>">
															<input type="hidden" class="form-control" id="doc" name="doc" value="2">
														</div>
													</form>
												</td>

											</tr>
										</tbody>
									</table>
									<?php //print_r($_POST) ?>
								</div>
								<!-- fin de botonera para generar documentacion de ingreso -->


								<?php
							} else {
								?>
								<h2><?php echo $nombre = $name . " " . $first_surname . " " . $second_surname; ?> sin Documentos</h2>
								<?php
							}
						} else {
							?>
							<h2>No se ncontró registro para <?php echo $_POST['findReference'] ?></h2>
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-default btn-warning" onclick="history.go(-1);">Regresar</button>
							</div>

							<?php
						}
						?>




					</div>

					<div class="col-xs-5" id="pdf"></div>
				</div>
			</div>

		</main>


		<script src="../../../assets/js/bootstrap.min.js"></script>
	</body>



	<?php require_once 'footer.php'?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="../../../assets/js/bootstrap.min.js"></script>

	<!-- Sección: Scripts -->
	<!-- jQuery -->
	<script type="text/javascript" src="../../../assets/js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>
	<!-- Material Design Bootstrap -->
	<!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
	<!-- barra de navegación-->
	<script type="text/javascipt" src="../../../assets/js/navbar.js"></script>
	<!-- /Sección: Scripts -->

	<script src="../../../assets/js/jquery-latest.js"></script>
	<script src="../../../assets/js/jquery.tablesorter.js"></script>
	<script>
		$("#tableAspirants").tablesorter();
	</script>
</body>
<?php
} else {
	?>
	<script>
		alert("Tienes que iniciar session antes"),
		window.location="../../../index.php";
	</script>
	<?php
}
?>
