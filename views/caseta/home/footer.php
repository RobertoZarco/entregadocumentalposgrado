  <footer class="main-footer">
    <div class="list-info">
      <div class="container">
        <div class="col-sm-3">
          <h5 tabindex="0">Contacto</h5>
          <p class="pmenor" tabindex="0">
            <i class="fa fa-phone"></i>
            &nbsp;Atención por Teléfono
            <br> 56226690
            <br> De 9:00 a 19:30 hrs.
          </p>
        </div>


      </div>
    </div>
    <div class="row" id="fondo">
      <div class="col-sm-12">
        <p class="pmenor" tabindex="0">
          Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2009 - 2014.
          <br>Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, requiere permiso previo por escrito de la institución
          <br>
          <br>Sitio web administrado por: Dirección General de Administración Escolar
        </p>
      </div>
    </div>
  </footer>


    <!-- Sección: Scripts -->
  <!-- jQuery -->
  <!-- <script type="text/javascript" src="../../../assets/js/jquery.js"></script> -->
  <!-- Bootstrap Core JavaScript -->
  <!-- <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script> -->
  <!-- Material Design Bootstrap -->
  <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
  <!-- Analytics -->
  <script type="text/javascript" src="../../../assets/js/analytics.js"></script>
  <!-- barra de navegación-->
  <script type="text/javascript" src="../../../assets/js/navbar.js"></script>

  <!-- /Sección: Scripts -->
</body>

</html>
