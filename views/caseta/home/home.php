<?php
require_once("../../../controllers/caseta/home_controller.php");

/**/
//echo $_SESSION['login'];
if(isset($_SESSION['login'])){

  require_once "header.php";
  ?>

  <body id="inicio">

    <?php
    require_once "navbar.php";
    ?>

    <style type="text/css">
      .main-footer {
        /*position: fixed;
        height:350px;
        bottom:0px;
        top:400px;
        left:0px;
        right:0px;
        margin-bottom:0px;*/

        position: absolute;
        bottom: 0px;
        margin-right: auto;
        margin-left: auto;
        left: 0px;
        right: 0px;
      }

    </style>

    <main id="maincontent" role="main" class="clearfix" height="1000px">

      <div class="row">
        <div class="col-sm-12">

          <div class="col-sm-offset-2 col-sm-4">
            <h2 id="Bienvenido">Bienvenid@ <?php echo $_SESSION['name'] ." ".$_SESSION['first_surname']." ".$_SESSION['second_surname']; ?></h2>

            <div class="col-sm-6">
              <form method="POST" action="viewAspirant.php" id="formFindAspirant">
                <div class="form-group">
                  <label for="formGroupExampleInput">Ingresa Referencia</label>
                  <input type="text" class="form-control" name="findReference" placeholder="Referencia">
                  <input type="hidden" name="findAspirant" value="si">
                </div>
                <button id="btnFindAspirant" type="submit" class="btn btn-primary">Buscar</button>
              </form>
            </div>

          </div>
        </div>
      </div>


    </main>


    <?php require_once 'footer.php' ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../../../assets/js/bootstrap.min.js"></script>

    <!-- Sección: Scripts -->
    <!-- jQuery -->
    <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../../../assets/js/bootstrap.js"></script>

    <!-- Material Design Bootstrap -->
    <!--<script type="text/javascript" src="../../../assets/js/mdb.js"></script>-->
    <!-- barra de navegación-->
    <script type="text/javascipt" src="../../../assets/js/navbar.js"></script>
    <!-- /Sección: Scripts -->

    <script src="../../../assets/js/jquery-latest.js"></script>
    <script src="../../../assets/js/jquery.tablesorter.js"></script>
  </body>

  </html>

  <?php
}else{
  ?>
  <script>
    alert("Tienes que iniciar session antes"),
    window.location="../../../index.php";
  </script>
  <?php
}
?>