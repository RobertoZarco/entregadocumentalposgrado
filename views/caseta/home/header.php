<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="UNAM, Direccion, General, Administracion, Escolar, Servicios, Escolares, Concursos, Ingreso, estudiantes, académicos, egresados, alumnos, publicacion, resultados, dgae, admisión, licenciatura,posgrado, maestría,bachillerato,educación,a,distancia,abierta">
  <meta name="description" content="UNAM, Direccion General de Administracion Escolar, Servicios Escolares, Concursos de Ingreso a la UNAM, Administracion Escolar">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#1C3D6C">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <!-- Sección: Title del Sitio -->
  <title>DGAE - UNAM</title>
  <!-- /Sección: Title del Sitio -->
  <!-- Sección: Links -->
  <link href="../../../assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="../../../assets/images/custom_icon.png" rel="apple-touch-icon">
  <link href="../../../assets/images/custom_icon.png" sizes="150x150" rel="icon">
  <!--<link href="../../../assets/css/login.css" rel="stylesheet">-->
  <link href="../../../assets/css/bootstrap.css" rel="stylesheet">
  <link href="../../../assets/css/responsive_parallax_navbar.css" rel="stylesheet">
  <link href="../../../assets/css/font-awesome.css" rel="stylesheet">
  <link href="../../../assets/css/estilo_dgae.css" rel="stylesheet">
  <script src='../../../assets/js/jquery.js'></script>

  <style type="text/css">
    .footer {
      position: relative;
      height:350px;
      bottom:0px;
      top:50px;
      left:0px;
      right:0px;
      margin-bottom:0px;
    }
  </style>

</head>