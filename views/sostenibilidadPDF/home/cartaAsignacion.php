<?php
require_once "../../../controllers/sostenibilidadPDF/sostenibilidad_pdf.php";

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">

		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="unam" src="../../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: center;">
				<h6><b>Carta de Asignación</b></h6>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsig" style="float: left; width: 20%;">
					<p>
						<br> No. de Cuenta <br> <b><?php echo $ncta ?></b>
					</p>
					<div class="foto">

					</div>
					<div class="barcode">
						<barcode code="<?php echo $ncta ?>" size=".68" type="CODABAR" />
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<div style="margin-left: 1.5em;">
						<p>
							<br>
							<b><?php echo $nombre ?></b> <br>
							Calle <br>
							Municipio <br>
							Delegación <br>
							MÉXICO D.F. C.P.
						</p>
					</div>
					<div class="asignacion">
						<br>
						<p>
							La UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO a través de la Secretaría General tiene el agrado de comunicar a usted que ha sido seleccionado para ingresar al:
						</p>
						<br>
						<p>
							<b>
								Programa: Posgrado en Bibliotecología y Estudios de la Información <br>
								Plan: 4166 Maestría en Bibliotecología y Estudios de la Información (Distancia) <br>
								Campo del conocimiento: Tecnología de a información <br>
								Entidad: 10 Facultad de Filosofía y Letras <br>
								Sistema: Universidad Abierta y a Distancia <br>
								Modalidad: A Distancia <br>
								Duración: 4 semestres Tiempo Completo <br>
							</b>
						</p>
						<br>
						<p>
							Este documento y el trámite que da origen, sólo tiene vigencia para el semestre 2018-1 y se entenderá que renuncia a su ingreso a la UNAM, si no cumple con los requisitos de ingreso.
						</p>
						<br>
						<p>
							Declara el aspirante aceptado, bajo protesta de decir verdad, que son ciertos los datos proporcionados, así como los documentos entregados y, que en caso de existir falsedad en ellos, parcial o total, quedará sin efecto su registro o inscripción correspondiente.
						</p>
						<br>
						<p>
							No olvide firmar la protesta Universitaria y conservar esta carta, ya que le será requerida.
						</p>
						<br>
						<p>
							Atentamente <br>
							"POR MI RAZA HABLARÁ EL ESPIRITU" <br>
							Ciudad Universitaria D.F, a 26 de enero de 2017 <br>
						</p>
						<div class="topC">
							<div style="text-align: center;">
								<h6><b>Protesta Universitaria</b></h6>
							</div>
						</div>
						<p>
							De acuerdo con el Estatuto General de la Universidad Nacional Autónoma de México, en su Título Quinto, Artículo 87 sección "De los alumnos", que a la letra dice: <br>
						</p>
						<p class="articulo">
							Reglamentos especiales determinarán los requisitos y condiciones para que los alumnos se inscriban y pertenezcan en la Universidad, así como sus deberes y derechos, por lo que, <b>YO, <?php echo $nombre ?></b>, me comprometo a hacer en todo tiempo honor a la institución, a cumplir mis compromisos académicos y administrativos, a respetar los reglamentos generales sin pretender excepción alguna y a mantener la disciplina.
						</p>
						<p class="firma">
							Nombre y firma de aceptación
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html