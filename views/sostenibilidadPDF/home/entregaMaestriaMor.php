<?php
require_once "../../../controllers/sostenibilidadPDF/sostenibilidad_pdf.php";

?>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
  <meta charset="utf-8">

  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-size:12px">

  <div class="carta">
    <div style="float: left; width: 80%; text-align: center;">
      <h6><b>CITA PARA ENTREGA DOCUMENTAL</b></h6>
    </div>
    <div style="float: right; width: 15%;">
      <h6 class="anio">2018-1</h6>
    </div>
  </div>
		<div class="row">
			<div class="contenido">
      <div style="float: right; width: 20%; padding: 1.1em 1.1em; margin-top: 1em;" >
        <barcode code="<?php echo $ncta ?>" type="CODABAR" /></barcode>
        <div style="margin-left:50px;">
          <?php echo $ncta ?>
        </div>
      </div>
				<p>
					<br>
					Estimado (a): <?php echo $nombre ?> <br><br>
					La entrega de documentos deberás presentarte el 27 o 28 de julio de 2017, de las 10:00 a las 15:00 horas en: 
					<b>Las Aulas interactivas 01 y 02, planta baja del Edificio de Docencia “A” de la ENES Morelia</b>, 
					ubicada en antigua carretera a Pátzcuaro No. 8701, Col. Ex Hacienda de San José de la Huerta, C.P. 58190, 
					Morelia, Michoacán. <br><br>

					O en el Local de Registro de Aspirantes, ubicado en Av. del Aspirante s/n casi esq. Con Av. del Imán, 
					Ciudad Universitaria, CD. MX., del 26 al 28 de julio de 2017 de las 10:00 a las 14:00 y de las 17:00 a 
					las 19:00 horas.<br>
				</p>
					<ol>
						<?php
$documentos = array(
	$documentos['appointment'],
	$documentos['birth_certificate'],
	$documentos['curp'],
	$documentos['identification'],
	$documentos['certificate'],
	$documentos['college_degree'],
	$documentos['appointment'],
);
$textos = array(
	0 => "Esta <b>cita para la entrega de documentos</b>, impresa. ",
	1 => "<b>Acta de nacimiento</b>, actualizada al 2017. ",
	2 => "<b>CURP</b> en fotocopia al 200%, en una hoja tamaño carta, usando solo un lado de la misma (no oscura o demasiado clara,
				ni borrosa, el texto debe ser totalmente legible). Los extranjeros podrán obtener este documento al ingresar al país.
				Consultar página:</br>
				<p style='text-align:center;'>
				 <a href='http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf'>
				   http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf
				 </a>
				</p>",
	3 => "<b>Identificación oficial vigente</b>, original, donde el texto y la fotografía sean completamente legibles (identificaciones aceptadas: credencial de elector, pasaporte, licencia de conducir, cartilla del servicio militar nacional y cédula profesional). ",
	4 => "<b> Certificado de licenciatura completo</b> (100% de créditos con promedio), en caso de que el certificado no lo indique se
				deberá entregar además una constancia con el promedio obtenido. Si realizaste tus estudios previos en el extranjero,
				deberás presentar la constancia de equivalencia de promedio emitida por la Dirección General de Incorporación y
				Revalidación de Estudios (DGIRE), consultar la página: </br>
				<p style='text-align:center;'>
				  <a href='http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/'>http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/</a>
				</p>",
	5 => "<b>Título de licenciatura</b>, y si se encuentra en trámite podrán presentar el acta de examen profesional, con fecha de emisión no mayor a un año'. Para los aspirantes de la UNAM que ingresan al programa de posgrado como opción a titulación mediante estudios de posgrado deberá presentar la carta de registro emitida por la Escuela o Facultad de la UNAM. ",
	6 => "Una fotografía de estudio reciente, tamaño infantil a color, de frente con la frente y orejas descubiertas.",
);
?>
						<?php for ($i = 0; $i < count($documentos); $i++): ?>
							<?php if ($documentos[$i]):?>
							  <li><?=$textos[$i]?></li>
							<?php endif;?>
						<?php endfor;?>
					</ol>
					<br>
					<?php if ($documentos[5]): ?>
							<footer>
					       	<div class='define'>
					           	<p><sup>1</sup>
					           	        Se otorgará un plazo para la entrega del Título correspondiente en apego a la legislación universitaria,
        para tal efecto y entre tanto,  es indispensable la entrega del acta de examen profesional.</p>
									</div>
					   	</footer>
							<pagebreak>
					<?php endif?>
					<br><br>
					<br>
					<b>Nota importante:</b> <br>
					<p>
						Los documentos expedidos en el extranjero deberán contar con el apostille o legalización correspondiente, y de ser el caso con traducción oficial al español.
					</p>
					<b>CONSIDERACIONES:</b> <br>
					<ol type="a">
					  <li>Se entenderá que renuncian a su inscripción aquellos aspirantes aceptados que no hayan completado los trámites correspondientes de entrega documental o proceso de inscripción establecidos en los instructivos, en apego a lo previsto en el artículo 29 del Reglamento General de Inscripciones.</li>
					  <li>Ningún documento será recibido si presenta roturas, enmendaduras, manchas, notas, tachaduras, perforaciones, mica adherible, clips, grapas, o cualquier otro daño.</li>
					  <li>No será posible realizar trámites sin identificación o en caso de no contar con la documentación tal y como ha sido solicitada. </li>
					</ol>
			</div>
		</div>
	</div>

</body>
</html>