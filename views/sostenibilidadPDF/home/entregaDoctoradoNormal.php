<?php
require_once "../../../controllers/sostenibilidadPDF/sostenibilidad_pdf.php";

switch ($day) {
	case 6:
	$day =	 26;
	break;

	case 7:
	$day =   27;
	break;

	case 8:
	$day = 28;
	break;
}

switch ($turn) {

	case 1:
	$turn = "11:00 a 12:00 ";
	break;

	case 2:
	$turn = "12:00 a 13:00";
	break;

	case 3:
	$turn = "13:00 a 14:00";
	break;

	case 4:
	$turn = "14:00 a 15:00";
	break;
}

?>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
	<meta charset="utf-8">

	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-size:12px">

	<div class="carta">
		<div style="float: left; width: 80%; text-align: center;">
			<h6><b>CITA PARA ENTREGA DOCUMENTAL</b></h6>
		</div>
		<div style="float: right; width: 15%;">
			<h6 class="anio">2018-1</h6>
		</div>
	</div>
	<div class="row">
		<div class="contenido">
			<div style="float: right; width: 20%; padding: 1.1em 1.1em; margin-top: 1em;" >
				<barcode code="<?php echo $ncta ?>" type="CODABAR" />
				</div>
				<p>
					Estimado (a): <?php echo $nombre ?>
					<br>
					La entrega de documentos deberás realizarla el día <?php echo $day; ?> de julio de 2017 de <?php echo $turn ?> horas, en el Local
					de Registro de Aspirantes, ubicado en Av. del Aspirante s/n casi esq. con Av. del Imán, Ciudad Universitaria
					<br><br>
					Los documentos que deberás entregar son:
					<br>
				</p>
				<ol>
					<?php
					$documentos = array(
						$documentos['appointment'],
						$documentos['birth_certificate'],
						$documentos['curp'],
						$documentos['identification'],
						$documentos['certificate'],
						$documentos['college_degree'],
						);

					//print_r($documentos);
					echo "certificate ->".$documentos['certificate'];

					$textos = array(
							0 => "Esta <b>cita para la entrega de documentos</b>, impresa. ",
							1 => "<b> Acta de nacimiento</b>, actualizada al 2017. ",
							2 => "<b>CURP</b> en fotocopia al 200%, en una hoja tamaño carta, usando solo un lado de la misma (no oscura o demasiado clara,
							ni borrosa, el texto debe ser totalmente legible). Los extranjeros podrán obtener este documento al ingresar al país.
							Consultar página:</br>
							<p style='text-align:center;'>
								<a href='http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf'>
									http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf
								</a>
							</p>",
							3 => "<b>Identificación oficial vigente</b>, original, donde el texto y la fotografía sean completamente legibles (identificaciones aceptadas: credencial de elector, pasaporte, licencia de conducir, cartilla del servicio militar nacional y cédula profesional). ",
							4 => "<b> Certificado de licenciatura completo</b> (100% de créditos con promedio), en caso de que el certificado no lo indique se
                  deberá entregar además una constancia con el promedio obtenido. Si realizaste tus estudios previos en el extranjero,
                  deberás presentar la constancia de equivalencia de promedio emitida por la Dirección General de Incorporación y
                  Revalidación de Estudios (DGIRE), consultar la página: </br>
                  <p style='text-align:center;'>
                    <a href='http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/'>http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/</a>
                  </p>",
         			5 => "<b>Título de licenciatura</b>.",
					);
						?>
							<?php for ($i = 0; $i < count($documentos); $i++): ?>
								<?php

								if ($documentos[$i]):
									?>
								<li><?=$textos[$i]?></li>
							<?php endif;?>
						<?php endfor;?>
</ol>
<?php if ($documentos[8]): ?>
	<footer>
		<div class='define'>
			<p><sup>1</sup>Se otorgará un plazo para la entrega del grado correspondiente en apego a la legislación universitaria, para tal efecto y entre tanto,  es indispensable la entrega del acta de examen de grado.</p>
		</div>
	</footer>
	<pagebreak>
		<br>
		<br>
		<br>
	<?php endif?>
	<br>
	<b>Nota importante:</b> <br>
	<p>
		Los documentos expedidos en el extranjero deberán contar con el apostille o legalización correspondiente, y de ser el caso con traducción oficial al español.
	</p>
	<b>CONSIDERACIONES:</b> <br>
	<ol type="a">
		<li>Se entenderá que renuncian a su inscripción aquellos aspirantes aceptados que no hayan completado los trámites correspondientes de entrega documental o proceso de inscripción establecidos en los instructivos, en apego a lo previsto en el artículo 29 del Reglamento General de Inscripciones. </li>
		<li>Ningún documento será recibido si presenta roturas, enmendaduras, manchas, notas, tachaduras, perforaciones, mica adherible, clips, grapas, o cualquier otro daño.</li>
		<li>No será posible realizar trámites sin identificación o en caso de no contar con la documentación tal y como ha sido solicitada. </li>
	</ol>
</div>
</div>
</div>
</body>
</html>