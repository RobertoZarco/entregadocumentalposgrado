<?php
require_once "../../../controllers/sostenibilidadPDF/sostenibilidad_pdf.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../../assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">

		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="unam" src="../../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: center;">
				<h6><b>Carta Compromiso</b></h6>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div>
					<h6 style="text-align: right;">No. de Cuenta: <b><?php echo $ncta ?></b></h6>
				</div>
				<br><br>
				<p>
					<b>DIRECCIÓN GENERAL DE ADMINISTRACIÓN ESCOLAR <br>PRESENTE</b>
				</p>
				<br>
				<p>
					El que suscribe <?php echo $nombre ?> hace entrega al Posgrado, del <b>documento de certificado de Licenciatura en Ciencias de la información</b>, en donde se acredita haber obtenido un promedio de 8.0. Documento que me comprometo a canjear por el <b>certificado original con promedio minimo de 8.0 y constancia de idioma español.</b>
				</p>
				<br>
				<p>
					En caso de <b>NO</b> entregar el certificado de estudios y constancia de idioma esidioma español original antes del <b>3 de octubre de 2017</b>, asumo que se me aplicará el Articulo 29 del Reglamento General de Inscripciones, que a la letra dice:
				</p>
				<p class="articulo">
					"Se entenderá que anuncian a su inscripción o reinscripción los alumnos que no hayan completado los trámites correspondientes, en las fechas que para el afecto se hayan establecido."
				</p>
				<p>
					Por los que acepto y me doy por enterado que se cancelará mi registro e inscripción, sin que la Universidad Nacional Autónoma de México, esté obligada a notificarme previamente.
				</p>
				<br>
				<p>
					<b>FIRMO LO ANTERIOR CONSCIENTE DE QUE ES MI TOTAL RESPONSABILIDAD REALIZAR LA ENTREGA DE MIS DOCUMENTOS EN LA COORDINACIÓN DEL POSGRADO EN BIBLIOTECOLOGÍA Y ESTUDIOS DE LA INFORMACIÓN, ANTES DEL 31 DE OCTUBRE DE 2018.</b>
				</p>
				<br>
				<p style="text-align: right;">
					Ciudad Universitaria, CD. de México, a 26 de julio de 2017.
				</p>
				<br>
				<p>
					Programa: <b>Posgrado en Bibliotecología y estudios de la información</b><br>
					Entidad: <b>Facultad de Filosofía y Letras</b><br>
					Maestría: <b>Bibliotecología y Estudios de la Información (Distancia)</b>
				</p>
				<br>
				<p class="firma">
					Nombre y firma de aceptación
				</p>
			</div>
		</div>
	</div>
</body>
</html>