<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="unam" src="../../assets/img/dgae1.png">
			</div>
		</div>

		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Acuse de recibo credencial UNAM &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Programa&emsp;&emsp; 2018-1</h5>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 18%;">
					<div class="fotoAsig">

					</div>
					<div class="barcode">
						<barcode code="A34698735B" size=".55" type="CODABAR" />
					</div>
					<div align="center" class="imgNac">
						<img class="nacion" src="../../assets/img/nacion.jpg" alt="">
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<div class="datos">
						<table>
							<tbody>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Nombre del alumno (a)</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $nombre ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Programa</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $program ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Entidad</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $campus ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">No. de cuenta</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $ncta ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Folio</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $folio_credential; ?></td>
			          </tr>
			        </tbody>
				    </table>
					</div>
					<div class="row">
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse colorCafeR">
								<h6><b>Indicaciones</b></h6>
								<p class="indicaciones">
									Preséntate en el Programa de Posgrado al que fuiste asignado para la entrega de tu credencial. En caso de que extravíes tu credencial, puedes solicitar una reposición en  esa  misma oficina.
								</p>
							</div>
						</div>
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse">
							</div>
								<div class="firma">
									Firma del alumno(a)
								</div>
						</div>
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse">
							</div>
								<div class="firma">
									Sello y/o Firma <br> Servicios Escolares del Plantel
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="recorte">

		</div>
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="unam" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Acuse de recibo credencial UNAM &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;DGAE&emsp;&emsp; 2018-1</h5>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 18%;">
					<div class="fotoAsig">

					</div>
					<div class="barcode">
						<barcode code="A34698735B" size=".55" type="CODABAR" />
					</div>
					<div align="center" class="imgNac">
						<img class="nacion" src="../../assets/img/nacion.jpg" alt="">
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<div class="datos">
						<table>
			        <tbody>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Nombre del alumno (a)</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $nombre ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Programa</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $program ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Entidad</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $campus ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">No. de cuenta</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $ncta ?></td>
			          </tr>
			          <tr>
			            <td style="font-size: 12px" class="tablaAcuse">Folio</td>
			            <td style="font-size: 12px" class="tablaAcuseO"><?php echo $folio_credential ?></td>
			          </tr>
			        </tbody>
					    </table>
					</div>
					<div class="row">
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse colorCafeR">
								<h6><b>Indicaciones</b></h6>
								<p class="indicaciones">
									Preséntate en el Programa de Posgrado al que fuiste asignado para la entrega de tu credencial. En caso de que extravíes tu credencial, puedes solicitar una reposición en  esa  misma oficina.
								</p>
							</div>
						</div>
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse">
							</div>
								<div class="firma">
									Firma del alumno(a)
								</div>
						</div>
						<div style="float: left; width: 33%;">
							<div class="cajaAcuse">
							</div>
								<div class="firma">
									Sello y/o Firma <br> Servicios Escolares del Plantel
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html