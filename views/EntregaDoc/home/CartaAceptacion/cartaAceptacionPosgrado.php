
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="./assets/css/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: center; float: left; width: 80%;">
				<h5><b>Carta de Asignación</b></h5>
			</div>
			<div style="float: right; width: 15%;" class="fechah">
				<h5><b>2018-1</b></h5>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div style="float: left; width: 20%;">
					<p>
						<br> No. de Cuenta <br> <b><?php echo $ncta ?></b>
					</p>
					<div class="foto">

					</div>
					<div class="barcode">
						<barcode code="<?php echo $ncta ?>" size=".68" type="CODABAR" />
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<div style="margin-left: 1.5em;">
						<p>
							<br>
							<b><?php echo $nombre ?></b> <br>
							<?php echo $street; ?> <br>
							<?php echo $colony; ?> <br>
							<?php echo $munipality; ?> <br>
							<?php echo $country . " " . $state . "  C.P " . $postal_code; ?>
						</p>
					</div>
					<div class="asignacion">
						<br>
						<p>
							La UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO a través de la Secretaría General tiene el agrado de comunicar a usted que ha sido seleccionado para ingresar al:
						</p>
						<br>
						<p>

							<b>
								Programa: <?php echo $program ?> <br>
								Plan: <?php echo $plan_id . " " . $plan ?><br>

								<?php
								if($field_knowledge == "DN" or $field_knowledge == "DD"){
									if($campus == "DD"){

									}elseif ($campus == "DN") {

									}else{
										echo "";
									}
								}else{
									echo "Campo del conocimiento: $field_knowledge <br>";
								}


								if($campus == null or $campus == ""){
									echo "";
								}else{
									echo "Entidad Académica: $campus<br>";
								}

								?>

								Sistema: Escolarizado <br>
								Modalidad: Presencial <br>
								<?php
								if($plan_id == "5172"){
									echo "Duración: 8 semestres, Tiempo Completo <br>";
								}else{
									echo "Duración: 4 semestres, Tiempo Completo <br>";
								}
								?>

							</b>
						</p>
						<br>
						<p>
							Este documento y el trámite que da origen, sólo tiene vigencia para el primer ingreso al semestre 2018-1 y se entenderá que renuncia a su ingreso a la UNAM, si no cumple con los requisitos de ingreso y concluye su inscripción oportunamente
						</p>
						<br>
						<p>
							Declara el aspirante aceptado, bajo protesta de decir verdad, que son ciertos los datos proporcionados, así como los documentos entregados y, que en caso de existir falsedad en ellos, parcial o total, quedará sin efecto su registro o inscripción correspondiente.
						</p>
						<br>
						<p>
							No olvide firmar la protesta Universitaria y conservar esta carta, ya que le será requerida.
						</p>
						<br>
						<p>
							<?php $hoy = getdate();?>
							Atentamente <br>
							"POR MI RAZA HABLARÁ EL ESPIRITU" <br>
							Ciudad Universitaria CDMX, a <?php echo $hoy['mday']; ?> de Julio de 2017. <br>
						</p>
						<div class="topC">
							<div style="text-align: center;">
								<h5><b>Protesta Universitaria</b></h5>
							</div>
						</div>
						<p>
							De acuerdo con el Estatuto General de la Universidad Nacional Autónoma de México, en su Título Quinto, Artículo 87 sección "De los alumnos": <br>
						</p>
						<p class="articulo">
							Reglamentos especiales se determinarán los requisitos y condiciones para que los alumnos se inscriban y permanezcan en la Universidad, así como sus deberes y derechos; por lo que, <b>YO <?php echo $nombre; ?></b>, me comprometo a hacer en todo tiempo honor a la institución, a cumplir mis compromisos académicos y administrativos, a respetar los reglamentos generales sin pretender excepción alguna y a mantener la disciplina.
						</p>
						<p class="firma">
							Nombre y firma de aceptación
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>