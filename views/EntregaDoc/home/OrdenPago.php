<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="./assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<!--Bloque -->
		<div class="orden">
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: left;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional <br> Autónoma de México <br></h6>
				</b>
				<h6>Patronato Universitario</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 15%;">
					<div class="centro"><br><br>
						<h6 align="center"><b>PRIMER<br>INGRESO</b></h6>
					</div>
				</div>
				<div style="float: left; width: 83%;">
					<div class="cabeceraOrden">
						<div style="float: left; width: 45%;">
							<h5>Orden de Pago</h5>
						</div>
						<div style="float: right; width: 30%;" align="right">
							No. de Comprobante
							<div>
								<table class="numComp">
							        <tbody>
							          <tr>
							            <td class="numComp" align="center"><b>1</b></td>
							            <td class="numComp" align="center">000000</td>
							          </tr>
							        </tbody>
							    </table>
							</div>
						</div>
						<div style="float: right; width: 25%;">
							<h5>Copia Caja</h5>
						</div>
					</div>
					<table>
				        <tbody>
				          <tr>
				            <td class="talon bordeD2px tampx1" align="center">No. de Cuenta</td>
				            <td class="talon bordeD2px tam20" align="center">Nivel</td>
				            <td class="talon bordeD2px" align="center">Plantel</td>
				            <td class="talon bordeD2px" align="center">Carrera</td>
				            <td class="talon bordeD2px" align="center">Nacionalidad</td>
				            <td class="talon" align="center">Ciclo Escolar</td>
				          </tr>
				          <tr class="colorCafeR">
				          	<td class="talon bordeD2px borderbotoom1" align="center"><?php echo $ncta ?></td>
				            <td class="talon bordeD2px tam20 borderbotoom1" align="center">3)DOCTORADO</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">000</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">000</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">MEX</td>
				            <td class="talon borderbotoom1" align="center">2017-2018</td>
				          </tr>
				          <tr>
				          	<td class="talon bordeD2px" align="center">Nombre del alumno (a)</td>
				            <td class="talon" align="left"><?php echo $nombre ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				    	<tbody>
				    		<tr>
					          	<td class="talon bordeD2px bordertop1 tampx150">CUOTA REGLAMENTARIA ANUAL</td>
					            <td class="talon bordeD2px bordertop1" align="center">Inscripción y Colegiatura</td>
					            <td class="talon bordeD2px bordertop1" align="center">Examen Médico</td>
					            <td class="talon bordeD2px bordertop1" align="center">Trámite de Credencial</td>
					            <td class="talon bordertop1" align="center">Reglamentaria</td>
				          	</tr>
				          	<tr class="colorCafeR">
					          	<td class="talon bordeD2px" align="center">IMPORTES EN MXN.</td>
					            <td class="talon bordeD2px tam20" align="center"><b>$ 0.20</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon" align="center"><b>$ 0.25</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center">Aportación Voluntaria</td>
					            <td class="talon" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center"></td>
					            <td class="talon bordeD2px colorCafeR bordertop1 borderbotoom1" align="right" style="padding-right: 1em;">TOTAL</td>
					            <td class="talon colorCafeR bordertop1 borderbotoom1" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				    	</tbody>
				    </table>
				</div>
			</div>
			<br>
			<div class="recorte"></div>
			<!--Bloque -->
			<!--Bloque -->
		<div class="orden">
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: left;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional <br> Autónoma de México <br></h6>
				</b>
				<h6>Patronato Universitario</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 15%;">
					<div class="centro"><br><br>
						<h6 align="center"><b>PRIMER<br>INGRESO</b></h6>
					</div>
				</div>
				<div style="float: left; width: 83%;">
					<div class="cabeceraOrden">
						<div style="float: left; width: 45%;">
							<h5>Orden de Pago</h5>
						</div>
						<div style="float: right; width: 30%;" align="right">
							No. de Comprobante
							<div>
								<table class="numComp">
							        <tbody>
							          <tr>
							            <td class="numComp" align="center"><b>1</b></td>
							            <td class="numComp" align="center">000000</td>
							          </tr>
							        </tbody>
							    </table>
							</div>
						</div>
						<div style="float: right; width: 25%;">
							<h5>Copia Alumno</h5>
						</div>
					</div>
					<table>
				        <tbody>
				          <tr>
				            <td class="talon bordeD2px tampx1" align="center">No. de Cuenta</td>
				            <td class="talon bordeD2px tam20" align="center">Nivel</td>
				            <td class="talon bordeD2px" align="center">Plantel</td>
				            <td class="talon bordeD2px" align="center">Carrera</td>
				            <td class="talon bordeD2px" align="center">Nacionalidad</td>
				            <td class="talon" align="center">Ciclo Escolar</td>
				          </tr>
				          <tr class="colorCafeR">
				          	<td class="talon bordeD2px borderbotoom1" align="center"><?php echo $ncta ?></td>
				            <td class="talon bordeD2px tam20 borderbotoom1" align="center">3)DOCTORADO</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">000</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">000</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">MEX</td>
				            <td class="talon borderbotoom1" align="center">2017-2018</td>
				          </tr>
				          <tr>
				          	<td class="talon bordeD2px" align="center">Nombre del alumno (a)</td>
				            <td class="talon" align="left"><?php echo $nombre ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				    	<tbody>
				    		<tr>
					          	<td class="talon bordeD2px bordertop1 tampx150">CUOTA REGLAMENTARIA ANUAL</td>
					            <td class="talon bordeD2px bordertop1" align="center">Inscripción y Colegiatura</td>
					            <td class="talon bordeD2px bordertop1" align="center">Examen Médico</td>
					            <td class="talon bordeD2px bordertop1" align="center">Trámite de Credencial</td>
					            <td class="talon bordertop1" align="center">Reglamentaria</td>
				          	</tr>
				          	<tr class="colorCafeR">
					          	<td class="talon bordeD2px" align="center">IMPORTES EN MXN.</td>
					            <td class="talon bordeD2px tam20" align="center"><b>$ 0.20</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon" align="center"><b>$ 0.25</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center">Aportación Voluntaria</td>
					            <td class="talon" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center"></td>
					            <td class="talon bordeD2px colorCafeR bordertop1 borderbotoom1" align="right" style="padding-right: 1em;">TOTAL</td>
					            <td class="talon colorCafeR bordertop1 borderbotoom1" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				    	</tbody>
				    </table>
				</div>
			</div>
			<br>
			<div class="recorte"></div>
			<!--Bloque -->
			<!--Bloque -->
		<div class="orden">
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: left;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional <br> Autónoma de México <br></h6>
				</b>
				<h6>Patronato Universitario</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 15%;">
					<div class="centro"><br><br>
						<h6 align="center"><b>PRIMER<br>INGRESO</b></h6>
					</div>
				</div>
				<div style="float: left; width: 83%;">
					<div class="cabeceraOrden">
						<div style="float: left; width: 30%;">
							<h5>Orden de Pago</h5>
						</div>
						<div style="float: right; width: 30%;" align="right">
							No. de Comprobante
							<div>
								<table class="numComp">
							        <tbody>
							          <tr>
							            <td class="numComp" align="center"><b>1</b></td>
							            <td class="numComp" align="center">000000</td>
							          </tr>
							        </tbody>
							    </table>
							</div>
						</div>
						<div style="float: right; width: 40%;">
							<h5>Copia Serv. Escolares</h5>
						</div>
					</div>
					<table>
				        <tbody>
				          <tr>
				            <td class="talon bordeD2px tampx1" align="center">No. de Cuenta</td>
				            <td class="talon bordeD2px tam20" align="center">Nivel</td>
				            <td class="talon bordeD2px" align="center">Plantel</td>
				            <td class="talon bordeD2px" align="center">Carrera</td>
				            <td class="talon bordeD2px" align="center">Nacionalidad</td>
				            <td class="talon" align="center">Ciclo Escolar</td>
				          </tr>
				          <tr class="colorCafeR">
				          	<td class="talon bordeD2px borderbotoom1" align="center"><?php echo $ncta ?></td>
				            <td class="talon bordeD2px tam20 borderbotoom1" align="center">3)DOCTORADO</td>
				            <td class="talon bordeD2px borderbotoom1" align="center"><?php echo $campus ?></td>
				            <td class="talon bordeD2px borderbotoom1" align="center">000</td>
				            <td class="talon bordeD2px borderbotoom1" align="center">MEX</td>
				            <td class="talon borderbotoom1" align="center">2017-2018</td>
				          </tr>
				          <tr>
				          	<td class="talon bordeD2px" align="center">Nombre del alumno (a)</td>
				            <td class="talon" align="left"><?php echo $nombre ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				    	<tbody>
				    		<tr>
					          	<td class="talon bordeD2px bordertop1 tampx150">CUOTA REGLAMENTARIA ANUAL</td>
					            <td class="talon bordeD2px bordertop1" align="center">Inscripción y Colegiatura</td>
					            <td class="talon bordeD2px bordertop1" align="center">Examen Médico</td>
					            <td class="talon bordeD2px bordertop1" align="center">Trámite de Credencial</td>
					            <td class="talon bordertop1" align="center">Reglamentaria</td>
				          	</tr>
				          	<tr class="colorCafeR">
					          	<td class="talon bordeD2px" align="center">IMPORTES EN MXN.</td>
					            <td class="talon bordeD2px tam20" align="center"><b>$ 0.20</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon bordeD2px" align="center"><b>$ 0.025</b></td>
					            <td class="talon" align="center"><b>$ 0.25</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center">Aportación Voluntaria</td>
					            <td class="talon" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				          	<tr>
					          	<td class="talon" align="center"></td>
					            <td class="talon" align="center"></td>
					            <td class="talon bordeD2px" align="center"></td>
					            <td class="talon bordeD2px colorCafeR bordertop1 borderbotoom1" align="right" style="padding-right: 1em;">TOTAL</td>
					            <td class="talon colorCafeR bordertop1 borderbotoom1" style="padding-left: 1em;"><b>$</b></td>
				          	</tr>
				    	</tbody>
				    </table>
				</div>
			</div>
			<br>
			<div class="recorte"></div>
			<!--Bloque -->
			<div class="instrucciones" style="width: 70%;">
				<h6 style="font-size: 15px;"><b>Instrucciones</b></h6>
				<p style="font-size: 10px;"><b>Con tu aportación voluntaria, la Universidad alcanza al igual que tú sus objetivos y continúa siendo el gran proyecto cultural y educativo de nuestra sociedad. ¡Gracias!</b></p>
				<ol type="1">
				  <li style="font-size: 10px;">¿DÓNDE PAGAR?</li>
				  	<ol type="a">
					  <li style="font-size: 10px;">En los Bancos, a partir de $100.00</li>
					  	<table>
					        <tbody>
					          <tr class="colorCafeR">
					            <td style="font-size: 10px;">SCOTIABANK, S.A.</td>
					            <td style="font-size: 10px;">Horario Bancario</td>
					            <td style="font-size: 10px;">Convenio No. 3751</td>
					          </tr>
					          <tr>
					            <td style="font-size: 10px;">SANTANDER, S.A.</td>
					            <td style="font-size: 10px;">Horario Bancario</td>
					            <td style="font-size: 10px;">Convenio No. 6102</td>
					          </tr>
					          <tr class="colorCafeR">
					            <td style="font-size: 10px;">BBVA-BANCOMER, S.A.</td>
					            <td style="font-size: 10px;">Horario Bancario</td>
					            <td style="font-size: 10px;">Convenio No. 1300962</td>
					          </tr>
					        </tbody>
				      	</table>
					  <li style="font-size: 10px;">Si tu aportación es menor a $100.00, paga en las cajas ubicadas en la zona comercial a un lado de la Torre de Rectoría, o en el plantel asignado, si cuenta con caja.
					  </li>
					</ol>
				  <li style="font-size: 10px;">¿QUÉ TRÁMITES SIGUEN?</li>
				  	<ol type="a">
					  <li style="font-size: 10px;">Una vez efectuado el pago, consulta la página Web de tu plantel para conocer el procedimiento de inscripción.</li>
					  <li style="font-size: 10px;">Si no continúas con los trámites o abandonas tus estudios en el curso del año escolar al que te inscribiste, perderás el derecho a la inscripción y a la devolución de cualquier cuota que hayas pagado</li>
					</ol>
				</ol>
			</div>
		</div>
	</div>
</body>
</html