<?php

	switch ($day) {
		case 6:
				$day="26 de Julio del 2017 ";
			break;
		case 7:
				$day="27 de Julio del 2017 ";
			break;
		case 8:
				$day="28 de Julio del 2017 ";
			break;
		default:
			# code...
			break;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<!--Bloque -->
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Cita para toma de fotografía, firma digitalizada y huella digital</h5> <h6>Posgrado</h6>
			</div>
		</div> <br>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse bordeD" style="float: left; width: 18%;">
					<div align="center" class="barcode">
						<barcode code="A34698735B" size=".60" type="CODABAR" />
					</div>
					<br>
					<div>
						<h6 align="center">2018-1</h6>
						<br>
						<h6 align="center">COMPROBANTE <br> DGAE</h6>
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam30">Nombre del Aspirante</td>
				            <td class="cita"><?php echo $nombre ?></td>
				          </tr>
				          <tr>
				            <td class="cita bordeD">No. de Folio</td>
				            <td class="cita"><?php echo $folio_credential ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam8">
				            	<b><?php echo $day . " de " . $turn ?></b> <br> <br>

				            	Av.  del  Aspirante  S/N  esquina  Av.  del  Imán,  C.U.,  Col.  Pedregal de Santa Úrsula, Deleg. Coyoacán. (A tres cuadras de la estación Perisur del Metrobus).
				            </td>
				            <td class="cita" align="center">
				            	<h6><b>SELLO UNAM</b></h6>
				            </td>
				          </tr>
				    </table>
				</div>
			</div>
		</div>
		 <br>
		<!--Bloque -->
		<div class="recorte"></div>
		<br>
		<!--Bloque -->
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Cita para toma de fotografía, firma digitalizada y huella digital</h5> <h6>Posgrado</h6>
			</div>
		</div> <br>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse bordeD" style="float: left; width: 18%;">
					<div align="center" class="barcode">
						<barcode code="A34698735B" size=".60" type="CODABAR" />
					</div>
					<br>
					<div>
						<h6 align="center">2018-1</h6>
						<br>
						<h6 align="center">COMPROBANTE <br> MÓDULO</h6>
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam30">Nombre del Aspirante</td>
				            <td class="cita"><?php echo $nombre ?></td>
				          </tr>
				          <tr>
				            <td class="cita bordeD">No. de Folio</td>
				            <td class="cita"><?php echo $folio_credential ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam8">
				            	<b><?php echo $day . " de " . $turn ?></b> <br> <br>

				            	Av.  del  Aspirante  S/N  esquina  Av.  del  Imán,  C.U.,  Col.  Pedregal de Santa Úrsula, Deleg. Coyoacán. (A tres cuadras de la estación Perisur del Metrobus).
				            </td>
				            <td class="cita" align="center">
				            	<h6><b>SELLO UNAM</b></h6>
				            </td>
				          </tr>
				    </table>
				</div>
			</div>
		</div>
		 <br>
		<!--Bloque -->
		<div class="recorte"></div>
		<br>
		<!--Bloque -->
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Cita para toma de fotografía, firma digitalizada y huella digital</h5> <h6>Posgrado</h6>
			</div>
		</div> <br>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse bordeD" style="float: left; width: 18%;">
					<div align="center" class="barcode">
						<barcode code="A34698735B" size=".60" type="CODABAR" />
					</div>
					<br>
					<div>
						<h6 align="center">2018-1</h6>
						<br>
						<h6 align="center">COMPROBANTE <br> ASPIRANTE</h6>
					</div>
				</div>
				<div style="float: right; width: 80%;">
					<table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam30">Nombre del Aspirante</td>
				            <td class="cita"><?php echo $nombre ?></td>
				          </tr>
				          <tr>
				            <td class="cita bordeD">No. de Folio</td>
				            <td class="cita"><?php echo $folio_credential ?></td>
				          </tr>
				        </tbody>
				    </table>
				    <table>
				        <tbody>
				          <tr>
				            <td class="cita bordeD tam8">
				            	<b><?php echo $day . " de " . $turn ?></b> <br> <br>

				            	Av.  del  Aspirante  S/N  esquina  Av.  del  Imán,  C.U.,  Col.  Pedregal de Santa Úrsula, Deleg. Coyoacán. (A tres cuadras de la estación Perisur del Metrobus).
				            </td>
				            <td class="cita" align="center">
				            	<h6><b>SELLO UNAM</b></h6>
				            </td>
				          </tr>
				    </table>
				</div>
			</div>
		</div>
		 <br>
		<!--Bloque -->
	</div>
</body>
</html