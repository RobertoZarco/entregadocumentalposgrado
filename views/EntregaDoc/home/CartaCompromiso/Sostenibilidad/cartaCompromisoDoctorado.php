<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../../assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.jpg">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: center;">
				<h5><b>	Carta Compromiso</b></h5>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<br><br>
				<p>
					<b>DIRECCIÓN GENERAL DE ADMINISTRACIÓN ESCOLAR <br>PRESENTE</b>
				</p>
				<br>
				<p>
					<?php
					if($plan_id == "4172" or $plan_id == "5172"){
						if($field_knowledge == "DD"){
							echo "El que suscribe ___________________________________________________________________________________________________
							hace  entrega  a  la  Subdirección  de  Asuntos  Escolares  del  Posgrado  de  la  Dirección  General  de  Administración
							Escolar (DGAE), el documento que hace prueba plena de haber concluido la Licenciatura
							en ____________________________________________________________________________________________________________________,
							con promedio mínimo de ocho (8.5), el cual me comprometo a canjear por todos los documentos de ingreso requeridos: <b>
							Certificado original con promedio mínimo de ocho (8.5), original y copia del título, acta de nacimiento original y copia del CURP.
							</b>";
						}else if($field_knowledge == "DN"){
							echo "El que suscribe ___________________________________________________________________________________________________
							hace  entrega  a  la  Subdirección  de  Asuntos  Escolares  del  Posgrado  de  la  Dirección  General  de  Administración
							Escolar (DGAE), el documento que hace prueba plena de haber concluido la Maestría
							en ____________________________________________________________________________________________________________________,
							con promedio mínimo de ocho (8.5), el cual me comprometo a canjear por todos los documentos de ingreso requeridos: <b>
							Certificado de Licenciatura original, Certificado de maestría original con promedio mínimo de ocho (8.5), original y
							copia del título de licenciatura, original y copia del grado de maestría, acta de nacimiento original y copia del CURP.</b>";
						}else{
							echo "El que suscribe ___________________________________________________________________________________________________
							hace  entrega  a  la  Subdirección  de  Asuntos  Escolares  del  Posgrado  de  la  Dirección  General  de  Administración
							Escolar (DGAE), el documento que hace prueba plena de haber concluido la Licenciatura en
							____________________________________________________________________________________________________________________,
							con promedio mínimo de ocho (8.0), el cual me comprometo a canjear por todos los documentos de ingreso requeridos: <b>
							Certificado original con promedio mínimo de ocho (8.0), original y copia del título, acta de nacimiento original y copia del CURP.
							</b>";
						}
					}else{
						//MADEMS
						echo "El que suscribe ___________________________________________________________________________________________________
									hace  entrega  a  la  Subdirección  de  Asuntos  Escolares  del  Posgrado  de  la  Dirección  General  de  Administración
									Escolar (DGAE), el documento que hace prueba plena de haber concluido la Licenciatura en
									____________________________________________________________________________________________________________________,

									con promedio mínimo de ocho (8.0), el cual me comprometo a canjear por todos los documentos de ingreso requeridos:
									<b>Certificado original con promedio mínimo de ocho (8.0), original y copia del título, acta de nacimiento original y copia del CURP.</b>";
					}
					?>
				</p>
				<br>
				<p>
					<b>Nota Importante:</b> <br>
					Los documentos expedidos en el extranjero deberán contar con el apostille o legalización correspondiente, y de ser el caso con traducción oficial al español.
				</p>
				<br>
				<p>
					En caso de <b>NO</b> entregar los documentos descritos, antes del ____ de ______________ de _______, asumo que se me aplicará el artículo 29 del Reglamento General de Inscripciones (RGI) de la UNAM, que a la letra dice:
				</p>
				<p class="articulo">
					<b>Artículo 29.-</b> Se entenderá que renuncian a su inscripción o reinscripción los alumnos que no hayan completado los trámites correspondientes, en las fechas que para el efecto se hayan establecido.
				</p>
				<p>
					Por lo que acepto y me doy por enterado que se cancelará mi registro e inscripción, sin que la Universidad Nacional Autónoma de México, esté obligada a notificarme previamente.
				</p>
				<br>
				<p>
					<b>FIRMO LO ANTERIOR CONSCIENTE DE QUE ES MI TOTAL RESPONSABILIDAD REALIZAR LA ENTREGA DE MIS DOCUMENTOS EN LA SUBDIRECCIÓN DE ASUNTOS ESCOLARES DEL POSGRADO, QUE SE ENCUENTRA UBICADA EN EL CIRCUITO DE POSGRADO S/N, COYOACÁN, CD. UNIVERSITARIA, ANTES DEL 30 DE NOVIEMBRE DE 2017 EN UN HORARIO DE 9:00 A 13:00 HORAS.</b>
				</p>
				<br>
				<p style="text-align: right;">
					<?php $hoy = getdate();?>
					Ciudad Universitaria, CD. de México, a <?php echo $hoy['mday']; ?> de Julio de 2017.
				</p>
				<br>
				<p>
					<b>PROGRAMA: <?php echo $program ?></b><br>
					<b>PLAN DE ESTUDIOS: <?php echo $plan ?></b><br>
					<?php
					if($campus == "" or  $campus == 'DN'){
						$entidad = "";
					}else{
						$entidad = "<b>ENTIDAD ACADÉMICA:" .$campus ."</b>";
					}
					?>
					<b><?php echo $entidad; ?></b>
				</p>
				<br>
				<p class="firma">
					Nombre y firma de aceptación
				</p>
			</div>
		</div>
	</div>
</body>
</html>