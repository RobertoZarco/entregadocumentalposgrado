<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="assets/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
		<div>
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%; text-align: center;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
				Dirección General de Administración Escolar</h6>
			</div>
			<div style="float: right; width: 15%;">
				<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
			</div>
		</div>
		<div class="topC">
			<div style="text-align: left;">
				<h5 class="titulotop">Acuse de recibo y Consentimiento <br> para la validación de documentos oficiales</h5>
			</div>
		</div>
		<div class="row">
			<div class="contenido">
				<div class="fotoAsigAcuse" style="float: left; width: 18%;">
					<div class="fotoAsig">

					</div>
				</div>
				<div style="float: right; width: 80%;">
					<div class="datos">
						<br>
						<table>
					        <tbody>
					          <tr>
					            <td class="infoG">Fecha:</td>
					            <td class="infoG">Hora:</td>
					            <td class="infoG">Lugar de entrega:</td>
					          </tr>
					        </tbody>
					    </table>
					    <br>
						<table>
					        <tbody>
					          <tr>
					            <td class="tablaAcuseC">Nombre:</td>
					          </tr>
					          <tr>
					            <td class="tablaAcuseC">Número de Cuenta:</td>
					          </tr>
					          <tr>
					            <td class="tablaAcuseC">Programa:</td>
					          </tr>
					          <tr>
					            <td class="tablaAcuseC">Entidad Académica:</td>
					          </tr>
					          <tr>
					            <td class="tablaAcuseC">Domicilio:</td>
					          </tr>
					          <tr>
					            <td class="tablaAcuseC">Teléfono:</td>
					          </tr>
					        </tbody>
					    </table>
					</div>
				</div>
				<div style="float: right; width: 100%;">
					<br>
					<table class="enlista">
				        <tbody>
				          <tr>
				            <td class="tablaAcuseC tam3"></td>
				            <td class="tablaAcuseC tam2" align="center">ORIGINAL</td>
				            <td class="tablaAcuseC" align="center">NO. DE FOLIO</td>
				          </tr>
				          <tr>
				            <td class="tablaAcuseC">Acta de Nacimiento</td>
				            <td class="tablaAcuseC" align="center">
				            	<form action="#">
								    <p>
								      <input name="group1" type="radio" id="test1" />
								    </p>
								</form>
				            </td>
				            <td class="tablaAcuseC"></td>
				          </tr>
				          <tr>
				            <td class="tablaAcuseC">CURP</td>
				            <td class="tablaAcuseC" align="center">
				            	<form action="#">
								    <p>
								      <input name="group1" type="radio" id="test1" />
								    </p>
								</form>
				            </td>
				            <td class="tablaAcuseC"></td>
				          </tr>
				          <tr>
				            <td class="tablaAcuseC">Titulo de Licenciatura(copia)</td>
				            <td class="tablaAcuseC" align="center">
				            	<form action="#">
								    <p>
								      <input name="group1" type="radio" id="test1" />
								    </p>
								</form>
				            </td>
				            <td class="tablaAcuseC"></td>
				          </tr>
				          <tr>
				            <td class="tablaAcuseC">Certificado de Licenciatura</td>
				            <td class="tablaAcuseC" align="center">
				            	<form action="#">
								    <p>
								      <input name="group1" type="radio" id="test1" />
								    </p>
								</form>
				            </td>
				            <td class="tablaAcuseC"></td>
				          </tr>
				        </tbody>
				    </table>
				    <br><br>
				    <div>
				    	<p>
				    		De acuerdo con el artículo 30 del Reglamento General de Inscripciones de la UNAM, se notifica que en caso de comprobar la falsedad total  o parcial de un documento, se anulará la inscripción respectiva y quedarán sin efecto todos los actos derivados de la misma. <br> <br>Declara el aspirante bajo protesta de decir verdad que son ciertos los datos aquí asentados, y que en este acto entrega a la UNAM los  documentos oficiales en original solicitados para la validación de su autenticidad. <br> <br>En este mismo acto, recibo de la Universidad Nacional Autónoma de México los siguientes documentos:
				    	</p>
				    		<ul>
							  <li>Carta de Aceptación y Protesta Universitaria.</li>
							  <li>Acuse de recibo de Credencial.</li>
							  <li>Acuse de recibo y Consentimiento para la validación de documentos oficiales.</li>
							</ul>
						<p>
							Me doy por enterado que la documentación original de ingreso pasara por el Proceso de Integración, Digitalización y Captura de Datos, así como las validaciones correspondientes de expedientes, por lo que será necesario que permanezcan por lo menos un tiempo estimado de  4 meses en resguardo de la Universidad Nacional Autónoma de México, y serán devueltos a petición del interesado después del tiempo señalado y a través del procedimiento establecido o una vez egresado del nivel en el que se encuentra inscrito.
						</p>
				    </div> <br>
				    <div style="float: left; width: 50%;">
					    <div class="firmasAR"><p class="firmasL">Nombre del aspirante</p></div>
				    </div>
				    <div style="float: right; width: 50%;">
					    <div class="firmasAR"><p class="firmasL">Firma del aspirante</p></div>
				    </div> <br> <div align="center">PARA USO DE LA DGAE</div>
				    <div style="float: left; width: 50%;">
					    <div class="firmasAR gris"><p class="firmasL">Nombre de quien recibe</p></div>
				    </div>
				    <div style="float: right; width: 50%;">
					    <div class="firmasAR gris"><p class="firmasL">Firma de quien recibe</p></div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>