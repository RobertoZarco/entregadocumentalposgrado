<?php
require_once "../../../controllers/mail/pdf_mor_controller.php";
?>

<!DOCTYPE html>
<html style="height: 100%;">
<head>
  <meta charset="utf-8">

  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-size:12px">

  <div class="carta">
    <div style="float: left; width: 80%; text-align: center;">
      <h6><b>CITA PARA ENTREGA DOCUMENTAL</b></h6>
    </div>
    <div style="float: right; width: 15%;">
      <h6 class="anio">2018-1</h6>
    </div>
  </div>

  <div class="row">
    <div class="contenido">
      <p>
        <b>Consideraciones:</b> <br>
        <ol type="a">
          <li>
            <b>
              Se entenderá que renuncian a su inscripción aquellos aspirantes aceptados que no hayan completado los trámites
              correspondientes de entrega documental o proceso de inscripción establecidos en los instructivos, en apego a lo
              previsto en el Reglamento General de Inscripciones en su artículo 29.
            </b>
          </li>

          <li>
            <b>
              Ningún documento será recibido si presenta roturas, enmendaduras, manchas, notas, tachaduras, perforaciones, mica adherible,
              clips, grapas, o cualquier otro daño.
            </b>
          </li>

          <li>
            <b>
              No será posible realizar trámites sin identificación o en caso de no contar con la documentación tal y
              como ha sido solicitada.
            </b>
          </li>
        </ol>
      </p>
    </div>
  </div>


</body>
</html>