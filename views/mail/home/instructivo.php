<?php
//require_once("../../../controllers/mail/instructivo_controller.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../../assets/css/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">

		<div class="carta">
			<div style="float: left; width: 80%; text-align: center;">
				<h6 class="instructivoI">
					<b>INSTRUCTIVO DE INSCRIPCIÓN</b>
				</h6>
			</div>
			<div style="float: right; width: 15%;">
				<h6 class="anio">2018-1</h6>
			</div>
		</div>

		<div class="row">
			<div class="contenido">
				<br>
				<ol class="instructivo">

					<li>
						Haber realizado la entrega de documentos en el lugar, fecha y hora según lo indicado en la presente cita. 
					</li>

					<li>
						En esta cita al entregar tus documentos, recibirás la documentación de ingreso que formaliza tu ingreso y que te permitirá inscribirte al Programa de   Posgrado, asimismo deberás pasar a la toma de biométricos para la elaboración de tu credencial UNAM.
					</li>

					<li>
						Con la documentación, deberás inscribirte del 31 de julio al 4 de agosto, realizando el registro de la carga académica a cursar en el primer semestre, en la siguiente dirección electrónica
						<a href='https://www.saep.unam.mx' style="font-size: 14px;">https://www.saep.unam.mx</a>
						sección: <i>alumnos</i>, en donde se te solicitará usuario y contraseña. En usuario anota tu número de cuenta, el cual podrás obtener de tu Carta de Asignación y Protesta Universitaria y para la contraseña escribe tu fecha de nacimiento con el formato dd/mm/aaaa, para concluir este proceso debes imprimir el COMPROBANTE DE REGISTRO ACADÉMICO.
					</li>

					<li>
						Del 18 al 29 de septiembre, deberás verificar en la dirección electrónica
						<a href="https://www.saep.unam.mx" style="font-size: 14px;">https://www.saep.unam.mx</a>
						que tu registro haya sido validado por el coordinador del programa de posgrado.
					</li>
				</ol>
			</div>
		</div>
	</div>
</body>
</html>
