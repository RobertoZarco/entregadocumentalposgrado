<?php
require_once "../../../controllers/mail/pdf_controller.php";

switch ($day) {
case 1:
	$day = 19;
	break;

case 2:
	$day = 20;
	break;

case 3:
	$day = 21;
	break;
}

switch ($turn) {
case 1:
	$turn = "10: 00 a 11:00";
	break;

case 2:
	$turn = "11:00 a 12:00 ";
	break;

case 3:
	$turn = "12:00 a 13:00";
	break;

case 4:
	$turn = "13:00 a 14:00";
	break;

case 5:
	$turn = "14:00 a 15:00";
	break;

case 6:
	$turn = "17:00 a 18:00";
	break;
}

?>

<!DOCTYPE html>
<html style="height: 100%;">
<head>
  <meta charset="utf-8">

  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
  <div class="container">

    <div class="cabecera">
      <div style="float: left; width: 15%;">
        <img class="unam" src="../../../assets/img/unam.jpg">
        dgae1
      </div>
      <div style="float: left; width: 70%;">
        <b>
          <h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
        </b>
        <h6>
          <br>Secretaría General
          <br>Dirección General de Administración Escolar
          <br>Coordinación de Estudios de Posgrado
        </h6>
      </div>
      <div style="float: right; width: 15%;">
        <img class="unamPos" src="../../../assets/img/UNAMPOSGRAGO.png">
        <!-- <img class="unamPos" src="../../../assets/img/dgae1.png"> -->
      </div>
    </div>

    <div class="carta">
      <div style="float: left; width: 80%; text-align: center;">
        <h6><b>Cita para entrega documental</b></h6>
      </div>
      <div style="float: right; width: 15%;">
        <h6 class="anio">2018-1</h6>
      </div>
    </div>

    <div class="row">
      <div class="contenido">
        <div style="float: right; width: 20%; padding: 1.1em 1.1em; margin-top: 1em;" >
          <barcode code="<?php echo $ncta ?>" type="CODABAR" />
            <div style="margin-left:50px;">
              <?php echo $ncta ?>
            </div>
          </div>
          <p>
            <br>
            Estimado (a): <?php echo $nombre ?> <br>
            La entrega de documentos deberás realizarla el día <?php echo $day; ?> de julio de 2017 de <?php echo $turn ?> horas, en el Local
            de Registro de Aspirantes, ubicado en Av. del Aspirante s/n casi esq. con Av. del Imán, Ciudad Universitaria <br><br>
            Los documentos que deberás entregar son: <br>
            <ol>
              <?php
//print_r($documentos);
$documentos = array(
	$documentos['appointment'],
	$documentos['birth_certificate'],
	$documentos['curp'],
	$documentos['identification'],
	$documentos['certificate'],
	$documentos['college_degree'],
	$documentos['appointment'],
);

$textos = array(
	0 => "Esta cita para la entrega de documentos, impresa. ",
	1 => "Acta de nacimiento, actualizada al 2017. ",
	2 => "CURP en fotocopia ampliada el 200% en una hoja tamaño carta, usando solo un lado de la misma (no oscura o demasiado clara, ni borrosa, el texto debe ser totalmente legible). Los extranjeros podrán obtener este documento al ingresar al país. Consultar página:</br> <p>http://www.saep.unam.mx/curp/manual_tramite_curp.pdf </p>",
	3 => "Identificación oficial vigente, original, donde el texto y la fotografía sean completamente legibles (identificaciones aceptadas: credencial de elector, pasaporte, licencia de conducir, cartilla del servicio militar nacional y cédula profesional). ",
	4 => "Certificado de licenciatura completo (100% de créditos) que especifique el promedio obtenido, en caso de que el certificado no indique el promedio se deberá entregar además una constancia con el promedio obtenido. Para los aspirantes que realizaron sus estudios previos en el extranjero, deberán presentar la constancia de equivalencia de promedio emitida por la Dirección de Incorporación y Revalidación de Estudios (DGIRE), consultar la página: </br>http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/",
	5 => "Título de licenciatura conforme al plan de estudios o acta de examen profesional (con fecha de emisión no mayor a un año)<sup>1</sup>.<br> Para los aspirantes que ingresan al programa de posgrado como opción a titulación mediante estudios de posgrado deberá presentar la carta de registro emitida por la Escuela o Facultad de la UNAM.",
	//6 => "Dispensa de promedio  emitida por la coordinación del Programa.",
);

for ($i = 0; $i < count($documentos); $i++) {
	if ($documentos[$i]) {
		?>
                  <li><?=$textos[$i]?></li>
                  <?php
}
}
?>
              </ol>

              <b>Nota importante:</b> <br>
              <p>Los documentos expedidos en un país diferente a México deberán contar con el apostille o legalización correspondiente, y de ser el caso con traducción oficial al español.</p>

              <b>CONSIDERACIONES:</b> <br>
              <ol type="a">
                <li><b>Se entenderá que renuncian a su inscripción aquellos aspirantes aceptados que no hayan completado los trámites correspondientes de entrega documental o proceso de inscripción establecidos en los instructivos, en apego a lo previsto en el Reglamento General de Inscripciones en su artículo 29.</b></li>
                <li><b>Ningún documento será recibido si presenta roturas, enmendaduras, manchas, notas, tachaduras, perforaciones, mica adherible, clips, grapas, o cualquier otro daño.</b></li>
                <li><b>No será posible realizar trámites sin identificación o en caso de no contar con la documentación tal y como ha sido solicitada.</b></li>
              </ol>
            </p>
          </div>
        </div>
      </div>

      <footer>
        <?php
if ($documentos[5]): ?>
          <div class='define'>
            <p style="padding-top:-40px">
              <sup>1</sup>
              Se otorgará un plazo para la entrega del Título correspondiente en apego a la legislación universitaria,
              para tal efecto y entre tanto,  es indispensable la entrega del acta de examen profesional.
            </p>
          </div>
        <?php endif?>
      </footer>
    </body>
    </html>