<?php
require_once "../../../controllers/mail/pdf_controller.php";

switch ($day) {
  case 1:
  	$day = 19;
  	break;

  case 2:
  	$day = 20;
  	break;

  case 3:
  	$day = 21;
  	break;
}

switch ($turn) {
  case 1:
  	$turn = "10: 00 a 11:00";
  	break;

  case 2:
  	$turn = "11:00 a 12:00 ";
  	break;

  case 3:
  	$turn = "12:00 a 13:00";
  	break;

  case 4:
  	$turn = "13:00 a 14:00";
  	break;

  case 5:
  	$turn = "14:00 a 15:00";
  	break;

  case 6:
  	$turn = "17:00 a 18:00";
  	break;
}

?>

<!DOCTYPE html>
<html style="height: 100%;">
<head>
  <meta charset="utf-8">

  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-size:12px">

  <div class="carta">
    <div style="float: left; width: 80%; text-align: center;">
      <h6><b>CITA PARA ENTREGA DOCUMENTAL</b></h6>
    </div>
    <div style="float: right; width: 15%;">
      <h6 class="anio">2018-1</h6>
    </div>
  </div>

  <div class="row">
    <div class="contenido">
      <div style="float: right; width: 20%; padding: 1.1em 1.1em; margin-top: 1em;" >
        <barcode code="<?php echo $ncta ?>" type="CODABAR" /></barcode>
        <div style="margin-left:50px;">
          <?php echo $ncta ?>
        </div>
      </div>

      <p>
        Estimado (a): <?php echo $nombre ?>
        <br>
        La entrega de documentos deberás realizarla el día <?php echo $day; ?> de julio de 2017 de <?php echo $turn ?> horas, en el Local
        de Registro de Aspirantes, ubicado en Av. del Aspirante s/n casi esq. con Av. del Imán, Ciudad Universitaria
        <br><br>
        Los documentos que deberás entregar son:
        <br>

        <ol>
          <?php
          //print_r($documentos);
          $documentos = array(
            $documentos['appointment'],
            $documentos['birth_certificate'],
            $documentos['curp'],
            $documentos['identification'],
            $documentos['certificate'],
            $documentos['college_degree'],
            $documentos['appointment'],
          );

          $textos = array(
            0 => "Esta cita para entrega de documentos, impresa. ",

            1 => "Acta de nacimiento, actualizada al 2017. ",

            2 => "CURP en fotocopia al 200%, en una hoja tamaño carta, usando solo un lado de la misma (no oscura o demasiado clara,
                   ni borrosa, el texto debe ser totalmente legible). Los extranjeros podrán obtener este documento al ingresar al país.
                   Consultar página:</br>
                   <p style='text-align:center;'>
                    <a href='http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf'>
                      http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf
                    </a>
                  </p>",

            3 => "Identificación oficial vigente, original, donde el texto y la fotografía sean completamente legibles (identificaciones aceptadas:
                  credencial de elector, pasaporte, licencia de conducir, cartilla del servicio militar nacional o cédula profesional). ",

            4 => "Certificado de licenciatura completo (100% de créditos con promedio), en caso de que el certificado no lo indique se
                  deberá entregar además una constancia con el promedio obtenido. Si realizaste tus estudios previos en el extranjero,
                  deberás presentar la constancia de equivalencia de promedio emitida por la Dirección General de Incorporación y
                  Revalidación de Estudios (DGIRE), consultar la página: </br>
                  <p style='text-align:center;'>
                    <a href='http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/'>http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/</a>
                  </p>",

            5 => "Título de licenciatura, y si se encuentra en trámite podrán presentar el acta de examen profesional, con fecha de emisión
                  no mayor a un año<sup>1</sup>. Los alumnos de la UNAM que ingresan al programa de posgrado como opción a titulación
                  mediante estudios de posgrado deberán presentar la carta de registro emitida por la Escuela o Facultad de la UNAM.
                  ",
            //6 => "Dispensa de promedio  emitida por la coordinación del Programa.",
          );

          for ($i = 0; $i < count($documentos); $i++) {
            if ($documentos[$i]) {
              ?>
              <li><?=$textos[$i]?></li>
              <?php
            }
          }
          ?>
        </ol>

        <b>Nota importante:</b>
        <p>
          Los documentos expedidos en el extranjero deberán contar con el apostille o legalización correspondiente, y de ser
          el caso con traducción oficial al español.
        </p>

        <br>

        <b>Consideraciones:</b> <br>
        <ol type="a">
          <li>
            <b>
              Se entenderá que renuncian a su inscripción aquellos aspirantes aceptados que no hayan completado los trámites
              correspondientes de entrega documental o proceso de inscripción establecidos en los instructivos, en apego a lo
              previsto en el Reglamento General de Inscripciones en su artículo 29.
            </b>
          </li>

          <li>
            <b>
              Ningún documento será recibido si presenta roturas, enmendaduras, manchas, notas, tachaduras, perforaciones, mica adherible,
              clips, grapas, o cualquier otro daño.
            </b>
          </li>

          <li>
            <b>
              No será posible realizar trámites sin identificación o en caso de no contar con la documentación tal y
              como ha sido solicitada.
            </b>
          </li>
        </ol>
      </p>
    </div>
  </div>


  <footer>
    <?php
    if ($documentos[5] ): ?>
      <p  style="padding-top:-33px">
        <sup>1</sup>
        Se otorgará un plazo para la entrega del Título correspondiente en apego a la legislación universitaria,
        para tal efecto y entre tanto,  es indispensable la entrega del acta de examen profesional.
      </p>
    <?php endif ?>
  </footer>

</body>
</html>