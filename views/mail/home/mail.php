
<table border="0" cellspacing="0" cellpadding="0">
  <tr style="width="100%;">
    <td>
      <div style="text-align: center;">
        <table cellspacing="0" cellpadding="0" border="0" style="border:none; padding:0 18px; margin:50px auto; width:65%">

          <tr style="background: #1c3d6c; height:90px;">
            <td style="text-align: left; width: 15%">
              <img height="70" title="UNAM"
              style="border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left;"
              src="https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png">
            </td>

            <td style="text-align: center;width: 10%">

            </td>

            <td style="text-align: right;width: 10%">

            </td>
          </tr>

          <!--cuerpo del correo-->
          <tr width="100%">
            <td valign="top" align="left" style="border-bottom-left-radius:4px; border-bottom-right-radius:4px; background:#f6f6f6; padding:18px" colspan="3"><br><br><br>

              <p align="center" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                <b>LA UNIVERSIDAD NACIONAL AUT&Oacute;NOMA DE M&Eacute;XICO</b>
              </p>

              <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                A trav&eacute;s de la Direcci&oacute;n General de Administraci&oacute;n Escolar y la Coordinaci&oacute;n de Estudios de Posgrado,
                dependientes de la Secretar&iacute;a General, emiten el presente aviso:
              </p>

              <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                Conforme a lo dispuesto en el Art&iacute;culo 7° del Reglamento General de Estudios de Posgrado mediante el cual se establecen
                los requisitos para que los aspirantes puedan ingresar a un programa de posgrado; nos es grato notificarte que has sido
                aceptado para cursar la Maestr&iacute;a en Docencia para la Educaci&oacute;n Media Superior (MADEMS) para el semestre 2018-1.
              </p>

              <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                Por lo anterior, y para dar continuidad al proceso de inscripci&oacute;n correspondiente, deber&aacute;s cumplir con los requisitos
                que especifica el documento anexo a trav&eacute;s del cual se te otorga una cita para la entrega documental y toma de fotograf&iacute;a,
                firma digitalizada y huella digital. Es importante mencionar que deber&aacute;s presentarte puntualmente el d&iacute;a y la hora
                establecidos para tal fin y presentar la documentaci&oacute;n que se te solicite.
              </p>

            </td>
          </tr>
          <!--fin cuerpo del correo-->

          <!--footer-->
          <tr width="100%" height="60px" >
            <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center;" >
            	<img height="70" title="UNAM" style="width: 40%; height: 48%"
              style="border-top-left-radius:0px;background:#3b4042;padding:12px 18px;text-align:left"
              src="https://ingreso.dgae.unam.mx/entregadocumentalposgrado/assets/img/CEP.png" style="width: 200px" height="60px">

              <p style="text-align: center; color: #FFF; font-size: 12px;">
              	Coordinaci&oacute;n de Estudios de Posgrado
              </p>
            </td>

            <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center;">
            	<p style="text-align: center; color: #FFF;">
            		ATENTAMENTE
            	</p>
            </td>

            <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center; font-size: 12px;">
            	<img height="70" title="UNAM" style="width: 30%; height: 50%"
              src="https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png">
              <p style="text-align: center; color: #FFF;">
              	Direcci&oacute;n General de Administraci&oacute;n Escolar
              </p>
            </td>
          </tr>
          <!--fin footer-->

        </table>
      </div>
    </td>
  </tr>
</table>



