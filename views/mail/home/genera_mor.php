<?php
require_once "../../../controllers/mail/pdf_mor_controller.php";
?>

<!DOCTYPE html>
<html style="height: 100%;">
<head>
  <meta charset="utf-8">

  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../../assets/css/styles.css"  media="screen,projection"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-size:12px">

  <div class="carta">
    <div style="float: left; width: 80%; text-align: center;">
      <h6><b>CITA PARA ENTREGA DOCUMENTAL</b></h6>
    </div>
    <div style="float: right; width: 15%;">
      <h6 class="anio">2018-1</h6>
    </div>
  </div>

  <div class="row">
    <div class="contenido">
      <div style="float: right; width: 20%; padding: 1.1em 1.1em; margin-top: 1em;" >
        <barcode code="<?php echo $ncta ?>" type="CODABAR" /></barcode>
        <div style="margin-left:50px;">
          <?php echo $ncta ?>
        </div>
      </div>

      <p>
        Estimado (a): <?php echo $nombre ?>
        <br>
        Para la entrega de documentos, deberás presentarte el 27 ó 28 de julio de 2017, de las 10:00 a las 15:00 horas en: Las Aulas Interactivas 01 y 02, planta baja del Edificio de Docencia
         “A”, de la ENES Morelia, ubicada en antigua carretera a Pátzcuaro No. 8701, Col. Ex Hacienda de San José de la Huerta, C.P. 58190, Morelia, Michoacán o en el Local de Registro de 
         Aspirantes, ubicado en Av. del Aspirante casi esq. con Av. del Imán, Ciudad Universitaria, CD. MX., el 25 ó 26 de julio de 2017, de las 10.00 a las 14.00 horas.
        <br>
        Por favor atiende las fechas establecidas en donde personal de la UNAM realizará la recepción documental en tu entidad. Este trámite no tiene ningún costo.
        <br>

        <ol>
          <?php
          //print_r($documentos);
          $documentos = array(
            $documentos['appointment'],
            $documentos['birth_certificate'],
            $documentos['curp'],
            $documentos['identification'],
            $documentos['certificate'],
            $documentos['college_degree'],
            $documentos['appointment'],
          );

          $textos = array(
           0 => "Esta cita para entrega de documentos, impresa. ",

           1 => "Acta de nacimiento, actualizada al 2017. ",

           2 => "CURP en fotocopia al 200%, en una hoja tamaño carta, usando solo un lado de la misma (no oscura o demasiado clara,
           ni borrosa, el texto debe ser totalmente legible). Los extranjeros podrán obtener este documento al ingresar al país.
           Consultar página:</br>
           <p style='text-align:center;'>
            <a href='http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf'>
              http://www.saep.unam.mx/static/ayuda/curp/manual_tramite_curp.pdf
            </a>
          </p>",

          3 => "Identificación oficial vigente, original, donde el texto y la fotografía sean completamente legibles (identificaciones aceptadas:
          credencial de elector, pasaporte, licencia de conducir, cartilla del servicio militar nacional o cédula profesional). ",

          4 => "Certificado de licenciatura completo (100% de créditos con promedio), en caso de que el certificado no lo indique se
          deberá entregar además una constancia con el promedio obtenido. Si realizaste tus estudios previos en el extranjero,
          deberás presentar la constancia de equivalencia de promedio emitida por la Dirección General de Incorporación y
          Revalidación de Estudios (DGIRE), consultar la página: </br>
          <p style='text-align:center;'>
            <a href='http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/'>http://www.dgire.unam.mx/contenido_wp/equivalencia-de-promedio/</a>
          </p>",

          5 => "Título de licenciatura, y si se encuentra en trámite podrán presentar el acta de examen profesional, con fecha de emisión
          no mayor a un año<sup>1</sup>. Los alumnos de la UNAM que ingresan al programa de posgrado como opción a titulación
          mediante estudios de posgrado deberán presentar la carta de registro emitida por la Escuela o Facultad de la UNAM.
          ",
          6 => "Una fotografía de estudio reciente, tamaño infantil a color, de frente con la frente y orejas descubiertas.",
          );

          for ($i = 0; $i < count($documentos); $i++) {
           if ($documentos[$i]) {
            ?>
            <li><?=$textos[$i]?></li>
            <?php
          }
        }
        ?>
      </ol>

      <b>Nota importante:</b>
      <p>
        Los documentos expedidos en el extranjero deberán contar con el apostille o legalización correspondiente, y de ser
        el caso con traducción oficial al español.
      </p>
      <br>
      <br>

    </p>
  </div>
</div>


<footer>
  <?php
  if ($documentos[5]): ?>
  <p  style="padding-top:-33px">
    <sup>1</sup>
    Se otorgará un plazo para la entrega del Título correspondiente en apego a la legislación universitaria,
    para tal efecto y entre tanto,  es indispensable la entrega del acta de examen profesional.
  </p>
<?php endif?>
</footer>

</body>
</html>