<?php

require '../../../public/PHPMailer/PHPMailerAutoload.php';
require_once("../../../controllers/mail/sendMail_controller.php");


$mail = new PHPMailer;

$fp = fopen("log_mail.txt", 'a+');

$archivo = file ( "lista.txt" );
$lineas = count ( $archivo );

//echo $lineas."\n";

// iniciamos un ciclo for que sea menor al nuemro de filas del archivo
for($i = 0; $i < $lineas; $i ++) {

//for($i = 0; $i < count($getStudents); $i++){
	//$account_number = '518007579';
  //$correo = "esanta@dgae.unam.mx";
  $correo         = null;
  $account_number = null;
  //$pdf            = null;

  $data = explode("|", $archivo[$i]);

  //$account_number = $getStudents[$i]['account_number'];
  //$correo         = $getStudents[$i]['email'];
  $account_number = $data[0];
  $correo         = $data[1];
  $pdf            = "Citas/$account_number.pdf";



	//$mail->SMTPDebug = 3;                               // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	//$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
	//$mail->SMTPAuth = true;                               // Enable SMTP authentication
	//$mail->Username = 'user@example.com';                 // SMTP username
	//$mail->Password = 'secret';                           // SMTP password
	//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	//$mail->Port = 587;                                    // TCP port to connect to


	$mail->setFrom('dgae@dgae.unam.mx', 'DGAE');
	//$mail->addAddress('josueponce.development@gmail.com', 'Josue');     // Add a recipient
	//$mail->addAddress('robertozarco17@gmail.com', 'Roberto');               // Name is optional
  $mail->addAddress("$correo");               // Name is optional
  //$mail->addAddress("jleon@dgae.unam.mx");
	//$mail->addReplyTo('rherrera@dgae.unam.mx', 'Information');

  //con copia a
	//$mail->addCC('jleon@dgae.unam.mx');
  //$mail->addCC('esanta@dgae.unam.mx');

  //con copia oculta a
  $mail->addBCC('jleon@dgae.unam.mx');
	//$mail->addBCC('esanta@dgae.unam.mx');



  //$mail->AddCustomHeader( "X-Confirm-Reading-To: zarco_860117@hotmail.com" );
  //$mail->AddCustomHeader( "Return-receipt-to: rherrera@dgae.unam.mx" );

	//$mail->addAttachment('Citas/Instructivo.pdf');         // Add attachments
	$mail->addAttachment($pdf);         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'AVISO IMPORTANTE DGAE-MADEMS';

	$message = utf8_decode('
  <table border="0" cellspacing="0" cellpadding="0">
    <tr style="width="100%;">
      <td>
        <div style="text-align: center;">
          <table cellspacing="0" cellpadding="0" border="0" style="border:none; padding:0 18px; margin:50px auto; width:65%">

            <tr style="background: #1c3d6c; height:90px;">
              <td style="text-align: left; width: 15%">
                <img height="70" title="UNAM"
                style="border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left;"
                src="https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png">
              </td>

              <td style="text-align: center;width: 10%">

              </td>

              <td style="text-align: right;width: 10%">

              </td>
            </tr>

            <!--cuerpo del correo-->
            <tr width="100%">
              <td valign="top" align="left" style="border-bottom-left-radius:4px; border-bottom-right-radius:4px; background:#f6f6f6; padding:18px" colspan="3"><br><br><br>

                <p align="center" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  <b>Favor de hacer caso omiso al correo anterior</b>
                </p>

                <p align="center" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  <b>LA UNIVERSIDAD NACIONAL AUT&Oacute;NOMA DE M&Eacute;XICO</b>
                </p>

                <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  A trav&eacute;s de la Direcci&oacute;n General de Administraci&oacute;n Escolar y la Coordinaci&oacute;n de Estudios de Posgrado,
                  dependientes de la Secretar&iacute;a General, emiten el presente aviso:
                </p>

                <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  Conforme a lo dispuesto en el Art&iacute;culo 7° del Reglamento General de Estudios de Posgrado mediante el cual se establecen
                  los requisitos para que los aspirantes puedan ingresar a un programa de posgrado; nos es grato notificarte que has sido
                  aceptado para cursar la Maestr&iacute;a en Docencia para la Educaci&oacute;n Media Superior (MADEMS) para el semestre 2018-1.
                </p>

                <p align="justify" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  Por lo anterior, y para dar continuidad al proceso de inscripci&oacute;n correspondiente, deber&aacute;s cumplir con los requisitos
                  que especifica el documento anexo a trav&eacute;s del cual se te otorga una cita para la entrega documental y toma de fotograf&iacute;a,
                  firma digitalizada y huella digital. Es importante mencionar que deber&aacute;s presentarte puntualmente el d&iacute;a y la hora
                  establecidos para tal fin y presentar la documentaci&oacute;n que se te solicite.
                </p>

                <p align="center" style="font:15px/1.25em &#39;Helvetica Neue&#39;,Arial,Helvetica;color:#333">
                  <a href="https://ingreso.dgae.unam.mx/ent_doc_pos/views/mail/home/enterado.php?account_number='.$account_number.'">
                    Dar click aqui de enterado.
                  </a>
                </p>

              </td>
            </tr>
            <!--fin cuerpo del correo-->

            <!--footer-->
            <tr width="100%" height="60px" >
              <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center;" >
                <img width="100px" height="20px" title="UNAM"
                style="border-top-left-radius:0px;background:#3b4042;padding:12px 18px;text-align:left"
                src="https://ingreso.dgae.unam.mx/entregadocumentalposgrado/assets/img/CEP.png" style="width: 200px" height="60px">

                <p style="text-align: center; color: #FFF; font-size: 12px;">
                  Coordinaci&oacute;n de Estudios de Posgrado
                </p>
              </td>

              <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center;">
                <p style="text-align: center; color: #FFF;">
                  ATENTAMENTE
                </p>
              </td>

              <td valign="top" align="center" style="background:#3b4042;padding:12px 18px;text-align:center; font-size: 12px;">
                <img height="70" title="UNAM" style="width: 30%; height: 50%"
                src="https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png">
                <p style="text-align: center; color: #FFF;">
                  Direcci&oacute;n General de Administraci&oacute;n Escolar
                </p>
              </td>
            </tr>
            <!--fin footer-->

          </table>
        </div>
      </td>
    </tr>
  </table>
  ');

  //echo $message;

	$mail->Body = $message;

	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  //echo $account_number."<br>";


	if(!$mail->send()) {
		//echo 'Message could not be sent.';
		echo "Mailer Error: $account_number -> $correo " . $mail->ErrorInfo ."<br>";
		fputs($fp, "$account_number error al enviar correo -> $correo  ".date("d-m-Y h:i:s") ." ". $mail->ErrorInfo."\n");
	} else {
		echo "Message has been sent: $account_number <br> ";
    fputs($fp, "$account_number correo enviado -> $correo ".date("d-m-Y h:i:s" )."\n");
	}

  $mail->clearAddresses();
  $mail->clearAttachments();

  $correo         = null;
  $account_number = null;
  //$pdf            = null;


}






?>