$(document).ready(function() {

	Concurso();
	Periodo();
	TipoIngreso();
	Sistema();

	var Pais        = '';
	var Estado      = '';
	var Municipio   = '';
	var Concurso    = '';
	var Periodo     = '';
	var tipoIngreso = '';
	var Sistema     = '';

	$("#selectContinente").change(function(event) {

		var continente = $("#selectContinente").val();

		$.ajax({
			url: '../../../controllers/admin/catalogs_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"continente": continente, searchConti: "si"},
			beforeSend: function(){
				//console.log("beforeSend");
				//console.log(data);
			},
			success: function(data){
				//console.log("successSend");

				Paises = jQuery.parseJSON(data);

				$("#labelPais").show();

			 	$("#selectPais").html("<select name='' id='selectedPais' class='form-control' required='required'>"+
                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

				for(var i = 0; i < Paises.length; i++){
					//console.log(Paises[i]['pais']);
					$("#selectedPais").append(new Option(Paises[i]['pais'], Paises[i]['cvePais']));
				}


				$("#selectedPais").change(function(event) {
					Pais = $("#selectedPais").val();

					//console.log(Pais);

					if(Pais == 260){
						$.ajax({
							url: '../../../controllers/admin/catalogs_controller.php',
							type: 'POST',
							dataType: 'html',
							data: {"Pais": Pais, searchPais: "si"},
							beforeSend: function(){

							},
							success: function(data){

								Estados = jQuery.parseJSON(data);

								//console.log(Estados);

								$("#labelEstado").show();

							 	$("#selectEstado").html("<select name='' id='selectedEstado' class='form-control' required='required'>"+
				                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

								for(var j = 0; j < Estados.length; j++){
									//console.log(Estados[j]['estado']);
									$("#selectedEstado").append(new Option(Estados[j]['estado'], Estados[j]['cveEstado']));
								}


								$("#selectedEstado").change(function(event) {
									Estado = $("#selectedEstado").val();

									$.ajax({
										url: '../../../controllers/admin/catalogs_controller.php',
										type: 'POST',
										dataType: 'html',
										data: {"Estado": Estado, searchEstado: "si"},
										beforeSend: function(){

										},
										success: function(data){
											Municipios = jQuery.parseJSON(data);

											//console.log(Estados);

											$("#labelMunicipio").show();

										 	$("#selectMunicipio").html("<select name='' id='selectedMunicipio' class='form-control' required='required'>"+
							                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

											for(var k = 0; k < Municipios.length; k++){
												//console.log(Estados[j]['estado']);
												$("#selectedMunicipio").append(new Option(Municipios[k]['municipio'], Municipios[k]['cveMunicipio']));
											}


											$("#selectedMunicipio").change(function(event) {
												Municipio = $("#selectedMunicipio").val();
												//console.log(Municipio);
											});
										}
									})

								});
							}
						})
					}
				});
			}
		})
	});

	function Concurso(){
		$.ajax({
			url: '../../../controllers/admin/catalogs_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"searchConcurso": 'si'},
			beforeSend: function(){

			},
			success: function(data){

				var Concursos = jQuery.parseJSON(data);

			 	$("#selectConcurso").html("<select name='' id='selectedConcurso' class='form-control' required='required'>"+
                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

				for(var i = 0; i < Concursos.length; i++){
					//console.log(Concursos[i]['pais']);
					$("#selectedConcurso").append(new Option(Concursos[i]['descConcurso'], Concursos[i]['cveConcurso']));
				}

				$("#selectedConcurso").change(function(event) {

					Concurso = $("#selectedConcurso").val();

					//console.log("concurso seleccionado -> "+Concurso);


				});


			}
		})
	}

	function Periodo(){
		$.ajax({
			url: '../../../controllers/admin/catalogs_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"searchPeriodo": 'si'},
			beforeSend: function(){

			},
			success: function(data){
				Periodos = jQuery.parseJSON(data);

				$("#selectPeriodo").html("<select name='' id='selectedPeriodo' class='form-control' required='required'>"+
                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

				for(var i = 0; i < Periodos.length; i++){
					//console.log(Concursos[i]['pais']);
					$("#selectedPeriodo").append(new Option(Periodos[i]['periodo'], Periodos[i]['periodo']));
				}

				$("#selectedPeriodo").change(function(event) {
					Periodo = $("#selectedPeriodo").val();
				});
			}
		})
	}

	function TipoIngreso(){
		$.ajax({
			url: '../../../controllers/admin/catalogs_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"searchTipoIngreso": 'si'},
			beforeSend: function(){

			},
			success: function(data){
				TiposIngreso = jQuery.parseJSON(data);

				$("#selectTipoIngreso").html("<select name='' id='selectedTipoIngreso' class='form-control' required='required'>"+
                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

				for(var i = 0; i < TiposIngreso.length; i++){
					//console.log(Concursos[i]['pais']);
					$("#selectedTipoIngreso").append(new Option(TiposIngreso[i]['descTipoIngreso'], TiposIngreso[i]['tipoIngreso']));
				}

				$("#selectedTipoIngreso").change(function(event) {
					TipoIngreso = $("#selectedTipoIngreso").val();
					//console.log("Tipo ingreso seleccionado ->"+TipoIngreso);
				});
			}
		})
	}

	function Sistema(){
		$.ajax({
			url: '../../../controllers/admin/catalogs_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"searchSistema": 'si'},
			beforeSend: function(){

			},
			success: function(data){
				var Sistemas = jQuery.parseJSON(data);
				$("#selectSistema").html("<select name='' id='selectedSistema' class='form-control' required='required'>"+
                     							"<option value='x' disabled='' selected=''>SELECCIONA</option>");

				for(var i = 0; i < Sistemas.length; i++){
					//console.log(Concursos[i]['pais']);
					$("#selectedSistema").append(new Option(Sistemas[i]['descSistema'], Sistemas[i]['cveSistema']));
				}

				$("#selectedSistema").change(function(event) {
					Sistema = $("#selectedSistema").val();
					//console.log("Sistema seleccionado ->"+Sistema);
				});
			}
		})
	}

	/*
	//098263
	$("#referenciaSearch").keypress(function(event) {
		var referencia = $("#referenciaSearch").val();

		$.ajax({
			url: '../../../controllers/admin/user_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"referencia": referencia, "searchAspi": "si"},
			beforeSend: function(){
				//$("#resultadoAspirante").empty();

			},
			success: function(data){
				//console.log(data);
				$("#resultadoAspirante").empty();
				$("#resultadoAspirante").append(data);
				//var obj = jQuery.parseJSON(data);


				$('a[id^="referencias"]').click(function(event) {
					event.preventDefault();
					$("#resultadoAspirante").empty();

					var dato = $(this).attr('data-content');

					var campo = dato.split("|");

					$("#referencia").val();
					$("#paterno").val();
					$("#materno").val();
					$("#nombre").val();
					$("#correo").val();
					$("#telefono").val();
					$("#lugar_residencia").val();
					$("#periodo").val();
					$("#tipo_ingreso").val();
					$("#sistema").val();
					$("#concurso").val();

					var referencia            = campo[0];
					var paterno          = campo[1];
					var materno          = campo[2];
					var nombre           = campo[3];
					var correo           = campo[4];
					var telefono         = campo[5];
					var lugar_residencia = campo[6];
					var periodo          = campo[7];
					var tipo_ingreso     = campo[8];
					var sistema          = campo[9];
					var concurso         = campo[10];

					$("#referencia").val(referencia);
					$("#paterno").val(paterno);
					$("#materno").val(materno);
					$("#nombre").val(nombre);
					$("#correo").val(correo);
					$("#telefono").val(telefono);
					$("#lugar_residencia").val(getEstado(lugar_residencia));
					$("#periodo").val(periodo);
					$("#tipo_ingreso").val(tipo_ingreso);
					$("#sistema").val(sistema);
					$("#concurso").val(concurso);

				});
			}
		})
	});
	*/

	$("#btnCreateUser").click(function(event) {
		event.preventDefault();

		alert("hola");

		$("#create_result").empty();


		var referencia       = $("#referencia").val();
		var paterno          = $("#paterno").val();
		var materno          = $("#materno").val();
		var nombre           = $("#nombre").val();
		var selectGenero     = $("#selectGenero").val();
		var fechaNacimiento  = $("#fechaNacimiento").val();
		var curp             = $("#CURP").val();
		var calle            = $("#calle").val();
		var numExt           = $("#numExt").val();
		var numInt           = $("#numInt").val();
		var colonia          = $("#colonia").val();
		var cp               = $("#cp").val();
		var correo           = $("#correo").val();
		var selectContinente = $("#selectContinente").val();
		//Pais
		//Estado
		//Municipio
		//var periodo          = $("#periodo").val();
		//var tipo_ingreso     = $("#tipo_ingreso").val();
		//var sistema          = $("#sistema").val();
		//var concurso         = $("#concurso").val();
		var login            = $("#login").val();
		var pass             = $("#pass").val();
		var pass2            = $("#pass2").val();
		var fechaAlta        = $("#fechaAlta").val();
		var activo           = $("#activo").val();

		/*
		console.log(referencia+"|"+paterno+"|"+materno+"|"+nombre+"|"+selectGenero+"|"+calle+"|"+
								numExt+"|"+numInt+"|"+colonia+"|"+Pais+"|"+Estado+"|"+Municipio+"|"+cp+"|"+
								correo+"|"+login+"|"+pass+"|"+pass2+"|"+Concurso+"|"+Periodo+"|"+TipoIngreso+"|"+Sistema+"|"+activo);
		*/


    if ( pass == pass2){

			$.ajax({
				url: '../../../controllers/admin/aspirante_controller.php',
				type: 'POST',
				dataType: 'html',
				data: {
								"referencia":referencia,
								"paterno":paterno,
								"materno":materno,
								"nombre":nombre,
								"fechaNacimiento":fechaNacimiento,
								"curp":curp,
								"selectGenero":selectGenero,
								"calle":calle,
								"numExt":numExt,
								"numInt":numInt,
								"colonia":colonia,
								"Pais":Pais,
								"Estado":Estado,
								"Municipio":Municipio,
								"cp":cp,
								"correo":correo,
								"login":login,
								"pass":pass,
								"concurso":Concurso,
								"periodo": Periodo,
								"tipoIngreso": TipoIngreso,
								"sistema":Sistema,
								"activo":activo,
								"createUser": "si",
							},
				beforeSend: function(){
				},
				success: function(data){

					var createUsers = jQuery.parseJSON(data);

					if(createUsers['usuario'] == "creado"){
						$("#create_result").append("<div class='alert alert-success'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																					"<strong>Usuario " + login + " creado correctamente</strong>;"+
																				"</div>");
					}else{
						$("#create_result").append("<div class='alert alert-danger'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																					"<strong>Error al crear al usuario " + login + " </strong>"+
																				"</div>");
					}

				}
			});


		}else{
			$("#create_result").append("<div class='alert alert-warning'>"+
				                          "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
				                          "<strong>Las contraseñas deben ser iguales</strong>"+
				                          "</div>" );
		}
	});

	$('button[id^="btnEditUser"]').click(function(event) {
		var id = $(this).attr('value');

		$.ajax({
			url:      '../../../controllers/admin/user_controller.php',
			type:     'POST',
			dataType: 'html',
			data:     {"id": id, "editUser": "si"},
			beforeSend: function(){
			},
			success: function(data){

				var datos = jQuery.parseJSON(data);
				//console.log(datos['referencia']);

				//console.log(datos['login']);

				//si es administrador
				if (datos['perfil'] == 1) {
					var perfil = 	"<option value='" + datos['perfil'] + "' selected=''>Administrador</option>"+
											 	"<option value='" + datos['perfil'] + "'>Aspirante</option>";
				}else{
					//si es aspirante
					var perfil = 	"<option value='" + datos['perfil'] + "'>Administrador</option>"+
											 	"<option value='" + datos['perfil'] + "' selected=''>Aspirante</option>";
				}

				var activo="";

				if (datos['usuario_activo'] == 1){
				  var activo = "checked";
				}

				// Limpiar el contenedor
				$("#maincontent").empty();
				var $newdiv = $("<div class='container'>" +
													"<div class='row'>" +
														"<div class='col-sm-6'>" +
															" <div class='col-sm-8'>" +
																"<form action='' method='POST' class='form-horizontal' role='form'>" +
																	"<div class='form-group'>" +
																		"<legend>Editar Usuario</legend>" +
																		"<br>" +
																	"</div>" +

																	"<div class='form-group'>" +
																		"<label class='' for='referencia'>Referencia:</label>" +
																		"<div class='alert alert-info' role='alert'>" + datos['referencia'] +
																		"</div>" +
																	"</div>" +
																	"<div id='resultadoAspirante'>" +
																	"</div>" +

																"</form>" +
															"</div>" +
														"</div>" +

														"<div class='col-sm-6'>" +
															"<div class='col-sm-12'>" +
																"<form action='' id='formCreateUser' method='POST' class='form-horizontal' role='form'>"+
																	"<div class='form-group'>" +
																		"<legend>Datos del Usuario</legend>" +
																		"<br/>" +
																	"</div>" +
																	"<div class='form-group'>" +
																	  // referencia
																		"<div class='col-sm-6'><label class='' for='referencia'>referencia:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='referencia' value='"+datos['referencia']+"'></div>" +
																		// Apellido paterno
																		"<div class='col-sm-6'><label class='' for='paterno'>Paterno:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='paterno' value='"+datos['paterno']+"'></div>" +
																		// Appellido materno
																		"<div class='col-sm-6'><label class='' for='materno'>Materno:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='materno' value='"+datos['materno']+"'></div>" +
																		// Nombre
																		"<div class='col-sm-6'><label class='' for='nombre'>Nombre:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='nombre' value='"+datos['nombre']+"'></div>" +
																		// Correo electrónico
																		"<div class='col-sm-6'><label class='' for='correo'>Correo:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='correo' value='"+datos['correo']+"'></div>" +
																		// Teléfono
																		"<div class='col-sm-6'><label class='' for='telefono'>Teléfono:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='telefono' value='"+datos['telefono']+"'></div>" +
																		// Lugar de residencia
																		"<div class='col-sm-6'><label class='' for='lugar_residencia'>Lugar de residencia:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='lugar_residencia' value='"+datos['lugar_residencia']+"'></div>" +
																		// Periodo
																		"<div class='col-sm-6'><label class='' for='periodo'>Periodo:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='periodo' value='"+datos['periodo']+"'></div>" +
																		// Tipo de ingreso
																		"<div class='col-sm-6'><label class='' for='tipo_ingreso'>Tipo de ingreso:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='tipo_ingreso' value='"+datos['tipo_ingreso']+"'></div>" +
																		// Sistema
																		"<div class='col-sm-6'><label class='' for='sistema'>Sistema:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='sistema' value='"+datos['sistema']+"'></div>" +
																		// Concurso
																		"<div class='col-sm-6'><label class='' for='concurso'>Concurso:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='concurso' value='"+datos['concurso']+"'></div>" +
																		// Nombre de usuario
																		"<div class='col-sm-6'><label class='' for='login'>Nombre de usuario:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='login' value='"+datos['login']+"' style='none'></div>" +
																		// Contraseña
																		"<div class='col-sm-6' style='display: none;'><label class='' for='pass'>Password:</label></div>" +
																		"<div class='col-sm-6'><input type='hidden' class='form-control' id='pass' value='"+datos['pass']+"'></div>" +
																		// Repetir contraseña
																		"<div class='col-sm-6' style='display: none;'><label class='' for='pass2'>Repetir Password:</label></div>" +
																		"<div class='col-sm-6'><input type='hidden0' class='form-control' id='pass2' value='"+datos['pass']+"'></div>" +
																		// Perfil de usuario
																		"<div class='col-sm-6'><label class='' for='perfil'>Perfil de Usuario:</label></div>" +
																		"<div class='col-sm-6'>" +
                    									"<select name='' id='perfil' class='form-control' required='required'>" +
                    										perfil+
                    									"</select>" +
                  									"</div>" +
                  									// Fecha de creación
                  									"<div class='col-sm-6'><label class='sr-only' fecha_creacionfor=''>Fecha de Creación:</label></div>" +
                  									"<div class='col-sm-6'><input type='hidden' class='form-control' id='fecha_creacion' value='"+datos['fecha_creacion']+"'></div>" +
                  									// Usuario activo:
                  									"<div class='col-sm-6'><label class='' for='usuario_activo'>Usuario Activo:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='usuario_activo' name='usuario_activo' " + activo + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +
																	"</div>" +

																	"<div class='form-group'>"+
																		"<div class='col-sm-10 col-sm-offset-2'>" +
																			"<button type='button' id='updateEditUser' class='btn btn-primary'>Actualizar</button>" +
																		"</div>" +
																	"</div>" +
																"</form>"+
																"<div id='update_result'></div>" +
															"</div>" +
														"</div>" +

													"</div>" +
												"</div>");

				$("#maincontent").append($newdiv);
				$("#updateEditUser").click(function(event) {
				event.preventDefault();
				$("#update_result").empty();
				var referencia       = datos['referencia'];
				var paterno          = $("#paterno").val();
				var materno          = $("#materno").val();
				var nombre           = $("#nombre").val();
				var correo           = $("#correo").val();
				var telefono         = $("#telefono").val();
				var lugar_residencia = $("#lugar_residencia").val();
				var periodo          = $("#periodo").val();
				var tipo_ingreso     = $("#tipo_ingreso").val();
				var sistema          = $("#sistema").val();
				var concurso         = $("#concurso").val();
				var login            = $("#login").val();
				var pass             = $("#pass").val();
				var pass2            = pass;
				var perfil           = $("#perfil").val();
				var fecha_creacion   = $("#fecha_creacion").val();
				var u_a              = $("#usuario_activo").is(":checked");

				if (u_a == true){
				  usuario_activo = 1;
				}else {
				  usuario_activo = 0;
				}

		      $.ajax({
		      url:      '../../../controllers/admin/user_controller.php',
		      type:     'POST',
		      dataType: 'html',
		      data: {
		       			"referencia"				: referencia,
								"paterno"						: paterno,
								"materno"						: materno,
								"nombre"						: nombre,
								"correo"						: correo,
								"telefono"					: telefono,
								"lugar_residencia"	: lugar_residencia,
								"periodo"						: periodo,
								"tipo_ingreso"			: tipo_ingreso,
								"sistema"						: sistema,
								"concurso"					: concurso,
								"pass"							: pass,
								"pass2"							: pass2,
								"perfil"						: perfil,
								"login" 						:login,
								"usuario_activo"		: usuario_activo,
								"updateEditUser"		: "si"},
		       	beforeSend: function(){
		       			//alert("datos: " + data);
		       	},
		       	success: function(data){
		       		//console.log("data: " + data);

		       		actualizado = jQuery.parseJSON(data);



		       		if(actualizado['actualizado'] == "ok"){
								$("#update_result").append("<div class='alert alert-success'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																					"<strong>Usuario " + login + " actualizado correctamente</strong>;"+
																				"</div>");
							}
		       	}
		       })
		       .done(function() {
		       	console.log("success");
		       })
		       .fail(function() {
		       	console.log("error");
		       })
		       .always(function() {
		       	console.log("complete");
		       });

	      });
			}
		});
	});

	$('button[id^="btnTrahsUser"]').click(function(event) {
		event.preventDefault();
		var id = $(this).attr('value');

		$.ajax({
			url: '../../../controllers/admin/user_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {
								"id": id,
								"deleteUser": "si"},
			beforeSend: function(data){
				alert("mis datos: " + id);
			},
			success: function(data){
				//alert("datos de regreso: " + data);
			},
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

});





