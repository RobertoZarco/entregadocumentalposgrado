$(document).ready(function() {

	$("#btnCreateUser").click(function(event) {
		//alert("Hola");
		$("#create_result").empty();
		var paterno           = $("#paterno").val();
		var materno           = $("#materno").val();
		var nombre            = $("#nombre").val();
		var fechaNacimiento   = $("#fechaNacimiento").val();
		var curp              = $("#CURP").val();
		var email             = $("#email").val();
		var selectGenero      = $("#selectGenero").val();

		//alert(curp);

		var selectDependencia = $("#selectDependencia").val();

		var PermisoCrearUsuario   = document.getElementById("PermisoCrearUsuario").checked;
		var PermisoCrearAspi      = document.getElementById("PermisoCrearAspi").checked;
		var PermisoEditarUsuarios = document.getElementById("PermisoEditarUsuarios").checked;
		var PermisoChat           = document.getElementById("PermisoChat").checked;
		var PermisoVerDocDig      = document.getElementById("PermisoVerDocDig").checked;
		var PermisoElimDocDig     = document.getElementById("PermisoElimDocDig").checked;
		var PermisoValiDocDig     = document.getElementById("PermisoValiDocDig").checked;
		var PermisoValiRecepDocs  = document.getElementById("PermisoValiRecepDocs").checked;
		var PermisoValiDocFis     = document.getElementById("PermisoValiDocFis").checked;
		var PermisoReport         = document.getElementById("PermisoReport").checked;

		var login            = $("#login").val();
		var pass             = $("#pass").val();
		var pass2            = $("#pass2").val();
		var fechaAlta        = $("#fechaAlta").val();
		var activo           = document.getElementById("activo").checked;

		/*
		console.log(paterno+"|"+materno+"|"+nombre+"|"+selectGenero+"|"+
			selectDependencia+"|"+PermisoCrearUsuario+"|"+PermisoCrearAspi+"|"+PermisoEditarUsuarios+"|"+
			PermisoChat+"|"+PermisoVerDocDig+"|"+PermisoElimDocDig+"|"+PermisoValiDocDig+"|"+PermisoValiRecepDocs+"|"+
			PermisoValiDocFis+"|"+PermisoReport+"|"+
			login+"|"+pass+"|"+pass2+"|"+activo);
		*/



    if ( pass == pass2){

			$.ajax({
				url: '../../../controllers/admin/aspirante_new_controller.php',
				type: 'POST',
				dataType: 'html',
				data: {
								"paterno":paterno,
								"materno":materno,
								"nombre":nombre,
								"fechaNacimiento":fechaNacimiento,
								"curp":curp,
								"email":email,
								"selectGenero":selectGenero,
								"selectDependencia":selectDependencia,
								"PermisoCrearUsuario":PermisoCrearUsuario,
								"PermisoCrearAspi":PermisoCrearAspi,
								"PermisoEditarUsuarios":PermisoEditarUsuarios,
								"PermisoChat":PermisoChat,
								"PermisoVerDocDig":PermisoVerDocDig,
								"PermisoElimDocDig":PermisoElimDocDig,
								"PermisoValiDocDig":PermisoValiDocDig,
								"PermisoValiRecepDocs":PermisoValiRecepDocs,
								"PermisoValiDocFis":PermisoValiDocFis,
								"PermisoReport":PermisoReport,
								"pass":pass,
								"login":login,
								"activo":activo,
								"createUser_admin": "si",
							},
				beforeSend: function(){
				},
				success: function(data){

					//alert(data);

					var createUsers = jQuery.parseJSON(data);

					if(createUsers['usuario'] == "creado"){
						$("#create_result").append("<div class='alert alert-success'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																					"<strong>Usuario " + login + " creado correctamente</strong>;"+
																				"</div>");
					}else{
						$("#create_result").append("<div class='alert alert-danger'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																					"<strong>Error al crear al usuario " + login + " </strong>"+
																				"</div>");
					}

				}
			});


		}else{
			$("#create_result").append("<div class='alert alert-warning'>"+
				                          "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
				                          "<strong>Las contraseñas deben ser iguales</strong>"+
				                          "</div>" );
		}

				});


});





