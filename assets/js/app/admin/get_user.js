$(document).ready(function() {

	$('button[id^="btnVisuaDocs"]').click(function(event){
		//var referencia      = $(this).attr('value');
		var account_number  = $(this).attr('value');
		var id_aspirant     = $(this).attr('data-id_aspirant');
		var nombre          = $(this).attr('data-nombre');
		var paterno         = $(this).attr('data-paterno');
		var materno         = $(this).attr('data-materno');
		//var exa_prop        = $(this).attr('data-cveExaPropedeutic');
		var exa_prop        = $(this).attr('data-validate');
		var correoAspirante = $(this).attr('data-correo');

		var nombreCompleto = nombre+" "+paterno+" "+materno;

		//alert(materno);

		//--- PROPEDEUTICO
		var status_proped     = null;
		var icon_proped       = null;
		var icon_color_proped = null;

		if(exa_prop == 1){
			status_proped     = "checked";
			icon_proped       = "ok";
			icon_color_proped = "449d44";
			var disabled      = "";
		}else{
			icon_proped       = "remove";
			icon_color_proped = "FF0000";
			var disabled      = "";
		};


		var modal =
		"<div class='modal fade' id='modalIdValidacion'>"+
			"<div class='modal-dialog'>"+
				"<div class='modal-content'>"+
					"<div class='modal-header'>"+
						"<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>"+
						"<h4 class='modal-title'>Modal title</h4>"+
					"</div>"+
					"<div class='modal-body'>"+
						"Se hará el envío del comprobante de validación de documentos "+
						"estas seguro?"+
					"</div>"+
					"<div class='modal-footer'>"+
						"<button type='button' class='btn btn-default' data-dismiss='modal'>Cancelar</button>"+
						"<button type='button' class='btn btn-primary' id='savePDFComprobante' >Enviar Comprobante</button>"+
					"</div>"+
				"</div>"+
			"</div>"+
		"</div>";

		var modal_validacion = $("#modal_validacion");

		var mymodal= modal_validacion.append(modal);

		var tabla1 =  $('<table id="Proped" />').addClass("table table-bordered table-condensed table-hover table-striped");
		var encabezado1 = $('<thead />').html("<tr>"+
																						"<th>Tipo</th>"+
																						"<th>Status</th>"+
																						"<th>Validar</th>");
		encabezado1.appendTo($(tabla1));

		//var fila1 = $('<tr id="tabla-ValProp"/>').html("<td>Examen Propedéutico</td>"+
		//							"<td align='center'><span class='glyphicon glyphicon-"+icon_proped+"' id='show_status-exm-prop' aria-hidden='true' style='color:#"+icon_color_proped+"' ></span></td>" +
		//							"<td align='center'><input type='checkbox' id='check-exm-prop' value='exa_prop' "+status_proped+"></label></td>");



		var fila1 = $('<tr id="tabla-Validado"/>').html("<td>Validado</td>"+
									"<td align='center'>"+
										"<span class='glyphicon glyphicon-"+icon_proped+"' id='show_status-validado' aria-hidden='true' style='color:#"+icon_color_proped+"' ></span>"+
									"</td>" +
									"<td align='center'>"+
										"<input type='checkbox' id='check-validado' value='validado' "+status_proped+ " data-toggle='modal' href='#modalIdValidacion'"+disabled+">"+
									"</td>");

		fila1.appendTo($(tabla1));
		tabla1.appendTo(".table-responsive")

		$("#Proped").html(tabla1);



		$("#savePDFComprobante").click(function(event) {

			var checked = $("#check-validado").is(":checked");
		  //var idchecked = $("#check-exm-prop").val();
			if (checked == false){
				$("#show_status-validado").removeClass().addClass( 'glyphicon glyphicon-remove' );
				$("#show_status-validado").css({color: '#FF0000'});
			}else{
				$("#show_status-validado").removeClass().addClass( 'glyphicon glyphicon-ok' );
				$("#show_status-validado").css({color: '#449d44'});
			};

			// llamamos al controller
			//para marcar la validacion en mysql
			$.ajax({
				url: '../../../controllers/admin/updateDocument_controller.php',
				type: 'POST',
				dataType: 'html',
				data: { "StatusChecked-validado": checked, "account_number": account_number },
				beforeSend: function(){

				},
				success: function(data){
					//console.log(data);
					$('#modalIdValidacion').modal('toggle');

					$.ajax({
						url: '../../../controllers/admin/pdfValidacion_controller.php',
						type: 'POST',
						dataType: 'html',
						data: { "pdfValidacion": 'si',
										"id_aspirant":id_aspirant,
										"nombre":nombre,
										"paterno":paterno,
										"materno":materno,
										"email":correoAspirante
										},
						success: function(data){
							//alert(data);
							//var pdf = jQuery.parseJSON(data);

							//if(pdf['pdf'] == 'creado'){
							//}

						},
						beforeSend: function(){

						}
					})
					.done(function() {
						console.log("success");
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});

				},
			})
		});

		///-- Fin Prop

		//Permisos
		var visualizarDocDigital = $("#visualizarDocDigital").val();
		var eliminarDocDigital   = $("#eliminarDocDigital").val();
		var validarDocDigital    = $("#validarDocDigital").val();
		var recepcionDoc         = $("#recepcionDoc").val();
		var validarDocFisico     = $("#validarDocFisico").val()

		//Condiciones permisos
		if(visualizarDocDigital == 1){
			var head_Ver_Dig = "<th>Ver</th>";
			var body_Ver_Dig = "";
		}else{
			var head_Ver_Dig = null;
			var body_Ver_Dig = "none";
		};

		if(validarDocDigital == 1){
			var head_Val_Dig = "<th>Validar Digital</th>";
			var body_Val_Dig = "";
		}else{
			var head_Val_Dig = null;
			var body_Val_Dig = "none";
		};

		if(recepcionDoc == 1){
			var head_Val_Recep = "<th>Confirmar Recepción</th>";
			var body_Val_Recep = "";
		}else{
			var head_Val_Recep = null;
			var body_Val_Recep = "none";
		};

		if(validarDocFisico == 1){
			var head_Val_Fis = "<th>Validar Físico</th>";
			var body_Val_Fis = "";
		}else{
			var head_Val_Fis = null;
			var body_Val_Fis = "none";
		};

		if(eliminarDocDigital == 1){
			var head_Eliminar = "<th>Eliminar</th>";
			var body_Eliminar = "";
		}else{
			var head_Eliminar = null;
			var body_Eliminar = "none";
		};

		$(this).removeClass('btn-default');
		$(this).addClass('btn-success');

		$.ajax({
			url: '../../../controllers/admin/getUsers_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"account_number": account_number, "searchUsers": "si"},
			beforeSend: function(datos){
			},
			success: function(data){
				//alert(data);
				$(".table-responsive").empty();
				var datos          = jQuery.parseJSON(data);
				var noDatos        = datos.length;

				if(datos[0]['idDocumentos'] != "no hay documentos"){
					var datosAspirante = null;
					$("#Bienvenido").text("Documentos de aspirante: " + nombre +" "+ paterno + " " + materno);

					var ind_checked = null;

					var tabla =  $('<table/>').addClass("table table-bordered table-condensed table-hover table-striped");

					var encabezado = $('<thead />').html("<tr>"+
																									"<th>No</th>"+
																									"<th>Tipos</th>"+
																									head_Ver_Dig+
																									"<th>Guardado</th>"+
																									"<th>Status Digital</th>"+
																									"<th>Status Físico</th>"+
																									head_Val_Dig+
																									head_Val_Fis+
																								"</tr>");
					encabezado.appendTo($(tabla));

					$.each(datos,function(index, element) {
						var idCatTipoDocumento = null;

						switch(element.idCatTipoDocumento){
							case '1':
								idCatTipoDocumento = "Acta";
								break;

							case '2':
								idCatTipoDocumento = "Curp";
								break;

							case '3':
								idCatTipoDocumento = "CertSec";
								break;

							case '4':
								idCatTipoDocumento = "CertBach";
								break;

							case '5':
								idCatTipoDocumento = "Cita";
								break;

							case '6':
								idCatTipoDocumento = "F69PDF";
								break;

							case '7':
								idCatTipoDocumento = "Firma";
								break;

							case '8':
								idCatTipoDocumento = "ComproDom";
								break;

							case '9':
								idCatTipoDocumento = "IdenOfic";
								break;

							case '10':
								idCatTipoDocumento = "ComproPagoPDF";
								break;
						}

						var ind              =  index + 1;
						//var status_doc     = null;
						var icon             = null;
						var icon_color       = null;
						var status           = null;
						var status_fis       = null;
						var status_recep     = null;
						var icon_color_recep = null;

						//Diferentes estados de validacion

						if (element.status_validacion_digital == 1){
							status      = "checked";
							icon        = "ok";
							icon_color  = "449d44";
							ind_checked = ind_checked +1;
					  }else{
							icon       = "remove";
							icon_color = "FF0000";
					  };


					  //Preguntamos por el estado de recepción
						if (element.status_validacion_recepcion == 1){
							status_recep     = "checked";
							icon_recep       = "ok";
							icon_color_recep = "449d44";
					 		//ind_checked_fis = ind_checked +1;
					  }else{
							icon_recep       = "remove";
							icon_color_recep = "FF0000";
					  };

					 	//Preguntamos por el estado de validacion digilal
						if (element.status_validacion_fisico == 1){
							status_fis     = "checked";
							icon_fis       = "ok";
							icon_color_fis = "449d44";
					 		//ind_checked_fis = ind_checked +1;
					  }else{
							icon_fis       = "remove";
							icon_color_fis = "FF0000";
					  };

						var fila = $('<tr id="tabla-'+idCatTipoDocumento+'"/>').html("<td>"+ ind + "</td>"+
														"<td>"+element.tipoDocumento +"</td>"+
														"<td align='center' style='display:"+body_Ver_Dig+";'>"+
															"<button class='btn btn-default' id='file-"+idCatTipoDocumento+"' value='"+element.ruta_doc+"' style='display:"+body_Ver_Dig+";' >"+
																"<span class='glyphicon glyphicon-file' aria-hidden='true' style='color:#337ab7;display:"+body_Ver_Dig+";'></span>" +
															"</button>"+
														"</td>"+
														"<td align='center'>"+
															"<span class='glyphicon glyphicon-ok' aria-hidden='true' style='color:#449d44'></span>"+
														"</td>" +
														"<td align='center'>"+
															"<span class='glyphicon glyphicon-"+icon+"' id='show_status-dig-"+idCatTipoDocumento+"' aria-hidden='true' style='color:#"+icon_color+"' ></span>"+
														"</td>"+
														"<td align='center'>"+
															"<span class='glyphicon glyphicon-"+icon_fis+"' id='show_status-fis-"+idCatTipoDocumento+"' aria-hidden='true' style='color:#"+icon_color_fis+"'></span>"+
														"</td>" +
														"<td align='center' style='display:"+body_Val_Dig+";'>"+
															"<input type='checkbox' id='check-dig-"+idCatTipoDocumento+"' value='"+element.idDocumentos+"' "+status+" style='display:"+body_Val_Dig+";'></label>"+
														"</td>" +
														"<td align='center' style='display:"+body_Val_Fis+";'>"+
															"<input type='checkbox' id='check-fis-"+idCatTipoDocumento+"' value='"+element.idDocumentos+"' "+status_fis+" style='display:"+body_Val_Fis+";'></label>"+
														"</td>"
												);

						fila.appendTo($(tabla));
					});

					tabla.appendTo(".table-responsive");



					/*

					$.ajax({
						url: '../../../controllers/admin/getUsers_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"documents": 'si'},
						beforeSend: function(){

						},
						success:function(data){
							var responseTypeDocuments = jQuery.parseJSON(data);

							$("#selectTypeDocument").empty();

						 	var selectTypeDocument1 = "<select name='' id='selectTypeDoc' class='form-control' required='required'>";
						 	$("#selectTypeDocument").append(selectTypeDocument1);

							$.each(responseTypeDocuments, function(index, val) {
								  iterate through array or object
								 //alert(index+"|"+val.id_type_document+"|"+val.document);

								selectTypeDocument2 = "<option value='"+val.id_type_document+"'>"+val.document+"</option>";
								var selectTypeDocument = selectTypeDocument2;
								$("#selectTypeDoc").append(selectTypeDocument);

							});

							selectTypeDocument3 = "</select>";

							$("#selectTypeDoc").append(selectTypeDocument3);

						}

					});
					*/







					$('input[id^="checkChangeTypeDocument"]').click(function(event) {
							var idDocument = $(this).attr('data-idDocument');
							var idAspirant = $(this).attr('data-idAspirant');

							alert(idDocument+"|"+idAspirant);

							/*
							$(".edit_tipo_documento").bind('click',function(event) {
				        var id_documento    = $(this).attr('value');
				        var num_docu = $(this).attr('name');

				        //console.log(num_docu);

				        if($(".edit_tipo_documento").is(":checked") ){
				            $("#modifTipoDocu"+num_docu).css({display: 'block'});
				        }else{
				            $("#modifTipoDocu"+num_docu).css({display: 'none'});
				        }
					    });
					    */
					});

					$("#correo").append("	<legend>Envio de correo: "+correoAspirante+"</legend>"+
            										"<textarea cols=100 rows=10 id='message'>"+
            										"</textarea>"+
            										"<button class='btn btn-primary' id='sendMail'>"+
            											"<span class='glyphicon glyphicon-send' aria-hidden='true'></span>"+
            											"Enviar"+
            										"</button>");

					//envio de correo
					$("#sendMail").click(function(event) {
						var mensaje = $("#message").val();

						$.ajax({
							url: '../../../controllers/admin/getUsers_controller.php',
							type: 'POST',
							dataType: 'html',
							data: {	"message": mensaje,
											"correo": correoAspirante,
											"nombreCompleto":nombreCompleto,
											"sendMail":"si",
											"idAspirant": id_aspirant
										},
							beforeSend: function(){

							},
							success: function(data){
								//alert(data);

								var respuesta          = jQuery.parseJSON(data);
								//console.log(respuesta);

								//console.log(respuesta['email']);

								if(respuesta['email'] == 'enviado'){
									$("#correo").append("	<div class='alert alert-success'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>"+
																					"<strong>Correo Enviado Correctamente</strong>"+
																				"</div>");
									//$("#message").empty();
									//$("#message").text();
									$("#message").val('');

								}else{
									$("#correo").append("	<div class='alert alert-danger'>"+
																					"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>"+
																					"<strong>Error al envia el correo </strong>"+
																				"</div>");
								}

								//alert(data);

							}
						})
						.done(function() {
							//console.log("success");
						})
						.fail(function() {
							//console.log("error");
						})
						.always(function() {
							//console.log("complete");
						});
					});


					//historial de mensajes enviados
					var history = "<button id='btnMessageHistory' type='button' class='btn btn-default'>"+
													"Ver Historial de mensajes"+
												"</button>";

					$("#history").append(history);


					$("#btnMessageHistory").click(function(event) {
						$.ajax({
							url: '../../../controllers/admin/getUsers_controller.php',
							type: 'POST',
							dataType: 'html',
							data: {	"idAspirant": id_aspirant,
											"historyMessages": 'si'},
							beforeSend: function(){

							},success: function(data){
								alert(data);
							}
						})
						.done(function() {
							//console.log("success");
						})
						.fail(function() {
							//console.log("error");
						})
						.always(function() {
							//console.log("complete");
						});

					});

				}else{
					var error =  $("#errores_home").html("<div class='alert alert-danger'>"+
																											"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>"+
																											"Aspirante sin <strong>documentos</strong>"+
																										"</div>");
				}


				$("#file-Curp").click(function(event) {
					//alert ("hola");
					$("#file-Curp").removeClass('btn btn-default').addClass( 'btn btn-success' );
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    //Traemos ID de documento
			    //var idDocFrame = $("#delete-Curp").val();
			    //crearFrame("../../../models/frame_pdf_model.php?id="+idDocFrame);
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#trashDocFromAdmin").click(function(event) {
					alert("Borrar documento? ");
				});

				$("#file-Acta").click(function(event){
					$("#file-Acta").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
			    var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-CertSec").click(function(event) {
					$("#file-CertSec").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
			    var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-CertBach").click(function(event) {
					$("#file-CertBach").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
			    var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-Cita").click(function(event) {
					$("#file-Cita").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-F69PDF").click(function(event) {
					$("#file-F69PDF").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-Firma").click(function(event) {
					$("#file-Firma").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-ComproDom").click(function(event) {
					$("#file-ComproDom").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-IdenOfic").click(function(event) {
					$("#file-IdenOfic").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproPagoPDF").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});

				$("#file-ComproPagoPDF").click(function(event) {
					$("#file-ComproPagoPDF").removeClass('btn btn-default').addClass( 'btn btn-success' );
					$("#file-Curp").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Acta").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertSec").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-CertBach").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Cita").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-F69PDF").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-Firma").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-ComproDom").removeClass('btn btn-success').addClass('btn btn-default');
			    $("#file-IdenOfic").removeClass('btn btn-success').addClass('btn btn-default');
					var docPDF = $(this).val();
					crearFrame("../../../"+docPDF);
				});


				//--- Botones eliminar DOCS
				$("#delete-Curp").click(function(event) {
					var deleteDoc = $("#delete-Curp").val();
					//alert("Curp id ="+deleteDoc);
			    $.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	//console.log(data);
			      	//alert(data);
			      	//var deleteDoc = jQuery.parseJSON(data);
			        $("#delete-Curp").hide();
			        $("#tabla-Curp").remove();
			      }
			    })
				});

				$("#delete-Acta").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-Acta").hide();
			      	$("#tabla-Acta").remove();

			      }
			    })
				});

				$("#delete-CertSec").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-CertSec").hide();
			      	$("#tabla-CertSec").remove();

			      }
			    })
				});

				$("#delete-CertBach").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-CertBach").hide();
			      	$("#tabla-CertBach").remove();

			      }
			    })
				});

				$("#delete-Cita").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-Cita").hide();
			      	$("#tabla-Cita").remove();

			      }
			    })
				});

				$("#delete-F69PDF").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-F69PDF").hide();
			      	$("#tabla-F69PDF").remove();

			      }
			    })
				});

				$("#delete-Firma").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-Firma").hide();
			      	$("#tabla-Firma").remove();

			      }
			    })
				});

				$("#delete-ComproDom").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-ComproDom").hide();
			      	$("#tabla-ComproDom").remove();

			      }
			    })
				});


				$("#delete-IdenOfic").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-IdenOfic").hide();
			      	$("#tabla-IdenOfic").remove();

			      }
			    })
				});

				$("#delete-ComproPagoPDF").click(function(event) {
					var deleteDoc = $(this).val();
					$.ajax({
			      url: './../../../controllers/admin/deleteDocument_controller.php',
			      type: 'POST',
			      dataType: 'html',
			      data: {"idDocumentoDelete": deleteDoc, "deleteDoc": "si"},
			      beforeSend: function(){
			      },
			      success: function(data){
			      	$("#delete-ComproPagoPDF").hide();
			      	$("#tabla-ComproPagoPDF").remove();

			      }
			    })
				});



				//---- Botones de validación de recepción
				$("#check-recep-Acta").click(function(event) {
					var checked = $("#check-recep-Acta").is(":checked");
				  var idchecked = $("#check-recep-Acta").val();
				  var clave_doc_fis = '7';
				  //alert (referencia);
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-Acta").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-Acta").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-Acta").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-Acta").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(data){
							//console.log(data);
						},
					})
				});


				$("#check-recep-Curp").click(function(event) {
					var checked = $("#check-recep-Curp").is(":checked");
				  var idchecked = $("#check-recep-Curp").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-Curp").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-Curp").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-Curp").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-Curp").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-recep-CertSec").click(function(event) {
					var checked = $("#check-recep-CertSec").is(":checked");
				  var idchecked = $("#check-recep-CertSec").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-CertSec").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-CertSec").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-CertSec").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-CertSec").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-recep-CertBach").click(function(event) {
					var checked = $("#check-recep-CertBach").is(":checked");
				  var idchecked = $("#check-recep-CertBach").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-CertBach").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-CertBach").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-CertBach").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-CertBach").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-recep-Cita").click(function(event) {
					var checked = $("#check-recep-Cita").is(":checked");
				  var idchecked = $("#check-recep-Cita").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-Cita").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-Cita").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-Cita").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-Cita").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-recep-F69PDF").click(function(event) {
					var checked = $("#check-recep-F69PDF").is(":checked");
				  var idchecked = $("#check-recep-F69PDF").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-F69PDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-F69PDF").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-F69PDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-F69PDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-recep-Firma").click(function(event) {
					var checked = $("#check-recep-Firma").is(":checked");
				  var idchecked = $("#check-recep-Firma").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-Firma").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-Firma").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-Firma").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-Firma").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-recep-ComproDom").click(function(event) {
					var checked = $("#check-recep-ComproDom").is(":checked");
				  var idchecked = $("#check-recep-ComproDom").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-ComproDom").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-ComproDom").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-ComproDom").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-ComproDom").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-recep-IdenOfic").click(function(event) {
					var checked = $("#check-recep-IdenOfic").is(":checked");
				  var idchecked = $("#check-recep-IdenOfic").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-IdenOfic").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-IdenOfic").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-recep-ComproPagoPDF").click(function(event) {
					var checked = $("#check-recep-ComproPagoPDF").is(":checked");
				  var idchecked = $("#check-recep-ComproPagoPDF").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-recep-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-recep-ComproPagoPDF").css({color: '#FF0000'});
					}else{
						$("#show_status-recep-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-recep-ComproPagoPDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-recep": checked, "referencia": referencia },
						beforeSend: function(){},
						success: function(){},
					})
				});

				//----

				//---- Botones de validación física
				$("#check-fis-Acta").click(function(event) {
						var checked = $("#check-fis-Acta").is(":checked");
					  var idchecked = $("#check-fis-Acta").val();
					  //Preguntamos por el estado del check y cambiamos el indicador de validación
						if (checked == false){
							$("#show_status-fis-Acta").removeClass().addClass( 'glyphicon glyphicon-remove' );
							$("#show_status-fis-Acta").css({color: '#FF0000'});
						}else{
							$("#show_status-fis-Acta").removeClass().addClass( 'glyphicon glyphicon-ok' );
							$("#show_status-fis-Acta").css({color: '#449d44'});
						};
						// llamamos al controller
						$.ajax({
							url: '../../../controllers/admin/updateDocument_controller.php',
							type: 'POST',
							dataType: 'html',
							data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
							beforeSend: function(){},
							success: function(data){
								//console.log(data);
							},
						})
			  });

				$("#check-fis-Curp").click(function(event) {
					var checked = $("#check-fis-Curp").is(":checked");
				  var idchecked = $("#check-fis-Curp").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-Curp").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-Curp").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-Curp").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-Curp").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-fis-CertSec").click(function(event) {
					var checked = $("#check-fis-CertSec").is(":checked");
				  var idchecked = $("#check-fis-CertSec").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-CertSec").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-CertSec").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-CertSec").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-CertSec").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-fis-CertBach").click(function(event) {
					var checked = $("#check-fis-CertBach").is(":checked");
				  var idchecked = $("#check-fis-CertBach").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-CertBach").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-CertBach").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-CertBach").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-CertBach").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-fis-Cita").click(function(event) {
					var checked = $("#check-fis-Cita").is(":checked");
				  var idchecked = $("#check-fis-Cita").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-Cita").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-Cita").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-Cita").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-Cita").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-fis-F69PDF").click(function(event) {
					var checked = $("#check-fis-F69PDF").is(":checked");
				  var idchecked = $("#check-fis-F69PDF").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-F69PDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-F69PDF").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-F69PDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-F69PDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-fis-Firma").click(function(event) {
					var checked = $("#check-fis-Firma").is(":checked");
				  var idchecked = $("#check-fis-Firma").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-Firma").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-Firma").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-Firma").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-Firma").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-fis-ComproDom").click(function(event) {
					var checked = $("#check-fis-ComproDom").is(":checked");
				  var idchecked = $("#check-fis-ComproDom").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-ComproDom").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-ComproDom").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-ComproDom").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-ComproDom").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-fis-IdenOfic").click(function(event) {
					var checked = $("#check-fis-IdenOfic").is(":checked");
				  var idchecked = $("#check-fis-IdenOfic").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-IdenOfic").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-IdenOfic").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-fis-ComproPagoPDF").click(function(event) {
					var checked = $("#check-fis-ComproPagoPDF").is(":checked");
				  var idchecked = $("#check-fis-ComproPagoPDF").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-fis-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-fis-ComproPagoPDF").css({color: '#FF0000'});
					}else{
						$("#show_status-fis-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-fis-ComproPagoPDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked-Fis": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				//----

				//---- Boton de validación de estado Digital
				$("#check-dig-Acta").click(function(event) {
					var checked = $("#check-dig-Acta").is(":checked");
				  var idchecked = $("#check-dig-Acta").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-Acta").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-Acta").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-Acta").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-Acta").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(data){
							//console.log(data);
						},
					})
				});


				$("#check-dig-Curp").click(function(event) {
					var checked = $("#check-dig-Curp").is(":checked");
				  var idchecked = $("#check-dig-Curp").val();
				  //Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-Curp").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-Curp").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-Curp").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-Curp").css({color: '#449d44'});
					};
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(data){
							//console.log(data);
						},
					})
				});


				$("#check-dig-CertSec").click(function(event) {
					var checked = $("#check-dig-CertSec").is(":checked");
				  var idchecked = $("#check-dig-CertSec").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-CertSec").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-CertSec").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-CertSec").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-CertSec").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-dig-CertBach").click(function(event) {
					var checked = $("#check-dig-CertBach").is(":checked");
				  var idchecked = $("#check-dig-CertBach").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-CertBach").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-CertBach").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-CertBach").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-CertBach").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-dig-Cita").click(function(event) {
					var checked = $("#check-dig-Cita").is(":checked");
				  var idchecked = $("#check-dig-Cita").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-Cita").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-Cita").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-Cita").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-Cita").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-dig-F69PDF").click(function(event) {
					var checked = $("#check-dig-F69PDF").is(":checked");
				  var idchecked = $("#check-dig-F69PDF").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-F69PDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-F69PDF").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-F69PDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-F69PDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-dig-Firma").click(function(event) {
					var checked = $("#check-dig-Firma").is(":checked");
				  var idchecked = $("#check-dig-Firma").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-Firma").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-Firma").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-Firma").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-Firma").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});


				$("#check-dig-ComproDom").click(function(event) {
					var checked = $("#check-dig-ComproDom").is(":checked");
				  var idchecked = $("#check-dig-ComproDom").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-ComproDom").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-ComproDom").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-ComproDom").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-ComproDom").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(data){
							//console.log(data);
						},
					})
				});


				$("#check-dig-IdenOfic").click(function(event) {
					var checked = $("#check-dig-IdenOfic").is(":checked");
				  var idchecked = $("#check-dig-IdenOfic").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-IdenOfic").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-IdenOfic").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-IdenOfic").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});

				$("#check-dig-ComproPagoPDF").click(function(event) {
					var checked = $("#check-dig-ComproPagoPDF").is(":checked");
				  var idchecked = $("#check-dig-ComproPagoPDF").val();
					//Preguntamos por el estado del check y cambiamos el indicador de validación
					if (checked == false){
						$("#show_status-dig-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-remove' );
						$("#show_status-dig-ComproPagoPDF").css({color: '#FF0000'});
					}else{
						$("#show_status-dig-ComproPagoPDF").removeClass().addClass( 'glyphicon glyphicon-ok' );
						$("#show_status-dig-ComproPagoPDF").css({color: '#449d44'});
					};
					// llamamos al controller
					$.ajax({
						url: '../../../controllers/admin/updateDocument_controller.php',
						type: 'POST',
						dataType: 'html',
						data: {"idDocChecked": idchecked, "StatusChecked": checked, "id_aspirant": id_aspirant },
						beforeSend: function(){},
						success: function(){},
					})
				});
				//-----

				$("#btnTrahsUser2").click(function(event) {
					alert ("hola");
				});


				//-----

			},
		})
	});


	//$('button[id^="referencia"]').submit(function(event) {
	//	return false();
	//});


	//checkTodos
	$("#checkTodos").click(function(event) {
		if($("#checkTodos").is(":checked")){
			valor = $("#checkTodos").val();
			$("#getByDependency").val(valor);

			$("#checkCuayed").attr('checked', false);
			$("#checkFonabec").attr('checked', false);
			//$('#checkCuayed').filter(':checkbox').removeAttr('checked');
		}
	});

	//checkCuayed
	$("#checkCuayed").click(function(event) {
		if($("#checkCuayed").is(":checked")){
			valor = $("#checkCuayed").val();
			$("#getByDependency").val(valor);

			$("#checkTodos").attr('checked', false);
			$("#checkFonabec").attr('checked', false);
		}
	});

	//checkFonabec
	$("#checkFonabec").click(function(event) {
		if($("#checkFonabec").is(":checked")){
			valor = $("#checkFonabec").val();
			$("#getByDependency").val(valor);

			$("#checkTodos").attr('checked', false);
			$("#checkCuayed").attr('checked', false);

		}
	});

});



//funcion para obtener el total de los documentos de cada aspirante


//funccion para crear un iframe y mostrar el pdf
function crearFrame(pdf) {
  //var testFrame = document.createElement("IFRAME");
  var testFrame = document.createElement("iframe");

  $("#testFrame").remove();
  testFrame.id          = "testFrame";
  testFrame.width       = "100%";
  testFrame.height      = "1000px";
  testFrame.scrolling   = "no";
  testFrame.frameborder = "10px";
  testFrame.src = pdf; //Sacar el nombre del fichero pdf desde el parametro

  var control = document.getElementById("testFrame")

  if (control==null) {
      $('#pdf').append(document.body.appendChild(testFrame));
  }
}
