$(document).ready(function(){

	registrerMessages();
	countMessages();
	takeChat();

	$.ajaxSetup({"cache":false});

	setInterval("loadOldMessage()", 3000);
})

var registrerMessages = function(){
	$("#send").click(function(event) {
		event.preventDefault();

		//serialize obtiene todos los valores que estan dentro del formularios
		//var frm = $("#formChat").serialize();
		var emisor     = $("#emisor").val();
		var receptor   = $("#receptor").val();
		var message    = $("#message").val();
		var statusChat = $("#statusChat").val();

		//console.log(receptor);
		//console.log(frm);
		$.ajax({
			//url: '../../../controllers/chat_controller.php',
			url: '../../../controllers/admin/chat_controller.php',
			type: 'POST',
			dataType: 'html',
			data:{'emisor': emisor,
						'receptor': receptor,
						'message': message,
						'statusChat': statusChat,
						"insertPeticion": "si"},
			beforeSend: function(){

			},
			success: function(data){
				//console.log(data);
				$("#message").val("");
				var altura = $("#conversation").prop("scrollHeight");
				$("#conversation").scrollTop(altura);
			}
		})
	});
}

var loadOldMessage = function(){
	var emisor     = $("#emisor").val();
	var receptor   = $("#receptor").val();

	$.ajax({
		//url: '../../../controllers/chat_controller.php',
		url: '../../../controllers/admin/chat_controller.php',
		type: 'POST',
		dataType: 'html',
		data: {"emisor":emisor, "receptor": receptor,  "searchConversation": 'si'},
		beforeSend: function(){

		},
		success: function(data){
			$("#conversation").html(data);
			$("#conversation p:last-child").css({
																						"background-color": "lightgreen",
																						"padding-botton": "20px"
																					});

			//$("#message").val("");
			var altura = $("#conversation").prop("scrollHeight");
			$("#conversation").scrollTop(altura);
		}
	});
}

var countMessages = function(){

	$.ajax({
		url: '../../../controllers/admin/chat_controller.php',
		type: 'POST',
		dataType: 'html',
		data: {"countMessages": 'si'},
		beforeSend: function(){

		},
		success: function(data){
			var messages = jQuery.parseJSON(data);

			for(var i = 0; i < messages.length; i++){
				//console.log("entre");
				$("#badge_"+messages[i]['referencia']).html( messages[0]['total']);
			}
		}
	})
}

var takeChat = function(){
	$('button[id^="referencyChat"]').click(function(event) {
		event.preventDefault();
		var referencia = $(this).val();
		//console.log(referencia);
		$.ajax({
			url: '../../../controllers/admin/chat_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {"takeChat": 'si', "referencia":referencia},
		})

		//$("#formChatMessage").submit();

		$('form').submit();

	});
}
