$(document).ready(function($) {

	var claveDocumento = null;
	var idDocumentChange = null;


	$("#btnPromedio").click(function(event) {
		event.preventDefault();

		var promCert   = $("#promCertLic").val();
		var folioCert  = $("#folioCertLic").val();
		var reference  = $("#referenceLic").val();
		var idDocument = $("#idDocumentLic").val();

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"promCert": promCert,
							"folioCert": folioCert,
							"reference": reference,
							"idDocument": idDocument,
							"savePromCert": "si"
						},
			beforeSend: function(){
				//loader
			},
			success: function(data){
				//alert(data);
				alert ("Promedio actualizado correctamente");
			}
		})

	});


	$("#btnConsProm").click(function(event) {
		event.preventDefault();

		var promCert   = $("#ConsProm").val();
		var folioCert  = $("#folioConsProm").val();
		var reference  = $("#referenceConsProm").val();
		var idDocument = $("#idDocumentConsProm").val();

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"promCert": promCert,
							"folioCert": folioCert,
							"reference": reference,
							"idDocument": idDocument,
							"savePromCert": "si"
						},
			beforeSend: function(){
				//loader
			},
			success: function(data){
				//alert(data);
				alert ("Promedio actualizado correctamente");
			}
		})

	});


	//validar documentos digitales
	$('input[id^="checkedDigital"]').click(function(event) {
		var validateDigitalDocument = $(this).val();

		if(validateDigitalDocument == 1){
			$(this).attr('value', '0');
			$("#statusDigital").removeClass('glyphicon-ok');
			$("#statusDigital").addClass('glyphicon-remove')
			$("#statusDigital").css({color: '#FF0000'});
		}else{
			$(this).attr('value', '1');
			$("#statusDigital").removeClass('glyphicon-remove');
			$("#statusDigital").addClass('glyphicon-ok')
			$("#statusDigital").css({color: '#449d44'});
		}

		var validated  = $(this).attr('value');
		var idDocument = $(this).attr('data-idDocument');

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"idDocument": idDocument,
							"validated": validated,
							"updateDocumentValidate": "si"
						},
			beforeSend:function(){

			},
			success: function(data){
				//alert(data);
			}
		});
	});

	//validar documentos fisicos
	$('input[id^="checkedPhysical"]').click(function(event) {
		var validatedPhysicalDocument = $(this).val();

		if(validatedPhysicalDocument == 1){
			$(this).attr('value', '0');
			$("#statusPhysical").removeClass('glyphicon-ok');
			$("#statusPhysical").addClass('glyphicon-remove')
			$("#statusPhysical").css({color: '#FF0000'});
		}else{
			$(this).attr('value', '1');
			$("#statusPhysical").removeClass('glyphicon-remove');
			$("#statusPhysical").addClass('glyphicon-ok')
			$("#statusPhysical").css({color: '#449d44'});
		}

		var validated  = $(this).attr('value');
		var idDocument = $(this).attr('data-idDocument');

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"idDocument": idDocument,
							"validated": validated,
							"updateDocumentPhysical": "si"
						},
			beforeSend:function(){

			},
			success: function(data){
				alert(data);
			}
		});
	});

	//validacion del aspirante
	$("#validateAspirant").click(function(event) {

		var validate = $(this).val();

		if(validate == "1"){
			$(this).attr('value', '0');
			$("#statusValidate").removeClass('glyphicon-ok');
			$("#statusValidate").addClass('glyphicon-remove')
			$("#statusValidate").css({color: '#FF0000'});
		}else{

			$(this).attr('value', '1');

			$("#statusValidate").removeClass('glyphicon-remove');
			$("#statusValidate").addClass('glyphicon-ok')
			$("#statusValidate").css({color: '#449d44'});
		}

		var idAspirant = $(this).attr('data-idAspirant');
		var validated  = $(this).attr('value');

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"validateAspirant": 'si',
							"idAspirant":idAspirant,
							"validated":validated
						},
			beforeSend:function(){

			},
			success:function(data){
				//alert(data)
			}

		});
	});


	//enviar msg de validacion
	$("#btnValidacion").click(function(event) {
		var email      = $(this).attr('data-email');
		var message    = $("#message").val();
		var name       = $(this).attr('data-name');
		var idAspirant = $(this).attr('data-idAspirant');
		var plan       = $(this).attr('data-plan');
		var idAdmin    = $(this).attr('data-Admin');

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"sendValidacion"  :'si',
							"email"      :email,
							"message"    :message,
							"name"       :name,
							"idAspirant" :idAspirant,
							"plan"      :plan
						},
			beforeSend:function(){

			},
			success: function(data){

				//alert(data);

				$("#modalValidacion").modal("toggle");
				//$('#modalValidacion').modal('hide')


				var datos = jQuery.parseJSON(data);

				if(datos['email'] == "enviado"){
					var alerta = "<div class='alert alert-success'>"+
													"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
													"Correo enviado"+
												"</div>"

					$("#erroMail").appendTo(alerta);
				}else{
					var alerta = "<div class='alert alert-danger'>"+
													"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
													"Error al enviar correo"+
												"</div>"

					$("#erroMail").append(alerta);

				}


			}
		})
	});

	//envio de correo
	$("#btnSendMail").click(function(event) {
		var email      = $(this).attr('data-email');
		var message    = $("#message").val();
		var name       = $(this).attr('data-name');
		var idAspirant = $(this).attr('data-idAspirant');
		var plan       = $(this).attr('data-plan');
		var idAdmin    = $(this).attr('data-Admin');


		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"sendMail"  :'si',
							"email"      :email,
							"message"    :message,
							"name"       :name,
							"idAspirant" :idAspirant,
							"plan"      :plan
						},
			beforeSend:function(){

			},
			success: function(data){
				var datos = jQuery.parseJSON(data);

				if(datos['email'] == "enviado"){
					var alerta = "<div class='alert alert-success'>"+
													"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
													"Correo enviado"+
												"</div>"

					$("#erroMail").appendTo(alerta);
				}else{
					var alerta = "<div class='alert alert-danger'>"+
													"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
													"Error al enviar correo"+
												"</div>"

					$("#erroMail").append(alerta);

				}


			}
		})
	});


	$('button[id^="viewDocument"]').click(function(event) {
		var pathDocument = $(this).attr('data-path');
		crearFrame("../../../"+pathDocument);
	});


	//Mostar cambios de documentos

	$(':checkbox').click(function(event) {
		/* Act on the event */
		idDocumentChange = $(this).val();
		//alert (idDocumentChange)
	    var checked = $("#changeDocumentCheck-"+idDocumentChange).is(":checked");
	    //alert (checked);

	    if (checked == true){
	    	$("#changeDocumentBoton-"+idDocumentChange).css({"display": 'block'});
				$("#changeList-"+idDocumentChange).css({"display": 'block'});

	    }else{
	    	$("#changeDocumentBoton-"+idDocumentChange).css({"display": 'none'});
				$("#changeList-"+idDocumentChange).css({"display": 'none'});

	    }
	});


	$('button[id^="changeDocumentBoton-"]').click(function(event) {
			newClaveDocChange = $("#changeList-"+idDocumentChange).val();
			//alert (idDocumentChange + "|" +newClaveDocChange);


			if (newClaveDocChange != null){
					$.ajax({
					url: '../../../controllers/admin/viewAspirant_controller.php',
					type: 'POST',
					dataType: 'html',
					data: {	"updateTypeDocument": 'si',
									"idDocChange":idDocumentChange,
									"newClaveDocChange":newClaveDocChange
								},
					beforeSend:function(){
					},
					success:function(data){
					//alert (data);
					var datos = jQuery.parseJSON(data);

					if(datos['updated'] == "ok"){
						$("#modaUpdatedDoc").modal("toggle");
	
					}

					}

				});
			}else{
				alert ("Debes de seleccionar un tipo de documento");
			}
	});


});



//funccion para crear un iframe y mostrar el pdf
function crearFrame(pdf) {
  //var testFrame = document.createElement("IFRAME");
  var testFrame = document.createElement("iframe");

  $("#testFrame").remove();
  testFrame.id          = "testFrame";
  testFrame.width       = "100%";
  testFrame.height      = "1000px";
  testFrame.scrolling   = "no";
  testFrame.frameborder = "10px";
  testFrame.src = pdf; //Sacar el nombre del fichero pdf desde el parametro

  var control = document.getElementById("testFrame")

  if (control==null) {
      $('#pdf').append(document.body.appendChild(testFrame));
  }
}