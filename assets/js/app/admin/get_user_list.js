$(document).ready(function() {

	$('button[id^="btnTrahsUserAdmin"]').click(function(event){
		var id = $(this).attr('value');
		//alert(id);

		$.ajax({
			url: '../../../controllers/admin/edit_user_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {
								"id": id,
								"deleteUser_Admin": "si"},
			beforeSend: function(data){
				alert("El usuario se ha dado de baja correctamente: " + id);
			},
			success: function(data){
				//alert("datos de regreso: " + data);
			},
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});


	$('button[id^="btnEditUserAdmin"]').click(function(event) {

		var id = $(this).attr('value');
		//alert (id);

		$.ajax({
			url:      '../../../controllers/admin/edit_user_controller.php',
			type:     'POST',
			dataType: 'html',
			data:     {"id": id, "editUserAdmin": "si"},
			beforeSend: function(){
			},
			success: function(data){

				//alert(data);

				var datos = jQuery.parseJSON(data);
				//console.log(datos['referencia']);
				//Genero
				switch (datos['gender']) {
			    case 'H':
		        var genero = 	"<option value='" + datos['gender'] + "' selected=''>Hombre</option>"+
									 				"<option value='M'>Mujer</option>";
		        break;
			    case 'M':
		        var genero = 	"<option value='H'>Hombre</option>"+
									 				"<option value='" + datos['gender'] + "' selected=''>Mujer</option>";
		        break;
				}

				//Dependencia
				//alert(datos['dependency']);
				switch (datos['dependency']) {
			    case '1' :
		        var dependencia = 	"<option value='1'>POSGRADO</option>"+
									 							"<option value='2'>LOCAL DE REGISTRO</option>";;
		        break;
			    case '2' :
		        var dependencia = 	"<option value='1'>LOCAL DE REGISTRO</option>"+
									 							"<option value='2'>POSGRADO</option>";
					case '3' :
		        var dependencia = 	"<option value='1'>LOCAL DE REGISTRO</option>"+
									 							"<option value='2' selected=''>POSGRADO</option>";
		        break;
				}

				//console.log(datos['login']);

				//Permiso
				var activo="";
				if (datos['active'] == 1){
				  var activo = "checked";
				}

				//Permiso
				var validar="";
				if (datos['validate'] == 1){
				  var validar = "checked";
				}

				//Permiso
				var permiso_crea_usr_adm ="";
				if (datos['create_users'] == 1){
				  var permiso_crea_usr_adm = "checked";
				}
				//Permiso
				var permiso_crea_aspi="";
				if (datos['create_aspirant'] == 1){
				  var permiso_crea_aspi = "checked";
				}
				//Permiso
				var permiso_edit_usr_adm ="";
				if (datos['edit_users'] == 1){
				  var permiso_edit_usr_adm = "checked";
				}

				//Permiso
				var permiso_chat ="";
				if (datos['chat'] == 1){
				  var permiso_chat = "checked";
				}
				//Permiso
				var permiso_ver_doc_dig ="";
				if (datos['view_digital_document'] == 1){
				  var permiso_ver_doc_dig = "checked";
				}
				//Permiso
				var permiso_elim_doc_dig ="";
				if (datos['delete_digital_document'] == 1){
				  var permiso_elim_doc_dig = "checked";
				}
				//Permiso
				var permiso_val_doc_dig ="";
				if (datos['validate_digital_document'] == 1){
				  var permiso_val_doc_dig = "checked";
				}

				//Permiso
				var permiso_recep ="";
				if (datos['receive_physical_document'] == 1){
				  var permiso_recep = "checked";
				}
				//Permiso
				var permiso_val_doc_fis ="";
				if (datos['validate_physical_document'] == 1){
				  var permiso_val_doc_fis = "checked";
				}
				//Permiso
				var permiso_report ="";
				if (datos['reports'] == 1){
				  var permiso_report = "checked";
				}

				// Limpiar el contenedor
				$("#maincontent").empty();
				var $newdiv = $("<script type='text/javascript'>"+
												    "function showContent() {"+
												        "element = document.getElementById('content');"+
												        "check = document.getElementById('check');"+
												        "if (check.checked) {"+
												            "element.style.display='block';"+
												        "}"+
												        "else {"+
												            "element.style.display='none';"+
												        "}"+
												    "}"+
													"</script>"+
													"<script src='../../../assets/js/md5.min.js'></script>"+
													"<div class='row'>" +
														"<div class='col-sm-6'>" +
															" <div class='col-sm-8'>" +
																"<form action='' method='POST' class='form-horizontal' role='form'>" +
																	"<div class='form-group'>" +
																		"<legend>Editar Usuario</legend>" +
																		"<br>" +
																	"</div>" +

																	"<div class='form-group'>" +
																		"<label class='' for='referencia'>Nombre:</label>" +
																		"<div class='alert alert-info' role='alert'>" + datos['name'] + " " + datos['first_surname'] + " " + datos['second_surname']+
																		"</div>" +
																	"</div>" +
																	"<div id='resultadoAspirante'>" +
																	"</div>" +

																"</form>" +
															"</div>" +
														"</div>" +

														"<div class='col-sm-6'>" +
															"<div class='col-sm-12'>" +
																"<form action='' id='formCreateUser' method='POST' class='form-horizontal' role='form'>"+
																	"<div class='form-group'>" +
																		"<legend>Datos del Usuario</legend>" +
																		"<br/>" +
																	"</div>" +
																	"<div class='form-group'>" +
																		// Apellido paterno
																		"<div class='col-sm-6'><label class='' for='paterno'>Paterno:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='paterno' value='"+datos['first_surname']+"'></div>" +
																		// Appellido materno
																		"<div class='col-sm-6'><label class='' for='materno'>Materno:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='materno' value='"+datos['second_surname']+"'></div>" +
																		// Nombre
																		"<div class='col-sm-6'><label class='' for='nombre'>Nombre:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='nombre' value='"+datos['name']+"'></div>" +

																		// Fecha naci
																		"<div class='col-sm-6'><label class='' for='nombre'>Fecha de nacimiento:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='sele_fecha_nac' value='"+datos['birthdate']+"'></div>" +

																		// CURP
																		"<div class='col-sm-6'><label class='' for='nombre'>CURP:</label></div>" +
																		"<div class='col-sm-6'><input type='text' maxlength='18' class='form-control' id='sele_CURP' value='"+datos['CURP']+"'></div>" +

																		// email
																		"<div class='col-sm-6'><label class='' for='nombre'>email:</label></div>" +
																		"<div class='col-sm-6'><input type='text' maxlength='18' class='form-control' id='sele_email' value='"+datos['email']+"'></div>" +

																		// Genero
																		"<div class='col-sm-6'><label class='' for='genero'>Genero:</label></div>" +
																		"<div class='col-sm-6'>" +
                    									"<select name='' id='genero_select' class='form-control' required='required'>" +
                    										genero+
                    									"</select>" +
                  									"</div>" +
                  									//Dependencia
																		"<div class='col-sm-6'><label class='' for='dependencia'>Dependencia:</label></div>" +
																		"<div class='col-sm-6'>" +
                    									"<select name='' id='dependencia' class='form-control' required='required'>" +
                    										dependencia+
                    									"</select>" +
                  									"</div>" +
																		// Nombre de usuario
																		"<div class='col-sm-6'><label class='' for='login'>Nombre de usuario:</label></div>" +
																		"<div class='col-sm-6'><input type='text' class='form-control' id='login' value='"+datos['login']+"' style='none'></div>" +

																		// Permiso Crear Usuarios
                  									"<div class='col-sm-6'><label class='' for='permiso_crea_usr_adm'>Crear Usuarios:</label></div>" +

  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_crea_usr_adm' name='permiso_crea_usr_adm' " + permiso_crea_usr_adm + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Crear Aspirantes
                  									"<div class='col-sm-6'><label class='' for='permiso_crea_aspi'>Crear Aspirantes:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_crea_aspi' name='permiso_crea_aspi' " + permiso_crea_aspi + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Editar Usuarios
                  									"<div class='col-sm-6'><label class='' for='permiso_edit_usr_adm'>Editar Usuarios:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_edit_usr_adm' name='permiso_edit_usr_adm' " + permiso_edit_usr_adm + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Chat con Aspirantes
                  									"<div class='col-sm-6'><label class='' for='permiso_chat'>Chat con Aspirantes:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_chat' name='permiso_chat' " + permiso_chat + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Ver Documentos Digitales
                  									"<div class='col-sm-6'><label class='' for='permiso_ver_doc_dig'>Ver Documentos Digitales:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_ver_doc_dig' name='permiso_ver_doc_dig' " + permiso_ver_doc_dig + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Eliminar Documentos Digitales
                  									"<div class='col-sm-6'>"+
                  										"<label class='' for='permiso_elim_doc_dig'>Eliminar Documentos Digitales:</label>"+
                										"</div>" +

  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label>"+
                      										"<input type='checkbox' value='' id='permiso_elim_doc_dig' name='permiso_elim_doc_dig' " + permiso_elim_doc_dig + " >"+
                      										"Activo"+
                    										"</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Validar Documentos Digitales
                  									"<div class='col-sm-6'>"+
                  										"<label class='' for='permiso_val_doc_dig'>Validar Documentos Digitales:</label>"+
                										"</div>" +
  								                  "<div class='col-sm-4'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_val_doc_dig' name='permiso_val_doc_dig' " + permiso_val_doc_dig + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +


																	// Permiso Validar Documentos Físicos
                  									"<div class='col-sm-6'><label class='' for='permiso_val_doc_fis'>Validar Documentos Físicos:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_val_doc_fis' name='permiso_val_doc_fis' " + permiso_val_doc_fis + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

																	// Permiso Generar Reportes
                  									"<div class='col-sm-6'><label class='' for='permiso_report'>Generar Reportes:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='permiso_report' name='permiso_report' " + permiso_report + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +

                  									// Cambiar pass
                  									"<div class='col-sm-6'><label class='' for='cambia_pss'>Cambiar Password:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' name='check' id='check' value='1' onchange='javascript:showContent()' >Cambiar</label>" +
                    									"</div>" +
                  									"</div>" +
                  									//////////
                  									"<div id='content' style='display: none;'>"+
                  										"<div class='col-sm-6'><label class='' for='login'>Password:</label></div>" +
																			"<div class='col-sm-6'><input type='password' class='form-control' id='pass' value='' style='none' required='required' ></div>" +
																			"<div class='col-sm-6'><label class='' for='pass2' >Repetir Password:</label></div>" +
																			"<div class='col-sm-6'><input type='password' class='form-control' id='pass2' value='' required='required' > </div>" +
																		"</div>"+
																		///////////

                  									// Fecha de creación
                  									"<div class='col-sm-6'><label class='sr-only' fecha_creacionfor=''>Fecha de Creación:</label></div>" +
                  									"<div class='col-sm-6'><input type='hidden' class='form-control' id='fecha_creacion' value='"+datos['fecha_creacion']+"'></div>" +


																		// validar:
                  									"<div class='col-sm-6'>"+
                  										"<label class='' for='usuario_activo'>Validar:</label>"+
                										"</div>" +

  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='validar' name='validar' " + validar + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +


                  									// Usuario activo:
                  									"<div class='col-sm-6'><label class='' for='usuario_activo'>Usuario Activo:</label></div>" +
  								                  "<div class='col-sm-6'>" +
                    									"<div class='checkbox'>" +
                      									"<label><input type='checkbox' value='' id='usuario_activo' name='usuario_activo' " + activo + " >Activo</label>" +
                    									"</div>" +
                  									"</div>" +


																	"<div class='form-group'>"+
																		"<div class='col-sm-10 col-sm-offset-2'>" +
																			"<button type='button' id='updateEditUser' class='btn btn-primary'>Actualizar</button>" +
																		"</div>" +
																	"</div>" +
																"</form>"+

																"<div id='update_result'></div>" +
															"</div>" +
														"</div>" +

													"</div>"
												);

				$("#maincontent").append($newdiv);


				$("#updateEditUser").click(function(event) {
					event.preventDefault();

					$("#update_result").empty();

					var id                   = datos['id_user'];
					var paterno              = $("#paterno").val();
					var materno              = $("#materno").val();
					var nombre               = $("#nombre").val();
					var fecha_nac            = $("#sele_fecha_nac").val();
					var CURP                 = $("#sele_CURP").val();
					var email                = $("#sele_email").val();
					var login                = $("#login").val();
					var genero               = $("#genero_select").val();
					var dependencia          = $("#dependencia").val();
					var permiso_crea_usr_adm = $("#permiso_crea_usr_adm").is(":checked");
					var permiso_crea_aspi    = $("#permiso_crea_aspi").is(":checked");
					var permiso_edit_usr_adm = $("#permiso_edit_usr_adm").is(":checked");
					var permiso_chat         = $("#permiso_chat").is(":checked");
					var permiso_ver_doc_dig  = $("#permiso_ver_doc_dig").is(":checked");
					var permiso_elim_doc_dig = $("#permiso_elim_doc_dig").is(":checked");
					var permiso_val_doc_dig  = $("#permiso_val_doc_dig").is(":checked");
					var permiso_recep        = $("#permiso_recep").is(":checked");
					var permiso_val_doc_fis  = $("#permiso_val_doc_fis").is(":checked");
					var permiso_report       = $("#permiso_report").is(":checked");
					var pass                 = $("#pass").val();
					var pass2                = $("#pass2").val();
					var u_a                  = $("#usuario_activo").is(":checked");
					var permiso_validar      = $("#validar").is(":checked");

	        //console.log(genero);
					//console.log(dependencia);

					//usario activo
					if (u_a == true){
					  usuario_activo = 1;
					}else {
					  usuario_activo = 0;
					}
					//validar
					if (permiso_validar == true){
					  permiso_validar = 1;
					}else {
					  permiso_validar = 0;
					}
					//Permisos
					if (permiso_crea_usr_adm == true){
					  permiso_crea_usr_adm = 1;
					}else {
					  permiso_crea_usr_adm = 0;
					}
					//
					if (permiso_crea_aspi == true){
					  permiso_crea_aspi = 1;
					}else {
					  permiso_crea_aspi = 0;
					}
					if (permiso_edit_usr_adm == true){
					  permiso_edit_usr_adm = 1;
					}else {
					  permiso_edit_usr_adm = 0;
					}
					if (permiso_chat == true){
					  permiso_chat = 1;
					}else {
					  permiso_chat = 0;
					}
					if (permiso_ver_doc_dig == true){
					  permiso_ver_doc_dig = 1;
					}else {
					  permiso_ver_doc_dig = 0;
					}
					if (permiso_elim_doc_dig == true){
					  permiso_elim_doc_dig = 1;
					}else {
					  permiso_elim_doc_dig = 0;
					}
					if (permiso_val_doc_dig == true){
					  permiso_val_doc_dig = 1;
					}else {
					  permiso_val_doc_dig = 0;
					}
					if (permiso_recep == true){
					  permiso_recep = 1;
					}else {
					  permiso_recep = 0;
					}
					if (permiso_val_doc_fis == true){
					  permiso_val_doc_fis = 1;
					}else {
					  permiso_val_doc_fis = 0;
					}
					if (permiso_report == true){
					  permiso_report = 1;
					}else {
					  permiso_report = 0;
					}
					//Cambiar Contraseña
					if (pass == 0){
					  pass = datos['pass'];
					  pass2 = datos['pass'];
					  //console.log(pass);
					}else{
						pass = md5(pass);//Convertir aqui
						pass2 = md5(pass2);
					}

					//
					if (pass == pass2){

			      $.ajax({
				      url:      '../../../controllers/admin/edit_user_controller.php',
				      type:     'POST',
				      dataType: 'html',
				      data: {
								"id": id,
								"first_surname": paterno,
								"second_surname": materno,
								"name": nombre,
								"CURP": CURP,
								"email": email,
								"birthdate": fecha_nac,
								"gender": genero,
								"dependency": dependencia,
								"create_users": permiso_crea_usr_adm,
								"create_aspirants":permiso_crea_aspi,
								"edit_users":permiso_edit_usr_adm,
								"permiso_chat":permiso_chat,
								"view_digital_document":permiso_ver_doc_dig,
								"delete_digital_document":permiso_elim_doc_dig,
								"validate_digital_document":permiso_val_doc_dig,
								"receive_physical_document":permiso_val_doc_fis,
								"reports":permiso_report,
								"pass":pass,
								"active":usuario_activo,
								"validate":permiso_validar,
								"updateEditUserAdmin"	 :"si"
							},
			       	beforeSend: function(){
			       			//alert("datos: " + data);
			       	},
			       	success: function(data){
			       		//console.log("data: " + data);
			       		//alert(data);
			       		actualizado = jQuery.parseJSON(data);

			       		if(actualizado['actualizado'] == "ok"){
									$("#update_result").append("<div class='alert alert-success'>"+
																						"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																						"<strong>Usuario " + login + " actualizado correctamente</strong>;"+
																					"</div>");
								}else{
									$("#update_result").append("<div class='alert alert-danger'>"+
																						"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
																						"<strong>Error al actualizar al usuario " + login + " </strong>;"+
																					"</div>");
								}
			       	}
			      })
				    .done(function() {
				     	console.log("success");
				    })
				    .fail(function() {
				     	console.log("error");
				    })
				    .always(function() {
				     	console.log("complete");
				    });
					}else{
						//alert("Los campos de password deben de ser iguales");
						$("#update_result").append("<div class='alert alert-warning'>"+
				                          				"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
				                          				"<strong>Las contraseñas deben ser iguales</strong>"+
				                          			"</div>" );
					}
	      });
			}
		});
	});


});





