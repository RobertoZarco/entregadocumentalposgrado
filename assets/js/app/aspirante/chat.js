$(document).ready(function(){
	registrerMessages();
	$.ajaxSetup({"cache":false});

	setInterval("loadOldMessage()", 3000);
})

var registrerMessages = function(){
	$("#send").click(function(event) {
		event.preventDefault();

		//serialize obtiene todos los valores que estan dentro del formularios
		//var frm = $("#formChat").serialize();
		var emisor     = $("#emisor").val();
		var receptor   = $("#receptor").val();
		var message    = $("#message").val();
		var statusChat = $("#statusChat").val();

		$.ajax({
			//url: '../../../controllers/chat_controller.php',
			url: '../../../controllers/aspirante/chat_controller.php',
			type: 'POST',
			dataType: 'html',
			data:{'emisor': emisor,
						'receptor': receptor,
						'message': message,
						'statusChat': statusChat,
						"insertPeticion": "si"},
			beforeSend: function(){

			},
			success: function(data){
				//console.log(data);
				$("#message").val("");
				var altura = $("#conversation").prop("scrollHeight");
				$("#conversation").scrollTop(altura);
			}
		})
	});
}

var loadOldMessage = function(){
	var emisor     = $("#emisor").val();
	var receptor   = $("#receptor").val();

	$.ajax({
		//url: '../../../controllers/chat_controller.php',
		url: '../../../controllers/aspirante/chat_controller.php',
		type: 'POST',
		dataType: 'html',
		data: {"emisor":emisor, "receptor": receptor,  "searchConversation": 'si'},
		beforeSend: function(){

		},
		success: function(data){
			$("#conversation").html(data);
			$("#conversation p:last-child").css({
																						"background-color": "lightgreen",
																						"padding-botton": "20px"
																					});

			//$("#message").val("");
			var altura = $("#conversation").prop("scrollHeight");
			$("#conversation").scrollTop(altura);
		}
	});
}

