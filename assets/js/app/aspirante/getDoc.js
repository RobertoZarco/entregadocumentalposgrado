$(document).ready(function(){

	//ver documentos
	$("#file-actaPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-actaPDF").val();


    //alert(reference+"|"+account_number+"|"+nombreDocumento);

    $("#file-actaPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    //Visualizar PDF de DB
    //var idDocFrame = $("#delete-actaPDF").val();
    //crearFrame("../../../models/frame_pdf_model.php?id="+idDocFrame);
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

	$("#file-curpPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#account_number").val();
    var nombreDocumento = $("#file-curpPDF").val();
    $("#file-curpPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

	$("#file-certifPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-certifPDF").val();

    $("#file-certifPDF").removeClass('btn btn-default').addClass( "btn btn-success" );
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-identOficPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-identOficPDF").val();

    $("#file-identOficPDF").removeClass('btn btn-default').addClass( "btn btn-success" );
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-tituloPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-tituloPDF").val();

    $("#file-tituloPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-constPromPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-constPromPDF").val();

    $("#file-constPromPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-actaExamProfPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-actaExamProfPDF").val();

    $("#file-actaExamProfPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-cartaRegTitPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-cartaRegTitPDF").val();

    $("#file-cartaRegTitPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-gradoMaestriaPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-gradoMaestriaPDF").val();

    $("#file-gradoMaestriaPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');

    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-carRegModGradPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-carRegModGradPDF").val();

    $("#file-carRegModGradPDF").removeClass('btn btn-default').addClass( "btn btn-success" );
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-certMaestriPDF").click(function(event) {
    var referencia      = $("#reference").val();
    var account_number  = $("#account_number").val();
    var nombreDocumento = $("#file-certMaestriPDF").val();

    $("#file-certMaestriPDF").removeClass('btn btn-default').addClass( "btn btn-success" );
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-constPromMaestriaPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#account_number").val();
    var nombreDocumento = $("#file-constPromMaestriaPDF").val();

    $("#file-constPromMaestriaPDF").removeClass('btn btn-default').addClass( "btn btn-success" );
    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-equivaPromPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#reference").val();
    var nombreDocumento = $("#file-equivaPromPDF").val();

    $("#file-equivaPromPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');
    
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-cedProfPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#reference").val();
    var nombreDocumento = $("#file-cedProfPDF").val();

    $("#file-cedProfPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-consCompreLectuPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#reference").val();
    var nombreDocumento = $("#file-consCompreLectuPDF").val();

    $("#file-consCompreLectuPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consEspanolPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });

  $("#file-consEspanolPDF").click(function(event) {
    var referencia = $("#reference").val();
    var account_number = $("#reference").val();
    var nombreDocumento = $("#file-consEspanolPDF").val();

    $("#file-consEspanolPDF").removeClass('btn btn-default').addClass( "btn btn-success" );

    $("#file-actaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-curpPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-tituloPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certifPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-identOficPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-actaExamProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cartaRegTitPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-constPromMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-gradoMaestriaPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-carRegModGradPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-certMaestriPDF").removeClass('btn btn-success').addClass('btn btn-default');

    $("#file-equivaPromPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-cedProfPDF").removeClass('btn btn-success').addClass('btn btn-default');
    $("#file-consCompreLectuPDF").removeClass('btn btn-success').addClass('btn btn-default');
    crearFrame("../../../uploads/"+referencia+"/"+nombreDocumento);
  });




  //borrar documentos
  //acta
  $("#delete-actaPDF").click(function(event) {
    var idDocumento = $("#delete-actaPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-actaPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_actaPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_actaPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-actaPDF").hide();
          $("#td-ok-upload-actaPDF").hide();
          $("#td-trash-actaPDF").hide();
          $("#td-show_status-actaPDF").hide();
          $("#td-show_status-recep-actaPDF").hide();
          $("#td-show_status-fisic-actaPDF").hide();
          $("#file-actaPDF").hide();
          $("#ok-upload-actaPDF").hide();
          //$("#ok-validate-actaPDF").show();
          $("#trash-actaPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#actaPDF").prop( "disabled", false );
          $("#submit_actaPDF").prop( "disabled", false );

          $("#mensaje_actaPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_actaPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //curp
  $("#delete-curpPDF").click(function(event) {
    var idDocumento = $("#delete-curpPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-curpPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_curpPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_curpPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-curpPDF").hide();
          $("#td-ok-upload-curpPDF").hide();
          $("#td-trash-curpPDF").hide();
          $("#td-show_status-curpPDF").hide();
          $("#td-show_status-recep-curpPDF").hide();
          $("#td-show_status-fisic-curpPDF").hide();
          $("#file-curpPDF").hide();
          $("#ok-upload-curpPDF").hide();
          //$("#ok-validate-curpPDF").show();
          $("#trash-curpPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#curpPDF").prop( "disabled", false );
          $("#submit_curpPDF").prop( "disabled", false );

          $("#mensaje_curpPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_curpPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //Certificado
  $("#delete-certifPDF").click(function(event) {
    var idDocumento = $("#delete-certifPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-certifPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_certifPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_certifPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-certifPDF").hide();
          $("#td-ok-upload-certifPDF").hide();
          $("#td-trash-certifPDF").hide();
          $("#td-show_status-certifPDF").hide();
          $("#td-show_status-recep-certifPDF").hide();
          $("#td-show_status-fisic-certifPDF").hide();
          $("#file-certifPDF").hide();
          $("#ok-upload-certifPDF").hide();
          //$("#ok-validate-certifPDF").show();
          $("#trash-certifPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#certifPDF").prop( "disabled", false );
          $("#submit_certifPDF").prop( "disabled", false );

          $("#mensaje_certifPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_certifPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //Titulo
  $("#delete-tituloPDF").click(function(event) {
    var idDocumento = $("#delete-tituloPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-tituloPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_tituloPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_tituloPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-tituloPDF").hide();
          $("#td-ok-upload-tituloPDF").hide();
          $("#td-trash-tituloPDF").hide();
          $("#td-show_status-tituloPDF").hide();
          $("#td-show_status-recep-tituloPDF").hide();
          $("#td-show_status-fisic-tituloPDF").hide();
          $("#file-tituloPDF").hide();
          $("#ok-upload-tituloPDF").hide();
          //$("#ok-validate-tituloPDF").show();
          $("#trash-tituloPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#tituloPDF").prop( "disabled", false );
          $("#submit_tituloPDF").prop( "disabled", false );

          $("#mensaje_tituloPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_tituloPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //Identificacion oficial
  $("#delete-identOficPDF").click(function(event) {
    var idDocumento = $("#delete-identOficPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-identOficPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_identOficPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_identOficPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-identOficPDF").hide();
          $("#td-ok-upload-identOficPDF").hide();
          $("#td-trash-identOficPDF").hide();
          $("#td-show_status-identOficPDF").hide();
          $("#td-show_status-recep-identOficPDF").hide();
          $("#td-show_status-fisic-identOficPDF").hide();
          $("#file-identOficPDF").hide();
          $("#ok-upload-identOficPDF").hide();
          //$("#ok-validate-identOficPDF").show();
          $("#trash-identOficPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#identOficPDF").prop( "disabled", false );
          $("#submit_identOficPDF").prop( "disabled", false );

          $("#mensaje_identOficPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_identOficPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //CONSTANCIA DE PROMEDIO
  $("#delete-constPromPDF").click(function(event) {
    var idDocumento = $("#delete-constPromPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-constPromPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_constPromPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_constPromPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-constPromPDF").hide();
          $("#td-ok-upload-constPromPDF").hide();
          $("#td-trash-constPromPDF").hide();
          $("#td-show_status-constPromPDF").hide();
          $("#td-show_status-recep-constPromPDF").hide();
          $("#td-show_status-fisic-constPromPDF").hide();
          $("#file-constPromPDF").hide();
          $("#ok-upload-constPromPDF").hide();
          //$("#ok-validate-constPromPDF").show();
          $("#trash-constPromPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#constPromPDF").prop( "disabled", false );
          $("#submit_constPromPDF").prop( "disabled", false );

          $("#mensaje_constPromPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_constPromPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //ACTA DE EXAMEN PROFESIONAL
  $("#delete-actaExamProfPDF").click(function(event) {
    var idDocumento = $("#delete-actaExamProfPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-actaExamProfPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_actaExamProfPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_actaExamProfPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-actaExamProfPDF").hide();
          $("#td-ok-upload-actaExamProfPDF").hide();
          $("#td-trash-actaExamProfPDF").hide();
          $("#td-show_status-actaExamProfPDF").hide();
          $("#td-show_status-recep-actaExamProfPDF").hide();
          $("#td-show_status-fisic-actaExamProfPDF").hide();
          $("#file-actaExamProfPDF").hide();
          $("#ok-upload-actaExamProfPDF").hide();
          //$("#ok-validate-actaExamProfPDF").show();
          $("#trash-actaExamProfPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#actaExamProfPDF").prop( "disabled", false );
          $("#submit_actaExamProfPDF").prop( "disabled", false );

          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_actaExamProfPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //CARTA DE REGISTRO DE LA OPCIÓN DE TITULACION
  $("#delete-cartaRegTitPDF").click(function(event) {
    var idDocumento = $("#delete-cartaRegTitPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){
      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-cartaRegTitPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_cartaRegTitPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_cartaRegTitPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-cartaRegTitPDF").hide();
          $("#td-ok-upload-cartaRegTitPDF").hide();
          $("#td-trash-cartaRegTitPDF").hide();
          $("#td-show_status-cartaRegTitPDF").hide();
          $("#td-show_status-recep-cartaRegTitPDF").hide();
          $("#td-show_status-fisic-cartaRegTitPDF").hide();
          $("#file-cartaRegTitPDF").hide();
          $("#ok-upload-cartaRegTitPDF").hide();
          //$("#ok-validate-cartaRegTitPDF").show();
          $("#trash-cartaRegTitPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#cartaRegTitPDF").prop( "disabled", false );
          $("#submit_cartaRegTitPDF").prop( "disabled", false );

          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_cartaRegTitPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Acta de Nacimieto eliminada Correctamente</strong>"+
            "</div>");
        }
      }
    })
  });

  //CARTA DE REGISTRO DE LA OPCIÓN DE TITULACION
  $("#delete-gradoMaestriaPDF").click(function(event) {
    var idDocumento = $("#delete-gradoMaestriaPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){
      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-gradoMaestriaPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_gradoMaestriaPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_gradoMaestriaPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-gradoMaestriaPDF").hide();
          $("#td-ok-upload-gradoMaestriaPDF").hide();
          $("#td-trash-gradoMaestriaPDF").hide();
          $("#td-show_status-gradoMaestriaPDF").hide();
          $("#td-show_status-recep-gradoMaestriaPDF").hide();
          $("#td-show_status-fisic-gradoMaestriaPDF").hide();
          $("#file-gradoMaestriaPDF").hide();
          $("#ok-upload-gradoMaestriaPDF").hide();
          //$("#ok-validate-gradoMaestriaPDF").show();
          $("#trash-gradoMaestriaPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#gradoMaestriaPDF").prop( "disabled", false );
          $("#submit_gradoMaestriaPDF").prop( "disabled", false );

          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();
          $("#mensaje_gradoMaestriaPDF").empty();


          //mostramos mansaje
          $("#mensaje_gradoMaestriaPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Grado de maestría eliminada Correctamente</strong>"+
            "</div>");
        }
      }
    })
  });

  //CARTA DE REGISTRO DE LA OPCIÓN DE TITULACION
  $("#delete-carRegModGradPDF").click(function(event) {
    var idDocumento = $("#delete-carRegModGradPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){
      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-carRegModGradPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_carRegModGradPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_carRegModGradPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-carRegModGradPDF").hide();
          $("#td-ok-upload-carRegModGradPDF").hide();
          $("#td-trash-carRegModGradPDF").hide();
          $("#td-show_status-carRegModGradPDF").hide();
          $("#td-show_status-recep-carRegModGradPDF").hide();
          $("#td-show_status-fisic-carRegModGradPDF").hide();
          $("#file-carRegModGradPDF").hide();
          $("#ok-upload-carRegModGradPDF").hide();
          //$("#ok-validate-carRegModGradPDF").show();
          $("#trash-carRegModGradPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#carRegModGradPDF").prop( "disabled", false );
          $("#submit_carRegModGradPDF").prop( "disabled", false );

          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();
          $("#mensaje_carRegModGradPDF").empty();


          //mostramos mansaje
          $("#mensaje_carRegModGradPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Carta de registro eliminada Correctamente</strong>"+
            "</div>");
        }
      }
    })
  });

  //CARTA DE REGISTRO DE LA OPCIÓN DE TITULACION
  $("#delete-certMaestriPDF").click(function(event) {
    var idDocumento = $("#delete-certMaestriPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){
      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-certMaestriPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_certMaestriPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_certMaestriPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-certMaestriPDF").hide();
          $("#td-ok-upload-certMaestriPDF").hide();
          $("#td-trash-certMaestriPDF").hide();
          $("#td-show_status-certMaestriPDF").hide();
          $("#td-show_status-recep-certMaestriPDF").hide();
          $("#td-show_status-fisic-certMaestriPDF").hide();
          $("#file-certMaestriPDF").hide();
          $("#ok-upload-certMaestriPDF").hide();
          //$("#ok-validate-certMaestriPDF").show();
          $("#trash-certMaestriPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#certMaestriPDF").prop( "disabled", false );
          $("#submit_certMaestriPDF").prop( "disabled", false );

          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();
          $("#mensaje_certMaestriPDF").empty();


          //mostramos mansaje
          $("#mensaje_certMaestriPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Certificado de maestría eliminada Correctamente</strong>"+
            "</div>");
        }
      }
    })
  });

  //constancia de promedio
  $("#delete-constPromMaestriaPDF").click(function(event) {
    var idDocumento = $("#delete-constPromMaestriaPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-constPromMaestriaPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_constPromMaestriaPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_constPromMaestriaPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-constPromMaestriaPDF").hide();
          $("#td-ok-upload-constPromMaestriaPDF").hide();
          $("#td-trash-constPromMaestriaPDF").hide();
          $("#td-show_status-constPromMaestriaPDF").hide();
          $("#td-show_status-recep-constPromMaestriaPDF").hide();
          $("#td-show_status-fisic-constPromMaestriaPDF").hide();
          $("#file-constPromMaestriaPDF").hide();
          $("#ok-upload-constPromMaestriaPDF").hide();
          //$("#ok-validate-constPromMaestriaPDF").show();
          $("#trash-constPromMaestriaPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#constPromMaestriaPDF").prop( "disabled", false );
          $("#submit_constPromMaestriaPDF").prop( "disabled", false );

          $("#mensaje_constPromMaestriaPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_constPromMaestriaPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Constancia de promedio de maestría eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //equivalencia promedio
  $("#delete-equivaPromPDF").click(function(event) {
    var idDocumento = $("#delete-equivaPromPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
          //console.log(data);
          var deleteDoc = jQuery.parseJSON(data);

          if(deleteDoc['delete'] == "eliminado"){
            $("#a-btn-file-equivaPromPDF").attr('disabled', false);
            //cerramos el modal
            $('#modal_equivaPromPDF').modal('toggle');
            //limpiamos el mensaje
            $("#mensaje_equivaPromPDF").empty();
            //deshabilitamos las iconos de las filas de la tabla
            $("#td-file-equivaPromPDF").hide();
            $("#td-ok-upload-equivaPromPDF").hide();
            $("#td-trash-equivaPromPDF").hide();
            $("#td-show_status-equivaPromPDF").hide();
            $("#td-show_status-recep-equivaPromPDF").hide();
            $("#td-show_status-fisic-equivaPromPDF").hide();
            $("#file-equivaPromPDF").hide();
            $("#ok-upload-equivaPromPDF").hide();
            //$("#ok-validate-equivaPromPDF").show();
            $("#trash-equivaPromPDF").hide();

            //habilitamos los botones de seleccionar y guardar
            $("#equivaPromPDF").prop( "disabled", false );
            $("#submit_equivaPromPDF").prop( "disabled", false );

            $("#mensaje_equivaPromPDF").empty();
            $("#mensaje_curpPDF").empty();
            $("#mensaje_certifPDF").empty();
            $("#mensaje_tituloPDF").empty();
            $("#mensaje_identOficPDF").empty();
            $("#mensaje_constPromPDF").empty();
            $("#mensaje_actaExamProfPDF").empty();
            $("#mensaje_cartaRegTitPDF").empty();


            //mostramos mansaje
            $("#mensaje_equivaPromPDF").append( "<div class='alert alert-danger text-center'>"+
              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
              "<strong>Equivalencia de promedio eliminada</strong>"+
              "</div>");
          }

        }
      })
  });

  //cédula profesional
  $("#delete-cedProfPDF").click(function(event) {
    var idDocumento = $("#delete-cedProfPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-cedProfPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_cedProfPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_cedProfPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-cedProfPDF").hide();
          $("#td-ok-upload-cedProfPDF").hide();
          $("#td-trash-cedProfPDF").hide();
          $("#td-show_status-cedProfPDF").hide();
          $("#td-show_status-recep-cedProfPDF").hide();
          $("#td-show_status-fisic-cedProfPDF").hide();
          $("#file-cedProfPDF").hide();
          $("#ok-upload-cedProfPDF").hide();
          //$("#ok-validate-cedProfPDF").show();
          $("#trash-cedProfPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#cedProfPDF").prop( "disabled", false );
          $("#submit_cedProfPDF").prop( "disabled", false );

          $("#mensaje_cedProfPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_cedProfPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Cédula profesional eliminada correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //constancia de comprensio de lectura 
  $("#delete-consCompreLectuPDF").click(function(event) {
    var idDocumento = $("#delete-consCompreLectuPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-consCompreLectuPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_consCompreLectuPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_consCompreLectuPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-consCompreLectuPDF").hide();
          $("#td-ok-upload-consCompreLectuPDF").hide();
          $("#td-trash-consCompreLectuPDF").hide();
          $("#td-show_status-consCompreLectuPDF").hide();
          $("#td-show_status-recep-consCompreLectuPDF").hide();
          $("#td-show_status-fisic-consCompreLectuPDF").hide();
          $("#file-consCompreLectuPDF").hide();
          $("#ok-upload-consCompreLectuPDF").hide();
          //$("#ok-validate-consCompreLectuPDF").show();
          $("#trash-consCompreLectuPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#consCompreLectuPDF").prop( "disabled", false );
          $("#submit_consCompreLectuPDF").prop( "disabled", false );

          $("#mensaje_consCompreLectuPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_consCompreLectuPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Constancia de comprensión eliminada Correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //contancia dominio español
  $("#delete-consEspanolPDF").click(function(event) {
    var idDocumento = $("#delete-consEspanolPDF").val();
    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"idDocumentoInsert": idDocumento, "deleteDoc": "si"},
      beforeSend: function(){

      },
      success: function(data){
        //console.log(data);
        var deleteDoc = jQuery.parseJSON(data);

        if(deleteDoc['delete'] == "eliminado"){
          $("#a-btn-file-consEspanolPDF").attr('disabled', false);
          //cerramos el modal
          $('#modal_consEspanolPDF').modal('toggle');
          //limpiamos el mensaje
          $("#mensaje_consEspanolPDF").empty();
          //deshabilitamos las iconos de las filas de la tabla
          $("#td-file-consEspanolPDF").hide();
          $("#td-ok-upload-consEspanolPDF").hide();
          $("#td-trash-consEspanolPDF").hide();
          $("#td-show_status-consEspanolPDF").hide();
          $("#td-show_status-recep-consEspanolPDF").hide();
          $("#td-show_status-fisic-consEspanolPDF").hide();
          $("#file-consEspanolPDF").hide();
          $("#ok-upload-consEspanolPDF").hide();
          //$("#ok-validate-consEspanolPDF").show();
          $("#trash-consEspanolPDF").hide();

          //habilitamos los botones de seleccionar y guardar
          $("#consEspanolPDF").prop( "disabled", false );
          $("#submit_consEspanolPDF").prop( "disabled", false );

          $("#mensaje_consEspanolPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();


          //mostramos mansaje
          $("#mensaje_consEspanolPDF").append( "<div class='alert alert-danger text-center'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
            "<strong>Constancia dominio español eliminada correctamente</strong>"+
            "</div>");
        }

      }
    })
  });

  //Manifiesto
  $("#checkManifiesto").click(function(event) {

    var valor = $(this).val();

    $.ajax({
      url: './../../../controllers/aspirante/deleteDocument_controller.php',
      type: 'POST',
      dataType: 'html',
      data: {"checkManifiesto": valor},
      beforeSend:function(){

      },
      success:function(data){
        //alert(data);
        console.log(data);
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

  var checkManifest = $("#checkManifest").val();

  if(checkManifest == 1){
    $("#checkManifiesto").prop("checked", "checked");
    $("#checkManifiesto").attr('disabled', 'true');
  }

  //funccion para crear un iframe y mostrar el pdf
  function crearFrame(pdf) {
    //var testFrame = document.createElement("IFRAME");
    var testFrame = document.createElement("iframe");

    $("#testFrame").remove();
    testFrame.id          = "testFrame";
    testFrame.width       = "100%";
    testFrame.height      = "1000px";
    testFrame.scrolling   = "no";
    testFrame.frameborder = "10px";
    testFrame.src = pdf; //Sacar el nombre del fichero pdf desde el parametro

    var control = document.getElementById("testFrame")

    if (control==null) {
      $('#pdf').append(document.body.appendChild(testFrame));
    }
  }

})