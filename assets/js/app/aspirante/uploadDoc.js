$(document).ready(function(){
  /**/

  var progressBar = "<div class='progress'>"+
                      "<div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='45' aria-valuemin='0' aria-valuemax='100' style='width: 85%'>"+
                        "<span class='sr-only'>85% Complete</span>"+
                      "</div>"+
                    "</div>";

	$("#actaPDF").change(function(event) {
		//Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_actaPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-actaPDF").html($(this).val());
          $("#submit_actaPDF").prop( "disabled", false );
        }
	});

  $("#curpPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_curpPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-curpPDF").html($(this).val());
          $("#submit_curpPDF").prop( "disabled", false );
        }
  });

  $("#certifPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_certifPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-certifPDF").html($(this).val());
          $("#submit_certifPDF").prop( "disabled", false );
        }
  });

  $("#tituloPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_tituloPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-tituloPDF").html($(this).val());
          $("#submit_tituloPDF").prop( "disabled", false );
        }
  });

  $("#identOficPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_identOficPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-identOficPDF").html($(this).val());
          $("#submit_identOficPDF").prop( "disabled", false );
        }
  });

  $("#constPromPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_constPromPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-constPromPDF").html($(this).val());
          $("#submit_constPromPDF").prop( "disabled", false );
        }
  });

  $("#actaExamProfPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_actaExamProfPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-actaExamProfPDF").html($(this).val());
          $("#submit_actaExamProfPDF").prop( "disabled", false );
        }
  });

  $("#cartaRegTitPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_cartaRegTitPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-cartaRegTitPDF").html($(this).val());
          $("#submit_cartaRegTitPDF").prop( "disabled", false );
        }
  });

  $("#gradoMaestriaPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_gradoMaestriaPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-gradoMaestriaPDF").html($(this).val());
          $("#submit_gradoMaestriaPDF").prop( "disabled", false );
        }
  });


  $("#carRegModGradPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_carRegModGradPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-carRegModGradPDF").html($(this).val());
          $("#submit_carRegModGradPDF").prop( "disabled", false );
        }
  });


  $("#certMaestriPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_certMaestriPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-certMaestriPDF").html($(this).val());
          $("#submit_certMaestriPDF").prop( "disabled", false );
        }
  });

  $("#constPromMaestriaPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_constPromMaestriaPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-constPromMaestriaPDF").html($(this).val());
          $("#submit_constPromMaestriaPDF").prop( "disabled", false );
        }
  });

  $("#equivaPromPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_equivaPromPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-equivaPromPDF").html($(this).val());
          $("#submit_equivaPromPDF").prop( "disabled", false );
        }
  });

  $("#cedProfPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_cedProfPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-cedProfPDF").html($(this).val());
          $("#submit_cedProfPDF").prop( "disabled", false );
        }
  });

  $("#consCompreLectuPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_consCompreLectuPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-consCompreLectuPDF").html($(this).val());
          $("#submit_consCompreLectuPDF").prop( "disabled", false );
        }
  });

  $("#consEspanolPDF").change(function(event) {
    //Validar extenciones
      var fileExtension = ['PDF', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Formatos Permitidos : "+fileExtension.join(', '));
            //window.location.reload();
            $("#submit_equivaPromPDF").prop( "disabled", true );
        }else{
          $("#upload-file-info-consEspanolPDF").html($(this).val());
          $("#submit_consEspanolPDF").prop( "disabled", false );
        }
  });



  //FORM ACTA
  $(function(){
    $("#formActaUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formActaUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_actaPDF").attr('disabled', 'true');
          $("#actaPDFLoad").css({display: 'block'});

        },
        success: function(data){
          console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_actaPDF").empty();
          $("#mensaje_actaPDF").css({"display": 'block'});
          $("#actaPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_actaPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

				      $("#mensaje_actaPDF").html("<div class='alert alert-danger text-center'>"+
              														  "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
              														  "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
			        break;
            case "actaPDF":
              //desabilitamos el boton de selecion de documentos
              $("#actaPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-actaPDF").show();
              $("#td-ok-upload-actaPDF").show();
              //$("#td-ok-validate-actaPDF").show();
              $("#td-trash-actaPDF").show();
              $("#td-show_status-actaPDF").show();
              $("#td-show_status-recep-actaPDF").show();
              $("#td-show_status-fisic-actaPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-actaPDF").show();
              $("#ok-upload-actaPDF").show();
              //$("#ok-validate-actaPDF").show();
              $("#trash-actaPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_actaPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Acta de Nacimiento Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-actaPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-actaPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_actaPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_actaPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-actaPDF").hide();
                      $("#td-ok-upload-actaPDF").hide();
                      $("#td-trash-actaPDF").hide();
                      $("#file-actaPDF").hide();
                      $("#ok-upload-actaPDF").hide();
                      //$("#ok-validate-actaPDF").show();
                      $("#trash-actaPDF").hide();
                      $("#td-show_status-actaPDF").hide();
                      $("#td-show_status-recep-actaPDF").hide();
                      $("#td-show_status-fisic-actaPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#actaPDF").prop( "disabled", false );
                      $("#submit_actaPDF").prop( "disabled", false );
                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_actaPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Acta de Nacimiento eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
					}
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //curp
  $(function(){
    $("#formCURPUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formCURPUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          //$("#submit_actaPDF").prop( "disabled", true );
          $("#submit_curpPDF").attr('disabled', 'true');
          $("#curpPDFLoad").css({display: 'block'});
        },
        success: function(data){
          //busca el documento
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          //console.log(idDocumentoInsert+"|"+docInsertado+"|"+ cat_doc_insert);

          $("#mensaje_curpPDF").empty();

          $("#mensaje_curpPDF").css({"display": 'block'});
          $("#curpPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_curpPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "curpPDF":
              //desabilitamos el boton de selecion de documentos
              $("#curpPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_curpPDF").prop( "disabled", true );

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-curpPDF").show();
              $("#td-ok-upload-curpPDF").show();
              //$("#td-ok-validate-curpPDF").show();
              $("#td-trash-curpPDF").show();
              $("#td-show_status-curpPDF").show();
              $("#td-show_status-recep-curpPDF").show();
              $("#td-show_status-fisic-curpPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-curpPDF").show();
              $("#ok-upload-curpPDF").show();
              //$("#ok-validate-curpPDF").show();
              $("#trash-curpPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_curpPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>CURP Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-curpPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){

                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    //console.log(dataPDF);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");

                  }
                })

              });


              //boton para eliminar el documento

              $("#delete-curpPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si"},
                  beforeSend: function(){

                  },
                  success: function(data){
                      //cerramos el modal
                      $('#modal_curpPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_curpPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-curpPDF").hide();
                      $("#td-ok-upload-curpPDF").hide();
                      $("#td-trash-curpPDF").hide();
                      $("#file-curpPDF").hide();
                      $("#ok-upload-curpPDF").hide();
                      //$("#ok-validate-curpPDF").show();
                      $("#trash-curpPDF").hide();
                      $("#td-show_status-curpPDF").hide();
                      $("#td-show_status-recep-curpPDF").hide();
                      $("#td-show_status-fisic-curpPDF").hide();

                      //habilitamos los botones de seleccionar y guardar
                      $("#curpPDF").prop( "disabled", false );
                      $("#submit_curpPDF").prop( "disabled", false );

                      //mostramos mansaje
                      $("#mensaje_curpPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>CURP eliminado Correctamente</strong>"+
                                                    "</div>");
                  }
                })
              });

              break;
          }
        }
      })

    });
  });

  //Identificacion
  $(function(){
    $("#formIden_OficUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formIden_OficUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          //$("#submit_identOficPDF").prop( "disabled", true );
          $("#submit_identOficPDF").attr('disabled', 'true');
          $("#identOficPDFLoad").css({display: 'block'});
        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_identOficPDF").empty();

          $("#mensaje_identOficPDF").css({"display": 'block'});
          $("#identOficPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();


            $("#mensaje_identOficPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "identOficPDF":
              //desabilitamos el boton de selecion de documentos
              $("#identOficPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_identOficPDF").prop( "disabled", true );

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-identOficPDF").show();
              $("#td-ok-upload-identOficPDF").show();
              //$("#td-ok-validate-actaPDF").show();
              $("#td-trash-identOficPDF").show();
              $("#td-show_status-identOficPDF").show();
              $("#td-show_status-recep-identOficPDF").show();
              $("#td-show_status-fisic-identOficPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-identOficPDF").show();
              $("#ok-upload-identOficPDF").show();
              //$("#ok-validate-actaPDF").show();
              $("#trash-identOficPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();


              //mostramos mansaje
              $("#mensaje_identOficPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Identificacion Oficial Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-identOficPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-identOficPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_identOficPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_identOficPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-identOficPDF").hide();
                      $("#td-ok-upload-identOficPDF").hide();
                      $("#td-trash-identOficPDF").hide();
                      $("#file-identOficPDF").hide();
                      $("#ok-upload-identOficPDF").hide();
                      //$("#ok-validate-F69PDF").show();
                      $("#trash-identOficPDF").hide();
                      $("#td-show_status-identOficPDF").hide();
                      $("#td-show_status-recep-identOficPDF").hide();
                      $("#td-show_status-fisic-identOficPDF").hide();

                      //habilitamos los botones de seleccionar y guardar
                      $("#identOficPDF").prop( "disabled", false );
                      $("#submit_identOficPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_identOficPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Identificacion Oficial eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })

    });
  });

  // Titulo licenciatura
  $(function(){
    $("#formTituloUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formTituloUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_tituloPDF").attr('disabled', 'true');
          $("#tituloPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_tituloPDF").empty();

          $("#mensaje_tituloPDF").css({"display": 'block'});
          $("#tituloPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_tituloPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "tituloPDF":
              //desabilitamos el boton de selecion de documentos
              $("#tituloPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_tituloPDF").prop( "disabled", true );
              //$("#submit_tituloPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-tituloPDF").show();
              $("#td-ok-upload-tituloPDF").show();
              //$("#td-ok-validate-tituloPDF").show();
              $("#td-trash-tituloPDF").show();
              $("#td-show_status-tituloPDF").show();
              $("#td-show_status-recep-tituloPDF").show();
              $("#td-show_status-fisic-tituloPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-tituloPDF").show();
              $("#ok-upload-tituloPDF").show();
              //$("#ok-validate-tituloPDF").show();
              $("#trash-tituloPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_tituloPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Certificado de Licenciatura Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-tituloPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-tituloPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_tituloPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_tituloPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-tituloPDF").hide();
                      $("#td-ok-upload-tituloPDF").hide();
                      $("#td-trash-tituloPDF").hide();
                      $("#file-tituloPDF").hide();
                      $("#ok-upload-tituloPDF").hide();
                      //$("#ok-validate-tituloPDF").show();
                      $("#trash-tituloPDF").hide();
                      $("#td-show_status-tituloPDF").hide();
                      $("#td-show_status-recep-tituloPDF").hide();
                      $("#td-show_status-fisic-tituloPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#tituloPDF").prop( "disabled", false );
                      $("#submit_tituloPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_tituloPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Certificado de Licenciatura eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //Certificado
  $(function(){
    $("#formCertLicUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formCertLicUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_certifPDF").attr('disabled', 'true');
          $("#certifPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_certifPDF").empty();

          $("#mensaje_certifPDF").css({"display": 'block'});
          $("#certifPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_certifPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "certifPDF":
              //desabilitamos el boton de selecion de documentos
              $("#certifPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_certifPDF").prop( "disabled", true );
              //$("#submit_certifPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-certifPDF").show();
              $("#td-ok-upload-certifPDF").show();
              //$("#td-ok-validate-certifPDF").show();
              $("#td-trash-certifPDF").show();
              $("#td-show_status-certifPDF").show();
              $("#td-show_status-recep-certifPDF").show();
              $("#td-show_status-fisic-certifPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-certifPDF").show();
              $("#ok-upload-certifPDF").show();
              //$("#ok-validate-certifPDF").show();
              $("#trash-certifPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_certifPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Certificado de Licenciatura Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-certifPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-certifPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_certifPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_certifPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-certifPDF").hide();
                      $("#td-ok-upload-certifPDF").hide();
                      $("#td-trash-certifPDF").hide();
                      $("#file-certifPDF").hide();
                      $("#ok-upload-certifPDF").hide();
                      //$("#ok-validate-certifPDF").show();
                      $("#trash-certifPDF").hide();
                      $("#td-show_status-certifPDF").hide();
                      $("#td-show_status-recep-certifPDF").hide();
                      $("#td-show_status-fisic-certifPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#certifPDF").prop( "disabled", false );
                      $("#submit_certifPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_certifPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Certificado de Licenciatura eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //Constancia promedio
  $(function(){
    $("#formConstPromUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formConstPromUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_constPromPDF").attr('disabled', 'true');
          $("#constPromPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_constPromPDF").empty();

          $("#mensaje_constPromPDF").css({"display": 'block'});
          $("#constPromPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_constPromPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "constPromPDF":
              //desabilitamos el boton de selecion de documentos
              $("#constPromPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_constPromPDF").prop( "disabled", true );
              //$("#submit_constPromPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-constPromPDF").show();
              $("#td-ok-upload-constPromPDF").show();
              //$("#td-ok-validate-constPromPDF").show();
              $("#td-trash-constPromPDF").show();
              $("#td-show_status-constPromPDF").show();
              $("#td-show_status-recep-constPromPDF").show();
              $("#td-show_status-fisic-constPromPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-constPromPDF").show();
              $("#ok-upload-constPromPDF").show();
              //$("#ok-validate-constPromPDF").show();
              $("#trash-constPromPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_constPromPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Constancia de Promedio Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-constPromPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-constPromPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_constPromPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_constPromPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-constPromPDF").hide();
                      $("#td-ok-upload-constPromPDF").hide();
                      $("#td-trash-constPromPDF").hide();
                      $("#file-constPromPDF").hide();
                      $("#ok-upload-constPromPDF").hide();
                      //$("#ok-validate-constPromPDF").show();
                      $("#trash-constPromPDF").hide();
                      $("#td-show_status-constPromPDF").hide();
                      $("#td-show_status-recep-constPromPDF").hide();
                      $("#td-show_status-fisic-constPromPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#constPromPDF").prop( "disabled", false );
                      $("#submit_constPromPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_constPromPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Constancia de Promedio eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //Acta Examen prof
  $(function(){
    $("#formActaExamProfUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formActaExamProfUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_actaExamProfPDF").attr('disabled', 'true');
          $("#actaExamProfPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_actaExamProfPDF").empty();

          $("#mensaje_actaExamProfPDF").css({"display": 'block'});
          $("#actaExamProfPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_actaExamProfPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "actaExamProfPDF":
              //desabilitamos el boton de selecion de documentos
              $("#actaExamProfPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_actaExamProfPDF").prop( "disabled", true );
              //$("#submit_actaExamProfPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-actaExamProfPDF").show();
              $("#td-ok-upload-actaExamProfPDF").show();
              //$("#td-ok-validate-actaExamProfPDF").show();
              $("#td-trash-actaExamProfPDF").show();
              $("#td-show_status-actaExamProfPDF").show();
              $("#td-show_status-recep-actaExamProfPDF").show();
              $("#td-show_status-fisic-actaExamProfPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-actaExamProfPDF").show();
              $("#ok-upload-actaExamProfPDF").show();
              //$("#ok-validate-actaExamProfPDF").show();
              $("#trash-actaExamProfPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_actaExamProfPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Acta de Examen Profesional Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-actaExamProfPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-actaExamProfPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_actaExamProfPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_actaExamProfPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-actaExamProfPDF").hide();
                      $("#td-ok-upload-actaExamProfPDF").hide();
                      $("#td-trash-actaExamProfPDF").hide();
                      $("#file-actaExamProfPDF").hide();
                      $("#ok-upload-actaExamProfPDF").hide();
                      //$("#ok-validate-actaExamProfPDF").show();
                      $("#trash-actaExamProfPDF").hide();
                      $("#td-show_status-actaExamProfPDF").hide();
                      $("#td-show_status-recep-actaExamProfPDF").hide();
                      $("#td-show_status-fisic-actaExamProfPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#actaExamProfPDF").prop( "disabled", false );
                      $("#submit_actaExamProfPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_actaExamProfPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Acta de Examen Profesional eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //Carta de registro
  $(function(){
    $("#formCartaRegTitUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formCartaRegTitUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_cartaRegTitPDF").attr('disabled', 'true');
          $("#cartaRegTitPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_cartaRegTitPDF").empty();

          $("#mensaje_cartaRegTitPDF").css({"display": 'block'});
          $("#cartaRegTitPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_cartaRegTitPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "cartaRegTitPDF":
              //desabilitamos el boton de selecion de documentos
              $("#cartaRegTitPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_cartaRegTitPDF").prop( "disabled", true );
              //$("#submit_cartaRegTitPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-cartaRegTitPDF").show();
              $("#td-ok-upload-cartaRegTitPDF").show();
              //$("#td-ok-validate-cartaRegTitPDF").show();
              $("#td-trash-cartaRegTitPDF").show();
              $("#td-show_status-cartaRegTitPDF").show();
              $("#td-show_status-recep-cartaRegTitPDF").show();
              $("#td-show_status-fisic-cartaRegTitPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-cartaRegTitPDF").show();
              $("#ok-upload-cartaRegTitPDF").show();
              //$("#ok-validate-cartaRegTitPDF").show();
              $("#trash-cartaRegTitPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_cartaRegTitPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Carta de Registro Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-cartaRegTitPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-cartaRegTitPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_cartaRegTitPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_cartaRegTitPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-cartaRegTitPDF").hide();
                      $("#td-ok-upload-cartaRegTitPDF").hide();
                      $("#td-trash-cartaRegTitPDF").hide();
                      $("#file-cartaRegTitPDF").hide();
                      $("#ok-upload-cartaRegTitPDF").hide();
                      //$("#ok-validate-cartaRegTitPDF").show();
                      $("#trash-cartaRegTitPDF").hide();
                      $("#td-show_status-cartaRegTitPDF").hide();
                      $("#td-show_status-recep-cartaRegTitPDF").hide();
                      $("#td-show_status-fisic-cartaRegTitPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#cartaRegTitPDF").prop( "disabled", false );
                      $("#submit_cartaRegTitPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_cartaRegTitPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Carta de Registro eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //GRADO DE MAESTRÍA
  $(function(){
    $("#formgradoMaestriaUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formgradoMaestriaUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_gradoMaestriaPDF").attr('disabled', 'true');
          $("#gradoMaestriaPDFLoad").css({display: 'block'});


        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_gradoMaestriaPDF").empty();

          $("#mensaje_gradoMaestriaPDF").css({"display": 'block'});
          $("#gradoMaestriaPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //$("#mensaje_gradoMaestriaPDF").empty();
              $("#mensaje_carRegModGradPDF").empty();
              $("#mensaje_certMaestriPDF").empty();

              $("#mensaje_gradoMaestriaPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "gradoMaestriaPDF":
              //desabilitamos el boton de selecion de documentos
              $("#gradoMaestriaPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_gradoMaestriaPDF").prop( "disabled", true );
              //$("#submit_gradoMaestriaPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-gradoMaestriaPDF").show();
              $("#td-ok-upload-gradoMaestriaPDF").show();
              //$("#td-ok-validate-gradoMaestriaPDF").show();
              $("#td-trash-gradoMaestriaPDF").show();
              $("#td-show_status-gradoMaestriaPDF").show();
              $("#td-show_status-recep-gradoMaestriaPDF").show();
              $("#td-show_status-fisic-gradoMaestriaPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-gradoMaestriaPDF").show();
              $("#ok-upload-gradoMaestriaPDF").show();
              //$("#ok-validate-gradoMaestriaPDF").show();
              $("#trash-gradoMaestriaPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();
              //$("#mensaje_gradoMaestriaPDF").empty();
              $("#mensaje_carRegModGradPDF").empty();
              $("#mensaje_certMaestriPDF").empty();


              //mostramos mansaje
              $("#mensaje_gradoMaestriaPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Grado de maestría guardado correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-gradoMaestriaPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-gradoMaestriaPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_gradoMaestriaPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_gradoMaestriaPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-gradoMaestriaPDF").hide();
                      $("#td-ok-upload-gradoMaestriaPDF").hide();
                      $("#td-trash-gradoMaestriaPDF").hide();
                      $("#file-gradoMaestriaPDF").hide();
                      $("#ok-upload-gradoMaestriaPDF").hide();
                      //$("#ok-validate-gradoMaestriaPDF").show();
                      $("#trash-gradoMaestriaPDF").hide();
                      $("#td-show_status-gradoMaestriaPDF").hide();
                      $("#td-show_status-recep-gradoMaestriaPDF").hide();
                      $("#td-show_status-fisic-gradoMaestriaPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#gradoMaestriaPDF").prop( "disabled", false );
                      $("#submit_gradoMaestriaPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_gradoMaestriaPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();
                      //$("#mensaje_gradoMaestriaPDF").empty();
                      $("#mensaje_carRegModGradPDF").empty();
                      $("#mensaje_certMaestriPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_gradoMaestriaPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Grado de maestría eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //CARTA DE REGISTRO A LA MODALIDAD DE GRADUACIÓN
  $(function(){
    $("#formcarRegModGradUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formcarRegModGradUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_carRegModGradPDF").attr('disabled', 'true');
          $("#carRegModGradPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_carRegModGradPDF").empty();

          $("#mensaje_carRegModGradPDF").css({"display": 'block'});
          $("#carRegModGradPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();
              $("#mensaje_gradoMaestriaPDF").empty();
              //$("#mensaje_carRegModGradPDF").empty();
              $("#mensaje_certMaestriPDF").empty();


              $("#mensaje_carRegModGradPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "carRegModGradPDF":
              //desabilitamos el boton de selecion de documentos
              $("#carRegModGradPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_carRegModGradPDF").prop( "disabled", true );
              //$("#submit_carRegModGradPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-carRegModGradPDF").show();
              $("#td-ok-upload-carRegModGradPDF").show();
              //$("#td-ok-validate-carRegModGradPDF").show();
              $("#td-trash-carRegModGradPDF").show();
              $("#td-show_status-carRegModGradPDF").show();
              $("#td-show_status-recep-carRegModGradPDF").show();
              $("#td-show_status-fisic-carRegModGradPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-carRegModGradPDF").show();
              $("#ok-upload-carRegModGradPDF").show();
              //$("#ok-validate-carRegModGradPDF").show();
              $("#trash-carRegModGradPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();
              $("#mensaje_gradoMaestriaPDF").empty();
              //$("#mensaje_carRegModGradPDF").empty();
              $("#mensaje_certMaestriPDF").empty();

              //mostramos mansaje
              $("#mensaje_carRegModGradPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Carta de Registro Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-carRegModGradPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-carRegModGradPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_carRegModGradPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_carRegModGradPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-carRegModGradPDF").hide();
                      $("#td-ok-upload-carRegModGradPDF").hide();
                      $("#td-trash-carRegModGradPDF").hide();
                      $("#file-carRegModGradPDF").hide();
                      $("#ok-upload-carRegModGradPDF").hide();
                      //$("#ok-validate-carRegModGradPDF").show();
                      $("#trash-carRegModGradPDF").hide();
                      $("#td-show_status-carRegModGradPDF").hide();
                      $("#td-show_status-recep-carRegModGradPDF").hide();
                      $("#td-show_status-fisic-carRegModGradPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#carRegModGradPDF").prop( "disabled", false );
                      $("#submit_carRegModGradPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();
                      $("#mensaje_gradoMaestriaPDF").empty();
                      //$("#mensaje_carRegModGradPDF").empty();
                      $("#mensaje_certMaestriPDF").empty();


                      //mostramos mansaje
                      $("#mensaje_carRegModGradPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Carta de Registro eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //CERTIFICADO DE MAESTRÍA
  $(function(){
    $("#formcertMaestriUploadPDF").on("submit", function(e){
      e.preventDefault();
      var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formcertMaestriUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_certMaestriPDF").attr('disabled', 'true');
          $("#certMaestriPDFLoad").css({display: 'block'});

        },
        success: function(data){
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];

          $("#mensaje_certMaestriPDF").empty();

          $("#mensaje_certMaestriPDF").css({"display": 'block'});
          $("#certMaestriPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();
              $("#mensaje_gradoMaestriaPDF").empty();
              $("#mensaje_carRegModGradPDF").empty();
              //$("#mensaje_certMaestriPDF").empty();

              $("#mensaje_certMaestriPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "certMaestriPDF":
              //desabilitamos el boton de selecion de documentos
              $("#certMaestriPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos
              //$("#submit_certMaestriPDF").prop( "disabled", true );
              //$("#submit_certMaestriPDF").attr('disabled', 'true');

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-certMaestriPDF").show();
              $("#td-ok-upload-certMaestriPDF").show();
              //$("#td-ok-validate-certMaestriPDF").show();
              $("#td-trash-certMaestriPDF").show();
              $("#td-show_status-certMaestriPDF").show();
              $("#td-show_status-recep-certMaestriPDF").show();
              $("#td-show_status-fisic-certMaestriPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-certMaestriPDF").show();
              $("#ok-upload-certMaestriPDF").show();
              //$("#ok-validate-certMaestriPDF").show();
              $("#trash-certMaestriPDF").show();

              $("#mensaje_actaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();
              $("#mensaje_gradoMaestriaPDF").empty();
              $("#mensaje_carRegModGradPDF").empty();
              //$("#mensaje_certMaestriPDF").empty();

              //mostramos mansaje
              $("#mensaje_certMaestriPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Certificado de maestría Guardado Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-certMaestriPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-certMaestriPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_certMaestriPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_certMaestriPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-certMaestriPDF").hide();
                      $("#td-ok-upload-certMaestriPDF").hide();
                      $("#td-trash-certMaestriPDF").hide();
                      $("#file-certMaestriPDF").hide();
                      $("#ok-upload-certMaestriPDF").hide();
                      //$("#ok-validate-certMaestriPDF").show();
                      $("#trash-certMaestriPDF").hide();
                      $("#td-show_status-certMaestriPDF").hide();
                      $("#td-show_status-recep-certMaestriPDF").hide();
                      $("#td-show_status-fisic-certMaestriPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#certMaestriPDF").prop( "disabled", false );
                      $("#submit_certMaestriPDF").prop( "disabled", false );

                      $("#mensaje_actaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();
                      $("#mensaje_gradoMaestriaPDF").empty();
                      $("#mensaje_carRegModGradPDF").empty();
                      //$("#mensaje_certMaestriPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_certMaestriPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Certificado de maestría eliminado Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });


  //FORM  CONSTANCIA DE PROM MAESTRIA
  $(function(){
    $("#formconstPromMaestriaUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formconstPromMaestriaUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_constPromMaestriaPDF").attr('disabled', 'true');
          $("#constPromMaestriaPDF").css({display: 'block'});

        },
        success: function(data){
          //console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_constPromMaestriaPDF").empty();
          $("#mensaje_constPromMaestriaPDF").css({"display": 'block'});
          $("#constPromMaestriaPDF").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_constPromMaestriaPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_constPromMaestriaPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "constPromMaestriaPDF":
              //desabilitamos el boton de selecion de documentos
              $("#constPromMaestriaPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-constPromMaestriaPDF").show();
              $("#td-ok-upload-constPromMaestriaPDF").show();
              //$("#td-ok-validate-constPromMaestriaPDF").show();
              $("#td-trash-constPromMaestriaPDF").show();
              $("#td-show_status-constPromMaestriaPDF").show();
              $("#td-show_status-recep-constPromMaestriaPDF").show();
              $("#td-show_status-fisic-constPromMaestriaPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-constPromMaestriaPDF").show();
              $("#ok-upload-constPromMaestriaPDF").show();
              //$("#ok-validate-constPromMaestriaPDF").show();
              $("#trash-constPromMaestriaPDF").show();

              $("#mensaje_constPromMaestriaPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_constPromMaestriaPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Constancia de promedio de maestría Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-constPromMaestriaPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-constPromMaestriaPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_constPromMaestriaPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_constPromMaestriaPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-constPromMaestriaPDF").hide();
                      $("#td-ok-upload-constPromMaestriaPDF").hide();
                      $("#td-trash-constPromMaestriaPDF").hide();
                      $("#file-constPromMaestriaPDF").hide();
                      $("#ok-upload-constPromMaestriaPDF").hide();
                      //$("#ok-validate-constPromMaestriaPDF").show();
                      $("#trash-constPromMaestriaPDF").hide();
                      $("#td-show_status-constPromMaestriaPDF").hide();
                      $("#td-show_status-recep-constPromMaestriaPDF").hide();
                      $("#td-show_status-fisic-constPromMaestriaPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#constPromMaestriaPDF").prop( "disabled", false );
                      $("#submit_constPromMaestriaPDF").prop( "disabled", false );
                      $("#mensaje_constPromMaestriaPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_constPromMaestriaPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Constancia de promedio de maestría eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });


  //FORM Equivalencia promedio
  $(function(){
    $("#formequivaPromUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formequivaPromUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_equivaPromPDF").attr('disabled', 'true');
          $("#equivaPromPDFLoad").css({display: 'block'});

        },
        success: function(data){
          console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_equivaPromPDF").empty();
          $("#mensaje_equivaPromPDF").css({"display": 'block'});
          $("#equivaPromPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_equivaPromPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_equivaPromPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "equivaPromPDF":
              //desabilitamos el boton de selecion de documentos
              $("#equivaPromPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-equivaPromPDF").show();
              $("#td-ok-upload-equivaPromPDF").show();
              //$("#td-ok-validate-equivaPromPDF").show();
              $("#td-trash-equivaPromPDF").show();
              $("#td-show_status-equivaPromPDF").show();
              $("#td-show_status-recep-equivaPromPDF").show();
              $("#td-show_status-fisic-equivaPromPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-equivaPromPDF").show();
              $("#ok-upload-equivaPromPDF").show();
              //$("#ok-validate-equivaPromPDF").show();
              $("#trash-equivaPromPDF").show();

              $("#mensaje_equivaPromPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_equivaPromPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Equivalencia de Promedio Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-equivaPromPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-equivaPromPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_equivaPromPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_equivaPromPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-equivaPromPDF").hide();
                      $("#td-ok-upload-equivaPromPDF").hide();
                      $("#td-trash-equivaPromPDF").hide();
                      $("#file-equivaPromPDF").hide();
                      $("#ok-upload-equivaPromPDF").hide();
                      //$("#ok-validate-equivaPromPDF").show();
                      $("#trash-equivaPromPDF").hide();
                      $("#td-show_status-equivaPromPDF").hide();
                      $("#td-show_status-recep-equivaPromPDF").hide();
                      $("#td-show_status-fisic-equivaPromPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#equivaPromPDF").prop( "disabled", false );
                      $("#submit_equivaPromPDF").prop( "disabled", false );
                      $("#mensaje_equivaPromPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_equivaPromPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Equivalencia de Promedio eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //FORM cédula profesional
  $(function(){
    $("#formcedProfUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formcedProfUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_cedProfPDF").attr('disabled', 'true');
          $("#cedProfPDFLoad").css({display: 'block'});

        },
        success: function(data){
          console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_cedProfPDF").empty();
          $("#mensaje_cedProfPDF").css({"display": 'block'});
          $("#cedProfPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_cedProfPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_cedProfPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "cedProfPDF":
              //desabilitamos el boton de selecion de documentos
              $("#cedProfPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-cedProfPDF").show();
              $("#td-ok-upload-cedProfPDF").show();
              //$("#td-ok-validate-cedProfPDF").show();
              $("#td-trash-cedProfPDF").show();
              $("#td-show_status-cedProfPDF").show();
              $("#td-show_status-recep-cedProfPDF").show();
              $("#td-show_status-fisic-cedProfPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-cedProfPDF").show();
              $("#ok-upload-cedProfPDF").show();
              //$("#ok-validate-cedProfPDF").show();
              $("#trash-cedProfPDF").show();

              $("#mensaje_cedProfPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_cedProfPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Cédula Profesional Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-cedProfPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-cedProfPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_cedProfPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_cedProfPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-cedProfPDF").hide();
                      $("#td-ok-upload-cedProfPDF").hide();
                      $("#td-trash-cedProfPDF").hide();
                      $("#file-cedProfPDF").hide();
                      $("#ok-upload-cedProfPDF").hide();
                      //$("#ok-validate-cedProfPDF").show();
                      $("#trash-cedProfPDF").hide();
                      $("#td-show_status-cedProfPDF").hide();
                      $("#td-show_status-recep-cedProfPDF").hide();
                      $("#td-show_status-fisic-cedProfPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#cedProfPDF").prop( "disabled", false );
                      $("#submit_cedProfPDF").prop( "disabled", false );
                      $("#mensaje_cedProfPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_cedProfPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Cédula Profesional eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //FORM comprension de lectura 
  $(function(){
    $("#formconsCompreLectuUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formconsCompreLectuUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_consCompreLectuPDF").attr('disabled', 'true');
          $("#consCompreLectuPDFLoad").css({display: 'block'});

        },
        success: function(data){
          console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_consCompreLectuPDF").empty();
          $("#mensaje_consCompreLectuPDF").css({"display": 'block'});
          $("#consCompreLectuPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_consCompreLectuPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_consCompreLectuPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "consCompreLectuPDF":
              //desabilitamos el boton de selecion de documentos
              $("#consCompreLectuPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-consCompreLectuPDF").show();
              $("#td-ok-upload-consCompreLectuPDF").show();
              //$("#td-ok-validate-consCompreLectuPDF").show();
              $("#td-trash-consCompreLectuPDF").show();
              $("#td-show_status-consCompreLectuPDF").show();
              $("#td-show_status-recep-consCompreLectuPDF").show();
              $("#td-show_status-fisic-consCompreLectuPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-consCompreLectuPDF").show();
              $("#ok-upload-consCompreLectuPDF").show();
              //$("#ok-validate-consCompreLectuPDF").show();
              $("#trash-consCompreLectuPDF").show();

              $("#mensaje_consCompreLectuPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_consCompreLectuPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Constancia de Lectura Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-consCompreLectuPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-consCompreLectuPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_consCompreLectuPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_consCompreLectuPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-consCompreLectuPDF").hide();
                      $("#td-ok-upload-consCompreLectuPDF").hide();
                      $("#td-trash-consCompreLectuPDF").hide();
                      $("#file-consCompreLectuPDF").hide();
                      $("#ok-upload-consCompreLectuPDF").hide();
                      //$("#ok-validate-consCompreLectuPDF").show();
                      $("#trash-consCompreLectuPDF").hide();
                      $("#td-show_status-consCompreLectuPDF").hide();
                      $("#td-show_status-recep-consCompreLectuPDF").hide();
                      $("#td-show_status-fisic-consCompreLectuPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#consCompreLectuPDF").prop( "disabled", false );
                      $("#submit_consCompreLectuPDF").prop( "disabled", false );
                      $("#mensaje_consCompreLectuPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_consCompreLectuPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Constancia de Lectura eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //FORM constancia dominio español
  $(function(){
    $("#formconsEspanolUploadPDF").on("submit", function(e){
      e.preventDefault();
      //var referencia = $("#referencia").val();
      var reference = $("#reference").val();
      var f = $(this);
      var formData = new FormData(document.getElementById("formconsEspanolUploadPDF"));
      //guardar pdf en bd
      $.ajax({
        url: './../../../controllers/aspirante/uploadDocuments_controller.php',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(){
          $("#submit_consEspanolPDF").attr('disabled', 'true');
          $("#consEspanolPDFLoad").css({display: 'block'});

        },
        success: function(data){
          console.log(data);
          var myobject          = jQuery.parseJSON(data);
          //filaInsertada
          var idDocumentoInsert = myobject['filaInsertada'];
          var docInsertado      = myobject['nombreDoc'];
          var cat_doc_insert    = myobject['cat_tipo_doc'];


          $("#mensaje_consEspanolPDF").empty();
          $("#mensaje_consEspanolPDF").css({"display": 'block'});
          $("#consEspanolPDFLoad").css({display: 'none'});

          switch(docInsertado) {
            case "error_tipo":

          $("#mensaje_consEspanolPDF").empty();
          $("#mensaje_curpPDF").empty();
          $("#mensaje_certifPDF").empty();
          $("#mensaje_tituloPDF").empty();
          $("#mensaje_identOficPDF").empty();
          $("#mensaje_constPromPDF").empty();
          $("#mensaje_actaExamProfPDF").empty();
          $("#mensaje_cartaRegTitPDF").empty();

              $("#mensaje_consEspanolPDF").html("<div class='alert alert-danger text-center'>"+
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                            "<strong>El tipo de archivo no es válido</strong> debe de ser de tipo .pdf"+
                                          "</div>");
              break;
            case "consEspanolPDF":
              //desabilitamos el boton de selecion de documentos
              $("#consEspanolPDF").prop( "disabled", true );
              //desabilitamos el boton de guardar de documentos

              //habilitamos las titulos de la tabla
              $("#ver").show();
              $("#save").show();
              //$("#saved").show();
              $("#trash").show();
              $("#status").show();
              $("#status_rec").show();
              $("#status_fis").show();

              //habilitamos las columnas de la tabla
              $("#td-file-consEspanolPDF").show();
              $("#td-ok-upload-consEspanolPDF").show();
              //$("#td-ok-validate-consEspanolPDF").show();
              $("#td-trash-consEspanolPDF").show();
              $("#td-show_status-consEspanolPDF").show();
              $("#td-show_status-recep-consEspanolPDF").show();
              $("#td-show_status-fisic-consEspanolPDF").show();

              //habilitamos las iconos de las filas de la tabla
              $("#file-consEspanolPDF").show();
              $("#ok-upload-consEspanolPDF").show();
              //$("#ok-validate-consEspanolPDF").show();
              $("#trash-consEspanolPDF").show();

              $("#mensaje_consEspanolPDF").empty();
              $("#mensaje_curpPDF").empty();
              $("#mensaje_certifPDF").empty();
              $("#mensaje_tituloPDF").empty();
              $("#mensaje_identOficPDF").empty();
              $("#mensaje_constPromPDF").empty();
              $("#mensaje_actaExamProfPDF").empty();
              $("#mensaje_cartaRegTitPDF").empty();

              //mostramos mansaje
              $("#mensaje_consEspanolPDF").append( "<div class='alert alert-success text-center'>"+
                                              "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                              "<strong>Constancia de Dominio de Español Guardada Correctamente</strong>"+
                                            "</div>");
              //boton para ver el pdf
              $("#file-consEspanolPDF").click(function(event) {
                $.ajax({
                  url: './../../../controllers/aspirante/showDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: { "idDocumentoInsert": idDocumentoInsert, "previewDoc": 'si'},
                  beforeSend: function(){
                  },
                  success: function(data){
                    //console.log(data);
                    var dataPDF = jQuery.parseJSON(data);
                    crearFrame("../../../uploads/"+reference+"/"+reference+"_"+cat_doc_insert+".pdf");
                  }
                })

              });


              //boton para eliminar el documento
              $("#delete-consEspanolPDF").click(function(event) {
                //var name_archivo = "../../uploads/"+referencia+"/"+referencia+"_"+cat_doc_insert+".pdf";
                //alert (name_archivo);
                //alert("deletes");
                $.ajax({
                  url: './../../../controllers/aspirante/deleteDocument_controller.php',
                  type: 'POST',
                  dataType: 'html',
                  data: {"idDocumentoInsert": idDocumentoInsert, "deleteDoc": "si" },
                  beforeSend: function(){
                  },
                  success: function(data){
                      $('#modal_consEspanolPDF').modal('toggle');
                      //limpiamos el mensaje
                      $("#mensaje_consEspanolPDF").empty();

                      //deshabilitamos las iconos de las filas de la tabla
                      $("#td-file-consEspanolPDF").hide();
                      $("#td-ok-upload-consEspanolPDF").hide();
                      $("#td-trash-consEspanolPDF").hide();
                      $("#file-consEspanolPDF").hide();
                      $("#ok-upload-consEspanolPDF").hide();
                      //$("#ok-validate-consEspanolPDF").show();
                      $("#trash-consEspanolPDF").hide();
                      $("#td-show_status-consEspanolPDF").hide();
                      $("#td-show_status-recep-consEspanolPDF").hide();
                      $("#td-show_status-fisic-consEspanolPDF").hide();


                      //habilitamos los botones de seleccionar y guardar
                      $("#consEspanolPDF").prop( "disabled", false );
                      $("#submit_consEspanolPDF").prop( "disabled", false );
                      $("#mensaje_consEspanolPDF").empty();
                      $("#mensaje_curpPDF").empty();
                      $("#mensaje_certifPDF").empty();
                      $("#mensaje_tituloPDF").empty();
                      $("#mensaje_identOficPDF").empty();
                      $("#mensaje_constPromPDF").empty();
                      $("#mensaje_actaExamProfPDF").empty();
                      $("#mensaje_cartaRegTitPDF").empty();

                      //mostramos mansaje
                      $("#mensaje_consEspanolPDF").append( "<div class='alert alert-danger text-center'>"+
                                                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                                                      "<strong>Constancia de Dominio de Español eliminada Correctamente</strong>"+
                                                    "</div>");

                  }
                })
              });

              break;
          }
        }
      })
      .fail(function() {
        console.log("error");
      })

    });
  });

  //funccion para crear un iframe y mostrar el pdf
  function crearFrame(pdf) {
    //var testFrame = document.createElement("IFRAME");
    var testFrame = document.createElement("iframe");

    $("#testFrame").remove();
    testFrame.id          = "testFrame";
    testFrame.width       = "100%";
    testFrame.height      = "1000px";
    testFrame.scrolling   = "no";
    testFrame.frameborder = "10px";
    testFrame.src = pdf; //Sacar el nombre del fichero pdf desde el parametro

    var control = document.getElementById("testFrame")

    if (control==null) {
        $('#pdf').append(document.body.appendChild(testFrame));
    }
  }

})

