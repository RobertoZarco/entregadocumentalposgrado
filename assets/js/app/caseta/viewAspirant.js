$(document).ready(function($) {

	var claveDocumento = null;
	var idDocumentChange = null;


	//validar documentos fisicos
	$('input[id^="checkedPhysical"]').click(function(event) {
		var validatedPhysicalDocument = $(this).val();
		var idDocument = $(this).attr('data-idDocument');
		var typeDocumnet = $(this).attr('data-typedocument');



		if(validatedPhysicalDocument == 1){
			$(this).attr('value', '0');
			$("#statusPhysical-"+idDocument).removeClass('glyphicon-ok');
			$("#statusPhysical-"+idDocument).addClass('glyphicon-remove')
			$("#statusPhysical-"+idDocument).css({color: '#FF0000'});

			if(typeDocumnet == 4){
				$("#savePromCertLic").css('display', 'none');
			}

			if(typeDocumnet == 11){
				$("#savePromCertMaes").css('display', 'none');
			}


		}else{
			$(this).attr('value', '1');
			$("#statusPhysical-"+idDocument).removeClass('glyphicon-remove');
			$("#statusPhysical-"+idDocument).addClass('glyphicon-ok')
			$("#statusPhysical-"+idDocument).css({color: '#449d44'});

			if(typeDocumnet == 4){
				$("#savePromCertLic").css('display', 'block');
			}

			if(typeDocumnet == 11){
				$("#savePromCertMaes").css('display', 'block');
			}

		}

		var validated  = $(this).attr('value');
		var idDocument = $(this).attr('data-idDocument');

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"idDocument": idDocument,
							"validated": validated,
							"updateDocumentPhysical": "si"
						},
			beforeSend:function(){

			},
			success: function(data){
				//alert(data);
			}
		});
	});



	$('button[id^="viewDocument"]').click(function(event) {
		var pathDocument = $(this).attr('data-path');

		if (pathDocument === ''){
			alert ('Documento registrado desde caseta');
		}else{
			crearFrame("../../../"+pathDocument);
		}
	});


	$("#btnSavePromCertLic").click(function(event) {
		event.preventDefault();
		var promCert   = $("#promCertLic").val();
		var folioCert  = $("#folioCertLic").val();
		var reference  = $("#referenceLic").val();
		var idDocument = $("#idDocumentLic").val();


		if(promCert <= "0700"){
			alert("menor a 7");
			//break;
		}else if(promCert => "1001"){
			alert("mayor a 10");
			//break;
		}

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"promCert": promCert,
							"folioCert": folioCert,
							"reference": reference,
							"idDocument": idDocument,
							"savePromCert": "si"
						},
			beforeSend: function(){
				//loader
			},
			success: function(data){
				alert(data);
			}
		})

	});


	$("#btnSavePromCertMaes").click(function(event) {
		event.preventDefault();
		var promCert   = $("#promCertMaes").val();
		var folioCert  = $("#folioCertMaes").val();
		var reference  = $("#referenceMaes").val();
		var idDocument = $("#idDocumentMaes").val();

		if(promCert <= "0700"){
			alert("menor a 7");
			//break;
		}else if(promCert => "1001"){
			alert("mayor a 10");
			//break;
		}

		$.ajax({
			url: '../../../controllers/admin/viewAspirant_controller.php',
			type: 'POST',
			dataType: 'html',
			data: {	"promCert": promCert,
							"folioCert": folioCert,
							"reference": reference,
							"idDocument": idDocument,
							"savePromCert": "si"
						},
			beforeSend: function(){
				//loader
			},
			success: function(data){
				alert(data);
			}
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});






	$('input[id^="DocFaltantes"]').click(function(event) {

		var checked = $(this).is(':checked');
		var idAspirant = $(this).attr('data-idaspirant');
		var DocUpload = $(this).attr('data-value');

    if (checked == true){
	    //Insertar Doc Faltante
			$.ajax({
				url: '../../../controllers/caseta/documents_controller.php',
				type: 'POST',
				dataType: 'html',
				data: {	"insertDocFaltante": 'si',
								"DocInsert":DocUpload,
								"idAspirant":idAspirant
							},
				beforeSend:function(){
				},
				success:function(data){
					alert(data);
				var datos = jQuery.parseJSON(data);

					if(datos['insertado'] == "ok"){
						//alert ("Ok");
						$("#modaUpdatedDoc").modal("toggle");
					}else{
						alert ("Error al actualizar el documento");
					}

				}

			});
    }else{
    	//Eliminar Registro
    	$.ajax({
				url: '../../../controllers/caseta/documents_controller.php',
				type: 'POST',
				dataType: 'html',
				data: {	"deleteDocFaltante": 'si',
								"DocInsert":DocUpload,
								"idAspirant":idAspirant
							},
				beforeSend:function(){
				},
				success:function(data){
					var datos = jQuery.parseJSON(data);

					if(datos['eliminado'] == "ok"){
						//alert ("Ok eliminado");
						$("#modaDelete").modal("toggle");
					}else{
						alert ("Error al eliminar documento");
					}
				}
			});
    }

	});



});



//funccion para crear un iframe y mostrar el pdf
function crearFrame(pdf) {
  //var testFrame = document.createElement("IFRAME");
  var testFrame = document.createElement("iframe");

  $("#testFrame").remove();
  testFrame.id          = "testFrame";
  testFrame.width       = "100%";
  testFrame.height      = "1000px";
  testFrame.scrolling   = "no";
  testFrame.frameborder = "10px";
  testFrame.src = pdf; //Sacar el nombre del fichero pdf desde el parametro

  var control = document.getElementById("testFrame")

  if (control==null) {
      $('#pdf').append(document.body.appendChild(testFrame));
  }
}