$(document).ready(function(){
	registrerMessages();
	$.ajaxSetup({"cache":false});

	//setInterval("loadOldMessage()", 1000);
})

var registrerMessages = function(){
	$("#send").click(function(event) {
		event.preventDefault();
		//serialize obtiene todos los valores que estan dentro del formularios
		//var frm = $("#formChat").serialize();
		var referencia = $("#referenciaUserChat").val();
		var idUsuarios = $("#idUsuarios").val();
		var message    = $("#message").val();
		var receptor   = $("#idReceptor").val();
		console.log(receptor);
		//console.log(frm);
		$.ajax({
			url: '../../../controllers/chat_controller.php',
			type: 'POST',
			dataType: 'html',
			data:{'idUsuarios': idUsuarios,
						'referencia': referencia,
						'message': message,
						'receptor': receptor,
						"insertMessage": "si"},
			beforeSend: function(){

			},
			success: function(data){
				//console.log(data);
				$("#message").val("");
				var altura = $("#conversation").prop("scrollHeight");
				$("#conversation").scrollTop(altura);
			}
		})
	});
}


var loadOldMessage = function(){
	var emisor   = $("#referenciaUserChat").val();
	var receptor = $("#idReceptor").val();
	$.ajax({
		url: '../../../controllers/chat_controller.php',
		type: 'POST',
		dataType: 'html',
		data: {"emisor":emisor, "receptor": receptor,  "searchConversation": 'si'},
		beforeSend: function(){

		},
		success: function(data){
			$("#conversation").html(data);
			$("#conversation p:last-child").css({
																						"background-color": "lightgreen",
																						"padding-botton": "20px"
																					});

			//$("#message").val("");
			var altura = $("#conversation").prop("scrollHeight");
			$("#conversation").scrollTop(altura);
		}
	});
}


