<?php
require_once '../../properties/properties.inc';
require_once "../../db/mysql.php";
/**
*
*/
class CasetaDocuments extends Conexion{
  private $_uploadDocument = null;
  private $_docAspirante   = null;
  private $_showDocument   = null;
  private $_deleteDocument = null;

  function __construct(){
    $this -> _uploadDocument = array();
    $this -> _docAspirante   = array();
    $this -> _showDocument   = array();
    $this -> _deleteDocument = array();
  }

  public function insertDocCaseta($plan_type_doc, $idAspirant){

    $fechaAlta   = date("Y-m-d h:i:s");
    $ipAlta      = $_SERVER['REMOTE_ADDR'];
    $userAlta    = $_SESSION['id_user'];

    $digital_document_uploaded = 1; //documento subido
    $ptd_id_plan_type_document = $plan_type_doc;


    $this -> conectar();


    $insert = " INSERT into documents_aspirants (aspirant_id, document_path, digital_document_uploaded, 
                            plan_type_doc, ptd_id_plan_type_document, 
                            created_at, user_created, ip_created)
                values('$idAspirant', null , $digital_document_uploaded, '$plan_type_doc', 
                        $ptd_id_plan_type_document, 
                        now(), $userAlta, '$ipAlta')";

    //echo "<br>$insert<br>";
    //die();

    //ejecutamos el query
    $this -> consulta($insert);
    //si hay filas afectadas

    //echo $this -> filasAfectadas();
    if($this -> filasAfectadas() == 1){
      $filaInsertada = $this -> ultimaFila();

      $response = array("insertado"=> "ok");
      echo json_encode($response);

    }else{

      $response = array("insertado"=> "no");
      echo json_encode($response);
    }
  }


  public function deleteDocCaseta($plan_type_doc, $idAspirant){

    $this -> conectar();

    //preparamos la query
    $delete = "DELETE from documents_aspirants 
                      where aspirant_id = $idAspirant
                      and   plan_type_doc= $plan_type_doc
                      and   ptd_id_plan_type_document= $plan_type_doc";

    //echo $delete;
    //die();

    //ejecutamos la query
    $this -> consulta($delete);

    if($this -> filasAfectadas() == 1){
      $response = array("eliminado"=> "ok");
      echo json_encode($response);
    }else{
      $response = array("eliminado"=> "no");
      echo json_encode($response);
    }
  }




}

?>