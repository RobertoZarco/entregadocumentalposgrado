<?php
require_once '../../../properties/properties.inc';
require_once "../../../db/mysql.php";
/**
*
*/
class Caseta extends Conexion{

	function __construct(){
	}

	public function findAspirant ($findAspirant){

		//echo $findAspirant;

		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT  id_aspirant, account_number, first_surname, second_surname, name, email,
											validate, plan_id_plan, dependency
              FROM    aspirants
              WHERE   account_number = $findAspirant";

		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _usuarios[] = $row;
			}
			return $this -> _usuarios;
		}else{
			return false;
		}
	}

	public function getDocumentoByReference($account_number){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  id_aspirant, reference, account_number, ptd_id_plan_type_document, id_document_aspirant, validate,
                      document_path, digital_document_uploaded, validated_digital_document,
                      validated_physical_document, physical_document_received, validated_physical_document, tag, plan_type_doc,
                      id_type_document, document, average, certificate_sheet
              from    aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd
              where   account_number = '$account_number'
              and     id_aspirant = aspirant_id
              and     id_type_document = plan_type_doc
              and     id_plan_type_document = ptd_id_plan_type_document
              and     digital_document_uploaded = 1";

    //echo json_encode($query);
    //echo "<br>$query<br>";
    //die();

    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocuments[] = $row;
      }
      return $this -> _homeDocuments;
    }
  }


  public function getDocumentoByPlan($plan){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  type_document_document_id
              from    plan_type_documents
              where   plan_plan_id = $plan;";

    //echo json_encode($query);

    //echo "<br>$query<br>";
    //die();
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocumentsPlan[] = $row;
      }
      return $this -> _homeDocumentsPlan;
    }
  }

  public function getDocumentoByPlanRef($account_number){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  plan_type_doc
              from    aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd
              where   account_number = '$account_number'
              and     id_aspirant = aspirant_id
              and     id_type_document = plan_type_doc
              and     id_plan_type_document = ptd_id_plan_type_document
              and     digital_document_uploaded = 1";

    //echo json_encode($query);

    //echo "<br>$query<br>";
    //die();
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _DocumetsAspirant[] = $row;
      }
      return $this -> _DocumetsAspirant;
    }
  }

}

?>