<?php
require_once './../../../db/mysql.php';
require_once './../../../properties/properties.inc';


/**
*
*/
class HomeAspirante extends Conexion{

	private $_homeDocuments = null;
	private $_catTipoDoc = null;
	private $_planTypeDocument = null;

	function __construct(){
		$this -> _homDocuments     = array();
		$this -> _catTipoDoc       = array();
		$this -> _planTypeDocument = array();
	}

	public function getDocumentoByReference($reference){
		//nos conectamos
		$this -> conectar();
		//preparamos la query

		$query = "SELECT 	id_aspirant, reference, account_number, ptd_id_plan_type_document, id_document_aspirant,
                      document_path, digital_document_uploaded, validated_digital_document,
                      validated_physical_document, physical_document_received, tag, plan_type_doc,
                      id_type_document, document
							from		aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd
							where 	reference = '$reference'
							and 		id_aspirant = aspirant_id
							and 		id_type_document = plan_type_doc
							and 		id_plan_type_document = ptd_id_plan_type_document
							and     digital_document_uploaded = 1";

		//echo json_encode($query);

		//echo "<br>$query<br>";
		//ejecutamos la consulta
		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while($row = $this -> fetchAssoc()){
				$this -> _homeDocuments[] = $row;
			}
			return $this -> _homeDocuments;
		}
	}

	public function getCatTipoDocumento(){
		$this -> conectar();

		$query = "SELECT * from type_documents";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _catTipoDoc[] = $row;
			}
			return $this -> _catTipoDoc;
		}
	}

	public function getPlanTypeDocument(){
		$this -> conectar();

		$query = "SELECT 	id_type_document, document, description_plan, description_area, program_name
							FROM 		plan_type_documents ptd, type_documents td, plans p, areas, programs prog
							where		type_document_document_id = id_type_document
							and 		id_plan = plan_plan_id
							and 		id_area = id_plan
							and 		id_program = id_plan";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _planTypeDocument[] = $row;
			}
			return $this -> _planTypeDocument;
		}else{
			return 0;
		}
	}

	public function getCheckManifest(){
		$id_aspirant = $_SESSION['id_aspirant'];
		//nos conectamos
		$this -> conectar();
		//preparamos la consulta
		$query = "SELECT 	manifest 
							from 		aspirants 
							where 	id_aspirant = $id_aspirant";
		//ejecutamos la consulta
		$this -> consulta($query);
		//si hay resultados
		if($this -> numeroFilas() == 1){
			//obtenemos los datos
			if($row = $this -> fetchAssoc()){
				$this -> _checkManifest[] = $row;
			}
			return $this -> _checkManifest;
		}else{
			return 0;
		}
	}

}

?>