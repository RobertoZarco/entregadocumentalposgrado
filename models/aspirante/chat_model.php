<?php
require_once '../../db/mysql.php';

/**
* clase que gestiona el chat
*/
class CHAT extends Conexion{

	private $_conversation = null;
	private $_admin        = null;

	function __construct(){
		$this -> _conversation = array();
		$this -> _admin        = array();
	}

	public function registrerChat($emisor, $receptor, $mensaje, $statusChat){

		$fechaAlta = date("Y/m/d h:i:s");

		//nos conectamos
		$this -> conectar();

		//buscamos que no este generado el chat
		$query = "SELECT 	idChat, referencia, idUsuario
							from 		CHAT
							where		referencia = '$emisor'";

		//ejecutamos la consulta
		$this -> consulta($query);

		//si no existe el chat
		if($this -> numeroFilas() == 0){
			//preparamos la query para iniciar el chat
			$insert = "INSERT INTO CHAT
								(referencia, idUsuario, statusChat, fechaAlta)
								VALUES
								('$emisor', $receptor, '$statusChat', '$fechaAlta')";

			//echo $insert;
			//die();
			//ejecutamos la consulta
			$this -> consulta($insert);

			if($this -> filasAfectadas() >= 1){
				//return "peticion realizada";
				$idChat = $this -> ultimaFila();
				$tipoMensaje = "1";

				$this -> registrerMessage($idChat, $emisor, $receptor, $mensaje, $statusChat, $fechaAlta, $tipoMensaje);

			}else{
				return "Error con la peticion";
			}
		}else{
			//si existe el chat
			//nos traemos los datos
			//para insertar el mensaje
			if($row = $this -> fetchAssoc()){
				$idChat = $row['idChat'];
				$tipoMensaje = "1";

				$this -> registrerMessage($idChat, $emisor, $receptor, $mensaje, $statusChat, $fechaAlta, $tipoMensaje);
			}
		}
	}

	public function registrerMessage($idChat, $emisor, $receptor, $mensaje, $statusMensaje, $fechaAlta, $tipoMensaje){

		if (isset($_POST) and @$_POST['message']) {
			$idEmisor      = $emisor;
			$idReceptor    = $receptor;
			$mensaje       = $mensaje;
			$fecha_mensaje = date("Y/m/d h:i:s");

			$this -> conectar();
			$insert = "	INSERT into MENSAJE(CHAT_idChat, CHAT_referencia, CHAT_idUsuario, mensaje, statusMensaje, fechaAlta, tipoMensaje)
									values($idChat, '$idEmisor', $receptor, '$mensaje', 'no leido', '$fechaAlta', '$tipoMensaje')";
			//echo "$insert<br>";
			//die();
			$this -> consulta($insert);
			if($this -> filasAfectadas()){
				return $this -> ultimaFila();
			}
		}
	}

	public function searchAdmin(){
		$this -> conectar();

		$query = "SELECT * from Usuarios where perfil = 1";

		$this -> consulta($query);

		if($this -> numeroFilas()){
			while ($row = $this -> fetchAssoc()) {
				$this -> _admin[] = $row;
			}
			return $this -> _admin;
		}
	}

	public function searchConversationEmisor($emisor){
		//nos conectamos
		$this -> conectar();
		//preparamos nuestra query
		/*
		$query = "SELECT 	idEmisor, IdReceptor, mensaje, fecha_mensaje
							from 		Conversacion
							where		idEmisor = '".$_SESSION['referencia']."'";
		     */

		$query = "SELECT 	nombre, a.referencia, idUsuario, statusChat, c.fechaAlta, mensaje, tipoMensaje
							from 		CHAT c, MENSAJE m, ASPIRANTE a
							where		c.idChat = m.CHAT_idChat
							and 		a.referencia = CHAT_referencia
							and 		CHAT_referencia = '$emisor'";

		//echo "<br>$query<br>";
		//die();
		//ejecutamos la consulta
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//obtenemos el resultado en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _conversation[] = $row;
			}
			return $this -> _conversation;
		}
	}

	public function searchConversationReceptor($emisor, $receptor){
		//nos conectamos
		$this -> conectar();
		//preparamos nuestra query
		/*
		$query = "SELECT 	idEmisor, IdReceptor, mensaje, fecha_mensaje
							from 		Conversacion
							where		idEmisor = '".$_SESSION['referencia']."'";
		     */

		/*
		$query = "SELECT 	idEmisor, IdReceptor, mensaje, fecha_mensaje
							from 		Conversacion
							where   IdReceptor = '$emisor'
							and			idEmisor = '$receptor'";
		*/


		$query = "SELECT 	idEmisor, IdReceptor, mensaje, fecha_mensaje
							from 		Conversacion
							where   IdReceptor = '$emisor'
							and			idEmisor = '$receptor'";

		//echo "<br>$query<br>";
		//ejecutamos la consulta
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//obtenemos el resultado en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _conversation[] = $row;
			}
			return $this -> _conversation;
		}
	}


}


?>