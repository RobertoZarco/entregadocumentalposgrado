<?php
date_default_timezone_set('America/Mexico_City');
require_once '../../db/mysql.php';
require_once '../../properties/properties.inc';
require_once "../../db/sybase.php";

/**
*
*//**/
class Documents extends Conexion{

  private $_uploadDocument = null;
  private $_docAspirante   = null;
  private $_showDocument   = null;
  private $_deleteDocument = null;

  function __construct(){
    $this -> _uploadDocument = array();
    $this -> _docAspirante   = array();
    $this -> _showDocument   = array();
    $this -> _deleteDocument = array();
  }

  public function UploadDocument($aspirant, $ptd_id_plan_type_document, $nombre_doc, $document_path){
    //$usuarioAlta = $_SESSION['id_aspirant'];
    $fechaAlta   = date("Y-m-d h:i:s");
    $ipAlta      = $_SERVER['REMOTE_ADDR'];

    $digital_document_uploaded = 1; //documento subido
    $plan_type_doc = $ptd_id_plan_type_document;
    //documento digital valido  $docDigVal = 0;
    //documento fisico recibido $docRec    = 0;
    //documento fisico validado $docFisVal = 0;
    //nos conectamos
    $this -> conectar();
    //preparamos el query
    //$reference_doc = $reference."_".$cat_tipo_doc;

    $insert = " INSERT into documents_aspirants (aspirant_id, document_path, digital_document_uploaded, plan_type_doc, ptd_id_plan_type_document, created_at)
                values('$aspirant', '$document_path', $digital_document_uploaded, '$plan_type_doc', $ptd_id_plan_type_document, now() )";

    //echo "<br>$insert<br>";
    //die();

    //ejecutamos el query
    $this -> consulta($insert);
    //si hay filas afectadas

    //echo $this -> filasAfectadas();
    if($this -> filasAfectadas() == 1){
      //fila insertada
      $filaInsertada = $this -> ultimaFila();
      $response = array("filaInsertada" => $filaInsertada, "nombreDoc" => $nombre_doc, "cat_tipo_doc" => $ptd_id_plan_type_document);

      //echo json_encode($filaInsertada."|$nombre_doc");
      echo json_encode($response);
    }else{
    }
  }

  public function getDocumentsById($reference, $tipoDoc){

    //echo "hola getDocuments";

    //nos conectamos
    $this -> conectar();
    //preparamos el query

    $query = "  SELECT  d.idDocumentos, d.reference_usuario reference,
                        c.tipo_documento nomDoc,
                        d.cat_tipo_doc tipoDoc,
                        d.documento docPDF,
                        d.status_documento_digital statusDD,
                        d.status_validacion_documento_digital statusVDD,
                        d.status_documento_fisico statusDF,
                        d.status_validacion_documento_fisico statusVDF,
                        d.num_seguimiento seguimiento
                from    Documentos d, CatTipoDocumento c
                where   d.reference_usuario = '$reference'
                and     c.idCatTipoDocumento = $tipoDoc
                and     c.idCatTipoDocumento = d.cat_tipo_doc";
    //echo $query;
    //die();
    //echo "<br>$insert<br>";

    //ejecutamos el query
    $this -> consulta($query);



    //si hay datos
    if($this -> numeroFilas() > 0){
      //nos treamoe los datos en un arreglo
      while ($row  = $this -> fetchAssoc()) {
        $idDocumento = $row['idDocumentos'];
        $reference  = $row['reference'];
        $nomDoc      = $row['nomDoc'];
        $tipoDoc     = $row['tipoDoc'];
        $docPDF      = $row['docPDF'];
        $statusDD    = $row['statusDD'];
        $statusVDD   = $row['statusVDD'];
        $statusDF    = $row['statusDF'];
        $statusVDF   = $row['statusVDF'];
        $seguimiento = $row['seguimiento'];

        //echo "$reference|$nomDoc|$tipoDoc|$docPDF|$statusDD|$statusVDD|$statusDF|$statusVDF|$seguimiento";
        //"$reference|$nomDoc|$tipoDoc|$docPDF|$statusDD|$statusVDD|$statusDF|$statusVDF|$seguimiento";

      }
        echo json_encode($idDocumento);
      //echo  json_encode($this -> _docAspirante);
      //echo json_encode($return);
      return $this -> _docAspirante;
    }
  }

  public function showDocument($id){
    $this -> conectar();

    $query = "SELECT  documento, nombre_documento, extension_documento
              from    Documentos
              where   idDocumentos = $id";

    $result = mysql_query($query);

    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      //obtenemos el resultado en un arreglo
      if($row = $this -> fetchAssoc()){
        $this -> _showDocument[] = $row;
      }
      return $this -> _showDocument;
    }else{
      return false;
    }

    $this -> desconectar();
  }

  public function deleteDocument($id){

    $this -> conectar();

    //preparamos la query
    $delete = "DELETE from documents_aspirants where id_document_aspirant = $id";

    //echo $delete;
    //die();

    //ejecutamos la query
    $this -> consulta($delete);

    if($this -> filasAfectadas() == 1){
      return "eliminado";
    }else{
      return "delete_error";
    }
  }

  public function validaDocument($id_aspirant, $id, $status){

    $this -> conectar();

    //preparamos la query
    $valida = " UPDATE  documents_aspirants
                SET     validated_digital_document = $status
                WHERE   id_document_aspirant = $id
                and     aspirant_id = $id_aspirant;  ";

    //echo $valida;
    //die();

    //ejecutamos la query
    $this -> consulta($valida);

    if($this -> filasAfectadas() == 1){
      return "actualizado";
    }else{
      return "actualizado_error";
    }
  }

  public function validaDocumentDig($id_aspirant, $id, $status){

    $this -> conectar();

    //preparamos la query

    $valida = " UPDATE  documents_aspirants
                SET     validated_physical_document = $status
                WHERE   id_document_aspirant = $id
                and     aspirant_id = $id_aspirant;  ";


    //ejecutamos la query
    $this -> consulta($valida);

    if($this -> filasAfectadas() == 1){
      return "actualizado";
    }else{
      return "actualizado_error";
    }
  }

  public function validaDocumentRecep($reference, $clave, $status){

    $this -> conectar();

    //preparamos la query
    $valida = "UPDATE DOCS_ASPIRANTE SET docRec=$status WHERE idDocsAspirante=$clave and reference='$reference'; ";
    //ejecutamos la query
    $this -> consulta($valida);

    if($this -> filasAfectadas() == 1){
      return "actualizado";
    }else{
      return "actualizado_error";
    }
  }

  public function UploadFoto($reference, $rutaFoto){
    $usuarioAlta = $_SESSION['idAspirante'];
    $fechaAlta   = date("Y-m-d h:i:s");
    $ipAlta      = $_SERVER['REMOTE_ADDR'];

    $this -> conectar();
    //preparamos el query
    //$reference_doc = $reference."_".$cat_tipo_doc;

    $insert = " INSERT into FOTO (reference, rutaFoto, usuarioAlta, fechaAlta, ipAlta)
                values('$reference', '$rutaFoto', $usuarioAlta, '$fechaAlta', '$ipAlta');";

    //echo "<br>$insert<br>";
    //die();

    //ejecutamos el query
    $this -> consulta($insert);
    //si hay filas afectadas
    if($this -> filasAfectadas()){
      //fila insertada
      $filaInsertada = $this -> ultimaFila();
      //$response = array("filaInsertada" => $filaInsertada, "nombreDoc" => $nombre_doc);
      //echo json_encode($filaInsertada."|$nombre_doc");
      //echo json_encode($response);
    }else{
    }
  }

  public function validaExamProp($reference, $valor){

    $this -> conectar();

    //preparamos la query
    $valida = "UPDATE ASPIRANTE
                SET cveExaPropedeutic=$valor
                WHERE reference='$reference'; ";
    //ejecutamos la query
    $this -> consulta($valida);

    if($this -> filasAfectadas() == 1){
      return "actualizado";
    }else{
      return "actualizado_error";
    }
  }

  public function validado($account_number, $valor){

    $this -> conectar();

    //preparamos la query
    $valida = " UPDATE  aspirants
                SET     validate   = $valor
                WHERE   account_number = '$account_number'; ";

    //echo $valida;
    //die();

    //ejecutamos la query
    $this -> consulta($valida);

    if($this -> filasAfectadas() == 1){
      return "actualizado";
    }else{
      return "actualizado_error";
    }
  }

  public function activeManifest(){

    $id_aspirant = $_SESSION['id_aspirant'];

    //nos conectamos
    $this -> conectar();
    //preparamos la consulta
    $update = " UPDATE  aspirants 
                set     manifest = 1 
                where   id_aspirant = $id_aspirant";
    //ejecutamos la consulta
    $this -> consulta($update);

    //si hay filas afectadas
    if($this -> filasAfectadas() ==  1){
      $response = array('manifisto' => 'ok');
      echo json_encode($response);
    }

  }
}

/**
*
*/
class AspiranteSybase extends ConexionSybase{

  private $_validado = null;

  function __construct()  {
    $this -> _validado = array();
  }

  public function setValidado($reference, $status){

    if($status == 2){
      $status = 0;
    }

    $reference = substr($reference, 0, -1);
    //echo $reference;
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  *
              from    PreReg_CSDB..BachDist_Movimientos_prb
              where   bmov_clvpr = '$reference'";

    //echo $query;

    //ejecutamos la consulta
    $this -> consulta($query);


    if($this -> numeroFilas() > 0){
      //hacemos el update
      $update = " UPDATE  PreReg_CSDB..BachDist_Movimientos_prb
                  set     bmov_docs_validados = $status,
                          bmov_fec_actualizacion = getdate(),
                          bmov_cve_act = 'ins_web'
                  where   bmov_clvpr = '$reference'";




      //ejecitamos el update
      $this -> consulta($update);

      if($this -> filasAfectadas() > 0){
        $response = array("aspirante" => "validado");
      }else{
        $response = array("aspirante" => "no validado");
      }

      echo json_encode($response);
    }

  }
}


?>