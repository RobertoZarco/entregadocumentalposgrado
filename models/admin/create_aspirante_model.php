<?php
require_once '../../properties/properties.inc';
require_once "../../db/mysql.php";
/**
*
*/
class Aspirante extends Conexion{

	function __construct(){
	}


	public function createAspirante_admin(){

		$paterno               = strtoupper($_POST['paterno']);
		$materno               = strtoupper($_POST['materno']);
		$nombre                = strtoupper($_POST['nombre']);
		$fechaNacimiento       = $_POST['fechaNacimiento'];
		$curp                  = strtoupper($_POST['curp']);
		$email                  = $_POST['email'];
		$genero                = $_POST['selectGenero'];
		$cveDependencia        = $_POST['selectDependencia'];

		$PermisoCrearUsuario   = $_POST['PermisoCrearUsuario'];
		$PermisoCrearAspi      = $_POST['PermisoCrearAspi'];
		$PermisoEditarUsuarios = $_POST['PermisoEditarUsuarios'];
		$PermisoChat           = $_POST['PermisoChat'];
		$PermisoVerDocDig      = $_POST['PermisoVerDocDig'];
		$PermisoElimDocDig     = $_POST['PermisoElimDocDig'];
		$PermisoValiDocDig     = $_POST['PermisoValiDocDig'];
		$PermisoValiRecepDocs  = $_POST['PermisoValiRecepDocs'];
		$PermisoValiDocFis     = $_POST['PermisoValiDocFis'];
		$PermisoReport         = $_POST['PermisoReport'];

		$login       = $_POST['login'];
		$password    = md5($_POST['pass']);

		//$usuarioAlta = $_SESSION['idUsuario'];
		$fechaAlta   = date("Y-m-d h:i:s");
		//$ipAlta      = $_SERVER['REMOTE_ADDR'];

		$activo      = $_POST['activo'];

		if($activo == "true"){
			$activo = 1;
			//echo "entre";
		}else{
			$activo = 0;
			//echo "no entre";
		}
		//Cambiar permisos a 0/1
		if($PermisoCrearUsuario == "true"){
			$PermisoCrearUsuario = 1;
		}else{
			$PermisoCrearUsuario = 0;
		}
		if($PermisoCrearAspi == "true"){
			$PermisoCrearAspi = 1;
		}else{
			$PermisoCrearAspi = 0;
		}
		if($PermisoEditarUsuarios == "true"){
			$PermisoEditarUsuarios = 1;
		}else{
			$PermisoEditarUsuarios = 0;
		}
		if($PermisoChat == "true"){
			$PermisoChat = 1;
		}else{
			$PermisoChat = 0;
		}
		if($PermisoVerDocDig == "true"){
			$PermisoVerDocDig = 1;
		}else{
			$PermisoVerDocDig = 0;
		}
		if($PermisoElimDocDig == "true"){
			$PermisoElimDocDig = 1;
		}else{
			$PermisoElimDocDig = 0;
		}
		if($PermisoValiDocDig == "true"){
			$PermisoValiDocDig = 1;
		}else{
			$PermisoValiDocDig = 0;
		}
		if($PermisoValiRecepDocs == "true"){
			$PermisoValiRecepDocs = 1;
		}else{
			$PermisoValiRecepDocs = 0;
		}
		if($PermisoValiDocFis == "true"){
			$PermisoValiDocFis = 1;
		}else{
			$PermisoValiDocFis = 0;
		}
		if($PermisoReport == "true"){
			$PermisoReport = 1;
		}else{
			$PermisoReport = 0;
		}


		//nos conectamos
		$this -> conectar();

		//preparamos la query
		$insert = "		INSERT INTO users(first_surname,second_surname,name,birthdate,curp,gender,email,login,password,dependency,active,created_at)
								VALUES
								('$paterno', '$materno', '$nombre', '$fechaNacimiento', '$curp', '$genero','$email','$login', '$password',
								$cveDependencia, $activo, '$fechaAlta')";

		//echo $insert;
		//die();

		//ejecutamos el query
		$this -> consulta($insert);

		if($this -> filasAfectadas() >= 1){
				//Consultamos el id ultimo registo
				$last_id = $this -> ultimaFila();
				//echo $last_id ;
				//nos conectamos
				$this -> conectar();
				//preparamos la query
				$insert = "INSERT INTO permissions (users_id,create_users,edit_users,create_aspirant,edit_aspirant,post,view_digital_document,delete_digital_document,
				validate_digital_document, receive_physical_document,validate_physical_document,reports,created_at)
									values
									($last_id, $PermisoCrearUsuario, $PermisoEditarUsuarios, $PermisoCrearAspi,
									$PermisoEditarUsuarios, $PermisoChat, $PermisoVerDocDig, $PermisoElimDocDig, $PermisoValiDocDig,
									$PermisoValiRecepDocs, $PermisoValiDocFis, $PermisoReport, '$fechaAlta')";

								//ejecutamos el query
								$this -> consulta($insert);
								if($this -> filasAfectadas() >= 1){
									return "Usuario insertado";
								}else{
									return "Error al crear permisos";
								}
			//return "Usuario insertado";
		}else{
			return "Error al crear usuario";
		}

	}

}

?>