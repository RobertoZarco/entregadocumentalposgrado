<?php

require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
*
*/
class Admin extends Conexion{

	private $_usuarios         = null;
	private $_docAlumno        = null;
	private $_aspiByDependency = null;
	private $_madems           = null;
	private $_mSostenibilidad  = null;
	private $_dSostenibilidad   = null;

	function __construct(){
		$this -> _usuarios         = array();
		$this -> _docAlumno        = array();
		$this -> _aspiByDependency = array();
		$this -> _checkManifest    = array();
		$this -> _madems           = array();
		$this -> _mSostenibilidad  = array();
		$this -> _dSostenibilidad  = array();
	}

	public function getUsuarios($dependency){

		if($_SESSION['login'] == "murrutia"){
			$plan_id_plan = " and plan_id_plan not in(5172,4172) ";
		}else{
			$plan_id_plan = "";
		}

		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	id_aspirant, reference, account_number, first_surname, second_surname, name, c.country, login, dependency, email, validate, welcome, plan_id_plan, plan,
		CASE
		WHEN modalities_id_modality= 1 THEN 'Presencial'
		WHEN modalities_id_modality= 2 THEN 'Distancia'
		END AS modalidad
							from 		aspirants aspi, countries c, plans p
							where		active = 1
							and 		aspi.country = c.code_country
							and			aspi.plan_id_plan = id_plan
							and 		periods_id_period = 2
							$plan_id_plan";
							
		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _usuarios[] = $row;
			}
			return $this -> _usuarios;
		}else{
			return false;
		}
	}

	public function getUsuariosByDependency($dependency){

		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	idAspirante,referencia, paterno, materno, nombre, pais, login, cveDependencia, cveExaPropedeutic, validado, correo
							from 		ASPIRANTE aspi, PAIS p
							where 	aspi.cveDependencia >= $dependency
							and 		activo = 1
							and 		aspi.cvePais = p.cvePais;";

		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _aspiByDependency[] = $row;
			}
			return $this -> _aspiByDependency;
		}else{
			return false;
		}
	}

	public function getCountDocsAspi($reference){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT 	id_aspirant, count(*) total, account_number
							from 		documents_aspirants, aspirants
							where		id_aspirant = aspirant_id
							and 		reference = '$reference'
							group by id_aspirant;";

		//echo $query;
		//die();


		//ejecutamos la query
		$this -> consulta($query);

		//si hay regsitros
		if($this -> numeroFilas() > 0){
			//mostramos los datos
			if($row = $this -> fetchAssoc()){
				$total = $row['total'];
				$account_number = $row['account_number'];
			}
			return $total;
		}else{
			return false;
		}
	}

	public function getDocumentoByReference($reference){

		$this -> conectar();

		$query = "SELECT 	d.referencia referencia,
											c.documento nomDoc,
											d.cat_tipo_doc tipoDoc,
											d.documento docPDF,
											d.status_documento_digital statusDD,
											d.status_validacion_documento_digital statusVDD,
											d.status_documento_fisico statusDF,
											d.status_validacion_documento_fisico statusVDF,
											d.num_seguimiento seguimiento
							from 		Documentos d, CatTipoDocumento c
							where		d.referencia = '$reference'
							and			c.idCatTipoDocumento = d.cat_tipo_doc";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _docAlumno[] = $row;
			}
			return $this -> _docAlumno;
		}else{
			return false;
		}
	}

	public function getUsuariosAdmin(){

		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	id_user, first_surname, second_surname, name, dependency, login, gender
							from 		users
							where 	active = 1";

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _usuariosAdmin[] = $row;
			}
			return $this -> _usuariosAdmin;
		}else{
			return false;
		}
	}

	public function getMadems(){
		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	id_aspirant, account_number, first_surname, second_surname, aspi.name, c.country, login, dependency, email, validate, welcome, plan_id_plan, plan
							from 		aspirants aspi, countries c, plans p, programs prog
							where		active = 1
							and 		aspi.country = c.code_country
							and			aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		id_program = 414
							and 		periods_id_period = 2
							order by first_surname, second_surname, aspi.name";

		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _madems[] = $row;
			}
			return $this -> _madems;
		}else{
			return false;
		}
	}

	public function getMSostenibilidad(){
		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	id_aspirant, account_number, first_surname, second_surname, aspi.name, c.country, login, dependency, email, validate, welcome, plan_id_plan, plan
							from 		aspirants aspi, countries c, plans p, programs prog
							where		active = 1
							and 		aspi.country = c.code_country
							and			aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		plan_id_plan = 4172
							and 		periods_id_period = 2
							order by first_surname, second_surname, aspi.name";

		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _madems[] = $row;
			}
			return $this -> _madems;
		}else{
			return false;
		}
	}

	public function getDSostenibilidad(){
		//nos conectamos
		$this -> conectar();
		//peparamos la query
		$query = "SELECT 	id_aspirant, account_number, first_surname, second_surname, aspi.name, c.country, login, dependency, email, validate, welcome, plan_id_plan, plan
							from 		aspirants aspi, countries c, plans p, programs prog
							where		active = 1
							and 		aspi.country = c.code_country
							and			aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		plan_id_plan = 5172
							and 		periods_id_period = 2
							order by first_surname, second_surname, aspi.name";

		//echo $query;
		//die();

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _madems[] = $row;
			}
			return $this -> _madems;
		}else{
			return false;
		}
	}

}

?>