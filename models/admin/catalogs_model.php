<?php
require_once '../../properties/properties.inc';
require_once "../../db/mysql.php";

/**
*
*/
class Catalogo extends Conexion{

	private $_pais        = array();
	private $_estado      = array();
	private $_comunidad   = array();
	private $_concurso    = array();
	private $_cveConcurso = array();
	private $_periodo     = array();
	private $_tipoIngreso = array();
	private $_sistema     = array();

	function __construct(){
	}

	public function getPaisByCve($cveContinente){
		//nos conectamos
		$this -> conectar();
		//preparamos nuestra query
		$query = "SELECT 	cvePais, pais
							from 		PAIS
							where 	cvePais like '$cveContinente%'";

		//echo "$query<br>";

		//ejecutamos la consulta
		$this -> consulta($query);

		//si hay datos
		if($this -> numerofilas() > 0){
			//obtenemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _pais[] = $row;
			}
			return $this -> _pais;
		}else{
			return false;
		}
	}

	public function getEstadoByCve(){
			//nos conectamos
			$this -> conectar();
			//preparamos nuestra query
			$query = "SELECT 	*
								from 		ESTADO
								where 	cvePais = '260'";

			//ejecutamos la consulta
			$this -> consulta($query);

			//si hay datos
			if($this -> numerofilas() > 0){
				//obtenemos los datos en un arreglo
				while ($row = $this -> fetchAssoc()) {
					$this -> _estado[] = $row;
				}

				return $this -> _estado;
			}else{
				return false;
			}
	}

	public function getComunidadByCve($estado){
		//nos conectamos
		$this -> conectar();
		//preparamos nuestra query
		$query = "SELECT 	*
							from 	MUNICIPIO
							where	cveEstado = '$estado'";

		//ejecutamos la consulta
		$this -> consulta($query);

		//si hay datos
		if($this -> numerofilas() > 0){
			//obtenemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _comunidad[] = $row;
			}

			return $this -> _comunidad;
		}else{
			return false;
		}
	}

	public function getConcurso(){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT * from CONCURSO";
		//ejecutamos la query
		$this -> consulta($query);
		//si hay datos
		if($this -> numerofilas() > 0){
			//los obtenemos en un arreglo
			while($row = $this -> fetchAssoc()){
				$this -> _concurso[] = $row;
			}
			return $this -> _concurso;
		}else{
			return false;
		}
	}

	public function getPeriodo(){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT * from PERIODO where activo = 1";
		//ejecutamos la query
		$this -> consulta($query);
		//si hay datos
		if($this -> numerofilas() > 0){
			//los obtenemos en un arreglo
			while($row = $this -> fetchAssoc()){
				$this -> _periodo[] = $row;
			}
			return $this -> _periodo;
		}else{
			return false;
		}
	}

	public function getTipoIngreso(){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT * from TIPO_INGRESO";
		//ejecutamos la query
		$this -> consulta($query);
		//si hay datos
		if($this -> numerofilas() > 0){
			//los obtenemos en un arreglo
			while($row = $this -> fetchAssoc()){
				$this -> _tipoIngreso[] = $row;
			}
			return $this -> _tipoIngreso;
		}else{
			return false;
		}
	}

	public function getSistema(){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT * from SISTEMA";
		//ejecutamos la query
		$this -> consulta($query);
		//si hay datos
		if($this -> numerofilas() > 0){
			//los obtenemos en un arreglo
			while($row = $this -> fetchAssoc()){
				$this -> _sistema[] = $row;
			}
			return $this -> _sistema;
		}else{
			return false;
		}
	}


}

?>