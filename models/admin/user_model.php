<?php
/**/
require_once '../../../properties/properties.inc';
require_once "../../../db/mysql.php";

/**
* clase que gestion a la conexion con sybase
* para obtener a los aspirantes
*/



/**
* clase que gestiona a los usuarios de mysql
*/
class Usuarios extends Conexion{

  private $_usuario       = null;
  private $_messages      = null;
  private $_typeDocuments = null;

  function __construct(){
    $this -> _usuario       = array();
    $this -> _messages      = array();
    $this -> _typeDocuments = array();
  }


  public function setUsuario(){
    //print_r($_POST);
    $folio              = $_POST['folio'];
    $first_surname      = strtoupper($_POST['first_surname']);
    $second_surname     = strtoupper($_POST['second_surname']);
    $name               = strtoupper($_POST['name']);
    $email              = $_POST['email'];
    //$telefono         = $_POST['telefono'];
    //$lugar_residencia = strtoupper($_POST['lugar_residencia']);
    $period             = $_POST['period'];
    $income_type        = $_POST['income_type'];
    $modality           = $_POST['modality'];
    $concourse          = $_POST['concourse'];
    $concourse          = "1";
    $login              = $_POST['login'];
    $pass               = md5($_POST['pass']);
    $pass2              = $_POST['pass2'];
    //$perfil           = $_POST['perfil'];
    $fecha_creacion     = date("Y/m/d h:i:s");
    $active     = 1;
    $updated_at = date("Y/m/d h:i:s");


    //nos conectamos
    $this -> conectar();

    //preparamos la query
    $insert = "INSERT INTO users
              (referencia, first_surname, second_surname, name, email, period, income_type, modality,
              concourse, login, pass, perfil, fecha_creacion, active, updated_at)
              VALUES('$folio', '$first_surname', '$second_surname', '$name', '$email' , '$telefono', '$lugar_residencia', '$period', '$income_type', '$modality',
                '$concourse', '$login', '$pass' , $perfil , '$fecha_creacion', $active , '$updated_at')";

    //echo "<br>$insert<br>";

    //ejecutamos la query
    $this -> consulta($insert);

    //si hay filas afectadas
    if($this -> filasAfectadas() == 1){

      echo "Usuario insertado";
    }else{
      echo "Erro al insertar";
    }
  }

  public function getUsuario ($id){

    $this -> conectar();

   $query = "SELECT  reference, first_surname, second_surname, name, email, period, income_type, modality,
                      concourse, login, pass
              FROM    aspirants
              WHERE   id_user = $id";

    echo "<br>$query<br>";
    die();

    $this -> consulta($query);

    if($this -> numeroFilas() == 1) {
      if($row = $this -> fetchAssoc()){
        $this -> _usuario[] = $row;
      }
      return $this -> _usuario;
    }else{
      return false;
    }
  }

  public function updateUsuario (){

    $folio            = $_POST['folio'];
    $first_surname    = strtoupper($_POST['first_surname']);
    $second_surname   = strtoupper($_POST['second_surname']);
    $name             = strtoupper($_POST['name']);
    $email            = $_POST['email'];
    $telefono         = $_POST['telefono'];
    $lugar_residencia = strtoupper($_POST['lugar_residencia']);
    $period           = $_POST['period'];
    $income_type      = $_POST['income_type'];
    $modality         = $_POST['modality'];
    $concourse        = $_POST['concourse'];
    $login            = $_POST['login'];
    $pass             = $_POST['pass'];
    $pass2            = $_POST['pass2'];
    $perfil           = $_POST['perfil'];
    $active           = $_POST['active'];
    $updated_at       = date("Y/m/d h:i:s");

    $this -> conectar();
    $query = "UPDATE users SET  first_surname='$first_surname', second_surname='$second_surname', name='$name',
                                   email='$email', telefono='$telefono', period='$period',
                                   income_type='$income_type', modality='$modality',
                                   concourse='$concourse', login='$login', pass='$pass', perfil='$perfil',
                                   active='$active', updated_at='$updated_at'
              WHERE referencia='$folio'";
    $this -> consulta($query);
    if($this -> filasAfectadas()){
      $actualizado = array("actualizado" => "ok");
      echo json_encode($actualizado);
    }else{
      $actualizado = array("actualizado" => "nok");
      echo json_encode($actualizado);
    }
  }

  public function deleteUsuario ($id){
    $this -> conectar();

    $query = "UPDATE users SET active = 0
               WHERE id_user = $id";
    echo $query;
    $this -> consulta($query);
    if($this -> filasAfectadas() == 1){
      echo "Usuario eliminado";
    }else{
      echo "Error al eliminar";
    }
  }

  public function getDocumentoByReference($reference){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  id_aspirant, reference, account_number, ptd_id_plan_type_document, id_document_aspirant, validate,
                      document_path, digital_document_uploaded, validated_digital_document,
                      validated_physical_document, physical_document_received, validated_physical_document, tag, plan_type_doc,
                      id_type_document, document
              from    aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd
              where   reference = '$reference'
              and     id_aspirant = aspirant_id
              and     id_type_document = plan_type_doc
              and     id_plan_type_document = ptd_id_plan_type_document
              and     digital_document_uploaded = 1";

    //echo json_encode($query);
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocuments[] = $row;
      }
      return $this -> _homeDocuments;
    }
  }

  public function getDocumentoByReferenceObligato1($reference){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT id_aspirant, reference, account_number, ptd_id_plan_type_document, 
                      id_document_aspirant, validate, document_path, digital_document_uploaded, 
                      validated_digital_document, validated_physical_document, physical_document_received, 
                      validated_physical_document, tag, plan_type_doc, id_type_document, document 
              from aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd 
              where reference = '$reference' 
              and id_aspirant = aspirant_id 
              and id_type_document = plan_type_doc 
              and id_plan_type_document = ptd_id_plan_type_document 
              and digital_document_uploaded = 1
              and (ptd_id_plan_type_document = 1 
              OR ptd_id_plan_type_document = 2
              OR ptd_id_plan_type_document = 5)";

    //echo json_encode($query);
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocumentsObligato1[] = $row;
      }
      return $this -> _homeDocumentsObligato1;
    }
  }

  public function getDocumentoByReferenceObligato2($reference){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT id_aspirant, reference, account_number, ptd_id_plan_type_document, 
                      id_document_aspirant, validate, document_path, digital_document_uploaded, 
                      validated_digital_document, validated_physical_document, physical_document_received, 
                      validated_physical_document, tag, plan_type_doc, id_type_document, document 
              from aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd 
              where reference = '$reference' 
              and id_aspirant = aspirant_id 
              and id_type_document = plan_type_doc 
              and id_plan_type_document = ptd_id_plan_type_document 
              and digital_document_uploaded = 1
              and (ptd_id_plan_type_document = 3 
              OR ptd_id_plan_type_document = 7
              OR ptd_id_plan_type_document = 8)";

    //echo json_encode($query);
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocumentsObligato2[] = $row;
      }
      return $this -> _homeDocumentsObligato2;
    }
  }

  public function getDocumentoByReferenceObligato3($reference){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT id_aspirant, reference, account_number, ptd_id_plan_type_document, 
                      id_document_aspirant, validate, document_path, digital_document_uploaded, 
                      validated_digital_document, validated_physical_document, physical_document_received, 
                      validated_physical_document, tag, plan_type_doc, id_type_document, document, average, certificate_sheet 
              from aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd 
              where reference = '$reference' 
              and id_aspirant = aspirant_id 
              and id_type_document = plan_type_doc 
              and id_plan_type_document = ptd_id_plan_type_document 
              and digital_document_uploaded = 1
              and (ptd_id_plan_type_document = 4 
              OR ptd_id_plan_type_document = 6)";

    //echo json_encode($query);
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocumentsObligato3[] = $row;
      }
      return $this -> _homeDocumentsObligato3;
    }
  }

  public function getDocumentoByReferenceOpcional($reference){
    //nos conectamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT id_aspirant, reference, account_number, ptd_id_plan_type_document, 
                      id_document_aspirant, validate, document_path, digital_document_uploaded, 
                      validated_digital_document, validated_physical_document, physical_document_received, 
                      validated_physical_document, tag, plan_type_doc, id_type_document, document 
              from aspirants a, documents_aspirants d, type_documents td, plan_type_documents ptd 
              where reference = '$reference' 
              and id_aspirant = aspirant_id 
              and id_type_document = plan_type_doc 
              and id_plan_type_document = ptd_id_plan_type_document 
              and digital_document_uploaded = 1
              and (ptd_id_plan_type_document = 14 
              OR ptd_id_plan_type_document = 15
              OR ptd_id_plan_type_document = 16
              OR ptd_id_plan_type_document = 17)";

    //echo json_encode($query);
    //ejecutamos la consulta
    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _homeDocumentsOpcional[] = $row;
      }
      return $this -> _homeDocumentsOpcional;
    }
  }



  public function getUsuarioAdmin ($id){

    $this -> conectar();

    $query = "SELECT  d.id_user , d.first_surname, d.second_surname, d.name, d.dependency, d.birthdate, d.curp,
                      d.email, d.gender, d.login, d.password, d.created_at, d.updated_at, d.active,
                      c.users_id, c.create_users, c.create_aspirant, c.edit_users,
                      c.post, c.view_digital_document, c.delete_digital_document, c.validate_digital_document,
                      c.receive_physical_document, c.validate_physical_document, c.reports, c.validate
              FROM    users d, permissions c
              WHERE   d.id_user = $id
              and     d.id_user = c.users_id;";

    //echo "<br>$query<br>";
    //die();

    $this -> consulta($query);

    if($this -> numeroFilas() == 1) {
      if($row = $this -> fetchAssoc()){
        $this -> _usuario[] = $row;
      }
      return $this -> _usuario;
    }else{
      return false;
    }
  }

  public function updateUsuarioAdmin (){
    //print_r($_POST);
    $id_user                   = $_POST['id'];
    $first_surname             = strtoupper($_POST['first_surname']);
    $second_surname            = strtoupper($_POST['second_surname']);
    $name                      = strtoupper($_POST['name']);

    $CURP                      = $_POST['CURP'];
    $email                     = $_POST['email'];
    $birthdate                 = $_POST['birthdate'];

    $gender                    = $_POST['gender'];
    $dependency                = $_POST['dependency'];
    $create_users              = $_POST['create_users'];
    $create_aspirants          = $_POST['create_aspirants'];
    $edit_users                = $_POST['edit_users'];

    $post                      = $_POST['permiso_chat'];
    $view_digital_document     = $_POST['view_digital_document'];
    $delete_digital_document   = $_POST['delete_digital_document'];
    $validate_digital_document = $_POST['validate_digital_document'];
    //$PermisoValiRecepDocs    = $_POST['permiso_recep'];
    $receive_physical_document = $_POST['receive_physical_document'];
    $reports                   = $_POST['reports'];
    $pass                      = ($_POST['pass']);
    $active                    = $_POST['active'];
    $validate                  = $_POST['validate'];

    //$usuarioUpdate             = $_SESSION['id_user'];
    //$fechaUpdate               = date("Y-m-d h:i:s");
    //$ipUpdate                  = $_SERVER['REMOTE_ADDR'];


    $this -> conectar();

    $query = "UPDATE users
              SET   first_surname   = '$first_surname',
                    second_surname  = '$second_surname',
                    name            = '$name',
                    birthdate       = '$birthdate',
                    curp            = '$CURP',
                    email           = '$email',
                    gender          = '$gender',
                    dependency      = '$dependency',
                    active          = '$active',
                    password        = '$pass'
              WHERE id_user         = '$id_user'";
    //echo $query;
    //die();

    $this -> consulta($query);

    //echo $this -> filasAfectadas();

    if($this -> filasAfectadas() == 1 or $this -> filasAfectadas() == 0 ){
      $insert = " UPDATE permissions
                  SET   create_users               = '$create_users',
                        edit_users                 = '$edit_users',
                        create_aspirant            = '$create_aspirants',
                        post                       = '$post',
                        view_digital_document      = '$view_digital_document',
                        delete_digital_document    = '$delete_digital_document',
                        validate_digital_document  = '$validate_digital_document',
                        validate_physical_document = '$receive_physical_document',
                        reports                    = '$reports',
                        validate                   = '$validate'
                  WHERE users_id                   = '$id_user'";

        //ejecutamos el query
        //echo $insert;
        //die();
        $this -> consulta($insert);

        //echo $this -> filasAfectadas();

        if($this -> filasAfectadas() == 1 or $this -> filasAfectadas() == 0){
          $actualizado = array("actualizado" => "ok");
          echo json_encode($actualizado);
        }else{
          //echo "Valió";
          return "Error al crear permisos";
        }
    }else{

      $actualizado = array("actualizado" => "nok");
      echo json_encode($actualizado);
    }
  }

  public function sendMail($email, $message, $name, $idAspirant){

    //$para      = 'shura_capricornio10@hotmail.com'.',';
    //$para      .= 'joshy@comunidad.unam.mx';


    $emailAdmin = $_SESSION['email'];

    $para    = $email;

    //$mensaje = "Estimad@ $name\n";

    $mensaje = "  <table style='color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica' cellspacing='0' cellpadding='0' border='0'>
                    <tbody>
                      <tr width='100%'>
                        <td style='background:#fff;font:15px 'Helvetica Neue',Arial,Helvetica' valign='top' align='left'>
                          <table style='border:none;padding:0 18px;margin:50px auto;width:500px' cellspacing='0' cellpadding='0' border='0'>
                            <tbody>
                              <tr width='100%' height='90px'>
                                <td style='border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left' valign='top' align='left'>
                                  <img class='CToWUd' title='UNAM' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci6.googleusercontent.com/proxy/QvPY_nhaS4J9SDuJFvCEGG-NbgbIutxYDkILdp1fH4YyZ8j5wOoxitkgWmTxj3zGKG4XabtKzIeyfHRNerOD1J9zN1clSnNld2LYVSH14tYIOMyHgg=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png' height='70'>
                                </td>
                                <td style='border-top-right-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:right' valign='top' align='right'>
                                  <img class='CToWUd' title='DGAE' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci5.googleusercontent.com/proxy/qAWWIallTjz8gPN7j69aUDH5GmTHNdNb-uLFJH582fo20_HiyJFNHRH8kXNj2nPD-gYDti-XkY0DFubcMoUBrgQ_zUt_3rOLLWwTNqmTBUT0t4JCbw=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png' height='50'>
                                </td>
                              </tr>
                              <tr width='100%'>
                                <td style='border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#f6f6f6;padding:18px' colspan='2' valign='top' align='left'>

                                  <h1 style='font-size:20px;margin:0;color:#333'>
                                    DGAE - Maestría en Docencia para la Educación Media Superior
                                  </h1>

                                  <p style='font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333'>
                                    Estimado aspirante: $name
                                    <br>
                                    $message
                                    <br>
                                    Atentamente
                                    <br>
                                    Dirección General de Administración Escolar

                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>";



    //$mensaje .= $message;

    $titulo = 'DGAE - MADEMS ';
    /*
    $mensaje   = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex sint beatae ratione quod, nesciunt,\r\n
                  quo adipisci commodi rerum, ut nisi nihil perferendis iure deleniti, explicabo dolorum consequuntur\r\n
                  quisquam natus est.";
    */


    //$cabeceras = 'From: rherrera@mercurio.dgae.unam.mx' . "\r\n" . 'X-Mailer: PHP/' . phpversion();


    // Para enviar un email HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Cabeceras adicionales
    //$cabeceras .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";


    $cabeceras .= 'From: DGAE - Maestría en Docencia para la Educación Media Superior (MADEMS).' . "\r\n";
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Roberto <rherrera@mercurio.dgae.unam.mx>' . "\r\n";
    //$cabeceras .= 'Bcc: aldova19@gmail.com' . "\r\n";

    // Cabeceras adicionales
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Imelda <imelda_gonzalez@cuaed.unam.mx>' . "\r\n";
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Alberto Vargas <alberto_vargas@cuaed.unam.mx>' . "\r\n";
    $cabeceras .= "Bcc: $emailAdmin" . "\r\n";


    $mail = mail($para, $titulo, $mensaje, $cabeceras);

    if($mail){
      $this -> setPost($idAspirant, $message);
      $response = array("email" => "enviado");
      echo json_encode($response);
    }else{
      $response = array("email" => "error");
      echo json_encode($response);
    }
  }

  public function setPost($idAspirant, $message){
    $idUser = $_SESSION['id_user'];

    $this -> conectar();

    $created_at = date("Y-m-d h:m:s");

    $query = "SELECT  id_post, users_id_user, aspirants_id_aspirant
              from    posts
              where   users_id_user = $idUser
              and     aspirants_id_aspirant = $idAspirant";

    $this -> consulta($query);


    if($this -> numeroFilas() == 0){

      $insert = "INSERT into posts (`users_id_user`,`aspirants_id_aspirant`,`created_at`)
                values  ($idUser, $idAspirant, '$created_at')";

      echo $insert;

      $this -> consulta($insert);

      if($this -> filasAfectadas() == 1){
        $idPost = $this -> ultimaFila();
        $this ->  setMessage($idPost, $message, $idUser, $idAspirant);
      }

    }else{

      if($row = $this -> fetchAssoc()){
        $idPost = $row['id_post'];

        $this ->  setMessage($idPost, $message, $idUser, $idAspirant);
      }

    }
  }

  public function setMessage($idPost, $message, $idUser, $idAspirant){

    $this -> conectar();

    $created_at = date("Y-m-d h:m:s");

    $query = "INSERT INTO messages(`message`,`posts_id_post`,`posts_users_id_user`,`posts_aspirants_id_aspirant`,`created_at`)
              values('$message', $idPost, $idUser, $idAspirant, '$created_at')";

    $this -> consulta($query);

    if($this -> filasAfectadas() == 1){
      $response = array('mensaje' => 'guardado');
      //echo json_encode($response);
    }
  }

  public function getHistoryMessages($id_aspirant){
    $this -> conectar();

    $query = "SELECT   id_post, users_id_user, aspirants_id_aspirant,
                      message, posts_id_post, posts_users_id_user, posts_aspirants_id_aspirant
              from    posts, messages
              where   id_post = posts_id_post
              and     aspirants_id_aspirant = $id_aspirant;";

    //echo $query;
    //die();

    $this->consulta($query);

    if($this->numeroFilas() >= 1){
      while($row = $this -> filasAfectadas()){
        $this -> _messages[] = $row;
      }
      return $this -> _messages;
    }else{
      return 0;
    }
  }

  public function getTypeDocuments(){

    $this -> conectar();

    $query = "SELECT id_type_document, document
              from type_documents";

    $this -> consulta($query);

    if($this -> numeroFilas() > 0){
      while($row = $this -> fetchAssoc()){
        $this -> _typeDocuments[] = $row;
      }
      return $this -> _typeDocuments;
    }else{
      return false;
    }
  }

}


?>
