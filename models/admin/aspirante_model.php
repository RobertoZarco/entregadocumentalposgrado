<?php
require_once '../../properties/properties.inc';
require_once "../../db/mysql.php";
/**
*
*/
class Aspirante extends Conexion{

	function __construct(){
	}


	public function createAspirante(){

		$referencia        = $_POST['referencia'];
		$paterno           = strtoupper($_POST['paterno']);
		$materno           = strtoupper($_POST['materno']);
		$nombre            = strtoupper($_POST['nombre']);
		$fechaNacimiento   = $_POST['fechaNacimiento'];
		$curp              = strtoupper($_POST['curp']);
		$genero            = $_POST['selectGenero'];
		$calle             = strtoupper($_POST['calle']);
		$numExterior       = $_POST['numExt'];
		$numInterior       = $_POST['numInt'];
		$colonia           = strtoupper($_POST['colonia']);
		$cvePais           = $_POST['Pais'];
		$cveEstado         = $_POST['Estado'];
		$cveMunicipio      = $_POST['Municipio'];
		$codigoPostal      = $_POST['cp'];
		$correo            = $_POST['correo'];
		$login             = $_POST['login'];
		$password          = md5($_POST['pass']);
		$cveConcurso       = $_POST['concurso'];
		$periodo           = $_POST['periodo'];
		$tipoIngreso       = $_POST['tipoIngreso'];
		$cveSistema        = $_POST['sistema'];
		$cveDependencia    = $_SESSION['cveDependencia'];
		$cveExaPropedeutic = 2;
		$usuarioAlta       = $_SESSION['idUsuario'];
		$fechaAlta         = date("Y-m-d h:i:s");
		$ipAlta            = $_SERVER['REMOTE_ADDR'];
		$activo            = $_POST['activo'];

		if(empty($cveEstado)){
			$cveEstado = "null";
		}

		if(empty($cveMunicipio)){
			$cveMunicipio = "null";
		}

		if(empty($numExterior)){
			$numExterior = 'null';
		}

		if(empty($numInterior)){
			$numInterior = 'null';
		}


		if($activo == "on"){
			$activo = 1;
			//echo "entre";
		}else{
			$activo = 0;
			//echo "no entre";
		}

		//nos conectamos
		$this -> conectar();

		//preparamos la query
		$insert = "INSERT INTO ASPIRANTE
							(referencia, paterno, materno, nombre, fechaNacimiento, curp, genero, calle, numExterior, numInterior, colonia,
							cvePais, cveEstado, cveMunicipio, codigoPostal, correo, login, password,
							cveConcurso, periodo, tipoIngreso, cveSistema,cveDependencia,
							cveExaPropedeutic, activo, usuarioAlta, fechaAlta, ipAlta)
							VALUES
							('$referencia', '$paterno', '$materno', '$nombre', '$fechaNacimiento', '$curp', '$genero', '$calle', '$numExterior', '$numInterior', '$colonia',
							$cvePais, $cveEstado, $cveMunicipio, '$codigoPostal', '$correo', '$login', '$password',
							$cveConcurso, '$periodo', '$tipoIngreso', $cveSistema, $cveDependencia,
							$cveExaPropedeutic, $activo, '$usuarioAlta', '$fechaAlta', '$ipAlta')";

		//echo $insert;
		//die();

		//ejecutamos el query
		$this -> consulta($insert);

		if($this -> filasAfectadas() >= 1){
			return "Usuario insertado";
		}else{
			return "Error al crear usuario";
		}
	}

}

?>