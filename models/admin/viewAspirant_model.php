<?php
require_once '../../properties/properties.inc';
require_once "../../db/mysql.php";

/**
*
*/
class Aspirants extends conexion
{

	private $_validatDigital = null;
  private $_id_aspirant = null;

	function __construct(){
		$this -> _validatDigital = array();
    $this -> _id_aspirant = array();
	}

	public function validatedDocumentDigital($idDocument, $validated){
		$this -> conectar();

		$query = "UPDATE 	documents_aspirants
							set 		validated_digital_document = $validated
							where 	id_document_aspirant = $idDocument";


		//echo $query;
		//die();

		$this->consulta($query);

		if($this->filasAfectadas()==1){
			$response = array("validado"=>"si");
			echo json_encode($response);
		}
	}

	public function validatedDocumentPhysical($idDocument, $validated){
		$this -> conectar();

		$query = "UPDATE 	documents_aspirants
							set 		validated_physical_document = $validated
							where 	id_document_aspirant = $idDocument";

		//echo $query;
		//die();

		$this->consulta($query);

		if($this->filasAfectadas()==1){
			$response = array("validado"=>"si");
			echo json_encode($response);
		}else{
			$response = array("validado"=>"no");
			echo json_encode($response);
		}
	}

	public function validateAspirant($idAspirant, $validate){

		$this -> conectar();

		$query = "UPDATE 	aspirants
							set 		validate = $validate
							where 	id_aspirant = $idAspirant";

		$this -> consulta($query);

		if($this -> filasAfectadas() == 1){
			$response = array("validate"=>"ok");
			echo json_encode($response);
		}else{
			$response = array("validate"=>"no");
			echo json_encode($response);
		}
	}

  public function sendMail($para, $titulo, $message, $cabeceras, $idAspirant ){


    $mail = mail($para, $titulo, $message, $cabeceras);

    if($mail){
      $this -> setPost($idAspirant, $message);
      $response = array("email" => "enviado");
      echo json_encode($response);
    }else{
      $response = array("email" => "error");
      echo json_encode($response);
    }
  }

  public function setPost($idAspirant, $message){
    $idUser = $_SESSION['id_user'];

    $this -> conectar();

    $created_at = date("Y-m-d h:m:s");

    $query = "SELECT  id_post, users_id_user, aspirants_id_aspirant
              from    posts
              where   users_id_user = $idUser
              and     aspirants_id_aspirant = $idAspirant";

    $this -> consulta($query);


    if($this -> numeroFilas() == 0){

      $insert = "INSERT into posts (`users_id_user`,`aspirants_id_aspirant`,`created_at`)
                values  ($idUser, $idAspirant, '$created_at')";

      //echo $insert;

      $this -> consulta($insert);

      if($this -> filasAfectadas() == 1){
        $idPost = $this -> ultimaFila();
        $this ->  setMessage($idPost, $message, $idUser, $idAspirant);
      }

    }else{

      if($row = $this -> fetchAssoc()){
        $idPost = $row['id_post'];

        $this ->  setMessage($idPost, $message, $idUser, $idAspirant);
      }

    }
  }

  public function setMessage($idPost, $message, $idUser, $idAspirant){

    $this -> conectar();

    $created_at = date("Y-m-d h:m:s");

    $query = "INSERT INTO messages(`message`,`posts_id_post`,`posts_users_id_user`,`posts_aspirants_id_aspirant`,`created_at`)
              values('$message', $idPost, $idUser, $idAspirant, '$created_at')";

    $this -> consulta($query);

    if($this -> filasAfectadas() == 1){
      $response = array('mensaje' => 'guardado');
      //echo json_encode($response);
    }
  }

  public function getHistoryMessages($id_aspirant){
    $this -> conectar();

    $query = "SELECT   id_post, users_id_user, aspirants_id_aspirant,
                      message, posts_id_post, posts_users_id_user, posts_aspirants_id_aspirant
              from    posts, messages
              where   id_post = posts_id_post
              and     aspirants_id_aspirant = $id_aspirant;";

    //echo $query;
    //die();

    $this->consulta($query);

    if($this->numeroFilas() >= 1){
      while($row = $this -> filasAfectadas()){
        $this -> _messages[] = $row;
      }
      return $this -> _messages;
    }else{
      return 0;
    }
  }

  public function updateTypeDocument($idDocument, $claveChangeDocument){
    $updated_at = date("Y-m-d h:m:s");

    $this -> conectar();

    $query = "UPDATE  documents_aspirants
              set     plan_type_doc = $claveChangeDocument ,
                      ptd_id_plan_type_document = $claveChangeDocument,
                      updated_at = '$updated_at'
              where   id_document_aspirant = $idDocument";

    //echo $query;
    //die();

    $this -> consulta($query);

    if($this -> filasAfectadas() == 1){
      $response = array("updated"=>"ok");
      echo json_encode($response);
    }else{
      $response = array("updated"=>"no");
      echo json_encode($response);
    }
  }

  public function setPromCert($promedio, $folio, $reference, $idDocument ){
    $updated_at = date("Y/m/d h:i:s");

    //nos coenctamos
    $this -> conectar();
    //preparamos la query
    $query = "SELECT  id_aspirant, reference
              from    aspirants, documents_aspirants
              where   reference = '$reference'
              and     id_aspirant = aspirant_id
              limit   1";

    //echo $query."\n<br>";

    //ejecutamos la consulta
    $this -> consulta($query);
    //si hay datos
    if($this -> numeroFilas() > 0){
      if($row = $this -> fetchAssoc()) {
        $aspirant_id = $row['id_aspirant'];
      }

      //preparamos la query
      $update = " UPDATE  documents_aspirants
                  set     average = '$promedio',
                          certificate_sheet = '$folio',
                          updated_at = '$updated_at'
                  where   aspirant_id = $aspirant_id
                  and     id_document_aspirant = $idDocument";

      //echo $update."\n";

      $this -> consulta($update);

      if($this -> filasAfectadas() == 1){
        $response = array("promedio"=>"ok");
        echo json_encode($response);
      }else{
        $response = array("promedio"=>"fail");
        echo json_encode($response);
      }

      //return $this -> _id_aspirant;
    }else{
      return 0;
    }
  }

}


?>