<?php

require_once '../../../properties/properties.inc';
require_once "../../../db/mysql.php";

/**
*
*/
class FIAF extends Conexion{


	private $_fiaf = null;
	private $_usuarios         = null;

	function __construct(){
		$this -> _fiaf = array();
		$this -> _usuarios         = array();
	}


	public function getFiaf(){

		//nos conectamos
		$this -> conectar();

		//preparanos la query
		$query = "SELECT 	a.referencia, d.rutaDocumento
							from 		ASPIRANTE a, DOCS_ASPIRANTE d
							where		a.referencia = d.referencia
							and 		d.cveTipoDoc = 7";

		//ejecutamos la query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() > 0){
			//traemos los datos en un arreglo
			while ($row = $this -> fetchAssoc()) {
				$this -> _fiaf[] = $row;
			}
			return $this -> _fiaf;
		}else{
			return 0;
		}


	}

}



 ?>