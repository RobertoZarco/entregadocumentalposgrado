<?php 
require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
*
*/
class EntregaReports extends Conexion
{

	private $_totalAspirants             = null;
	private $_totalDocumentsAspirants    = null;
	private $_totalAccesoAspirants       = null;
	private $_totalAspirantsValidate     = null;
	private $_totalNotValidatedAspirants = null;

	function __construct(){
		$this -> _totalAspirants = array();
		$this -> _totalDocumentsAspirants = array();
	}

	public function getAspirantsReport(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, validate, welcome,
		CASE
		WHEN modalities_id_modality= 1 THEN 'Presencial'
		WHEN modalities_id_modality= 2 THEN 'Distancia'
		END AS modalidad
							from 		aspirants
							where 		periods_id_period = 2;";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirants[] = $row;
			}
			return $this -> _totalAspirants;
		}else{
			return false;
		}
		$this -> desconectar();
	}

	public function getAccessAspirants(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name
							from		aspirants
							where 	welcome = 1
							and 		periods_id_period = 2;";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAccesoAspirants[] = $row;
			}
			return $this -> _totalAccesoAspirants;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getFullDocumentsAspirants(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, count(aspirant_id) documents
							from 		aspirants left join documents_aspirants on id_aspirant = aspirant_id
							WHERE 		periods_id_period = 2;
							group by id_aspirant;";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalDocumentsAspirants[] = $row;
			}
			return $this -> _totalDocumentsAspirants;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getValidatedAspirants(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, validate
							from 		aspirants
							where		validate = 1
							and 		periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsValidate[] = $row;
			}
			return $this -> _totalAspirantsValidate;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getNotValidatedAspirants(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, validate
							from 		aspirants
							where		(validate is null or validate = 0)
							and 		periods_id_period = 2;";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalNotValidatedAspirants[] = $row;
			}
			return $this -> _totalNotValidatedAspirants;
		}else{
			return false;
		}

		$this -> desconectar();
	}

}

?>