<?php

require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
*
*/
class Reports extends Conexion
{

	private $_totalAspirants          = null;
	private $_totalAspirantsMADEMS    = null;
	private $_totalAspirantsMSoste    = null;
	private $_totalAspirantsDSoste    = null;

	private $_totalDocumentsAspirants 		= null;
	private $_totalDocumentsAspirantsMADEMS = null;
	private $_totalDocumentsAspirantsMSoste = null;
	private $_totalDocumentsAspirantsDSoste = null;

	private $_totalAccesoAspirants    		= null;
	private $_totalAccesoAspirantsMADEMS    = null;
	private $_totalAccesoAspirantsMSoste	=null;
	private $_totalAccesoAspirantsDSoste	=null;

	private $_totalAspirantsValidate  	  = null;
	private $_totalAspirantsValidateMADEMS=null;
	private $_totalAspirantsValidateMSoste=null;
	private $_totalAspirantsValidateDSoste=null;

	function __construct(){
		$this -> _totalAspirants = array();
		$this -> _totalDocumentsAspirants = array();
	}

	public function getTotalAspirans(){

		$this -> conectar();

		$query = "SELECT 	count(*) aspirantes
							from 		aspirants";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirants[] = $row;
			}
			return $this -> _totalAspirants;
		}else{
			return false;
		}
		$this -> desconectar();
	}

/* @author Josué Ponce de León Carreño
   @return variable con totales
*/

	public function getTotalAspiransMADEMS(){

		$this -> conectar();

		$query = "SELECT 	count(*) aspirantesMADEMS
							from 		aspirants aspi, plans p, programs prog
							WHERE		aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		id_program = 414
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsMADEMS[] = $row;
			}
			return $this -> _totalAspirantsMADEMS;
		}else{
			return false;
		}
		$this -> desconectar();
	}

	public function getTotalAspiransMSoste(){

		$this -> conectar();

		$query = "SELECT 	count(*) aspirantesMSoste
							from 		aspirants aspi, plans p, programs prog
							where		aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		plan_id_plan = 4172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsMSoste[] = $row;
			}
			return $this -> _totalAspirantsMSoste;
		}else{
			return false;
		}
		$this -> desconectar();
	}

	public function getTotalAspiransDSoste(){

		$this -> conectar();

		$query = "SELECT 	count(*) aspirantesDSoste
							from 		aspirants aspi, plans p, programs prog
							where		aspi.plan_id_plan = id_plan
							and 		programs_id = id_program
							and 		plan_id_plan = 5172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsDSoste[] = $row;
			}
			return $this -> _totalAspirantsDSoste;
		}else{
			return false;
		}
		$this -> desconectar();
	}

	public function getTotalDocumentsAspirants(){

		$this -> conectar();

		$query = "SELECT 	count(distinct(aspirant_id)) as totalDocumentos
							from 		documents_aspirants";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalDocumentsAspirants[] = $row;
			}
			return $this -> _totalDocumentsAspirants;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalDocumentsAspirantsMADEMS(){

		$this -> conectar();

		$query = "SELECT 	count(distinct(docs.aspirant_id)) as documentosMADEMS
							from 		documents_aspirants as docs
							LEFT JOIN   aspirants as aspi
							ON docs.aspirant_id = aspi.id_aspirant
							WHERE   	aspi.plan_id_plan!=4172
							AND         aspi.plan_id_plan!=5172
							and 		aspi.periods_id_period = 2 ";


		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalDocumentsAspirantsMADEMS[] = $row;
			}
			return $this -> _totalDocumentsAspirantsMADEMS;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalDocumentsAspirantsMSoste(){

		$this -> conectar();

		$query = "SELECT 	count(distinct(docs.aspirant_id)) as documentosMSoste
							from 		documents_aspirants as docs
							LEFT JOIN   aspirants as aspi
							ON docs.aspirant_id = aspi.id_aspirant
							WHERE   	aspi.plan_id_plan=4172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalDocumentsAspirantsMSoste[] = $row;
			}
			return $this -> _totalDocumentsAspirantsMSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalDocumentsAspirantsDSoste(){

		$this -> conectar();

		$query = "SELECT 	count(distinct(docs.aspirant_id)) as documentosDSoste
							from 		documents_aspirants as docs
							LEFT JOIN   aspirants as aspi
							ON docs.aspirant_id = aspi.id_aspirant
							WHERE   	aspi.plan_id_plan=5172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalDocumentsAspirantsDSoste[] = $row;
			}
			return $this -> _totalDocumentsAspirantsDSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalAccesoAspirants(){

		$this -> conectar();

		$query = "SELECT 	count(welcome) accesos
							from		aspirants
							where 	welcome = 1
							and 	periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAccesoAspirants[] = $row;
			}
			return $this -> _totalAccesoAspirants;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalAccesoAspirantsMADEMS(){

		$this -> conectar();

		$query = "SELECT 	count(welcome) accesosMADEMS
							from		aspirants
							where 	welcome = 1
							and      	plan_id_plan!=4172
							and         plan_id_plan!=5172
							and 		periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAccesoAspirantsMADEMS[] = $row;
			}
			return $this -> _totalAccesoAspirantsMADEMS;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalAccesoAspirantsMSoste(){

		$this -> conectar();

		$query = "SELECT 	count(welcome) accesosMSoste
							from		aspirants
							where 	welcome = 1
							and      	plan_id_plan=4172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAccesoAspirantsMSoste[] = $row;
			}
			return $this -> _totalAccesoAspirantsMSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}	

	public function getTotalAccesoAspirantsDSoste(){

		$this -> conectar();

		$query = "SELECT 	count(welcome) accesosDSoste
							from		aspirants
							where 	welcome = 1
							and      	plan_id_plan=5172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAccesoAspirantsDSoste[] = $row;
			}
			return $this -> _totalAccesoAspirantsDSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}	

	public function getTotalAspirantsValidate(){

		$this -> conectar();

		$query = "SELECT 	count(*) validados
							from 		aspirants
							where		validate = 1
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsValidate[] = $row;
			}
			return $this -> _totalAspirantsValidate;
		}else{
			return false;
		}

		$this -> desconectar();
	}

public function getTotalAspiransValidateMADEMS(){

		$this -> conectar();

		$query = "SELECT 	count(*) validadosMADEMS
							from 		aspirants
							where		validate = 1
							and      	plan_id_plan!=4172
							and         plan_id_plan!=5172
							and 		periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsValidateMADEMS[] = $row;
			}
			return $this -> _totalAspirantsValidateMADEMS;
		}else{
			return false;
		}

		$this -> desconectar();
	}

	public function getTotalAspiransValidateMSoste(){

		$this -> conectar();

		$query = "SELECT 	count(*) validadosMSoste
							from 		aspirants
							where		validate = 1
							and      	plan_id_plan=4172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsValidateMSoste[] = $row;
			}
			return $this -> _totalAspirantsValidateMSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}


	public function getTotalAspiransValidateDSoste(){

		$this -> conectar();

		$query = "SELECT 	count(*) validadosDSoste
							from 		aspirants
							where		validate = 1
							and      	plan_id_plan=5172
							and 		aspi.periods_id_period = 2";

		$this -> consulta($query);


		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _totalAspirantsValidateDSoste[] = $row;
			}
			return $this -> _totalAspirantsValidateDSoste;
		}else{
			return false;
		}

		$this -> desconectar();
	}

}



 ?>