<?php
require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
 *
 */
class PDFs extends Conexion {


	private $_pdfStudents= null;
	private $_mailStudents= null;


	function __construct() {
		$this->_pdfStudents = array();
	}

	public function getStudentsPDF() {

		$this->conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, appointment, birth_certificate, curp,
											identification, certificate, college_degree,day,turn
							from 		students
							where account_number='304036954'";
		//account_number NOT IN (	'518007517', '518007679', '518007469', '518007627', '518007737','088315078', '518007823', '518007878', '518007476', '304036954')


		$this->consulta($query);

		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			return $this->_pdfStudents;
		} else {
			return false;
		}

		$this->desconectar();
	}

	public function getStudentsPDF_MOR() {

		$this->conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name, appointment, birth_certificate, curp,
											identification, certificate, college_degree,day,turn
							from 		students
							where   program_id=0";

		$this->consulta($query);

		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			return $this->_pdfStudents;
		} else {
			return false;
		}

		$this->desconectar();
	}

	public function getStudentsMail(){

		$this -> conectar();

		$query = "SELECT 	account_number, first_surname, second_surname, name,
							from 		students";

		$this -> consulta($query);

		if($this -> numeroFilas() > 0){
			while ($row = $this -> fetchAssoc()) {
				$this -> _mailStudents[] = $row;
			}
			return $this -> _mailStudents;
		}else{
			return false;
		}
		$this -> desconectar();
	}

	public function generateCita($ncta, $nombre, $documentos, $day, $turn){
		//$mpdf = new mPDF();
		//$mpdf = new mPDF('utf-8', 'A4');
		$mpdf = new mPDF('utf-8', array(205.105,265.43));
		$mpdf->Bookmark('Cita para entrega documental');
		/* Activa el almacenamiento en bufer de la salida*/
		ob_start();
		/* Cargamos el documento que se requiere generar */
		//include 'comprobante1.php';
		include 'genera_pdf.php';
		/* Obtiene el contenido del bufer actual*/
		$html = ob_get_clean();
		/* Cargamos los estilos para el documento PDF*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/* Los cargamos al documento PDF*/
		$mpdf->WriteHTML($stylesheet, 1);
		/* Cargamos el documento*/
		$mpdf->WriteHTML($html, 2);

		$mpdf->SetFooter('<img class="unam" style="width: 10%;" src="../../../assets/img/dgae1.png">');
		//$mpdf->SetFooter('<img class="unam" style="width: 10%;" src="../../../assets/img/UNAMPOSGRAGO.png">');
		/*Lo mandamos a la vista web*/
		$mpdf->Output("Citas/".$ncta.".pdf", 'F');
	}

	//Funcion que permite generar el PDF para el instructivo
	public function generateInstructivo(){
		//$mpdf = new mPDF('utf-8', 'A4');
		$mpdf = new mPDF('utf-8', array(205.105,265.43));
		$mpdf->Bookmark('Instructivo');

		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivo.php';

		$html = ob_get_clean();

		$stylesheet = file_get_contents('./../../../assets/css/styles.css');

		$mpdf->WriteHTML($stylesheet, 1);

		$mpdf->WriteHTML($html, 2);

		$mpdf->SetFooter('<img class="unam" style="width: 10%;" src="../../../assets/img/dgae1.png">');
		//$mpdf->SetFooter('<img class="unam" style="width: 20%;" src="../../../assets/img/UNAMPOSGRAGO.png">');

		$mpdf->Output("Citas/Instructivo.pdf", 'F');
	}

	public function generatePDF($ncta, $nombre, $documentos, $day, $turn){

		//$mpdf = new mPDF();
		//$mpdf = new mPDF('utf-8', array(205.105,265.43));
		$mpdf = new mPDF('utf-8', array(205.105,270));
		//$mpdf->mirroMargins = true;
		//$mpdf->setFooter('{PAGENO}');
		//$mpdf->SetHeader('Document Title|texto centrado|{PAGENO}');
		//$mpdf->SetFooter('Document Title||{PAGENO}');
		/*
		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
				<tr>
					<td width="33%">
						<span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span>
					</td>

					<td width="33%" align="center" style="font-weight: bold; font-style: italic;">
						{PAGENO}/{nbpg}
					</td>

					<td width="33%" style="text-align: right; ">
						My document
					</td>
				</tr>
			</table>
			');
		*/
		$mpdf->Bookmark('Cita para entrega documental');
		/* Activa el almacenamiento en bufer de la salida*/
		ob_start();

		$mpdf->SetHTMLHeader('
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">

				<tr style="background: #1c3d6c;">
          <td style="text-align: left;">
            <img height="30" title="UNAM"
            style="border-top-left-radius: 4px;background:#1c3d6c; padding:6px 9px; text-align:left;"
            src="../../../assets/img/escudo_unam_solow.png"
            >
          </td>
        </tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" cellspacing="0" cellpadding="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"
			>
	      <tr width="100%">

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;" >
	          <img title="Posgrado" width="100px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/CEP.png"
	           >

	          <p style="text-align: center; color: #FFF; font-size: 10px;">
	            Coordinaci&oacute;n de Estudios de Posgrado
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <p style="text-align: center; color: #FFF; font-size1:10px;">
	            ATENTAMENTE
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <img title="UNAM" width="40px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/escudo_dgae_solow.png">

	          <p style="text-align: center; color: #FFF; font-size:10px;">
	            Direcci&oacute;n General de Administraci&oacute;n Escolar
	          </p>
	        </td>
	      </tr>
			</table>
		');

		/* Cargamos el documento que se requiere generar */
		//include 'comprobante1.php';
		include 'genera.php';
		/* Obtiene el contenido del bufer actual*/
		$html = ob_get_clean();
		/* Cargamos los estilos para el documento PDF*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/* Los cargamos al documento PDF*/
		$mpdf->WriteHTML($stylesheet, 1);

		$mpdf->WriteHTML($html, 2);

		$mpdf->AddPage();

		//----------------------------------------------------------

		// Now the right-margin (inner margin on page 2) = 30

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivo.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		//$mpdf->Output();
		$mpdf->Output("Citas/".$ncta.".pdf", 'F');
	}

	public function generatePDF_MOR($ncta, $nombre, $documentos, $day, $turn){

		//$mpdf = new mPDF();
		//$mpdf = new mPDF('utf-8', array(205.105,265.43));
		$mpdf = new mPDF('utf-8', array(205.105,270));
		//$mpdf->mirroMargins = true;
		//$mpdf->setFooter('{PAGENO}');
		//$mpdf->SetHeader('Document Title|texto centrado|{PAGENO}');
		//$mpdf->SetFooter('Document Title||{PAGENO}');
		/*
		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
				<tr>
					<td width="33%">
						<span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span>
					</td>

					<td width="33%" align="center" style="font-weight: bold; font-style: italic;">
						{PAGENO}/{nbpg}
					</td>

					<td width="33%" style="text-align: right; ">
						My document
					</td>
				</tr>
			</table>
			');
		*/
		$mpdf->Bookmark('Cita para entrega documental');
		/* Activa el almacenamiento en bufer de la salida*/
		ob_start();

		$mpdf->SetHTMLHeader('
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">

				<tr style="background: #1c3d6c;">
          <td style="text-align: left;">
            <img height="30" title="UNAM"
            style="border-top-left-radius: 4px;background:#1c3d6c; padding:6px 9px; text-align:left;"
            src="../../../assets/img/escudo_unam_solow.png"
            >
          </td>
        </tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" cellspacing="0" cellpadding="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"
			>
	      <tr width="100%">

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;" >
	          <img title="Posgrado" width="100px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/CEP.png"
	           >

	          <p style="text-align: center; color: #FFF; font-size: 10px;">
	            Coordinaci&oacute;n de Estudios de Posgrado
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <p style="text-align: center; color: #FFF; font-size1:10px;">
	            ATENTAMENTE
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <img title="UNAM" width="40px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/escudo_dgae_solow.png">

	          <p style="text-align: center; color: #FFF; font-size:10px;">
	            Direcci&oacute;n General de Administraci&oacute;n Escolar
	          </p>
	        </td>
	      </tr>
			</table>
		');

		/* Cargamos el documento que se requiere generar */
		//include 'comprobante1.php';
		include 'genera_mor.php';
		/* Obtiene el contenido del bufer actual*/
		$html = ob_get_clean();
		/* Cargamos los estilos para el documento PDF*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/* Los cargamos al documento PDF*/
		$mpdf->WriteHTML($stylesheet, 1);

		$mpdf->WriteHTML($html, 2);

		$mpdf->AddPage();



		//------------------------------------------
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'genera_mor2.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);




		//----------------------------------------------------------

		// Now the right-margin (inner margin on page 2) = 30

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivo.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		//$mpdf->Output();
		$mpdf->Output("Citas/Morelia/".$ncta.".pdf", 'F');
	}

}

?>