<?php

require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
*
*/
class SendMail extends Conexion
{
	private $_students = null;

	function __construct(){
		$this -> _students = array();
	}

	public function getStudents(){
		//nos conectamos
		$this -> conectar();
		//preparamos la query
		$query = "SELECT 	*
							from 		students
							where   account_number not in (	'518007517', '518007679', '518007469', '518007627', '518007737',
																							'088315078', '518007823', '518007878', '518007476', '304036954')";
		//ejecutamos la consulta
		$this -> consulta($query);

		//si hay datos
		if($this->numeroFilas() > 0){
			while ( $row = $this->fetchAssoc()) {
				$this -> _students[] = $row;
			}
			return $this -> _students;
		}else{
			return 0;
		}
	}


}


?>