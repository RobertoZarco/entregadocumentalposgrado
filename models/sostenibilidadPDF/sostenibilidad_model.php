<?php
require_once '../../../db/mysql.php';
require_once '../../../properties/properties.inc';

/**
 * Modelo para generar los PDF's de Sostenibilidad (maestria y doctorado)
 */
class PDFsSostenibilidad extends Conexion {
	/* Variable para obtener los datos de los aspirantes*/
	private $_pdfStudents = null;

	function __construct() {
		$this->_pdfStudents = array();
	}
	/**
	 * Funcion que regresa la cadena que contiene el Header
	 */
	private function getHeader() {
		return '
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">

				<tr style="background: #1c3d6c;">
          <td style="text-align: left;">
            <img height="30" title="UNAM"
            style="border-top-left-radius: 4px;background:#1c3d6c; padding:6px 9px; text-align:left;"
            src="../../../assets/img/escudo_unam_solow.png"
            >
          </td>
        </tr>
			</table>
		';
	}
	/**
	 * Funcion que regresa la cadena que contiene el footer
	 */
	private function getFooter() {
		return '
			<table width="100%" cellspacing="0" cellpadding="0"
				style="font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"
			>
	      <tr width="100%">

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;" >
	          <img title="Posgrado" width="100px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/CEP.png"
	           >

	          <p style="text-align: center; color: #FFF; font-size: 10px;">
	            Coordinaci&oacute;n de Estudios de Posgrado
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <p style="text-align: center; color: #FFF; font-size1:10px;">
	            ATENTAMENTE
	          </p>
	        </td>

	        <td valign="top" align="center" style="background:#3b4042; padding:6px 9px; text-align:center;">
	          <img title="UNAM" width="40px" height="20px"
	          style="border-top-left-radius:0px; background:#3b4042; padding:6px 9px; text-align:left;"
	          src="../../../assets/img/escudo_dgae_solow.png">

	          <p style="text-align: center; color: #FFF; font-size:10px;">
	            Direcci&oacute;n General de Administraci&oacute;n Escolar
	          </p>
	        </td>
	      </tr>
			</table>
		';
	}
	/**
	 * Query que obtiene los estudiantes de maestria
	 */
	public function getStudentsPDFMaestria() {
		/*Realiza la conexion*/
		$this->conectar();

		$query = "	SELECT 	account_number, first_surname,
							second_surname, name, appointment,
							birth_certificate, curp,
							identification, certificate,
							college_degree,day,turn
					FROM 	students
								WHERE 	program_id = 210
								AND 		plan_id	   = 4172
								AND 		campus    != 'ENES MORELIA' ";
		/*Realizamos la consulta*/
		$this->consulta($query);
		/*Verificamos que no este vacia*/
		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			/*Agregamos los estudiantes al arreglo*/
			return $this->_pdfStudents;
		} else {
			return false;
		}
		/*Nos desconectamos*/
		$this->desconectar();
	}
	/**
	 * Query que obtiene los estudiantes de maestria con Cita en Morelia
	 */
	public function getStudentsPDFMaestriaMorelia() {
		/*Realiza la conexion*/
		$this->conectar();

		$query = "	SELECT 	account_number, first_surname,
												second_surname, name, appointment,
												birth_certificate, curp,
												identification, certificate,
												college_degree,day,turn
								FROM 		students
								WHERE 	program_id = 210
								AND 		plan_id	   = 4172
								AND 		campus     = 'ENES MORELIA' ";
		/*Realizamos la consulta*/
		$this->consulta($query);
		/*Verificamos que no este vacia*/
		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			/*Agregamos los estudiantes al arreglo*/
			return $this->_pdfStudents;
		} else {
			return false;
		}
		/*Nos desconectamos*/
		$this->desconectar();
	}
	/**
	 * Query que obtiene los estudiantes de doctorado
	 */
	public function getStudentsPDFDoctoradoDirecto() {
		/*Realiza la conexion*/
		$this->conectar();

		$query = "	SELECT 	account_number, first_surname,
												second_surname, name, appointment,
												birth_certificate, curp,
												identification, certificate,
												college_degree,day,turn
								FROM 		students
								WHERE 	program_id     = 210
								AND 		plan_id	       = 5172
								AND 		campus        != 'ENES MORELIA'
								AND     field_knowledge = 'DD'";
		/*Realizamos la consulta*/
		$this->consulta($query);
		/*Verificamos que no este vacia*/
		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			/*Agregamos los estudiantes al arreglo*/
			return $this->_pdfStudents;
		} else {
			return false;
		}
		/*Nos desconectamos*/
		$this->desconectar();
	}

	/**
	 * Query que obtiene los estudiantes de doctorado
	 */
	public function getStudentsPDFDoctorado() {
		/*Realiza la conexion*/
		$this->conectar();

		$query = "	SELECT 	account_number, first_surname,
												second_surname, name, appointment,
												birth_certificate, curp,
												identification, certificate,
												college_degree,campus,day,turn
								FROM 		students
								WHERE 	program_id       = 210
								AND 		plan_id	         = 5172
								AND 		campus          != 'ENES MORELIA'
								AND     field_knowledge != 'DD'";
		/*Realizamos la consulta*/
		$this->consulta($query);
		/*Verificamos que no este vacia*/
		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			/*Agregamos los estudiantes al arreglo*/
			return $this->_pdfStudents;
		} else {
			return false;
		}
		/*Nos desconectamos*/
		$this->desconectar();
	}

	/**
	 * Query que obtiene los estudiantes de doctorado con Cita en Morelia
	 */
	public function getStudentsPDFDoctoradoMorelia() {
		/*Realiza la conexion*/
		$this->conectar();

		$query = "	SELECT 	account_number, first_surname,
												second_surname, name, appointment,
												birth_certificate, curp,
												identification, certificate,
												college_degree,day,turn
								FROM 		students
								WHERE 	program_id = 210
								AND 		plan_id	   = 5172
								AND 		campus     = 'ENES MORELIA' ";
		/*Realizamos la consulta*/
		$this->consulta($query);
		/*Verificamos que no este vacia*/
		if ($this->numeroFilas() > 0) {
			while ($row = $this->fetchAssoc()) {
				$this->_pdfStudents[] = $row;
			}
			/*Agregamos los estudiantes al arreglo*/
			return $this->_pdfStudents;
		} else {
			return false;
		}
		/*Nos desconectamos*/
		$this->desconectar();
	}
	/**
	 * Funcion que genera los PDFs para los aspirantes a maestria
	 */
	public function generatePDFMaestria($ncta, $nombre, $documentos, $day, $turn) {
		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', array(205.105, 270));
		/*Ponemos un bookmark*/
		$mpdf->Bookmark('Cita para entrega documental');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();
		/* mandamos llamar al header*/
		$mpdf->SetHTMLHeader($this->getHeader());
		/* mandamos llamar al footer*/
		$mpdf->SetHTMLFooter($this->getFooter());

		/*Cargamos el archivo a generar*/
		include 'entregaMaestria.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivoI.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		/*Generamos el PDF*/
		$mpdf->Output("Sostenibilidad/Maestria/" . $ncta . ".pdf", 'F');
	}

	/**
	 * Funcion que genera los PDFs para los aspirantes a maestria
	 */
	public function generatePDFMaestriaMorelia($ncta, $nombre, $documentos, $day, $turn) {

		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', array(205.105, 270));
		/*Ponemos un bookmark*/
		$mpdf->Bookmark('Cita para entrega documental');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		$mpdf->SetHTMLHeader($this->getHeader());

		$mpdf->SetHTMLFooter($this->getFooter());

		/*Cargamos el archivo a generar*/
		include 'entregaMaestriaMor.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivoI.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		/*Generamos el PDF*/
		$mpdf->Output("Sostenibilidad/maestriaMorelia/" . $ncta . ".pdf", 'F');

	}
	/**
	 * Funcion que genera los PDFs para los aspirantes a doctorado
	 */
	public function generatePDFDoctoradoDirecto($ncta, $nombre, $documentos, $day, $turn) {
		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', array(205.105, 270));
		/*Ponemos un bookmark*/
		$mpdf->Bookmark('Cita para entrega documental');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		$mpdf->SetHTMLHeader($this->getHeader());

		$mpdf->SetHTMLFooter($this->getFooter());

		/*Cargamos el archivo a generar*/
		include 'entregaDoctoradoDirecto.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivoI.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		/*Generamos el PDF*/
		$mpdf->Output("Sostenibilidad/doctoradoDirecto/" . $ncta . ".pdf", 'F');
	}

	/**
	 * Funcion que genera los PDFs para los aspirantes a doctorado normal
	 */
	public function generatePDFDoctorado($ncta, $nombre, $documentos, $day, $turn) {
		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', array(205.105, 270));
		/*Ponemos un bookmark*/
		$mpdf->Bookmark('Cita para entrega documental');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		$mpdf->SetHTMLHeader($this->getHeader());

		$mpdf->SetHTMLFooter($this->getFooter());

		/*Cargamos el archivo a generar*/
		include 'entregaDoctorado.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivoI.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		/*Generamos el PDF*/
		$mpdf->Output("Sostenibilidad/Doctorado/" . $ncta . ".pdf", 'F');
	}
	/**
	 * Funcion que genera los PDFs para los aspirantes a doctorado
	 */
	public function generatePDFDoctoradoMorelia($ncta, $nombre, $documentos, $day, $turn) {
		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', array(205.105, 270));
		/*Ponemos un bookmark*/
		$mpdf->Bookmark('Cita para entrega documental');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		$mpdf->SetHTMLHeader($this->getHeader());

		$mpdf->SetHTMLFooter($this->getFooter());

		/*Cargamos el archivo a generar*/
		include 'entregaDoctoradoMor.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('./../../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		/* Cargamos el documento que se requiere generar */
		ob_start();
		/* Cargamos el documento que se requiere generar */
		include 'instructivoI.php';

		$html = ob_get_clean();

		$mpdf->WriteHTML($html, 2);

		/*Generamos el PDF*/
		$mpdf->Output("Sostenibilidad/doctoradoMorelia/" . $ncta . ".pdf", 'F');
	}
}
?>
