<?php
require_once '../../db/mysql.php';
require_once '../../properties/properties.inc';

/**
 * Modelo para generar los PDF's de entrega
 */
class PDFsSostenibilidad extends Conexion {
	/* Variable para obtener los datos de los aspirantes*/
	private $_pdfStudents = null;
	private $_dataAspirant = null;

	function __construct() {
		$this->_pdfStudents = array();
		$this->_dataAlumn = array();
	}


	public function convirteFecha($fecha){
		$dia   = substr($fecha,4,2);
		$month = substr($fecha,0,3);
		$anio  = substr($fecha,7,4);

		switch ($month) {
			case 'Jan':
				$mes = '01';
				break;
			case 'Feb':
				$mes = '02';
				break;
			case 'Mar':
				$mes = '03';
				break;
			case 'Apr':
				$mes = '04';
				break;
			case 'May':
				$mes = '05';
				break;
			case 'Jun':
				$mes = '06';
				break;
			case 'Jul':
				$mes = '07';
				break;
			case 'Aug':
				$mes = '08';
				break;
			case 'Sep':
				$mes = '09';
				break;
			case 'Oct':
				$mes = '10';
				break;
			case 'Nov':
				$mes = '11';
				break;
			case 'Dec':
				$mes = '12';
				break;
		}

		$fecha2 = $dia."/".$mes."/".$anio;

		return $fecha2;
	}
	//metodo que invierte la fecha de dd/mm/aaaa a aaaa/mm/dd
	public function invirteFecha($fecha){
		//17/04/1930
		$dia   = substr($fecha,0,2);
		$mes   = substr($fecha,3,2);
		$anio  = substr($fecha,6,11);

		$fecha2 = trim($anio."/".$mes."/".$dia);

		return $fecha2;
	}

	public function fechaStandar($fecha){
		$dia = substr($fecha,8,2);
		$mes = substr($fecha,5,2);
		$año = substr($fecha,0,4);

		return $dia."/".$mes."/".$año;
	}


	/**
	 * Funcion que regresa la cadena que contiene el Header
	 */
	private function getHeader() {
		return '
			<div>
				<div style="float: left; width: 15%;">
					<img class="unam" src="../../assets/img/unam.png">
				</div>
				<div style="float: left; width: 70%; text-align: center;">
					<b>
						<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
					</b>
					<h6><br>Secretaría General <br>
					Dirección General de Administración Escolar</h6>
				</div>
				<div style="float: right; width: 15%;">
					<img class="dgae1" style="width: 80%; margin-left: 2.2em;" src="../../assets/img/dgae1.png">
				</div>
			</div>
		';
	}
	/**
	 * Funcion que regresa la cadena que contiene el footer para expediente
	 */
	private function getFooterE() {
		return '
			<footer>
				<div style="float: left; width: 50%;">
					Expediente DGAE
				</div>
				<div style="float: right; width: 50%;" align="right">
					<img src="../../assets/img/nacion.jpg" alt="" width="12%">
				</div>
			</footer>
		';
	}
	/**
	 * Funcion que regresa la cadena que contiene el footer para alumno
	 */
	private function getFooterA() {
		return '
			<footer>
				<div style="float: left; width: 50%;">
					Alumno
				</div>
				<div style="float: right; width: 50%;" align="right">
					<img src="../../assets/img/nacion.jpg" alt="" width="12%">
				</div>
			</footer>
		';
	}
	/**
	 * Funcion que regresa la cadena que contiene el footer para alumno
	 */
	private function getFooterP() {
		return '
			<footer>
				<div style="float: left; width: 50%;">
					Posgrado
				</div>
				<div style="float: right; width: 50%;" align="right">
					<img src="../../assets/img/nacion.jpg" alt="" width="12%">
				</div>
			</footer>
		';
	}

	/*
		 * Funcion para obtener los datos del alumno
	*/
	public function getDataAlumn($ncta) {
		//nos conectamos
		$this->conectar();
		//preparamos la query
		$query = "SELECT 	s.account_number, s.first_surname, s.second_surname, s.name, s.email, s.field_knowledge, s.campus,
											s.program_id, s.plan_id, pl.plan plan, pr.name program, s.day, s.turn,
							        street, outdoor_number, colony, municipality, cou.country, aspi.state, aspi.postal_code,
							        unam, folio_credential, document_delivered, user_delivered, date_delivered, ip_delivered
							from 		students s, programs pr, plans pl, aspirants aspi, countries cou
							where  	s.account_number = '$ncta'
							and			pr.id_program = program_id
							and 		pl.id_plan = plan_id
							and     s.account_number = aspi.account_number
							and     aspi.country = cou.code_country;";

		//ejecutamos la query
		$this->consulta($query);
		//si hay datos
		if ($this->numeroFilas() > 0) {
			//obtenemos los datois en en array
			while ($row = $this->fetchAssoc()) {
				$this->_dataAlumn[] = $row;
			}
			return $this->_dataAlumn;
		} else {
			return 0;
		}
	}

	/*
	* funcion para obtener documentacion completa
	*/
	public function documentComplete($ncta, $nombre, $email, $field_knowledge, $campus, $program_id, $plan_id, $plan, $program, $street,$outdoor_number, $colony, $munipality, $country, $state, $postal_code, $day, $turn, $unam, $folio_credential, $document_delivered) {


		if($plan_id == '4172' or  $plan_id == '5172'){
			$rutaCarpetaSalida = "../../views/EntregaDoc/home/Docs/SOSTENIBILIDAD/$ncta";
			$carpetaSalida     = $rutaCarpetaSalida."/" . $ncta . ".pdf";
		}else{
			$rutaCarpetaSalida = "../../views/EntregaDoc/home/Docs/MADEMS/$ncta";
			$carpetaSalida     = $rutaCarpetaSalida."/" . $ncta . ".pdf";
		}

		if (!file_exists($rutaCarpetaSalida)) {
			mkdir($rutaCarpetaSalida);
		}

		//----------------carta de asignacion alumno----------------------------------------
		/*Establecemos el tamaño del PDF, tipo de hoja y el escalamiento*/
		$mpdf = new mPDF('utf-8', '', 9, 'dejavusans');
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionAlumno.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al header*/
		//$mpdf->SetHTMLHeader($this->getHeader());
		/* mandamos llamar al footer para el alumno*/
		$mpdf->SetHTMLFooter($this->getFooterA());
		//echo $maestria;
		/*Generamos el PDF*/

		$mpdf->AddPage();


		//----------------carta de asignacion dgae----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();
		/* mandamos llamar al header*/
		//$mpdf->SetHTMLHeader($this->getHeader());

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionDGAE.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al footer para expediente dgae*/
		$mpdf->SetHTMLFooter($this->getFooterE());

		$mpdf->AddPage();


		//----------------carta de asignacion posgrado----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();
		/* mandamos llamar al header*/
		$mpdf->SetHTMLHeader($this->getHeader());

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionPosgrado.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al footer para expediente dgae*/
		$mpdf->SetHTMLFooter($this->getFooterP());


		$mpdf->AddPage();


		//----------------Cita Foto----------------------------------------

		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CitaFoto.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();


		//----------------Acuse de recibo de credencial----------------------------------------

		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/AcuseCredencial.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);




		$mpdf->AddPage();


		if($unam == 0){

			//----------------Acuse de recibo y consentimiento----------------------------------------
			/*Activamos el almacenamiento del bufer de salida*/
			ob_start();

			/*Cargamos el archivo a generar*/
			include '../../views/EntregaDoc/home/AcuseConsentimiento.php';
			/*Obtenemos el archivo en el bufer*/
			$maestria = ob_get_clean();
			/*Cargamos los estilos*/
			$stylesheet = file_get_contents('../../assets/css/styles.css');
			/*Agregamos los estilos*/
			$mpdf->WriteHTML($stylesheet, 1);
			/*Agregamos el archivo que queremos generar*/
			$mpdf->WriteHTML($maestria, 2);

			$mpdf->SetHTMLFooter($this->getFooterA());

			$mpdf->AddPage();



			//----------------Acuse de recibo y consentimiento----------------------------------------
			/*Activamos el almacenamiento del bufer de salida*/
			ob_start();

			/*Cargamos el archivo a generar*/
			include '../../views/EntregaDoc/home/AcuseConsentimiento.php';
			/*Obtenemos el archivo en el bufer*/
			$maestria = ob_get_clean();
			/*Cargamos los estilos*/
			$stylesheet = file_get_contents('../../assets/css/styles.css');
			/*Agregamos los estilos*/
			$mpdf->WriteHTML($stylesheet, 1);
			/*Agregamos el archivo que queremos generar*/
			$mpdf->WriteHTML($maestria, 2);

			$mpdf->SetHTMLFooter($this->getFooterE());


			$mpdf->AddPage();
		}


		//----------------Divulgacion cientifica----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/Divulga2.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);


		$tipoDoc       =1;
		$updateEntrega = $this -> updateEntrega($ncta,$tipoDoc);



		$mpdf->Output($carpetaSalida, 'F');
		$mpdf->Output();
	}

	/*
	* funcion para obtener carta compromiso
	*/
	public function documentNoComplete($ncta, $nombre, $email, $field_knowledge, $campus, $program_id, $plan_id, $plan, $program, $street,$outdoor_number, $colony, $munipality, $country, $state, $postal_code, $day, $turn, $unam, $folio_credential, $document_delivered){


		if($plan_id == '4172' or  $plan_id == '5172'){
			$rutaCarpetaSalida = "../../views/EntregaDoc/home/Docs/SOSTENIBILIDAD/$ncta";
			$carpetaSalida     = $rutaCarpetaSalida."/" . $ncta . ".pdf";
		}else{
			$rutaCarpetaSalida = "../../views/EntregaDoc/home/Docs/MADEMS/$ncta";
			$carpetaSalida     = $rutaCarpetaSalida."/" . $ncta . ".pdf";
		}

		if (!file_exists($rutaCarpetaSalida)) {
			mkdir($rutaCarpetaSalida);
		}


		$mpdf = new mPDF('utf-8', '', 9, 'dejavusans');

		//si son de sostenibilidad
		if($plan_id == '4172' or  $plan_id == '5172'){

			//si es de maestria
			if($plan_id == '4172'){
				//--------------------------carta compromiso alumno---------------------------

				/*Activamos el almacenamiento del bufer de salida*/
				ob_start();

				/*Cargamos el archivo a generar*/
				include '../../views/EntregaDoc/home/CartaCompromiso/Sostenibilidad/cartaCompromisoMaestria.php';
				/*Obtenemos el archivo en el bufer*/
				$maestria = ob_get_clean();
				/*Cargamos los estilos*/
				$stylesheet = file_get_contents('../../assets/css/styles.css');
				/*Agregamos los estilos*/
				$mpdf->WriteHTML($stylesheet, 1);
				/*Agregamos el archivo que queremos generar*/
				$mpdf->WriteHTML($maestria, 2);
				/* mandamos llamar al footer para expediente dgae*/
				$mpdf->SetHTMLFooter($this->getFooterA());

				$mpdf->AddPage();

				//--------------------------carta compromiso dgae---------------------------

				/*Activamos el almacenamiento del bufer de salida*/
				ob_start();

				/*Cargamos el archivo a generar*/
				include '../../views/EntregaDoc/home/CartaCompromiso/Sostenibilidad/cartaCompromisoMaestriaDGAE.php';

				/*Obtenemos el archivo en el bufer*/
				$maestria = ob_get_clean();
				/*Cargamos los estilos*/
				$stylesheet = file_get_contents('../../assets/css/styles.css');
				/*Agregamos los estilos*/
				$mpdf->WriteHTML($stylesheet, 1);
				/*Agregamos el archivo que queremos generar*/
				$mpdf->WriteHTML($maestria, 2);
				/* mandamos llamar al footer para expediente dgae*/
				$mpdf->SetHTMLFooter($this->getFooterE());

				$mpdf->AddPage();
			}else{
				//si son de doctorado
				//--------------------------carta compromiso alumno---------------------------

				/*Activamos el almacenamiento del bufer de salida*/
				ob_start();

				/*Cargamos el archivo a generar*/
				include '../../views/EntregaDoc/home/CartaCompromiso/Sostenibilidad/cartaCompromisoDoctorado.php';
				/*Obtenemos el archivo en el bufer*/
				$maestria = ob_get_clean();
				/*Cargamos los estilos*/
				$stylesheet = file_get_contents('../../assets/css/styles.css');
				/*Agregamos los estilos*/
				$mpdf->WriteHTML($stylesheet, 1);
				/*Agregamos el archivo que queremos generar*/
				$mpdf->WriteHTML($maestria, 2);
				/* mandamos llamar al footer para expediente dgae*/
				$mpdf->SetHTMLFooter($this->getFooterA());

				$mpdf->AddPage();


				//--------------------------carta compromiso dgae---------------------------

				/*Activamos el almacenamiento del bufer de salida*/
				ob_start();

				/*Cargamos el archivo a generar*/
				include '../../views/EntregaDoc/home/CartaCompromiso/Sostenibilidad/cartaCompromisoDoctoradoDGAE.php';

				/*Obtenemos el archivo en el bufer*/
				$maestria = ob_get_clean();
				/*Cargamos los estilos*/
				$stylesheet = file_get_contents('../../assets/css/styles.css');
				/*Agregamos los estilos*/
				$mpdf->WriteHTML($stylesheet, 1);
				/*Agregamos el archivo que queremos generar*/
				$mpdf->WriteHTML($maestria, 2);
				/* mandamos llamar al footer para expediente dgae*/
				$mpdf->SetHTMLFooter($this->getFooterE());

				$mpdf->AddPage();
			}
		}else{
			//--------------------------carta compromiso alumno---------------------------

			/*Activamos el almacenamiento del bufer de salida*/
			ob_start();

			/*Cargamos el archivo a generar*/
			include '../../views/EntregaDoc/home/CartaCompromiso/EducacionMediaSuperior/cartaComprimisoMaestria.php';
			/*Obtenemos el archivo en el bufer*/
			$maestria = ob_get_clean();
			/*Cargamos los estilos*/
			$stylesheet = file_get_contents('../../assets/css/styles.css');
			/*Agregamos los estilos*/
			$mpdf->WriteHTML($stylesheet, 1);
			/*Agregamos el archivo que queremos generar*/
			$mpdf->WriteHTML($maestria, 2);
			/* mandamos llamar al footer para expediente dgae*/
			$mpdf->SetHTMLFooter($this->getFooterA());

			$mpdf->AddPage();


			//--------------------------carta compromiso dgae---------------------------

			/*Activamos el almacenamiento del bufer de salida*/
			ob_start();

			/*Cargamos el archivo a generar*/
			include '../../views/EntregaDoc/home/CartaCompromiso/EducacionMediaSuperior/cartaComprimisoMaestriaDGAE.php';

			/*Obtenemos el archivo en el bufer*/
			$maestria = ob_get_clean();
			/*Cargamos los estilos*/
			$stylesheet = file_get_contents('../../assets/css/styles.css');
			/*Agregamos los estilos*/
			$mpdf->WriteHTML($stylesheet, 1);
			/*Agregamos el archivo que queremos generar*/
			$mpdf->WriteHTML($maestria, 2);
			/* mandamos llamar al footer para expediente dgae*/
			$mpdf->SetHTMLFooter($this->getFooterE());

			$mpdf->AddPage();
		}


		//----------------carta de asignacion alumno----------------------------------------

		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionAlumno.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al header*/
		//$mpdf->SetHTMLHeader($this->getHeader());
		/* mandamos llamar al footer para el alumno*/
		$mpdf->SetHTMLFooter($this->getFooterA());
		//echo $maestria;
		/*Generamos el PDF*/

		$mpdf->AddPage();



		//----------------carta de asignacion dgae----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();
		/* mandamos llamar al header*/
		//$mpdf->SetHTMLHeader($this->getHeader());

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionDGAE.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al footer para expediente dgae*/
		$mpdf->SetHTMLFooter($this->getFooterE());

		$mpdf->AddPage();



		//----------------carta de asignacion posgrado----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();
		/* mandamos llamar al header*/
		$mpdf->SetHTMLHeader($this->getHeader());

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CartaAceptacion/cartaAceptacionPosgrado.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);
		/* mandamos llamar al footer para expediente dgae*/
		$mpdf->SetHTMLFooter($this->getFooterP());


		$mpdf->AddPage();




		//----------------Cita Foto----------------------------------------

		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/CitaFoto.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);

		$mpdf->AddPage();

		//----------------Acuse de recibo de credencial----------------------------------------

		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/AcuseCredencial.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);


		$mpdf->AddPage();


		if($unam == 0){

			//----------------Acuse de recibo y consentimiento----------------------------------------
			/*Activamos el almacenamiento del bufer de salida*/
			// ob_start();

			// /*Cargamos el archivo a generar*/
			// include '../../views/EntregaDoc/home/AcuseConsentimiento.php';
			// /*Obtenemos el archivo en el bufer*/
			// $maestria = ob_get_clean();
			// /*Cargamos los estilos*/
			// $stylesheet = file_get_contents('../../assets/css/styles.css');
			// /*Agregamos los estilos*/
			// $mpdf->WriteHTML($stylesheet, 1);
			// /*Agregamos el archivo que queremos generar*/
			// $mpdf->WriteHTML($maestria, 2);
			// $mpdf->SetHTMLFooter($this->getFooterA());


			// $mpdf->AddPage();



			// //----------------Acuse de recibo y consentimiento----------------------------------------
			// /*Activamos el almacenamiento del bufer de salida*/
			// ob_start();

			// /*Cargamos el archivo a generar*/
			// include '../../views/EntregaDoc/home/AcuseConsentimiento.php';
			// /*Obtenemos el archivo en el bufer*/
			// $maestria = ob_get_clean();
			// /*Cargamos los estilos*/
			// $stylesheet = file_get_contents('../../assets/css/styles.css');
			// /*Agregamos los estilos*/
			// $mpdf->WriteHTML($stylesheet, 1);
			// /*Agregamos el archivo que queremos generar*/
			// $mpdf->WriteHTML($maestria, 2);
			// $mpdf->SetHTMLFooter($this->getFooterE());


			// $mpdf->AddPage();
		}


		//----------------Divulgacion cientifica----------------------------------------
		/*Activamos el almacenamiento del bufer de salida*/
		ob_start();

		/*Cargamos el archivo a generar*/
		include '../../views/EntregaDoc/home/Divulga2.php';
		/*Obtenemos el archivo en el bufer*/
		$maestria = ob_get_clean();
		/*Cargamos los estilos*/
		$stylesheet = file_get_contents('../../assets/css/styles.css');
		/*Agregamos los estilos*/
		$mpdf->WriteHTML($stylesheet, 1);
		/*Agregamos el archivo que queremos generar*/
		$mpdf->WriteHTML($maestria, 2);


		$tipoDoc       =2;
		$this -> updateEntrega($ncta,$tipoDoc);



		$mpdf->Output($carpetaSalida, 'F');
		$mpdf->Output();
	}

	/*
	* funcion para marcar que ya se entrego documentacion de inscripcion
	* 1 -> completa
	* 2 -> carta compromiso
	*/
	public function updateEntrega($ncta,$tipoDoc){

		$user = $_SESSION['id_user'];
		$date = date("Y/m/d");
		$ip = $_SERVER['REMOTE_ADDR'];
		//nod conectamos
		$this -> conectar();
		//preparamos la query
		$query = "UPDATE students
							set 	user_delivered = '$user',
										date_delivered = '$date',
										ip_delivered = '$ip',
										document_delivered = $tipoDoc
							where account_number = '$ncta'";

		//echo $query;
		//die();

		//ejectumos la consulta

		$this -> consulta($query);
	}

}

?>