<?php

error_reporting(E_ALL);


require_once '../db/mysql.php';


/**
* clase que gestiona el login tanto del aspirante como del admin
*/
class Login extends Conexion{

	private $_login       = null;

	function __construct(){
		$this -> _login = array();
	}

	public function getLoginUsuario($usuario, $pass){
		//nos conectamos
		$this -> conectar();
		//preparamos la consulta
		$query = "SELECT 	u.id_user, first_surname, second_surname, name, dependency, login, email,
											create_users, edit_users, create_aspirant, edit_aspirant, post, view_digital_document,
											delete_digital_document, validate_digital_document, receive_physical_document,
											validate_physical_document, reports, p.post
							from 		users u, permissions p
							WHERE 	login        = '$usuario'
							and 		password     = '$pass'
							and			u.dependency = 1
							and 		u.id_user    = p.users_id;";

		//echo $query;
		//die();

		//ejecutamos el query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() == 1){
			//nos traemos los datos en un arreglo
			if ($row = $this -> fetchAssoc()) {
				//se ecran las variables de session
				$_SESSION['id_user']                    = $row['id_user'];
				$_SESSION['first_surname']              = $row['first_surname'];
				$_SESSION['second_surname']             = $row['second_surname'];
				$_SESSION['name']                       = $row['name'];
				$_SESSION['email']                      = $row['email'];
				$_SESSION['dependency']                 = $row['dependency'];
				$_SESSION['login']                      = $row['login'];
				$_SESSION['session']                    = 1;
				$_SESSION['create_user']                = $row['create_users'];
				$_SESSION['edit_user']                  = $row['edit_users'];
				$_SESSION['create_aspirant']            = $row['create_aspirant'];
				$_SESSION['edit_aspirant']              = $row['edit_aspirant'];
				$_SESSION['post']                       = $row['post'];
				$_SESSION['view_digital_document']      = $row['view_digital_document'];
				$_SESSION['delete_digital_document']    = $row['delete_digital_document'];
				$_SESSION['validate_digital_document']  = $row['validate_digital_document'];
				$_SESSION['receive_physical_document']  = $row['receive_physical_document'];
				$_SESSION['validate_physical_document'] = $row['validate_physical_document'];
				$_SESSION['reports']                    = $row['reports'];

				//$this -> Permisos($row['id_user']);
				$this -> _login[] = $row;
			}

			return $this -> _login;
		}else{
			$logueado = "no logueado";
			//die("error");
			//echo json_encode($logueado);
			return false;
		}
	}

	public function getLoginUsuarioCaseta($usuario, $pass){
		//nos conectamos
		$this -> conectar();
		//preparamos la consulta
		$query = "SELECT 	u.id_user, first_surname, second_surname, name, dependency, login, email,
											create_users, edit_users, create_aspirant, edit_aspirant, post, view_digital_document,
											delete_digital_document, validate_digital_document, receive_physical_document,
											validate_physical_document, reports, p.post
							from 		users u, permissions p
							WHERE 	login    = '$usuario'
							and 		password = '$pass'
							and			u.dependency = 2
							and 		u.id_user = p.users_id;";

		//echo $query;
		//die();

		//ejecutamos el query
		$this -> consulta($query);

		//si hay resultados
		if($this -> numeroFilas() == 1){
			//nos traemos los datos en un arreglo
			if ($row = $this -> fetchAssoc()) {
				//se ecran las variables de session
				$_SESSION['id_user']                    = $row['id_user'];
				$_SESSION['first_surname']              = $row['first_surname'];
				$_SESSION['second_surname']             = $row['second_surname'];
				$_SESSION['name']                       = $row['name'];
				$_SESSION['email']                      = $row['email'];
				$_SESSION['dependency']                 = $row['dependency'];
				$_SESSION['login']                      = $row['login'];
				$_SESSION['session']                    = 1;
				$_SESSION['create_user']                = $row['create_users'];
				$_SESSION['edit_user']                  = $row['edit_users'];
				$_SESSION['create_aspirant']            = $row['create_aspirant'];
				$_SESSION['edit_aspirant']              = $row['edit_aspirant'];
				$_SESSION['post']                       = $row['post'];
				$_SESSION['view_digital_document']      = $row['view_digital_document'];
				$_SESSION['delete_digital_document']    = $row['delete_digital_document'];
				$_SESSION['validate_digital_document']  = $row['validate_digital_document'];
				$_SESSION['receive_physical_document']  = $row['receive_physical_document'];
				$_SESSION['validate_physical_document'] = $row['validate_physical_document'];
				$_SESSION['reports']                    = $row['reports'];

				//$this -> Permisos($row['id_user']);
				$this -> _login[] = $row;
			}

			return $this -> _login;
		}else{
			$logueado = "no logueado";
			//die("error");
			//echo json_encode($logueado);
			return false;
		}
	}

	public function getLoginAspirante($usuario, $pass){
    //nos conectamos
    $this -> conectar();
    //preparamos la consulta
    $query = "SELECT  id_aspirant, account_number, reference, first_surname, second_surname, name, dependency, email, plan_id_plan, birthdate, plan, welcome
							from 		aspirants, plans
							where   email    = '$usuario'
							and     birthdate = '$pass'
							and 		id_plan = plan_id_plan
							and 		periods_id_period = 2";


			/*$query = "SELECT count(doc.aspirant_id) as totalDocumentosMADEMS ,doc.aspirant_id
							and			plan_id_plan in ('4172', '5172')
								from 	documents_aspirants AS doc
								LEFT JOIN   aspirants AS aspi ON doc.aspirant_id=aspi.id_aspirant
								LEFT JOIN   plans AS pl ON aspi.plan_id_plan = pl.id_plan
								LEFT JOIN   programs AS prog ON pl.programs_id = prog.id_program
								WHERE (pl.id_plan = 5172 OR pl.id_plan = 4172)
								and   email    = '$email'
								and     birthdate = '$pass'
								GROUP BY doc.aspirant_id;";*/


    //echo "$query";
    //die();

    //die("Error en login ASPIRANTE");

    $this -> consulta($query);

    //si hay resultados
    if($this -> numeroFilas() == 1){
      //nos traemos los datos en un arreglo
      if ($row = $this -> fetchAssoc()) {
				//se ecran las variables de session
				$_SESSION['id_aspirant']    = $row['id_aspirant'];
				$_SESSION['account_number'] = $row['account_number'];
				$_SESSION['reference']      = $row['reference'];
				$_SESSION['first_surname']  = $row['first_surname'];
				$_SESSION['second_surname'] = $row['second_surname'];
				$_SESSION['name']           = $row['name'];
				$_SESSION['dependency']     = $row['dependency'];
				$_SESSION['email']          = $row['email'];
				$_SESSION['plan']           = $row['plan'];
				$_SESSION['plan_id_plan']   = $row['plan_id_plan'];

				if($row['welcome'] == 1){
					$this -> _login[] = $row;
				}else{

					//$this -> sessionInit();

	        //$this -> _login[] = $row;
				}

      }

      return $this -> _login;
    }else{
      //$logueado = "no logueado";
      //echo json_encode($logueado);
      return false;
    }
  }

	//public function Permisos($idUsuario){
	//}

	public function LoginCuayed($cadena, $nchm, $chm, $llia){

		$correo = substr($chm, 0, -4);

		//nos conectamos
    $this -> conectar();
    //preparamos la consulta
    $query = "SELECT  idAspirante, referencia, paterno, materno, nombre, fechaNacimiento, cveDependencia, login
              from 		ASPIRANTE
              where 	correo = '$correo'";

    //echo "$query";

    //die("Error en login ASPIRANTE");

    $this -> consulta($query);

    //echo "<br>Filas -> ".$this -> numeroFilas() ."<br>";

    //si hay resultados
    if($this -> numeroFilas() == 1){
      //nos traemos los datos en un arreglo
      if ($row = $this -> fetchAssoc()) {
        //se ecran las variables de session
				//$fechaNacimiento            = substr($row['fechaNacimiento'],8,2 ).substr($row['fechaNacimiento'],5,2 ).substr($row['fechaNacimiento'],0,4 );
				$_SESSION['idAspirante']    = $row['idAspirante'];
				$_SESSION['referencia']     = $row['referencia'];
				$_SESSION['nombre']         = $row['paterno']." ". $row['materno']." ".$row['nombre'];
				$_SESSION['cveDependencia'] = $row['cveDependencia'];
				//$_SESSION['login']          = $row['login'];
        //$this -> Permisos($row['referencia']);
				//echo $fechaNacimiento;
        //die();
        $this -> _login[] = $row;
      }

			//$llia2 = sha1($nchm.$chm.$fechaNacimiento.$cadena);
			$llia2 = sha1($nchm.$chm.$cadena);

			if($llia == $llia2){
				$response = array("login" => "Exitoso");
				return $response;
			}else{
				$response = array("login" => "Incorrecto");
				return $response;
			}

    }else{
      $response = array("login" => "Incorrecto");
      return $response;
    }
	}


	public function sessionInit(){

		$this -> conectar();

		$query = "SELECT welcome from aspirants where id_aspirant = ". $_SESSION['id_aspirant'];

		$this -> consulta($query);


		if($this -> numeroFilas() == 1 ){
			if($row = $this -> fetchAssoc()){
				$welcome = $row['welcome'];

				if($welcome == null or $welcome == 0){
					$update = "UPDATE aspirants set welcome = 1 where id_aspirant = ". $_SESSION['id_aspirant'];

					$this -> consulta($update);

					if($this -> filasAfectadas() == 1){
						$this -> sendWelcome();
					}

				}else{
					return 1;
					//echo "ya habia iniciado";
				}
			}
		}
	}


	public function sendWelcome(){

		//$para      = 'shura_capricornio10@hotmail.com'.',';
    //$para      .= 'joshy@comunidad.unam.mx';

		$para         = $_SESSION['email'];
		$name         = $_SESSION['name']." ".$_SESSION['first_surname']." ".$_SESSION['second_surname'];
		$plan         = $_SESSION['plan'];
		$plan_id_plan = $_SESSION['plan_id_plan'];

		if($plan_id_plan == "5172"){
			$planEstudios = $plan;
		}else{
			$planEstudios = $plan;
		}

    //$mensaje = "Estimad@ $name\n";

    $mensaje = "  <table style='color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica' cellspacing='0' cellpadding='0' border='0'>
                    <tbody>
                      <tr width='100%'>
                        <td style='background:#fff;font:15px 'Helvetica Neue',Arial,Helvetica' valign='top' align='left'>
                          <table style='border:none;padding:0 18px;margin:50px auto;width:500px' cellspacing='0' cellpadding='0' border='0'>
                            <tbody>
                              <tr width='100%' height='90px'>
                                <td style='border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left' valign='top' align='left'>
                                  <img class='CToWUd' title='UNAM' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci6.googleusercontent.com/proxy/QvPY_nhaS4J9SDuJFvCEGG-NbgbIutxYDkILdp1fH4YyZ8j5wOoxitkgWmTxj3zGKG4XabtKzIeyfHRNerOD1J9zN1clSnNld2LYVSH14tYIOMyHgg=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png' height='70'>
                                </td>
                                <td style='border-top-right-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:right' valign='top' align='right'>
                                  <img class='CToWUd' title='DGAE' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci5.googleusercontent.com/proxy/qAWWIallTjz8gPN7j69aUDH5GmTHNdNb-uLFJH582fo20_HiyJFNHRH8kXNj2nPD-gYDti-XkY0DFubcMoUBrgQ_zUt_3rOLLWwTNqmTBUT0t4JCbw=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png' height='50'>
                                </td>
                              </tr>
                              <tr width='100%'>
                                <td style='border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#f6f6f6;padding:18px' colspan='2' valign='top' align='left'>

                                  <h1 style='font-size:20px;margin:0;color:#333'>
                                  	$planEstudios
                                	</h1>

                                  <p style='font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333'>
                                    Estimado aspirante: $name
                                    <br>
                                    Te damos la bienvenida al Sistema de Recepción Documental para tu participación en el proceso

                                    de selección de ingreso a $planEstudios.

                                    Deberás imprimir este correo
																	</p>

																	<p>
                                    Atentamente:<br>

                                    Dirección General de Administración Escolar

                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>";

    //$mensaje .= $mensaje;

    $titulo = "$planEstudios";

    //$cabeceras = 'From: rherrera@mercurio.dgae.unam.mx' . "\r\n" . 'X-Mailer: PHP/' . phpversion();

    // Para enviar un email HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Cabeceras adicionales
    //$cabeceras .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";


    $cabeceras .= "From: DGAE - $planEstudios." . "\r\n";
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Roberto <rherrera@mercurio.dgae.unam.mx>' . "\r\n";
    //$cabeceras .= 'Bcc: aldova19@gmail.com' . "\r\n";

    // Cabeceras adicionales
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Imelda <imelda_gonzalez@cuaed.unam.mx>' . "\r\n";
    //$cabeceras .= 'Cc: Mary <mtabares@dgae.unam.mx>, Alberto Vargas <alberto_vargas@cuaed.unam.mx>' . "\r\n";
    //$cabeceras .= "Bcc: $emailAdmin" . "\r\n";


    $mail = mail($para, $titulo, $mensaje, $cabeceras);

    if($mail){
      $response = array("email" => "enviado");
      return $response;
    }
	}


}

?>