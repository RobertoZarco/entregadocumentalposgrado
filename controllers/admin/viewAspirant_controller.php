<?php 

require_once '../../models/admin/viewAspirant_model.php';

//validate documento digital
if(isset($_POST) and @$_POST['updateDocumentValidate'] == "si"){
	$Aspirants  = new Aspirants();
	$idDocument = $_POST['idDocument'];
	$validated  = $_POST['validated'];
	$validatedDocumentDigital = $Aspirants -> validatedDocumentDigital($idDocument, $validated);
}

//validate documento fisico
if(isset($_POST) and @$_POST['updateDocumentPhysical'] == "si"){
	$Aspirants  = new Aspirants();
	$idDocument = $_POST['idDocument'];
	$validated  = $_POST['validated'];
	$validatedDocumentPhysical = $Aspirants -> validatedDocumentPhysical($idDocument, $validated);
}

//validate aspirante
if(isset($_POST) and @$_POST['validateAspirant'] == "si"){
	$Aspirants  = new Aspirants();
	$idAspirant = $_POST['idAspirant'];
	$validated  = $_POST['validated'];

	$validateAspirant = $Aspirants -> validateAspirant($idAspirant, $validated);
}


//Actualizar doc
if(isset($_POST) and @$_POST['updateTypeDocument'] == "si"){
  $idDocument = $_POST['idDocChange'];
  $claveChangeDocument = $_POST['newClaveDocChange'];

  $Aspirants  = new Aspirants();
  $updateClavDoc = $Aspirants -> updateTypeDocument($idDocument, $claveChangeDocument);

}


//envio de correo
if(isset($_POST) and @$_POST['sendMail'] == "si"){
  $Aspirants  = new Aspirants();
  $email      = $_POST['email'];
  $message1   = $_POST['message'];
  $name       = $_POST['name'];
  $idAspirant = $_POST['idAspirant'];
  $plan       = $_POST['plan'];

  $emailAdmin = $_SESSION['email'];

  $para    = $email;

  $message = "  <table style='color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica' cellspacing='0' cellpadding='0' border='0'>
                  <tbody>
                    <tr width='100%'>
                      <td style='background:#fff;font:15px 'Helvetica Neue',Arial,Helvetica' valign='top' align='left'>
                        <table style='border:none;padding:0 18px;margin:50px auto;width:500px' cellspacing='0' cellpadding='0' border='0'>
                          <tbody>
                            <tr width='100%' height='90px'>
                              <td style='border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left' valign='top' align='left'>
                                <img class='CToWUd' title='UNAM' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci6.googleusercontent.com/proxy/QvPY_nhaS4J9SDuJFvCEGG-NbgbIutxYDkILdp1fH4YyZ8j5wOoxitkgWmTxj3zGKG4XabtKzIeyfHRNerOD1J9zN1clSnNld2LYVSH14tYIOMyHgg=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png' height='70'>
                              </td>
                              <td style='border-top-right-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:right' valign='top' align='right'>
                                <img class='CToWUd' title='DGAE' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci5.googleusercontent.com/proxy/qAWWIallTjz8gPN7j69aUDH5GmTHNdNb-uLFJH582fo20_HiyJFNHRH8kXNj2nPD-gYDti-XkY0DFubcMoUBrgQ_zUt_3rOLLWwTNqmTBUT0t4JCbw=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png' height='50'>
                              </td>
                            </tr>
                            <tr width='100%'>
                              <td style='border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#f6f6f6;padding:18px' colspan='2' valign='top' align='left'>

                                <h1 style='font-size:20px;margin:0;color:#333'>
                                  DGAE - $plan
                                </h1>

                                <p style='font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333'>
                                  Estimado aspirante: $name
                                  <br>
                                  $message1
                                  <br>
                                  Atentamente
                                  <br>
                                  Dirección General de Administración Escolar

                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>";


  $titulo = "DGAE - $plan ";
  
  // Para enviar un email HTML, debe establecerse la cabecera Content-type
  $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
  $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";


  $cabeceras .= "From: DGAE - $plan." . "\r\n";


  // Cabeceras adicionales
  $cabeceras .= "Bcc: $emailAdmin";



	$sendMail = $Aspirants -> sendMail($para, $titulo, $message, $cabeceras, $idAspirant);
}

//envio de correo de validacion
if(isset($_POST) and @$_POST['sendValidacion'] == "si"){
  $Aspirants  = new Aspirants();
  $email      = $_POST['email'];
  $message1   = $_POST['message'];
  $name       = $_POST['name'];
  $idAspirant = $_POST['idAspirant'];
  $plan       = $_POST['plan'];

  $emailAdmin = $_SESSION['email'];

  $para    = $email;

  $message = "  <table style='color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica' cellspacing='0' cellpadding='0' border='0'>
                  <tbody>
                    <tr width='100%'>
                      <td style='background:#fff;font:15px 'Helvetica Neue',Arial,Helvetica' valign='top' align='left'>
                        <table style='border:none;padding:0 18px;margin:50px auto;width:500px' cellspacing='0' cellpadding='0' border='0'>
                          <tbody>
                            <tr width='100%' height='90px'>
                              <td style='border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left' valign='top' align='left'>
                                <img class='CToWUd' title='UNAM' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci6.googleusercontent.com/proxy/QvPY_nhaS4J9SDuJFvCEGG-NbgbIutxYDkILdp1fH4YyZ8j5wOoxitkgWmTxj3zGKG4XabtKzIeyfHRNerOD1J9zN1clSnNld2LYVSH14tYIOMyHgg=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png' height='70'>
                              </td>
                              <td style='border-top-right-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:right' valign='top' align='right'>
                                <img class='CToWUd' title='DGAE' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci5.googleusercontent.com/proxy/qAWWIallTjz8gPN7j69aUDH5GmTHNdNb-uLFJH582fo20_HiyJFNHRH8kXNj2nPD-gYDti-XkY0DFubcMoUBrgQ_zUt_3rOLLWwTNqmTBUT0t4JCbw=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png' height='50'>
                              </td>
                            </tr>
                            <tr width='100%'>
                              <td style='border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#f6f6f6;padding:18px' colspan='2' valign='top' align='left'>

                                <h1 style='font-size:20px;margin:0;color:#333'>
                                  DGAE - $plan
                                </h1>

                                <p style='font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333'>
                                  Estimado aspirante: $name
                                  <br>

                                  Tu documentación para postulación al programa de $plan fue revisada y validada exitosamente.
                                  
                                  <br>
                                  Atentamente
                                  <br>
                                  Dirección General de Administración Escolar

                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>";


  $titulo = "DGAE - $plan ";
  
  // Para enviar un email HTML, debe establecerse la cabecera Content-type
  $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
  $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";


  $cabeceras .= "From: DGAE - $plan." . "\r\n";


  // Cabeceras adicionales
  $cabeceras .= "Bcc: $emailAdmin";



  $sendMail = $Aspirants -> sendMail($para, $titulo, $message, $cabeceras, $idAspirant);
}



if(isset($_POST) and @$_POST['savePromCert'] == 'si' ){
  $Aspirants = new Aspirants();

  $promCert   = $_POST['promCert'];
  $folioCert  = $_POST['folioCert'];
  $reference  = $_POST['reference'];
  $idDocument = $_POST['idDocument'];

  $setPromCert = $Aspirants -> setPromCert($promCert, $folioCert, $_POST['reference'], $idDocument);


  //print_r($setPromCert);

}



?>