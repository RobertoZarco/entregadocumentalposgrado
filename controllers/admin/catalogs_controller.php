<?php
//require_once(dirname(__FILE__)."/../archivo_banderas.php");

require_once '../../models/admin/catalogs_model.php';

if(isset($_POST) and  @$_POST['searchConcurso'] == "si"){
	$catalogos        = new Catalogo();
	$catalogoConcurso = $catalogos -> getConcurso();

	for($i = 0; $i < count($catalogoConcurso); $i++ ){
		$cveConcurso  = $catalogoConcurso[$i]['cveConcurso'];
		$descConcurso = $catalogoConcurso[$i]['descConcurso'];

		$response[$i] = array (
														"cveConcurso"  => $cveConcurso,
														"descConcurso" => $descConcurso
													);
	}

	echo json_encode($response);
}

if(isset($_POST) and  @$_POST['searchPeriodo'] == "si"){
	$catalogos        = new Catalogo();
	$catalogoPeriodo = $catalogos -> getPeriodo();

	for($i = 0; $i < count($catalogoPeriodo); $i++ ){
		$periodo  = $catalogoPeriodo[$i]['periodo'];

		$response[$i] = array ( "periodo"  => $periodo );
	}

	echo json_encode($response);
}

if(isset($_POST) and  @$_POST['searchTipoIngreso'] == "si"){
	$catalogos        = new Catalogo();
	$catalogoTipoIngreso = $catalogos -> getTipoIngreso();

	for($i = 0; $i < count($catalogoTipoIngreso); $i++ ){
		$tipoIngreso     = $catalogoTipoIngreso[$i]['tipoIngreso'];
		$descTipoIngreso = $catalogoTipoIngreso[$i]['descTipoIngreso'];

		$response[$i] = array (
														"tipoIngreso" => "$tipoIngreso",
														"descTipoIngreso"  => $descTipoIngreso
													);
	}

	echo json_encode($response);
}

if(isset($_POST) and  @$_POST['searchSistema'] == "si"){
	$catalogos        = new Catalogo();
	$catalogoSistema = $catalogos -> getSistema();

	for($i = 0; $i < count($catalogoSistema); $i++ ){
		$cveSistema     = $catalogoSistema[$i]['cveSistema'];
		$descSistema = $catalogoSistema[$i]['descSistema'];

		$response[$i] = array (
														"cveSistema" => "$cveSistema",
														"descSistema"  => $descSistema
													);
	}

	echo json_encode($response);
}




if(isset($_POST) and @$_POST['searchConti'] == "si"){
	$catalogos = new Catalogo();
	$catalogoPais = $catalogos -> getPaisByCve($_POST['continente']);


	for($i = 0; $i < count($catalogoPais); $i++){
		$cvePais = $catalogoPais[$i]['cvePais'];
		$pais    = $catalogoPais[$i]['pais'];

		$response[$i] = array (
														"cvePais" => $cvePais,
														"pais" => $pais
													);
	}
	echo json_encode($response);
}

if(isset($_POST) and @$_POST['searchPais'] == "si"){
	$catalogos = new Catalogo();
	$catalogoPais = $catalogos -> getEstadoByCve();


	for($i = 0; $i < count($catalogoPais); $i++){
		$cveEstado = $catalogoPais[$i]['cveEstado'];
		$estado    = $catalogoPais[$i]['estado'];

		$response[$i] = array (
														"cveEstado" => $cveEstado,
														"estado" => $estado
													);
	}
	echo json_encode($response);
}

if(isset($_POST) and @$_POST['searchEstado'] == "si"){
	$catalogos = new Catalogo();
	$catalogoPais = $catalogos -> getComunidadByCve($_POST['Estado']);


	for($i = 0; $i < count($catalogoPais); $i++){
		$cveMunicipio = $catalogoPais[$i]['cveMunicipio'];
		$municipio    = $catalogoPais[$i]['municipio'];

		$response[$i] = array (
														"cveMunicipio" => $cveMunicipio,
														"municipio" => $municipio
													);
	}
	echo json_encode($response);
}

?>