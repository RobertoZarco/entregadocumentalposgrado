<?php
require_once '../../models/admin/chat_model.php';
require_once '../../properties/properties.inc';

$chat = new CHAT();

if(isset($_POST) and @$_POST['insertPeticion'] == "si"){
	$emisor     = $_POST['emisor'];
	$receptor   = $_POST['receptor'];
	$mensaje    = $_POST['message'];
	$statusChat = $_POST['statusChat'];
	$registrerMessage = $chat -> registrerChat($emisor, $receptor, $mensaje, $statusChat);
}

if(isset($_POST) and @$_POST['searchConversation'] == "si" ){

	$CHAT_idUsuario  = @$_POST['emisor'];
	$CHAT_referencia = @$_POST['receptor'];

	$searchConversationEmisor   = $chat -> searchConversationEmisor($CHAT_referencia);
	//$searchConversationReceptor = $chat -> searchConversationReceptor($emisor, $receptor);

	for ($i= 0; $i < count($searchConversationEmisor) ; $i++) {

		if($searchConversationEmisor[$i]['tipoMensaje'] == 1){
			//aspirante
			echo "<p>
						<b>".$searchConversationEmisor[$i]['nombre']."<br>
						dice: ".$searchConversationEmisor[$i]['mensaje']."<br>
						fecha: ".$searchConversationEmisor[$i]['fechaAlta'].
					"</p>";
		}else{
			//administrador
			echo "<p>
						<b>Administrador<br>
						dice: ".$searchConversationEmisor[$i]['mensaje']."<br>
						fecha: ".$searchConversationEmisor[$i]['fechaAlta'].
					"</p>";
		}
	}


	/*
	for ($i= 0; $i < count($searchConversationReceptor) ; $i++) {
		echo "<p>
						<b>".$searchConversationReceptor[$i]['idEmisor']."<br>
						dice: ".$searchConversationReceptor[$i]['mensaje']."<br>
						fecha: ".$searchConversationReceptor[$i]['fecha_mensaje'].
					"</p>";
	}
	*/
}

if(isset($_POST) and @$_POST['countMessages'] == "si"){

	$countMessages = $chat -> countMessages();

	//print_r($countMessages);

	for($i = 0; $i < count($countMessages); $i++){
		$referencia  = $countMessages[$i]['referencia'];
		$statusChat  = $countMessages[$i]['statusChat'];
		$tipoMensaje = $countMessages[$i]['tipoMensaje'];
		$total       = $countMessages[$i]['total'];

		$response[$i] = array (
														"referencia"  => $referencia,
    												"statusChat"  => $statusChat,
    												"tipoMensaje" => $tipoMensaje,
    												"total"       => $total
    										  );
	}

	echo json_encode($response);
}

if(isset($_POST) and @$_POST['takeChat'] == 'si'){
	$takeChat = $chat -> takeChat($_POST['referencia']);
}

?>