<?php

require_once '../../../models/admin/reports_model.php';
//require_once '../../models/admin/aspirante_model.php';

$Report = new EntregaReports();

$getAspirantsReport        = $Report -> getAspirantsReport();
$getAccessAspirants        = $Report -> getAccessAspirants();
$getFullDocumentsAspirants = $Report -> getFullDocumentsAspirants();
$getValidatedAspirants     = $Report -> getValidatedAspirants();
$getNotValidatedAspirants  = $Report -> getNotValidatedAspirants();

?>