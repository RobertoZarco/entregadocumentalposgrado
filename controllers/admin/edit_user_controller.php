<?php

require_once '../../models/admin/edit_user_model.php';


if(isset($_POST) and @$_POST['searchAspi'] == "si"){
	$folio = $_POST['folio'];
	$usuarioUser = new User();
	$usuario = $usuarioUser -> getAspirante($folio);
	require_once '../../helpers/admin/usuarios_helper.php';
}


if(isset($_POST) and @$_POST['createUser'] == "si"){

	$usuario = new Usuarios();
	$setUsuario = $usuario -> setUsuario();
}


if(isset($_POST) and @$_POST['editUser'] == "si"){

	$usuario = new Usuarios();
	$getUsuario = $usuario -> getUsuario($_POST['id']);

	$reference          = $getUsuario[0]['reference'];
	$first_surname      = $getUsuario[0]['first_surname'];
	$second_surname     = $getUsuario[0]['second_surname'];
	$name               = $getUsuario[0]['name'];
	$email              = $getUsuario[0]['email'];
	//$telefono         = $getUsuario[0]['telefono'];
	//$lugar_residencia = $getUsuario[0]['lugar_residencia'];
	$period             = $getUsuario[0]['period'];
	$income_type        = $getUsuario[0]['income_type'];
	$modality           = $getUsuario[0]['modality'];
	$concourse          = $getUsuario[0]['concourse'];
	$login              = $getUsuario[0]['login'];
	$pass               = $getUsuario[0]['pass'];
	//$perfil           = $getUsuario[0]['perfil'];
	$created_at         = $getUsuario[0]['created_at'];
	$active             = $getUsuario[0]['active'];
	$updated_at         = $getUsuario[0]['updated_at'];

	$response = array(
									"reference"          => $reference,
									"first_surname"      => $first_surname,
									"second_surname"     => $second_surname,
									"name"               => $name,
									"email"              => $email,
									"period"             => $period,
									"income_type"        => $income_type,
									"modality"           => $modality,
									"concourse"          => $concourse,
									"login"              => $login,
									"pass"               => $pass,
									//"perfil"             => $perfil,
									"created_at"     => $created_at,
									"active"     => $active,
									"updated_at" => $updated_at
									);


	echo json_encode($response);
}


if(isset($_POST) and @$_POST['updateEditUserAdmin'] == "si"){
	$usuario = new Usuarios();
	$updateUsuario = $usuario -> updateUsuarioAdmin();
}


if ( isset($_POST) and @$_POST['deleteUser'] == "si" ){
	print_r($_POST);
	$id = $_POST['id'];
	$usuario = new Usuarios();
	$deleteUsuario = $usuario -> deleteUsuario($id);
}

if ( isset($_POST) and @$_POST['viewDocsDig'] == "si" ){
	print_r($_POST);
	$id = $_POST['id'];
	$usuario = new Usuarios();
	$getDocDigital = $usuario -> getDocumentsById($reference, $tipoDoc);
}

//Desactivar Usuario Administrador
if ( isset($_POST) and @$_POST['deleteUser_Admin'] == "si" ){
	//print_r($_POST);
	$id = $_POST['id'];
	$usuario = new Usuarios();
	$deleteUsuario = $usuario -> deleteUsuario($id);
}


//Editar Usuarios Administradores
if(isset($_POST) and @$_POST['editUserAdmin'] == "si"){

	$usuario = new Usuarios();
	$getUsuario = $usuario -> getUsuarioAdmin($_POST['id']);

	//print_r($_POST);

	$id_user                   = $getUsuario[0]['id_user'];
	$first_surname             = $getUsuario[0]['first_surname'];
	$second_surname            = $getUsuario[0]['second_surname'];
	$name                      = $getUsuario[0]['name'];
	$CURP                      = $getUsuario[0]['curp'];
	$email                     = $getUsuario[0]['email'];
	$birthdate                 = $getUsuario[0]['birthdate'];
	$dependency                = $getUsuario[0]['dependency'];
	$gender                    = $getUsuario[0]['gender'];
	$login                     = $getUsuario[0]['login'];
	$pass                      = $getUsuario[0]['password'];
	$created_at                = $getUsuario[0]['created_at'];
	$active                    = $getUsuario[0]['active'];
	$updated_at                = $getUsuario[0]['updated_at'];
	$cvePermiso                = $getUsuario[0]['users_id'];
	$create_users              = $getUsuario[0]['create_users'];
	$create_aspirant           = $getUsuario[0]['create_aspirant'];
	$edit_users                = $getUsuario[0]['edit_users'];
	$chat                      = $getUsuario[0]['post'];
	$view_digital_document     = $getUsuario[0]['view_digital_document'];
	$delete_digital_document   = $getUsuario[0]['delete_digital_document'];
	$validate_digital_document = $getUsuario[0]['validate_digital_document'];
	$validate_physical_document = $getUsuario[0]['validate_physical_document'];
	//$validarDocFisico        = $getUsuario[0]['validarDocFisico'];
	$reports                   = $getUsuario[0]['reports'];
	$validate                  = $getUsuario[0]['validate'];


	$response = array(
									"id_user"                   => $id_user,
									"first_surname"             => $first_surname,
									"second_surname"            => $second_surname,
									"name"                      => $name,
									"CURP"                      => $CURP,
									"email"                     => $email,
									"birthdate"                 => $birthdate,
									"dependency"                => $dependency,
									"gender"                    => $gender,
									"login"                     => $login,
									"pass"                      => $pass,
									"created_at"                => $created_at,
									"active"                    => $active,
									"updated_at"                => $updated_at,
									"cvePermiso"                => $cvePermiso,
									"create_users"              => $create_users,
									"create_aspirant"           => $create_aspirant,
									"edit_users"                => $edit_users,
									"chat"                      => $chat,
									"view_digital_document"     => $view_digital_document,
									"delete_digital_document"   => $delete_digital_document,
									"validate_digital_document" => $validate_digital_document,
									"validate_physical_document" => $validate_physical_document,
									"reports"                   => $reports,
									"validate"                   => $validate
									);

	echo json_encode($response);
}

?>
