<?php

require_once '../../../models/admin/report_model.php';

$Reports = new Reports();

$getTotalAspirans           = $Reports -> getTotalAspirans();
$getTotalAspiransMADEMS     = $Reports -> getTotalAspiransMADEMS();
$getTotalAspiransMSoste     = $Reports -> getTotalAspiransMSoste();
$getTotalAspiransDSoste     = $Reports -> getTotalAspiransDSoste();

$getTotalDocumentsAspirants 	  = $Reports -> getTotalDocumentsAspirants();
$getTotalDocumentsAspirantsMADEMS = $Reports -> getTotalDocumentsAspirantsMADEMS();
$getTotalDocumentsAspirantsMSoste = $Reports -> getTotalDocumentsAspirantsMSoste();
$getTotalDocumentsAspirantsDSoste = $Reports -> getTotalDocumentsAspirantsDSoste();

$getTotalAccesoAspirants    	= $Reports -> getTotalAccesoAspirants();
$getTotalAccesoAspirantsMADEMS 	= $Reports -> getTotalAccesoAspirantsMADEMS();
$getTotalAccesoAspirantsMSoste 	= $Reports -> getTotalAccesoAspirantsMSoste();
$getTotalAccesoAspirantsDSoste 	= $Reports -> getTotalAccesoAspirantsDSoste();

$getTotalAspirantsValidate  		= $Reports -> getTotalAspirantsValidate();
$getTotalAspiransValidateMADEMS 	= $Reports -> getTotalAspiransValidateMADEMS();
$getTotalAspiransValidateMSoste 	= $Reports -> getTotalAspiransValidateMSoste();
$getTotalAspiransValidateDSoste 	= $Reports -> getTotalAspiransValidateDSoste();

?>