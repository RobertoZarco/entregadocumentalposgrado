<?php
require_once '../../models/admin/pdfValidacion_model.php';
//require '../../vendor/autoload.php';

//use Dompdf\Dompdf;
//use Nette\Mail\Message;
//use Nette\Mail\SendmailMailer;

if(isset($_POST) and $_POST['pdfValidacion'] == 'si'){

	$documentsValidate = new documentsValidate();

	$id_aspirant = $_POST['id_aspirant'];
	$nombre      = $_POST['nombre'];
	$paterno     = $_POST['paterno'];
	$materno     = $_POST['materno'];
	$mail        = $_POST['email'];

	$setPDFValidacion = $documentsValidate -> setPDFValidacion($id_aspirant);

	$fecha = date('d/m/Y');

	$emailAdmin = $_SESSION['email'];

	// instantiate and use the dompdf class
	//$dompdf = new Dompdf();

	/*
	$html = "
		<!DOCTYPE html>
		<html lang='en'>
		  <head>
		    <meta charset='utf-8'>
		    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
		    <meta name='viewport' content='width=device-width, initial-scale=1'>
		    <title>Comprobante</title>

		    <link href='../../assets/css/bootstrap.min.css' rel='stylesheet'>

		    <style type='text/css'>

		        hr.style1{
		          border-top: 1px solid #8c8b8b;
		        }


		        hr.style2 {
		          border-top: 3px double #8c8b8b;
		        }

		        hr.style3 {
		          border-top: 1px dashed #8c8b8b;
		        }

		        hr.style4 {
		          border-top: 1px dotted #8c8b8b;
		        }

		        hr.style5 {
		          background-color: #fff;
		          border-top: 2px dashed #8c8b8b;
		        }


		        hr.style6 {
		          background-color: #fff;
		          border-top: 2px dotted #8c8b8b;
		        }

		        hr.style7 {
		          border-top: 1px solid #8c8b8b;
		          border-bottom: 1px solid #fff;
		        }


		        hr.style8 {
		          border-top: 1px solid #8c8b8b;
		          border-bottom: 1px solid #fff;
		        }
		        hr.style8:after {
		          content: '';
		          display: block;
		          margin-top: 2px;
		          border-top: 1px solid #8c8b8b;
		          border-bottom: 1px solid #fff;
		        }

		        hr.style9 {
		          border-top: 1px dashed #8c8b8b;
		          border-bottom: 1px dashed #fff;
		        }

		        hr.style10 {
		          border-top: 1px dotted #8c8b8b;
		          border-bottom: 1px dotted #fff;
		        }


		        hr.style11 {
		          height: 6px;
		          background: url(http://ibrahimjabbari.com/english/images/hr-11.png) repeat-x 0 0;
		            border: 0;
		        }


		        hr.style12 {
		          height: 6px;
		          background: url(http://ibrahimjabbari.com/english/images/hr-12.png) repeat-x 0 0;
		            border: 0;
		        }

		        hr.style13 {
		          height: 10px;
		          border: 0;
		          box-shadow: 0 10px 10px -10px #8c8b8b inset;
		        }


		        hr.style14 {
		          border: 0;
		          height: 1px;
		          background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
		          background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
		          background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
		          background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
		        }


		        hr.style15 {
		          border-top: 4px double #8c8b8b;
		          text-align: center;
		        }
		        hr.style15:after {
		          content: '\002665';
		          display: inline-block;
		          position: relative;
		          top: -15px;
		          padding: 0 10px;
		          background: #f0f0f0;
		          color: #8c8b8b;
		          font-size: 18px;
		        }

		        hr.style16 {
		          border-top: 1px dashed #8c8b8b;
		        }
		        hr.style16:after {
		          content: '\002702';
		          display: inline-block;
		          position: relative;
		          top: -12px;
		          left: 40px;
		          padding: 0 3px;
		          background: #f0f0f0;
		          color: #8c8b8b;
		          font-size: 18px;
		        }


		        hr.style17 {
		          border-top: 1px solid #8c8b8b;
		          text-align: center;
		        }
		        hr.style17:after {
		          content: '§';
		          display: inline-block;
		          position: relative;
		          top: -14px;
		          padding: 0 10px;
		          background: #f0f0f0;
		          color: #8c8b8b;
		          font-size: 18px;
		          -webkit-transform: rotate(60deg);
		          -moz-transform: rotate(60deg);
		          transform: rotate(60deg);
		        }


		        hr.style18 {
		          height: 30px;
		          border-style: solid;
		          border-color: #8c8b8b;
		          border-width: 1px 0 0 0;
		          border-radius: 20px;
		        }
		        hr.style18:before {
		          display: block;
		          content: '';
		          height: 30px;
		          margin-top: -31px;
		          border-style: solid;
		          border-color: #8c8b8b;
		          border-width: 0 0 1px 0;
		          border-radius: 20px;
		        }

		         hr.style19{
		          border-top: 4px solid #8c8b8b;
		        }
		    </style>
		  </head>
		  <body>

	      <div class='row'>

					<div class='col-sm-12'>

						<div class='col-sm-6'>
		          <img src='../../assets/img/unam.png' alt='escudo_unam' style='width:50px; height:50px;' class='text-left'>
		          <div class='text-center'>
		            Universidad Nacional Autónoma de México
		            <br>
		            Dirección General de Administración Escolar
		          </div>
		          <img src='../../assets/img/dgae.png'  alt='escudo_unam' style='width:50px; height:50px;' class='text-right'>
						</div>

					</div>
	      </div>

	      <hr class='style19'>

	      <div class='row'>
	        <div class='col-sm-12'>
	          Nombre del Aspirante: $nombre $paterno $materno
	        </div>
	      </div>

	      <div class='row'>
	        <br>
	        <div class='col-sm-offset-2 col-sm-8'>
	          <div class='table-responsive' style='font-size:8px'>
	            <table class='table table-bordered table-condensed table-hover table-striped'>
	              <thead>
	                <tr>
	                  <th>Nombre del Documento</th>
	                  <th>Validado</th>
	                </tr>
	              </thead>
	              <tbody>
	              ";

									for($i = 0; $i < count($setPDFValidacion); $i++){
	      						$html.=
		                "<tr>
		                  <td>".$setPDFValidacion[$i]['document']."</td>
		                  <td>
		                  	<img src='../../assets/img/check_ok.jpg' class='img-responsive' alt='check_ok'>
		                  </td>
		                </tr>";
									}
	              $html.="
	              </tbody>
	            </table>
	          </div>
	        </div>
	      </div>

	      <div class='row'>
	        <div class='col-sm-6'>
	          Fecha de emisión: $fecha
	        </div>


	        <div class='col-sm-2 col-sm-offset-4 text-center'>

	          Firma del aspirante

	        </div>

	        </div>
	      </div>

	      <hr class='style3'>

        <div class='row'>
	        <div class='col-sm-12'>
          	Nombre del Aspirante: $nombre $paterno $materno
        	</div>
      	</div>

	      <div class='row'>
	        <br>
	        <div class='col-sm-offset-2 col-sm-8'>
	          <div class='table-responsive' style='font-size:8px'>
	            <table class='table table-bordered table-condensed table-hover table-striped'>
	              <thead>
	                <tr>
	                  <th>Nombre del Documento</th>
	                  <th>Validado</th>
	                </tr>
	              </thead>
	              <tbody>
	              ";

									for($i = 0; $i < count($setPDFValidacion); $i++){
	      						$html.=
		                "<tr>
		                  <td>".$setPDFValidacion[$i]['document']."</td>
		                  <td>
		                  	<img src='../../assets/img/check_ok.jpg' class='img-responsive' alt='check_ok'>
		                  </td>
		                </tr>";
									}
	              $html.="
	              </tbody>
	            </table>
	          </div>
	        </div>
	      </div>

	      <div class='row'>
	        <div class='col-sm-6'>
	          Fecha de emisión: $fecha
	        </div>

	        <div class='col-sm-2 col-sm-offset-4 text-center'>

	          Firma del aspirante
	        </div>
	      </div>

		    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
		    <script src='../../`assets/js/bootstrap.min.js'></script>
		  </body>
		</html>";

	*/


		$message = "mensaje de prueba";

		$html = "
							<link href='../../assets/css/bootstrap.min.css' rel='stylesheet'>
							<table style='color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica' cellspacing='0' cellpadding='0' border='0'>
		          <tbody>
		            <tr width='100%'>
		              <td style='background:#fff;font:15px 'Helvetica Neue',Arial,Helvetica' valign='top' align='left'>
		                <table style='border:none;padding:0 18px;margin:50px auto;width:500px' cellspacing='0' cellpadding='0' border='0'>
		                  <tbody>
		                    <tr width='100%' height='90px'>
		                      <td style='border-top-left-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:left' valign='top' align='left'>
		                        <img class='CToWUd' title='UNAM' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci6.googleusercontent.com/proxy/QvPY_nhaS4J9SDuJFvCEGG-NbgbIutxYDkILdp1fH4YyZ8j5wOoxitkgWmTxj3zGKG4XabtKzIeyfHRNerOD1J9zN1clSnNld2LYVSH14tYIOMyHgg=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_unam_solow.png' height='70'>
		                      </td>
		                      <td style='border-top-right-radius:4px;background:#1c3d6c;padding:12px 18px;text-align:right' valign='top' align='right'>
		                        <img class='CToWUd' title='DGAE' style='font-weight:bold;font-size:18px;color:rgb(255,255,255);vertical-align:middle' src='https://ci5.googleusercontent.com/proxy/qAWWIallTjz8gPN7j69aUDH5GmTHNdNb-uLFJH582fo20_HiyJFNHRH8kXNj2nPD-gYDti-XkY0DFubcMoUBrgQ_zUt_3rOLLWwTNqmTBUT0t4JCbw=s0-d-e1-ft#https://www.escolar.unam.mx/assets/images/escudo_dgae_solow.png' height='50'>
		                      </td>
		                    </tr>
		                    <tr width='100%'>
		                      <td style='border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#f6f6f6;padding:18px' colspan='2' valign='top' align='left'>

											      <div class='row'>

															<div class='col-sm-12'>
											          <div class='text-center'>
											            Universidad Nacional Autónoma de México
											            <br>
											            Dirección General de Administración Escolar
											          </div>
															</div>
											      </div>

											      <hr class='style19'>

											      <div class='row'>
											        <div class='col-sm-12'>
											          Nombre del Aspirante: $nombre $paterno $materno
											        </div>
											      </div>

											      <div class='row'>
											        <br>
											        <div class='col-sm-offset-2 col-sm-8'>
											          <div class='table-responsive'>
											            <table class='table table-bordered table-condensed table-hover table-striped'>
											              <thead>
											                <tr>
											                  <th align='left'>Nombre del Documento</th>
											                </tr>
											              </thead>
											              <br>
											              <tbody>
											              ";

																			for($i = 0; $i < count($setPDFValidacion); $i++){
											      						$html.=
												                "<tr>
												                  <td>".$setPDFValidacion[$i]['document']."</td>
												                </tr>";
																			}
											              $html.="
											              </tbody>
											            </table>
											          </div>
											        </div>
											      </div>



											      <hr class='style3'>

										        <div class='row'>
											        <div class='col-sm-12'>
										          	Nombre del Aspirante: $nombre $paterno $materno
										        	</div>
										      	</div>

											      <div class='row'>
											        <br>
											        <div class='col-sm-offset-2 col-sm-8'>
											          <div class='table-responsive'>
											            <table class='table table-bordered table-condensed table-hover table-striped'>
											              <thead>
											                <tr>
											                  <th align='left'>Nombre del Documento</th>
											                </tr>
											              </thead>
											              <br>
											              <tbody>
											              ";

																			for($i = 0; $i < count($setPDFValidacion); $i++){
											      						$html.=
												                "<tr>
												                  <td>".$setPDFValidacion[$i]['document']."</td>
												                </tr>";
																			}
											              $html.="
											              </tbody>
											            </table>
											          </div>
											        </div>
											      </div>






		                      </td>
		                    </tr>
		                  </tbody>
		                </table>
		              </td>
		            </tr>
		          </tbody>
		        </table>";


	//$dompdf->loadHtml($html);

	// (Optional) Setup the paper size and orientation
	//$dompdf->setPaper('A4', 'landscape');
	//$dompdf->setPaper('letter', 'portrait');

	// Render the HTML as PDF
	//$dompdf->render();

	$account_number = $setPDFValidacion[0]['account_number'];

	// Output the generated PDF to Browser
	//$dompdf->stream();

	//$pdf = $dompdf->output();

	//if(file_put_contents("../../uploads/$account_number/$account_number"."_comprobante.pdf", $pdf) != false){
		//$response = array("pdf"=>"creado");
		//echo json_encode($response);

		//$sendPDFValidacion = $documentsValidate -> sendPDFValidacion($nombre,$paterno, $materno, $account_number, $email);

		/*
		$mail = new Message;

		$mail->setFrom('Roberto Zarco DGAE <rherrera@dgae.unam.mx>')
		    		->setSubject('Order Confirmation')
		    		->setBody('<b>Sample HTML</b> <p>Ejemplo de pdf adjumto</p>');

		$ruta = "../../uploads/$account_number/$account_number"."_comprobante.pdf";

		$mail->addAttachment($ruta);


		$mailer = new SendmailMailer;

		//$mail->setHTMLBody('<b>Sample HTML</b> <p>Ejemplo de pdf adjumto</p>');


		$mailer->send($mail);
		*/


		$titulo = 'DGAE - MADEMS ';

    // Para enviar un email HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $cabeceras .= 'From: DGAE - Maestría en Docencia para la Educación Media Superior (MADEMS).' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= "Bcc: $emailAdmin" . "\r\n";


    $mail = mail($mail, $titulo, $html, $cabeceras);

    if($mail){
      //$this -> setPost($idAspirant, $message);
      $response = array("email" => "enviado");
      echo json_encode($response);
    }else{
      $response = array("email" => "error");
      echo json_encode($response);
    }

	//}








}


?>