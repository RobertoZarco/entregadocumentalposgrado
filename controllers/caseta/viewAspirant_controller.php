<?php

require_once '../../../models/caseta/viewAspirant_model.php';


if(isset($_POST) and @$_POST['findAspirant'] == "si"){

	$findReference = $_POST['findReference'];

	$findAspirant = new Caseta();
	$getAspirant = $findAspirant -> findAspirant($findReference);

	$account_number = $getAspirant[0]['account_number'];
	$plan = $getAspirant[0]['plan_id_plan'];
	$idAspirant = $getAspirant[0]['id_aspirant'];


	//echo $plan;

	if ($account_number != null){

		$getDocumentoByPlan = $findAspirant-> getDocumentoByPlan ($plan);
		$getDocumentoByPlanRef = $findAspirant-> getDocumentoByPlanRef ($account_number);

		$ArrayDocsPlan = array();
    $ArrayDocsAspi = array();


		for ($i = 0; $i < count($getDocumentoByPlan) ; $i++) {
			$docCata = $getDocumentoByPlan[$i]['type_document_document_id'];
			array_push($ArrayDocsPlan, $docCata);

		}


		for ($i = 0; $i < count($getDocumentoByPlanRef) ; $i++) {
			$docAspi = $getDocumentoByPlanRef[$i]['plan_type_doc'];
			array_push($ArrayDocsAspi, $docAspi);

		}

		//print_r($ArrayDocsAspi);
		$result = array_diff($ArrayDocsPlan,$ArrayDocsAspi);
		$getDocumentoByReference = $findAspirant -> getDocumentoByReference($account_number);
		$NotFoundAspirant = false;

	}else{

		$NotFoundAspirant = true;
	}
}
