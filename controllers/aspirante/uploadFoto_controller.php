<?php
require_once '../../models/aspirante/documents_model.php';

$Documents = new Documents();

$referencia = $_SESSION['referencia'];

  if(isset($_FILES["file"]["type"]))
  {
      $validextensions = array("jpeg", "jpg", "png");
      $temporary = explode(".", $_FILES["file"]["name"]);
      $file_extension = end($temporary);

      if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
      ) && ($_FILES["file"]["size"] < 10000000)//Approx. 1000kb files can be uploaded.
      && in_array($file_extension, $validextensions)){
      if ($_FILES["file"]["error"] > 0){
        echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
      }else{


                $rutaDocumento  = "uploads/".$referencia."/".$referencia."_foto".".".$file_extension;
                //echo $rutaDocumento;
                $UploadFoto = $Documents -> UploadFoto($referencia, $rutaDocumento);

                $dirUpload = "../../uploads/".$referencia."/";
                if (!file_exists($dirUpload)) {
                 mkdir($dirUpload, 0777, true);
                }

                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                $targetPath = $dirUpload.$referencia."_foto".".".$file_extension; // Target path where file is to be stored
                move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                //echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
            
      }
      }
      else{
        echo "***Formato no valido e excede peso***";
      }
  }


?>

