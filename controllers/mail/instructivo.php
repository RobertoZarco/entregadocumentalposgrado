<?php
require_once 'instructivo_controller.php';

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../../../assets/css/styles.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">

		<div class="cabecera">
			<div style="float: left; width: 15%;">
				<img class="unam" src="../../../assets/img/unam.png">
			</div>
			<div style="float: left; width: 70%;">
				<b>
					<h6 class="nomUNAM">Universidad Nacional Autónoma de México <br></h6>
				</b>
				<h6><br>Secretaría General <br>
					Dirección General de Administración Escolar</h6>
				</div>
				<div style="float: right; width: 15%;">
					<img class="unam" src="../../../assets/img/dgae1.png">
				</div>
			</div>
			<div class="carta">
				<div style="float: left; width: 80%; text-align: center;">
					<h6 class="instructivoI
					"><b>INSTRUCTIVO DE INSCRIPCIÓN</b></h6>
				</div>
				<div style="float: right; width: 15%;">
					<h6 class="anio">2017 - 2018</h6>
				</div>
			</div>
			<div class="row">
				<div class="contenido">
					<br>
					<ol class="instructivo">
						<li>Realizar la entrega de documentos en el lugar, fecha y hora según lo indicado en el correo que recibirás por parte de la Dirección General de Administración Escolar (DGAE).</li>
						<li>Recibir la documentación de ingreso que te permitirá inscribirte al Posgrado y pasar a la toma de biométricos para la elaboración de tu credencial UNAM.</li>
						<li>Una vez recibida la documentación de ingreso, del --- al ---- julio de 2017, deberás formalizar tu inscripción realizando el registro de la carga académica a cursar en el primer semestre, en la siguiente dirección electrónica https://www.saep.unam.mx, en la sección: alumnos ingresar tu número de cuenta y en NIP tu fecha de nacimiento con el formato dd/mm/aaaa.</li>
						<li>Imprimir el COMPROBANTE DE REGISTRO ACADÉMICO. Es importante que entre el 18 y 29 de septiembre verifiques, en la dirección electrónica https://www.saep.unam.mx, que tu registro haya sido validado por el coordinador del programa de posgrado.</li>
					</ol>
				</div>
			</div>
		</div>
	</body>
	</html>
