<?php

require_once '../../../models/mail/pdf_model.php';
/*biblioteca mpdf*/
require_once '../../../public/mpdf/mpdf.php';

$PDFs = new PDFs();

$getStudentsPDF = $PDFs->getStudentsPDF();

//print_r($getStudentsPDF);


for($i = 0; $i < count($getStudentsPDF); $i++){
	$ncta   = $getStudentsPDF[$i]['account_number'];
	$nombre = $getStudentsPDF[$i]['name']." ".$getStudentsPDF[$i]['first_surname']." ".$getStudentsPDF[$i]['second_surname'];

	$appointment       = $getStudentsPDF[$i]['appointment'];
	$birth_certificate = $getStudentsPDF[$i]['birth_certificate'];
	$curp              = $getStudentsPDF[$i]['curp'];
	$identification    = $getStudentsPDF[$i]['identification'];
	$certificate       = $getStudentsPDF[$i]['certificate'];
	$college_degree    = $getStudentsPDF[$i]['college_degree'];

	$day               = $getStudentsPDF[$i]['day'];
	$turn              = $getStudentsPDF[$i]['turn'];

	$documentos = array (
												"appointment"       => $appointment,
												"birth_certificate" => $birth_certificate,
												"curp"              => $curp,
												"identification"    => $identification,
												"certificate"       => $certificate,
												"college_degree"    => $college_degree
											);

	//echo $ncta."@".$nombre."@".$documentos."@".$day."@".$turn."<br>";
	//$generateCita = $PDFs -> generateCita($ncta, $nombre, $documentos, $day, $turn);
	$generatePDF = $PDFs -> generatePDF($ncta, $nombre, $documentos, $day, $turn);

	/*
	if($i == 1){
		break;
	}
	*/
}









?>