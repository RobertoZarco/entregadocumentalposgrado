<?php

require_once '../../../models/mail/pdf_model.php';
/*biblioteca mpdf*/
require_once '../../../public/mpdf/mpdf.php';

$PDFs = new PDFs();

$getStudentsPDF_MOR = $PDFs->getStudentsPDF_MOR();

//print_r($getStudentsPDF_MOR);


for($i = 0; $i < count($getStudentsPDF_MOR); $i++){
	$ncta   = $getStudentsPDF_MOR[$i]['account_number'];
	$nombre = $getStudentsPDF_MOR[$i]['name']." ".$getStudentsPDF_MOR[$i]['first_surname']." ".$getStudentsPDF_MOR[$i]['second_surname'];

	$appointment       = $getStudentsPDF_MOR[$i]['appointment'];
	$birth_certificate = $getStudentsPDF_MOR[$i]['birth_certificate'];
	$curp              = $getStudentsPDF_MOR[$i]['curp'];
	$identification    = $getStudentsPDF_MOR[$i]['identification'];
	$certificate       = $getStudentsPDF_MOR[$i]['certificate'];
	$college_degree    = $getStudentsPDF_MOR[$i]['college_degree'];

	$day               = $getStudentsPDF_MOR[$i]['day'];
	$turn              = $getStudentsPDF_MOR[$i]['turn'];

	$documentos = array (
												"appointment"       => $appointment,
												"birth_certificate" => $birth_certificate,
												"curp"              => $curp,
												"identification"    => $identification,
												"certificate"       => $certificate,
												"college_degree"    => $college_degree
											);

	//echo $ncta."@".$nombre."@".$documentos."@".$day."@".$turn."<br>";
	//$generateCita = $PDFs -> generateCita($ncta, $nombre, $documentos, $day, $turn);
	$generatePDF_MOR = $PDFs -> generatePDF_MOR($ncta, $nombre, $documentos, $day, $turn);

	/*
	if($i == 1){
		break;
	}
	*/
}









?>