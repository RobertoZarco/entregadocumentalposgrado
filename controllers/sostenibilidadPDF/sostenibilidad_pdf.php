<?php

require_once '../../../models/sostenibilidadPDF/sostenibilidad_model.php';
/*biblioteca mpdf*/
require_once '../../../public/mpdf/mpdf.php';

/*
 * Función para obtener los estudiantes de maestria
 */
function getMaestria() {

	/*Creamos un objeto de tipo PDFsSostenibilidad*/
	$sostenibilidad = new PDFsSostenibilidad();

	/*Obtenemos los estudiantes*/
	$getStudents = $sostenibilidad->getStudentsPDFMaestria();

	for ($i = 0; $i < count($getStudents); $i++) {
		$ncta = $getStudents[$i]['account_number'];
		$nombre = $getStudents[$i]['name'] . " " . $getStudents[$i]['first_surname'] . " " . $getStudents[$i]['second_surname'];

		$appointment = $getStudents[$i]['appointment'];
		$birth_certificate = $getStudents[$i]['birth_certificate'];
		$curp = $getStudents[$i]['curp'];
		$identification = $getStudents[$i]['identification'];
		$certificate = $getStudents[$i]['certificate'];
		$college_degree = $getStudents[$i]['college_degree'];

		$day = $getStudents[$i]['day'];
		$turn = $getStudents[$i]['turn'];

		$documentos = array(
			"appointment" => $appointment,
			"birth_certificate" => $birth_certificate,
			"curp" => $curp,
			"identification" => $identification,
			"certificate" => $certificate,
			"college_degree" => $college_degree,
		);

		$generatePDF = $sostenibilidad->generatePDFMaestria($ncta, $nombre, $documentos, $day, $turn);

	}
}

/*
 * Función para obtener los estudiantes de maestria en Morelia
 */
function getMaestriaMorelia() {

	/*Creamos un objeto de tipo PDFsSostenibilidad*/
	$sostenibilidad = new PDFsSostenibilidad();

	/*Obtenemos los estudiantes*/
	$getStudents = $sostenibilidad->getStudentsPDFMaestriaMorelia();

	for ($i = 0; $i < count($getStudents); $i++) {
		$ncta = $getStudents[$i]['account_number'];
		$nombre = $getStudents[$i]['name'] . " " . $getStudents[$i]['first_surname'] . " " . $getStudents[$i]['second_surname'];

		$appointment = $getStudents[$i]['appointment'];
		$birth_certificate = $getStudents[$i]['birth_certificate'];
		$curp = $getStudents[$i]['curp'];
		$identification = $getStudents[$i]['identification'];
		$certificate = $getStudents[$i]['certificate'];
		$college_degree = $getStudents[$i]['college_degree'];

		$day = $getStudents[$i]['day'];
		$turn = $getStudents[$i]['turn'];

		$documentos = array(
			"appointment" => $appointment,
			"birth_certificate" => $birth_certificate,
			"curp" => $curp,
			"identification" => $identification,
			"certificate" => $certificate,
			"college_degree" => $college_degree,
		);

		$generatePDF = $sostenibilidad->generatePDFMaestriaMorelia($ncta, $nombre, $documentos, $day, $turn);

	}
}

/*
 * Función para obtener los estudiantes de doctorado directo
 */
function getDoctoradoDirecto() {

	/*Creamos un objeto de tipo PDFsSostenibilidad*/
	$sostenibilidad = new PDFsSostenibilidad();

	/*Obtenemos los estudiantes*/
	$getStudents = $sostenibilidad->getStudentsPDFDoctoradoDirecto();

	for ($i = 0; $i < count($getStudents); $i++) {
		$ncta = $getStudents[$i]['account_number'];
		$nombre = $getStudents[$i]['name'] . " " . $getStudents[$i]['first_surname'] . " " . $getStudents[$i]['second_surname'];

		$appointment = $getStudents[$i]['appointment'];
		$birth_certificate = $getStudents[$i]['birth_certificate'];
		$curp = $getStudents[$i]['curp'];
		$identification = $getStudents[$i]['identification'];
		$certificate = $getStudents[$i]['certificate'];
		$college_degree = $getStudents[$i]['college_degree'];

		$day = $getStudents[$i]['day'];
		$turn = $getStudents[$i]['turn'];

		//echo "acount_number-> $ncta|certificate11111->$certificate<br>";

		$documentos = array(
			"appointment" => $appointment,
			"birth_certificate" => $birth_certificate,
			"curp" => $curp,
			"identification" => $identification,
			"certificate" => $certificate,
			"college_degree" => $college_degree,
			"doctorado" => $doctorado,
		);

		$generatePDF = $sostenibilidad->generatePDFDoctoradoDirecto($ncta, $nombre, $documentos, $day, $turn);

	}
}

/*
 * Función para obtener los estudiantes de doctorado directo
 */
function getDoctorado() {

	/*Creamos un objeto de tipo PDFsSostenibilidad*/
	$sostenibilidad = new PDFsSostenibilidad();

	/*Obtenemos los estudiantes*/
	$getStudents = $sostenibilidad->getStudentsPDFDoctorado();

	for ($i = 0; $i < count($getStudents); $i++) {
		$ncta = $getStudents[$i]['account_number'];
		$nombre = $getStudents[$i]['name'] . " " . $getStudents[$i]['first_surname'] . " " . $getStudents[$i]['second_surname'];

		$appointment = $getStudents[$i]['appointment'];
		$birth_certificate = $getStudents[$i]['birth_certificate'];
		$curp = $getStudents[$i]['curp'];
		$identification = $getStudents[$i]['identification'];
		$certificate = $getStudents[$i]['certificate'];
		$college_degree = $getStudents[$i]['college_degree'];
		$campus = $getStudents[$i]['campus'];

		$day = $getStudents[$i]['day'];
		$turn = $getStudents[$i]['turn'];

		//echo "acount_number-> $ncta|certificate11111->$certificate<br>";

		$documentos = array(
			"appointment" => $appointment,
			"birth_certificate" => $birth_certificate,
			"curp" => $curp,
			"identification" => $identification,
			"certificate" => $certificate,
			"college_degree" => $college_degree,
			"campus" => $campus,
		);

		$generatePDF = $sostenibilidad->generatePDFDoctorado($ncta, $nombre, $documentos, $day, $turn);

	}
}

/*
 * Función para obtener los estudiantes de doctorado
 */
function getDoctoradoMorelia() {

	$sostenibilidad = new PDFsSostenibilidad();
	/*Obtenemos los estudiantes*/
	$getStudents = $sostenibilidad->getStudentsPDFDoctoradoMorelia();

	for ($i = 0; $i < count($getStudents); $i++) {
		$ncta = $getStudents[$i]['account_number'];
		$nombre = $getStudents[$i]['name'] . " " . $getStudents[$i]['first_surname'] . " " . $getStudents[$i]['second_surname'];

		$appointment = $getStudents[$i]['appointment'];
		$birth_certificate = $getStudents[$i]['birth_certificate'];
		$curp = $getStudents[$i]['curp'];
		$identification = $getStudents[$i]['identification'];
		$certificate = $getStudents[$i]['certificate'];
		$college_degree = $getStudents[$i]['college_degree'];

		$day = $getStudents[$i]['day'];
		$turn = $getStudents[$i]['turn'];

		$documentos = array(
			"appointment" => $appointment,
			"birth_certificate" => $birth_certificate,
			"curp" => $curp,
			"identification" => $identification,
			"certificate" => $certificate,
			"college_degree" => $college_degree,
		);

		$generatePDF = $sostenibilidad->generatePDFDoctoradoMorelia($ncta, $nombre, $documentos, $day, $turn);

	}
}

//getMaestria();
//getMaestriaMorelia();
//getDoctoradoDirecto();
getDoctorado();
//getDoctoradoMorelia();

?>