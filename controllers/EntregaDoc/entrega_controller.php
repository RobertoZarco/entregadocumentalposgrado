<?php

require_once '../../models/EntregaDoc/EntregaDoc_model.php';
require_once '../../public/mpdf/mpdf.php';

//documentacion completa
if(isset($_POST) and @$_POST['doc'] == "1"){

	$ncta = $_POST['ncta'];

	$PDFsSostenibilidad = new PDFsSostenibilidad();


	$getDataAlumn = $PDFsSostenibilidad -> getDataAlumn($ncta);

	//obtenemos los datos del alumno

	$account_number     = $getDataAlumn[0]['account_number'];
	$first_surname      = $getDataAlumn[0]['first_surname'];
	$second_surname     = $getDataAlumn[0]['second_surname'];
	$name               = $getDataAlumn[0]['name'];
	$email              = $getDataAlumn[0]['email'];
	$field_knowledge    = $getDataAlumn[0]['field_knowledge'];
	$campus             = $getDataAlumn[0]['campus'];
	$program_id         = $getDataAlumn[0]['program_id'];
	$plan_id            = $getDataAlumn[0]['plan_id'];
	$plan               = $getDataAlumn[0]['plan'];
	$program            = $getDataAlumn[0]['program'];
	$street             = $getDataAlumn[0]['street'];
	$outdoor_number     = $getDataAlumn[0]['outdoor_number'];
	$colony             = $getDataAlumn[0]['colony'];
	$munipality         = $getDataAlumn[0]['munipality'];
	$country            = $getDataAlumn[0]['country'];
	$state              = $getDataAlumn[0]['state'];
	$postal_code        = $getDataAlumn[0]['postal_code'];
	$day                = $getDataAlumn[0]['day'];
	$turn               = $getDataAlumn[0]['turn'];
	$unam               = $getDataAlumn[0]['unam'];
	$folio_credential   = $getDataAlumn[0]['folio_credential'];
	$document_delivered = $getDataAlumn[0]['document_delivered'];
	$user_delivered     = $getDataAlumn[0]['user_delivered'];
	$date_delivered     = $getDataAlumn[0]['date_delivered'];
	$ip_delivered       = $getDataAlumn[0]['ip_delivered'];

	$nombre = $name." ".$first_surname." ".$second_surname;


	switch ($day) {
	  case 1:
	  	$day = "19 de Julio de 2017";
	  	break;

	  case 2:
	  	$day = "20 de Julio de 2017";
	  	break;

	  case 3:
	  	$day = "21 de Julio de 2017";
	  	break;
	}

	switch ($turn) {
	  case 1:
	  	$turn = "10: 00 a 11:00";
	  	break;

	  case 2:
	  	$turn = "11:00 a 12:00 ";
	  	break;

	  case 3:
	  	$turn = "12:00 a 13:00";
	  	break;

	  case 4:
	  	$turn = "13:00 a 14:00";
	  	break;

	  case 5:
	  	$turn = "14:00 a 15:00";
	  	break;

	  case 6:
	  	$turn = "17:00 a 18:00";
	  	break;
	}

	//carta de aceptacion
	$documentComplete = $PDFsSostenibilidad -> documentComplete(	$ncta, $nombre, $email, $field_knowledge, $campus, $program_id, $plan_id, $plan,
																																$program, $street,$outdoor_number, $colony, $munipality, $country, $state, $postal_code,
																																$day, $turn, $unam, $folio_credential, $document_delivered
																															);


}


//carta compromiso
if(isset($_POST) and @$_POST['doc'] == "2"){

	$ncta = $_POST['ncta'];

	$PDFsSostenibilidad = new PDFsSostenibilidad();


	$getDataAlumn = $PDFsSostenibilidad -> getDataAlumn($ncta);

	//obtenemos los datos del alumno

	$account_number     = $getDataAlumn[0]['account_number'];
	$first_surname      = $getDataAlumn[0]['first_surname'];
	$second_surname     = $getDataAlumn[0]['second_surname'];
	$name               = $getDataAlumn[0]['name'];
	$email              = $getDataAlumn[0]['email'];
	$field_knowledge    = $getDataAlumn[0]['field_knowledge'];
	$campus             = $getDataAlumn[0]['campus'];
	$program_id         = $getDataAlumn[0]['program_id'];
	$plan_id            = $getDataAlumn[0]['plan_id'];
	$plan               = $getDataAlumn[0]['plan'];
	$program            = $getDataAlumn[0]['program'];
	$street             = $getDataAlumn[0]['street'];
	$outdoor_number     = $getDataAlumn[0]['outdoor_number'];
	$colony             = $getDataAlumn[0]['colony'];
	$munipality         = $getDataAlumn[0]['munipality'];
	$country            = $getDataAlumn[0]['country'];
	$state              = $getDataAlumn[0]['state'];
	$postal_code        = $getDataAlumn[0]['postal_code'];
	$day                = $getDataAlumn[0]['day'];
	$turn               = $getDataAlumn[0]['turn'];
	$unam               = $getDataAlumn[0]['unam'];
	$folio_credential   = $getDataAlumn[0]['folio_credential'];
	$document_delivered = $getDataAlumn[0]['document_delivered'];
	$user_delivered     = $getDataAlumn[0]['user_delivered'];
	$date_delivered     = $getDataAlumn[0]['date_delivered'];
	$ip_delivered       = $getDataAlumn[0]['ip_delivered'];

	$nombre = $name." ".$first_surname." ".$second_surname;


	switch ($day) {
	  case 1:
	  	$day = 19;
	  	break;

	  case 2:
	  	$day = 20;
	  	break;

	  case 3:
	  	$day = 21;
	  	break;
	}

	switch ($turn) {
	  case 1:
	  	$turn = "10: 00 a 11:00";
	  	break;

	  case 2:
	  	$turn = "11:00 a 12:00 ";
	  	break;

	  case 3:
	  	$turn = "12:00 a 13:00";
	  	break;

	  case 4:
	  	$turn = "13:00 a 14:00";
	  	break;

	  case 5:
	  	$turn = "14:00 a 15:00";
	  	break;

	  case 6:
	  	$turn = "17:00 a 18:00";
	  	break;
	}

	$documentNoComplete = $PDFsSostenibilidad -> documentNoComplete(	$ncta, $nombre, $email, $field_knowledge, $campus, $program_id, $plan_id, $plan,
																																		$program, $street,$outdoor_number, $colony, $munipality, $country, $state, $postal_code,
																																		$day, $turn, $unam, $folio_credential, $document_delivered
																																	);

}


?>